<!-- Header -->
<div id="sns_header" class="wrap <?php echo esc_attr(snsvicky_getoption('header_style', 'style1')); ?>">
	<?php if (is_active_sidebar('top-header-2')) { ?>
	<div class="top-header visible-lg visible-md">
		<div class="container">
		<?php dynamic_sidebar('top-header-2'); ?>
		</div>
	</div>
	<?php } ?>
	<div class="main-header">
		<div class="container">
			<div class="row">
				<div class="search col-lg-3 col-md-4 col-xs-4">
					<div class="sns-icon-nav hidden-lg hidden-md">
						<span class="btn2 btn-navbar leftsidebar">
						    <span class="overlay"></span>
						</span>
						<span class="btn2 btn-navbar offcanvas">
						    <span class="overlay"></span>
						</span>
						<span class="btn2 btn-navbar rightsidebar">
						    <span class="overlay"></span>
						</span>
					</div>
					<div id="sns_content_rsm">
						<?php
						if ( snsvicky_getoption('enable_search_cat') == true ) snsvicky_get_searchform('def');
						else snsvicky_get_searchform('hide_cat');
						// Responsive Menu
						?>
						<div id="sns_resmenu" class="res-menu hidden-lg hidden-md">
							<?php
							$main_menu = '';
							if(is_page() && ($menu_selected = get_post_meta(get_the_ID(), 'snsvicky_main_menu', true))){
								$main_menu = $menu_selected;
							}
							
					        if(has_nav_menu('main_navigation')):
					           wp_nav_menu( array(
					           				'theme_location' => 'main_navigation',
					           				'container' => false,
					           				'menu'		=> $main_menu,
					           				'menu_id' => 'res_main_nav',
					           				'menu_class' => 'resp-nav'
					           	) ); 
							else:
								esc_html_e('Please sellect menu for Main navigation', 'snsvicky');
							endif;
							?>
						</div>
					</div>
				</div>
				<div class="header-logo col-md-4 col-xs-4">
					<div id="logo">
						<?php $logourl = snsvicky_getoption('header_logo', SNSVICKY_THEME_URI.'/assets/img/logo.png', 'image'); ?>
						<a href="<?php echo esc_url( home_url('/') ) ?>" title="<?php bloginfo( 'sitename' ); ?>">
							<img src="<?php echo esc_attr($logourl); ?>" alt="<?php bloginfo( 'sitename' ); ?>"/>
						</a>
					</div>		
				</div>
				<div class="cart-account col-md-4 col-xs-4"><div class="inner">
					<?php
					$welcome = esc_html__('Hi, ','snsvicky');
					$link = '';
					$current_user = wp_get_current_user();
					if ( 0 == $current_user->ID ) {
						$welcome .= esc_html__('Guest!', 'snsvicky');
						$link = '<a href="'.wp_login_url().'">'.esc_html__('Sign In', 'snsvicky').'</a> '.esc_html__('or', 'snsvicky').' <a href="'.wp_registration_url().'">'.esc_html__('Register', 'snsvicky').'</a>';
					}else{
						$welcome .= $current_user->user_login.'!';
						 $link = esc_html__('Want to ', 'snsvicky').'<a href="'.wp_logout_url().'">'.esc_html__('Logout?', 'snsvicky').'</a>';
					}
					?>
					<div class="login-regis visible-lg visible-md">
						<h5 class="usr-welcome"><?php echo $welcome; ?></h5>
						<div class="usr-link"><?php echo $link; ?></div>
					</div>
					<?php
					if ( class_exists('WooCommerce') ) : ?>
						<?php
						if ( function_exists('YITH_WCWL') ) { ?>
						<div class="mini-wishlist hidden-xs">
							<a class="tongle" href="<?php echo YITH_WCWL()->get_wishlist_url(); ?>">
								<span class="number"><?php echo YITH_WCWL()->count_products(); ?></span>						
							</a>
						</div>
						<?php
						}?>
						<div class="mini-cart sns-ajaxcart">
							<a href="<?php echo wc_get_cart_url(); ?>" class="tongle">
								<span class="cart-label"><?php echo esc_html__("Cart", "snsvicky"); ?>
								</span>
								<span class="number">
									<?php echo sizeof( WC()->cart->get_cart() );?>
								</span>
							</a>
							<?php if ( !is_cart() && !is_checkout() ) : ?>
								<div class="content">
									<div class="cart-title"><h4><?php echo esc_html__("My cart", "snsvicky"); ?></h4></div>
									<div class="block-inner">
										<?php the_widget( 'WC_Widget_Cart', 'title= ', array('before_title' => '', 'after_title' => '') ); ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div></div>
			</div>
		</div>
	</div>
	<!-- Main menu wrap -->
	<div id="sns_menu" class="menu-header">
        <div class="container">
            <div class="row">
                <div class="sns-mainnav-wrapper col-md-12 col-xs-12">
                    <div id="sns_mainnav">
                        <div id="sns_mainmenu" class="visible-lg visible-md">
                            <?php
                            if(has_nav_menu('main_navigation')):
                               wp_nav_menu( array(
                                            'theme_location' => 'main_navigation',
                                            'container' => false, 
                                            'menu_id' => 'main_navigation',
                                            'walker' => new snsvicky_Megamenu_Front,
                                            'menu_class' => 'nav navbar-nav'
                                ) ); 
                            else:
                                echo '<p class="main_navigation_alert">'.esc_html__('Please sellect menu for Main navigation', 'snsvicky').'</p>';
                            endif;
                            ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- SlideShow -->
<?php snsvicky_slideshow_wrap(); ?>
<!-- Breadcrump -->
<?php snsvicky_getbreadcrumbs(); ?>
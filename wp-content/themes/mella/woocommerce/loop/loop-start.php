<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */
?>
<div class="products products-<?php echo esc_attr(mella_woocommerce_get_display_mode()); ?> clearfix">
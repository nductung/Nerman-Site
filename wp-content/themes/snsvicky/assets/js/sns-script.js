(function ($) {
	"use strict";
	$(document).ready(function($){
		var $win = $(window);
		// Sticky menu
		if ( $('body').hasClass('header-style2') || $('body').hasClass('header-style3') ) {
			if( $('#sns_header .main-header').length && $('body').hasClass('use_stickmenu') ){
			    var headerOrgOffset = $('#sns_header .main-header').offset().top;
			    $(window).scroll(function() {
			        var currentScroll = $(this).scrollTop();
			        if(currentScroll > headerOrgOffset) {
			        	$('#sns_header .main-header').addClass('keep-menu');
			        } else {
			        	$('#sns_header .main-header').removeClass('keep-menu');
			        }
			    });
			}
		}else{
			if( $('#sns_menu').length && $('body').hasClass('use_stickmenu') ){
			    var headerOrgOffset = $('#sns_menu').offset().top;
			    $(window).scroll(function() {
			        var currentScroll = $(this).scrollTop();
			        if(currentScroll > headerOrgOffset) {
			        	$('#sns_menu').addClass('keep-menu');
			        } else {
			        	$('#sns_menu').removeClass('keep-menu');
			        }
			    });
			}
		}
		// Responsive menu
		$('#sns_resmenu').SnsAccordion({
			btn_open: '<span class="ac-tongle open"></span>',
			btn_close: '<span class="ac-tongle close"></span>',
		});
		$('.sns-icon-nav .btn2.offcanvas').on('click', function(){
			if($('#sns_content_rsm').hasClass('active')){
				$(this).find('.overlay').fadeOut(250);
				$('#sns_content_rsm').removeClass('active');
				$('body').removeClass('show-sidebar', 4000);
			} else {
				$('#sns_content_rsm').addClass('active');
				$(this).find('.overlay').fadeIn(250);
				$('body').addClass('show-sidebar');
			}
		});
		// Sidebar on mobile
		var right_sidebars = '#sns_content .sns-right, #sns_content .sns-right-sidebar, body.single-product .inner-sidebar';
		$(right_sidebars).each(function(){
			var right_sd = $(this);
			if(right_sd.length) {
				$('.sns-icon-nav .btn2.rightsidebar').css('display', 'inline-block').on('click', function(){
					if($(this).hasClass('active')){
						$(this).find('.overlay').fadeOut(250); $(this).removeClass('active');
						right_sd.removeClass('active');
						$('body').removeClass('show-sidebar', 4000);
					} else {
						$(this).addClass('active');
						$(this).find('.overlay').fadeIn();
						right_sd.addClass('active');
						$('body').addClass('show-sidebar');
					}
				});
			}
		});
		var left_sidebars = '#sns_content .sns-left, #sns_content .sns-left-sidebar';
		$(left_sidebars).each(function(){
			var left_sd = $(this);
			if(left_sd.length) {
				$('.sns-icon-nav .btn2.leftsidebar').css('display', 'inline-block').on('click', function(){
					if($(this).hasClass('active')){
						$(this).find('.overlay').fadeOut(250); $(this).removeClass('active');
						left_sd.removeClass('active');
						$('body').removeClass('show-sidebar', 4000);
					} else {
						$(this).addClass('active');
						$(this).find('.overlay').fadeIn();
						left_sd.addClass('active');
						$('body').addClass('show-sidebar');
					}
				});
			}
		});
		// Back to top
		$('.sns-croll-to-top #sns-totop').hide();
		var wh = $(window).height();
		var whtml =  $(document).height();
		$(window).scroll(function () {
			if ($(this).scrollTop() > whtml/10) {
				$('.sns-croll-to-top #sns-totop').fadeIn();
			} else {
				$('.sns-croll-to-top #sns-totop').fadeOut();
			}
		});
		$('.sns-croll-to-top #sns-totop').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
		// Time count down
		$('.time-count-down').each(function(){
			if ( $(this).length ) {
				$(this).find('.clock-digi').countdown($(this).attr('data-date'), function(event){
	                $(this).find('.day').html(event.strftime('%D'));
	                $(this).find('.hours').html(event.strftime('%H'));
	                $(this).find('.minutes').html(event.strftime('%M'));
	                $(this).find('.seconds').html(event.strftime('%S'));
	            });
			}
		});
		// Promo bar
		$('.sns-promobar').each(function(){
			var $this = $(this);
			var status;
			if ( $(this).find('p').length ) {
				$(this).find('p:first-child').prepend('<i class="fa fa-gift"></i>');
			}
			if ($this.hasClass('active') ) {
				$this.find('.content').slideDown(300);
			}else{
				$this.find('.content').slideUp(300);
			}
			$this.find('.btn-tongle').click(function(){
				if ($this.hasClass('active') ) {
					$this.find('.content').slideUp(300); $this.removeClass('active'); status = false;
				}else{
					$this.find('.content').slideDown(300); $this.addClass('active'); status = true;
				}
				$.ajax({
		            url: ajaxurl,
		            data:{
		            	action : 'sns_setcookies',
		            	key	: 'promo_status',
		            	value : status
		            },
		            type: 'POST'
		        });
		        return false;
			});
		})
		$('.search .search-tongle').bind('click', function(){
			if( $(this).parents('.search').hasClass('active') ){
				$(this).parents('.search').removeClass('active');
			}else{
				$(this).parents('.search').addClass('active');
			}
		});
		$('.menu-tongle').bind('click', function(){
			$(this).addClass('active'); $('.menu-search-content').addClass('active');
		});
		$('.menu-search-content .overlay .bt-close').bind('click', function(){
			$('.menu-tongle').removeClass('active'); $('.menu-search-content').removeClass('active');
		});
		function sns_equal_col(){
			var $col_h = 0;
			$('.wpb_row.equal-col > .wpb_column').each(function(){
				var _this_height = $(this).height();
				if( $col_h > 0 && _this_height < $col_h){
					$(this).css( 'min-height', $col_h);
				}else{
					$col_h = _this_height;
				}
			});
		}
		sns_equal_col();
		// On mobile
		function addHoverOnMobile(el){
			if( $(el).length ){
				$(el).mouseover(function(){
					$(el).addClass('hover');
				}).mouseout(function() {
				    $(el).removeClass('hover');
				});
			}
		}
		addHoverOnMobile('.top-header .contact-info > div');
		addHoverOnMobile('.main-header .search .sns-searchwrap');
		addHoverOnMobile('.top-header .my-account .inner');
		addHoverOnMobile('.main-header .search-box');
		// Ajax search
		$('.sns-searchwrap').each(function(){
			var sns_useajaxsearch = $(this).attr('data-useajaxsearch');
			var sns_usecat_ajaxsearch  = $(this).attr('data-usecat-ajaxsearch');
			
			if( typeof $.fn.select2 == 'function' && sns_usecat_ajaxsearch == 'true' ){
				$('.sns-ajaxsearchbox select.select-cat').select2();
				$('.sns-ajaxsearchbox').addClass('loaded');
			}
			if (sns_useajaxsearch == 'true') {
				var keywords = '';
				var keywords_old = '';
				var search_timeout;
				var search_inputtext;
				var array_old_result = {};
				$('body').append('<div id="sns_searchresult_wrap"></div>');
				var sns_searchresult_wrap = $('#sns_searchresult_wrap');
				
				$('.sns-searchwrap .search-input input[name="s"]').bind('keyup', function(e){
					sns_searchresult_wrap.hide();
					search_inputtext = $(this);
					keywords = $.trim($(this).val());
					// Define keywords length to start ajax
					if( keywords.length < 3 ){
						search_inputtext.parents('.search-input').removeClass('loading');
						return;
					}
					if( array_old_result[keywords] ){
						sns_searchresult_wrap.html(array_old_result[keywords]);
						sns_searchresult_wrap.show();
						keywords_old = '';
						search_inputtext.parents('.search-input').removeClass('loading');
						sns_searchresult_wrap.find('.viewall-result a').bind('click', function(e){
							e.preventDefault();
							search_inputtext.parents('form').submit();
						});
						return;
					}
					
					clearTimeout(search_timeout);
					search_timeout = setTimeout(function(){
						if( keywords == keywords_old || keywords.length < 3 ){
							return;
						}
						keywords_old = keywords;
						search_inputtext.parents('.search-input').addClass('loading');
						// Check category
						var category = '';
						if( sns_usecat_ajaxsearch == 'true' ){
							var select_category = search_inputtext.parents('.search-input').siblings('.select-cat');
							if( select_category.length > 0 ){
								category = select_category.find(':selected').val();
							}
						}
						$.ajax({
							type : 'POST',
							url : ajaxurl,
							data : {action : 'snsvicky_ajax_search', keywords: keywords, category: category},
							error : function(xhr,err){
								search_inputtext.parents('.search-input').removeClass('loading');
							},
							success : function(response){
								if( response != '' ){
									response = JSON.parse(response);
									if( response.keywords == keywords ){
										sns_searchresult_wrap.html(response.html);
										// Set array_old_result
										array_old_result[keywords] = response.html;
										// Set style for result wrap
										var border_width = parseInt(search_inputtext.parent('.search-input').css('border-left-width'));
										var top = search_inputtext.offset().top + search_inputtext.outerHeight(true) + 1;
										// if( $('#wpadminbar').length ){
										// 	top = top - $('#wpadminbar').outerHeight(true);
										// }
										var width = search_inputtext.outerWidth(true) + border_width;
										var left = Math.ceil(search_inputtext.offset().left) - border_width;
										if( (left + width) > $(window).width() ){
											left = left - (width - search_inputtext.outerWidth(true));
										}
										sns_searchresult_wrap.css({
											'position': 'absolute',
											'top': top-1,
											'left': left,
											'width': width,
											'display': 'block'
										});
										search_inputtext.parents('.search-input').removeClass('loading');
										sns_searchresult_wrap.find('.viewall-result a').bind('click', function(e){
											e.preventDefault();
											search_inputtext.parents('form').submit();
										});
									}
								}else{
									search_inputtext.parents('.search-input').removeClass('loading');
								}
							}
						});
					}, 600);
				});
				// Hide result wrap when hover out or click anything
				sns_searchresult_wrap.hover(function(){
					//
				},function(){
					sns_searchresult_wrap.hide();
				});
				$('body').bind('click', function(){
					sns_searchresult_wrap.hide();
				});
				// Change sellect category
				if ( sns_usecat_ajaxsearch == 'true' ){
					$('.sns-ajaxsearchbox select.select-cat').bind('change', function(){
						keywords_old = '';
						array_old_result = {};
						$(this).parents('.sns-ajaxsearchbox').find('.search-input input[name="s"]').trigger('keyup');
					});
				}
			}
		});
		// Short code custom heading
		$('.vc_custom_heading.want-span-line').each(function(){
			if ( $(this).length ){
				$(this).wrapInner('<span></span>');
			}
		});
		// Shortcode widget
		$('.want-span-line .widget .widgettitle').each(function(){
			if ( $(this).length ){
				$(this).wrapInner('<span></span>');
			}
		});
		// Comment nava
		if ( $('.sns-comments .navigation .pagination').length && $('.sns-comments .navigation .pagination').html().trim().length == 0 ){
			$('.sns-comments .navigation').hide();
		}
		// SNS Carousel shortcode
		$('.sns-carousel').each(function(){
			var f_this = $(this);
			if ( $(this).length > 0 && $(this).find('.carousel-content').length > 0 ){
				if ( $(this).attr('data-type') == 'v' ) {
					$(this).find('.carousel-content').slick({
						vertical: true,
						infinite: true,
						slidesToScroll: 1,
						slidesToShow: $(this).attr('data-desktop'),
						arrows: ($(this).attr('data-nav') == 1) ? true : false,
						autoplay: true, 
					});
				} else {
					var c_param = {
						loop: ( $(this).find('.carousel-content').find('> *').length > $(this).data('desktop')) ? true : false,
						dots: ($(this).data('paging') == 1) ? true : false,
						nav: ($(this).data('nav') == 1) ? true : false,
						autoplay : true, autoplayHoverPause: true,
		                autoHeight: true,
					    items : $(this).data('desktop'),
					    smartSpeed:500,
		                responsive : {
		                    0 : { items: $(this).data('mobilep') },
		                    480 : { items: $(this).data('mobilel') },
		                    768 : { items: $(this).data('tabletp') },
		                    992 : { items: $(this).data('tabletl') },
		                    1200 : { items: $(this).data('desktop') }
		                }
		            }
		            if ($(this).data('showdotimg') == 1 && $(this).data('paging') == 1 ) {
						var $i = 0, $avatar = {}, wrap_class = 'show-dot-'+Math.random();
						wrap_class = wrap_class.replace('.', ''); $(this).addClass(wrap_class);
						$(this).find('.carousel-content > div').each(function(){
							$avatar[$i] = $(this).data('dotimg');
							$i = $i + 1;
						});
						function sns_dotimg_callback(event) {
							var $j = 0;
							$('.' + wrap_class).find('.owl-dot').each( function(){
								$(this).html('<img src="'+ $avatar[$j] +'" />');
								$j = $j + 1;
							});
						}
						c_param.animateOut = 'fadeOut'; c_param.animateIn = 'fadeInUp'; c_param.onInitialized = sns_dotimg_callback;
					}
					if ( $(this).hasClass('testimonial-have-border') ){
						$(this).append('<div class="box-avarta"></div>');
						f_this.find('.box-avarta').html('<img src="'+ f_this.find('.carousel-content > div').first().data('avatar') +'" />');
						function sns_change_avatar_callback(event) {
							var new_scr = f_this.find('.owl-item.active').find('.sns-single-testimonial').data('avatar');
							if ( typeof new_scr != 'undefined' ) {
								f_this.find('.box-avarta').find('img').attr("src", new_scr).fadeIn();
							}
						}
						c_param.animateOut = 'fadeOut'; c_param.animateIn = 'fadeInUp'; c_param.onTranslated = sns_change_avatar_callback;
					}
					$(this).find('.carousel-content').owlCarousel(c_param);
				}
			}
		});
		
		// Accordion for category blog
		if( $('.widget_categories ul').length > 0 ) {
			$('.widget_categories ul').SnsAccordion({
				btn_open: '<span class="ac-tongle open">+</span>',
				btn_close: '<span class="ac-tongle close">-</span>',
			});
		}
		// Related post
		if ( $('.post-related .owl-carousel').length ) {
			$('.post-related .owl-carousel').owlCarousel({
                items: 3,
                responsive : {
                    0 : { items: 1},
                    480 : { items: 2 },
                    768 : { items: 2 },
                    992 : { items: 3 },
                    1200 : { items: 3 }
                },
                loop: ( $('.post-related .owl-carousel').find('> *').length > 3 ) ? true : false,
                nav: true,
                dots: false
            });
		}
		// Post gallery
		function sns_post_gallery(){
			$('article.type-post').each(function(){
				if ( $(this).find('.thumb-container').length ) {
					var article_g_id = $(this).attr('id');
		            $('#' + article_g_id + ' .thumb-container').owlCarousel({
		                items: 1,
		                loop:true,
		                nav: true,
                		dots: false
		            });
		        }
			});
		}
		sns_post_gallery();
		// handle-preload
		$('.handle-preload').removeClass('handle-preload');
		// Set color, border-color for social icon
		$('.connect-us [data-color]').hover(function(){
			$(this).css({
				'color':$(this).attr('data-color'), 
				'border-color':$(this).attr('data-color')
			});
		},function(){
			$(this).css({
				'color':'',
				'border-color':''
			})
		});
		// products slider preload
		if( $('.sns-products-slider').length > 0 ){
			setTimeout(function(){
				$('.sns-products-slider').addClass('loaded');
			},1100);
		}

		// SNS Preload
		if( $('.sns-preload').length > 0 ){
			setTimeout(function(){
				$('.sns-preload').addClass('sns-loaded');
			},1000);
		}

		
		// blog masonry
		if($('.sns-grid-masonry').length > 0){
			$('.sns-grid-masonry').masonry({
				// options
				itemSelector: '.sns-grid-item',
			});
		}
		
		
	});
		
	$(window).load(function(){
		// Tooltip
	    $("body.use-tooltip *[data-toggle='tooltip']").each(function(){
			$(this).tooltip({
	    		container: 'body'
	    	}, 'show');
		});
		$(document).ajaxComplete(function(){
			$("body.use-tooltip *[data-toggle='tooltip']").each(function(){
				$(this).tooltip({
		    		container: 'body'
		    	});
			});
			
			// For Search in header style 3
			if ( $('body.header-style3 #sns_header .search-box').length && $('#sns_searchresult_wrap').length ){
				$('#sns_searchresult_wrap').mouseover(function(){
					$('body.header-style3 #sns_header .search-box').addClass('active');
				}).mouseout(function() {
				    $('body.header-style3 #sns_header .search-box').removeClass('active');
				});
			}
		});
		if ( $('#sns_searchresult_wrap').length ){
			$('#sns_searchresult_wrap').mouseover(function(){
				$('#sns_mainnav .sns-searchwrap').addClass('active');
			}).mouseout(function() {
			    $('#sns_mainnav .sns-searchwrap').removeClass('active');
			});
		}
	});

	$.fn.SnsAccordion = function(options) {
		var $el    = $(this);
		var defaults = {
			active: 'open',
			active_default: 'nav-2',
			el_wrap: 'li',
			el_content: 'ul',
			accordion: true,
			expand: true,
			btn_open: '<i class="fa fa-plus-square-o"></i>',
			btn_close: '<i class="fa fa-minus-square-o"></i>'
		};
		var options = $.extend({}, defaults, options);
		
		$el.find(options.el_wrap).each(function(){
			$(this).find('> a:first-child, > span:first-child, > h4:first-child').wrap('<div class="accr_header"></div>');
			if(($(this).find(options.el_content)).length){
				$(this).find('> .accr_header').append('<span class="btn_accor">' + options.btn_open + '</span>');
				$(this).find('> '+options.el_content+':not(".accr_header")').wrap('<div class="accr_content"></div>');
			}
		});
		if(options.accordion){
			$('.accr_content').hide();
			$el.find(options.el_wrap).each(function(){
				if(options.active_default!==''){
					if( $(this).hasClass(options.active_default) ){
						$(this).addClass(options.active);
					}
				}
				if($(this).hasClass(options.active)) {
					$(this).find('> .accr_content')
						   .addClass(options.active)
						   .slideDown();
					$(this).find('> .accr_header')
						   .addClass(options.active);
				}
			});
		} else {
			$el.find(options.el_wrap).each(function(){
				if(!options.expand){
					$('.accr_content').hide();
				} else {
					$(this).find('> .accr_content').addClass(options.active);
					$(this).find('> .accr_header').addClass(options.active);
					$(this).find('> .accr_header .btn_accor').html(options.btn_close);
				}
			});
		}
		$el.find(options.el_wrap).each(function(){
			var $wrap = $(this);
			var $accrhead = $wrap.find('> .accr_header');
			var btn_accor = '.btn_accor';
			
			$accrhead.find(btn_accor).on('click', function(event) {
				event.preventDefault();
				var obj = $(this);
				var slide = true;
				if($accrhead.hasClass(options.active)) {
					slide = false;
				}
				if(options.accordion){
					$wrap.siblings(options.el_wrap).find('> .accr_content').slideUp().removeClass(options.active);
					$wrap.siblings(options.el_wrap).find('> .accr_header').removeClass(options.active);
					$wrap.siblings(options.el_wrap).find('> .accr_header ' + btn_accor).html(options.btn_open);
				}
				if(slide) {
					$accrhead.addClass(options.active);
					obj.html(options.btn_close);
					$accrhead.siblings('.accr_content').addClass(options.active).stop(true, true).slideDown();
				} else {
					$accrhead.removeClass(options.active);
					obj.html(options.btn_open);
					$accrhead.siblings('.accr_content').removeClass(options.active).stop(true, true).slideUp();
				}
				return false;
			});
		});
	};  

	 // Top header toggle menu
    if($('#sns_header.style2 .icon-menu').length > 0){
    	//$('.sns-mainnav-wrapper').hide();
    	$('#sns_header.style2 .icon-menu').on('click', function(event){
    		
    		event.preventDefault();
    		var _this = $(this);
    		if(_this.hasClass('close')){
    			_this.removeClass('close');
    			$('.sns-mainnav-wrapper #sns_mainmenu').addClass('open');
    			$('.menu-text').addClass('hid');
    		}else{
    			_this.addClass('close');
    			$('.sns-mainnav-wrapper #sns_mainmenu').removeClass('open');
    			$('.menu-text').removeClass('hid');
    		}
    	});
    }

    // cpanel
		$('#sns_tools #sns_cpanel #sns_cpanel_btn').click(function(){
		if( $('#sns_tools #sns_cpanel').hasClass('open') ){
			$('#sns_tools #sns_cpanel').animate({right:'-290px'});
			$('#sns_tools #sns_cpanel').removeClass('open');
			
		}else{
			
			$('#sns_tools #sns_cpanel').animate({right:'0px'});
			$('#sns_tools #sns_cpanel').addClass('open');
		}
	});

	// var themecolorOptions = {
	// 		change: function(event, ui){
	// 		    $('input[name="sns_themecolor"]').val(ui.color.toString());
	// 		    //console.log(ui.color.toString());
	// 		},
	// };
	
	//$("#sns_themeboxcolor").wpColorPicker(themecolorOptions);
	
	// SCSS Compile
	$('#sns_tools #sns_cpanel .scss_compile a').click(function(){
        var scsscompile = $(this).data('scsscompile');
        var href = $(this).attr('href');
        $('#sns_tools #sns_cpanel').addClass('wait');
        $.ajax({
            url: ajaxurl,
            data:{
            	action : 'sns_setcookies',
            	key	: 'advance_scss_compile',
            	value : scsscompile
            },
            type: 'POST',
            success:function(result){
            	if ( href != '#' ) window.location.href = href;
            	else window.location.href = window.location.href;
            }
        });
        return false;
    });
	
	$('#sns_tools #cpanel_themecolor a').each(function(){
		$(this).css({
			'background-color': '#'+$(this).data('color')
		});
	})

	// Click theme color
	$('#sns_tools #cpanel_themecolor a').click(function(){
        var color = $(this).data('color');
        var href = $(this).attr('href');
        $('#sns_tools #sns_cpanel').addClass('wait');
        if ( href != '#' ) window.location.href = href;
        else window.location.href = window.location.href;
        return false;
    });

    // Click Boxed Layout
	$('#sns_tools #sns_cpanel .boxed_layout a').click(function(){
        var boxed = $(this).attr('data-boxed');
        var href = $(this).attr('href');
        $('#sns_tools #sns_cpanel').addClass('wait');
        $.ajax({
            url: ajaxurl,
            data:{
            	action : 'sns_setcookies',
            	key	: 'use_boxedlayout',
            	value : boxed
            },
            type: 'POST',
            success:function(result){
            	if ( href != '#' ) window.location.href = href;
            	else window.location.href = window.location.href;
            }
        });
        return false;
    });
    
    // Click Sticky Menu
	$('#sns_tools #sns_cpanel .sticky_menu a').click(function(){
        var sticky = $(this).data('sticky');
        var href = $(this).attr('href');
        $('#sns_tools #sns_cpanel').addClass('wait');
        $.ajax({
            url: ajaxurl,
            data:{
            	action : 'sns_setcookies',
            	key	: 'use_stickmenu',
            	value : sticky
            },
            type: 'POST',
            success:function(result){
            	if ( href != '#' ) window.location.href = href;
            	else window.location.href = window.location.href;
            }
        });
        return false;
    });
    
    // Reset cookie
    $('#sns_tools #sns_cpanel .btn-reset').click(function(){
        var href = $('#sns_tools #sns_cpanel .btn-reset').attr('data-url');
        $('#sns_tools #sns_cpanel').addClass('wait');
        $.ajax({
            url: ajaxurl,
            data:{
            	action : 'sns_resetcookies'
            },
            type: 'POST',
            success:function(result){
            	if ( href != '#' ) window.location.href = href;
            	else window.location.href = window.location.href;
            }
        });
        return false;
    });
     // Top header toggle menu
    if($('#sns_header.style4 .icon-menu').length > 0){
    	//$('.sns-mainnav-wrapper').hide();
    	$('#sns_header.style4 .icon-menu').on('click', function(event){
    		
    		event.preventDefault();
    		var _this = $(this);
    		if(_this.hasClass('close')){
    			_this.removeClass('close');
    			$('.sns-mainnav-wrapper #sns_mainmenu').addClass('open');
    			$('.menu-text').addClass('hid');
    			$('#logo').addClass('hid');
    		}else{
    			_this.addClass('close');
    			$('.sns-mainnav-wrapper #sns_mainmenu').removeClass('open');
    			$('.menu-text').removeClass('hid');
    			$('#logo').removeClass('hid');
    		}
    	});
    }

})(jQuery);
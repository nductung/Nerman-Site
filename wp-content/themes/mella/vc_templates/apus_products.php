<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

if ( !empty($categories) ) {
    $categories = explode(',', $categories);
} else {
	$categories = array();
}
$args = array(
    'categories' => $categories,
    'product_type' => $type,
    'post_per_page' => $number,
);
$product_style = !empty($product_style) ? $product_style : 'inner';
$style_item_product = $product_style == 'inner' ? ' item-grid' : ' '.$product_style ;
$loop = mella_get_products( $args );
if ( $loop->have_posts() ) {
?>
    <div class="widget widget-products <?php echo esc_attr($el_class.' '.$layout_type); ?>">
        <?php if($title!='' || $subtitle!='' ) {?>
            <div class="top-info">
                <?php if ($title!=''): ?>
                    <h3 class="widget-title">
                        <?php echo esc_attr( $title ); ?>
                    </h3>
                <?php endif; ?>
                <?php if ($subtitle!=''): ?>
                    <div class="subtitle">
                        <?php echo esc_attr( $subtitle ); ?>
                    </div>
                <?php endif; ?>
            </div>
        <?php } ?>
        <div class="widget-content woocommerce clearfix <?php echo esc_attr($layout_type.$style_item_product.' row-carousel-'.$rows); ?>">
            <?php wc_get_template( 'layout-products/'.$layout_type.'.php' , array(
                'loop' => $loop,
                'columns' => $columns,
                'product_item' => $product_style,
                'show_nav' => $show_nav,
                'show_pagination' => $show_pagination,
                'rows' => $rows,
            ) ); ?>
        </div>
    </div>
<?php } ?>
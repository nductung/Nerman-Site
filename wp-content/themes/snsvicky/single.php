<?php get_header(); ?>
<!-- Content -->
<div id="sns_content">
	<div class="container">
		<div class="row sns-content">
		    <!-- left sidebar -->
			<?php snsvicky_leftcol(); ?>
			<!-- Main content -->
			<div class="<?php echo snsvicky_maincolclass(); ?>">
			    <?php
			    // Start the loop.
			    if(have_posts()):
					while ( have_posts() ) : the_post();
						// Set post view count
						snsvicky_set_post_views(get_the_ID());
		        		get_template_part( 'framework/tpl/single/single', get_post_format() );
			      	endwhile;
			      else:
			      	get_template_part( 'content', 'none' );
			      endif;
			    ?>
			</div>
			<!-- Right sidebar -->
			<?php snsvicky_rightcol(); ?>
		</div>
	</div>
</div>
<!-- End Content -->
<?php get_footer(); ?>
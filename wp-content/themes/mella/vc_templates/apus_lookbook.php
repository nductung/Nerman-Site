<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<div class="widget widget-lookbook <?php echo esc_attr($el_class.' '.$style); ?>">
    <?php if ( $url ) { ?>
        <a class="link-img" href="<?php echo esc_url($url); ?>">
    <?php } ?>
        <?php echo trim(mella_get_attachment_thumbnail($image, 'full')); ?>
    <?php if ( $url ) { ?>
        </a>
    <?php } ?>
    <?php if ($title!=''): ?>
        <h3 class="widget-title">
            <?php if ( $url ) { ?>
                <a href="<?php echo esc_url($url); ?>">
            <?php } ?>
            <?php echo esc_attr( $title ); ?>
            <?php if ( $url ) { ?>
                </a>
            <?php } ?>
        </h3>
    <?php endif; ?>
</div>
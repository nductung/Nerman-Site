<?php

if ( class_exists('TA_WC_Variation_Swatches_Frontend') ) {
    TA_WC_Variation_Swatches_Frontend::instance();
}
if ( !function_exists('mella_product_quicview_wishlist') ) {
    function mella_product_quicview_wishlist(){
        if ( class_exists( 'YITH_WCWL' ) ) {
            echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
        }
    }
    add_action('woocommerce_single_product_summary', 'mella_product_quicview_wishlist', 35);
}
?>
<div class="woocommerce">
    <div id="product-<?php the_ID(); ?>" <?php post_class('product'); ?>>
    	<div id="single-product" class="product-info details-product">
    		<div class="row">
    			<div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="wrapper-img-main">
        				<?php
        					/**
        					 * woocommerce_before_single_product_summary hook
        					 *
        					 * @hooked woocommerce_show_product_sale_flash - 10
        					 * @hooked woocommerce_show_product_images - 20
        					 */
        					//do_action( 'woocommerce_before_single_product_summary' );
        					wc_get_template( 'single-product/product-image-carousel.php' );
        				?>
                    </div>
    			</div>
    			<div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="information">
                        <?php
                            /**
                            * woocommerce_single_product_summary hook
                            *
                            * @hooked woocommerce_template_single_title - 5
                            * @hooked woocommerce_template_single_rating - 10
                            * @hooked woocommerce_template_single_price - 10
                            * @hooked woocommerce_template_single_excerpt - 20
                            * @hooked woocommerce_template_single_add_to_cart - 30
                            * @hooked woocommerce_template_single_meta - 40
                            * @hooked woocommerce_template_single_sharing - 50
                            */
                            
                            remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
                            do_action('woocommerce_single_product_summary');

                            ?>
                        
                    </div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
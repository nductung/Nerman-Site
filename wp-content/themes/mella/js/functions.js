(function ($) {
    "use strict";

    if (!$.apusThemeExtensions)
        $.apusThemeExtensions = {};
    
    function ApusThemeCore() {
        var self = this;
        self.init();
    };

    ApusThemeCore.prototype = {
        /**
         *  Initialize
         */
        init: function() {
            var self = this;
            
            self.preloadSite();

            // slick init
            self.initSlick($("[data-carousel=slick]"));

            // Unveil init
            setTimeout(function(){
                self.layzyLoadImage();
            }, 200);

            // isoto
            self.initIsotope();
            self.initCounterUp();

            // Sticky Header
            self.intChangeHeaderMarginTop();
            self.initHeaderSticky();

            // back to top
            self.backToTop();

            // popup image
            self.popupImage();

            
            self.initGmap();
            self.initWOW();

            $('[data-toggle="tooltip"]').tooltip();

            // perfectScrollbar
            $('.main-menu-top').perfectScrollbar();

            self.initPopupNewsletter();

            self.initMobileMenu();

            $('.footer-search-btn').on('click', function(){
                $('.footer-search-mobile').toggleClass('active');
            });
            $('.more').on('click', function(){
                $('.wrapper-morelink').toggleClass('active');
            });
            // for search header
            $('.btn-search-top,.close-search-fix').on('click', function(e){
                e.preventDefault();
                $('.search-header').toggleClass('active');
                $('.btn-search-top i').toggleClass("icon_search ti-close");
                setTimeout(function(){
                    $('.search-header input.apus-search').focus();
                }, 50);
            });
            $(document.body).on('click', '.nav [data-toggle="dropdown"]' ,function(){
                if(  this.href && this.href != '#'){
                    window.location.href = this.href;
                }
            });

            self.initHeader();
            self.loadExtension();
        },
        /**
         *  Extensions: Load scripts
         */
        loadExtension: function() {
            var self = this;
            
            if ($.apusThemeExtensions.shop) {
                $.apusThemeExtensions.shop.call(self);
            }
            
            if ($.apusThemeExtensions.bundle) {
                $.apusThemeExtensions.bundle.call(self);
            }
        },
        initSlick: function(element) {
            var self = this;
            element.each( function(){
                var config = {
                    infinite: false,
                    arrows: $(this).data( 'nav' ),
                    dots: $(this).data( 'pagination' ),
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    prevArrow:"<button type='button' class='slick-arrow slick-prev pull-left'><i class='icon-back3'></i></span><span class='textnav'>"+ mella_ajax.previous +"</span></button>",
                    nextArrow:"<button type='button' class='slick-arrow slick-next pull-right'><span class='textnav'>"+ mella_ajax.next +"</span><i class=' icon-next4'></i></button>",
                };
            
                var slick = $(this);
                if( $(this).data('items') ){
                    config.slidesToShow = $(this).data( 'items' );
                    config.slidesToScroll = $(this).data( 'items' );
                }
                if( $(this).data('infinite') ){
                    config.infinite = true;
                }
                if( $(this).data('vertical') ){
                    config.vertical = true;
                }
                if( $(this).data('rows') ){
                    config.rows = $(this).data( 'rows' );
                }
                if( $(this).data('asnavfor') ){
                    config.asNavFor = $(this).data( 'asnavfor' );
                }
                if( $(this).data('slidestoscroll') ){
                    config.slidesToScroll = $(this).data( 'slidestoscroll' );
                }
                if( $(this).data('focusonselect') ){
                    config.focusOnSelect = $(this).data( 'focusonselect' );
                }
                if ($(this).data('large')) {
                    var desktop = $(this).data('large');
                } else {
                    var desktop = config.items;
                }
                if ($(this).data('smalldesktop')) {
                    var smalldesktop = $(this).data('smalldesktop');
                } else {
                    if ($(this).data('large')) {
                        var smalldesktop = $(this).data('large');
                    } else{
                        var smalldesktop = config.items;
                    }
                }
                if ($(this).data('medium')) {
                    var medium = $(this).data('medium');
                } else {
                    var medium = config.items;
                }
                if ($(this).data('smallmedium')) {
                    var smallmedium = $(this).data('smallmedium');
                } else {
                    var smallmedium = 2;
                }
                if ($(this).data('extrasmall')) {
                    var extrasmall = $(this).data('extrasmall');
                } else {
                    var extrasmall = 1;
                }
                if ($(this).data('smallest')) {
                    var smallest = $(this).data('smallest');
                } else {
                    var smallest = 1;
                }
                config.responsive = [
                    {
                        breakpoint: 321,
                        settings: {
                            slidesToShow: smallest,
                            slidesToScroll: smallest,
                        }
                    },
                    {
                        breakpoint: 481,
                        settings: {
                            slidesToShow: extrasmall,
                            slidesToScroll: extrasmall,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: smallmedium,
                            slidesToScroll: smallmedium
                        }
                    },
                    {
                        breakpoint: 981,
                        settings: {
                            slidesToShow: medium,
                            slidesToScroll: medium
                        }
                    },
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: smalldesktop,
                            slidesToScroll: smalldesktop
                        }
                    },
                    {
                        breakpoint: 1501,
                        settings: {
                            slidesToShow: desktop,
                            slidesToScroll: desktop
                        }
                    }
                ];
                if ( $('html').attr('dir') == 'rtl' ) {
                    config.rtl = true;
                }

                $(this).slick( config );

            } );

            // Fix owl in bootstrap tabs
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href");
                var $slick = $("[data-carousel=slick]", target);

                if ($slick.length > 0 && $slick.hasClass('slick-initialized')) {
                    $slick.slick('refresh');
                }
                self.layzyLoadImage();
            });
        },
        layzyLoadImage: function() {
            $(window).off('scroll.unveil resize.unveil lookup.unveil');
            var $images = $('.image-wrapper:not(.image-loaded) .unveil-image'); // Get un-loaded images only
            if ($images.length) {
                $images.unveil(1, function() {
                    $(this).load(function() {
                        $(this).parents('.image-wrapper').first().addClass('image-loaded');
                        $(this).removeAttr('data-src');
                        $(this).removeAttr('data-srcset');
                        $(this).removeAttr('data-sizes');
                    });
                });
            }

            var $images = $('.product-image:not(.image-loaded) .unveil-image'); // Get un-loaded images only
            if ($images.length) {
                $images.unveil(1, function() {
                    $(this).load(function() {
                        $(this).parents('.product-image').first().addClass('image-loaded');
                    });
                });
            }
        },
        initCounterUp: function() {
            if($('.counterUp').length > 0){
                $('.counterUp').counterUp({
                    delay: 10,
                    time: 800
                });
            }
        },
        initIsotope: function() {
            $('.isotope-items').each(function(){  
                var $container = $(this);
                
                $container.imagesLoaded( function(){
                    $container.isotope({
                        itemSelector : '.isotope-item',
                        transformsEnabled: true,         // Important for videos
                        masonry: {
                            columnWidth: $container.data('columnwidth')
                        }
                    }); 
                });
            });

            /*---------------------------------------------- 
             *    Apply Filter        
             *----------------------------------------------*/
            $('.isotope-filter li a').on('click', function(){
               
                var parentul = $(this).parents('ul.isotope-filter').data('related-grid');
                $(this).parents('ul.isotope-filter').find('li a').removeClass('active');
                $(this).addClass('active');
                var selector = $(this).attr('data-filter'); 
                $('#'+parentul).isotope({ filter: selector }, function(){ });
                
                return(false);
            });
        },
        changeHeaderMarginTop: function() {
            if ($(window).width() > 991) {
                if ( $('.main-sticky-header').length > 0 ) {
                    var header_height = $('.main-sticky-header').outerHeight();
                    $('.main-sticky-header-wrapper').css({'height': header_height});
                }
                // top for cart
                if ( $('#apus-header').length > 0 ) {
                    var header_height = $('#apus-header').outerHeight();
                    $('.apus-breadscrumb.has_bg ').css({'padding-top': header_height});
                }
            }
        },
        intChangeHeaderMarginTop: function() {
            var self = this;
            setTimeout(function(){
                self.changeHeaderMarginTop();
            }, 50);
            $(window).resize(function(){
                self.changeHeaderMarginTop();
            });
        },
        initHeaderSticky: function() {
            var self = this;
            var main_sticky = $('.main-sticky-header');
            setTimeout(function(){
                if ( main_sticky.length > 0 ){
                    if ($(window).width() > 991) {
                        var _menu_action = main_sticky.offset().top;
                        $(window).scroll(function(event) {
                            self.headerSticky(main_sticky, _menu_action);
                        });
                        self.headerSticky(main_sticky, _menu_action);
                    }
                }
            }, 50);
        },
        headerSticky: function(main_sticky, _menu_action) {
            if( $(document).scrollTop() > _menu_action ){
                main_sticky.addClass('sticky-header');
            }else{
                main_sticky.removeClass('sticky-header');
            }
        },
        backToTop: function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 400) {
                    $('#back-to-top').addClass('active');
                } else {
                    $('#back-to-top').removeClass('active');
                }
            });
            $('#back-to-top').on('click', function () {
                $('html, body').animate({scrollTop: '0px'}, 800);
                return false;
            });
        },
        popupImage: function() {
            // popup
            $(".popup-image").magnificPopup({type:'image'});
            $('.popup-video').magnificPopup({
                disableOn: 700,
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,
                fixedContentPos: false
            });

            $('.widget-gallery').each(function(){
                var tagID = $(this).attr('id');
                $('#' + tagID).magnificPopup({
                    delegate: '.popup-image-gallery',
                    type: 'image',
                    tLoading: 'Loading image #%curr%...',
                    mainClass: 'mfp-img-mobile',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                    }
                });
            });
        },
        preloadSite: function() {
            // preload page
            if ( $('body').hasClass('apus-body-loading') ) {
                $('body').removeClass('apus-body-loading');
                $('.apus-page-loading').fadeOut(100);
            }
        },
        initGmap: function() {
            var self = this;
            
            $('.apus-google-map').each(function(){
                var $item = $(this);
                var id = $item.attr('id');
                var lat = $item.data('lat');
                var lng = $item.data('lng');
                var map = null;
                var fenway = new google.maps.LatLng(lat, lng);
                var mapOptions = {
                    center: fenway,
                    zoom: $item.data('zoom'),
                    scrollwheel: false
                };

                map = new google.maps.Map(document.getElementById(id), mapOptions);
                
                map.setOptions({styles: $item.data('style')});
                
                var latlng = new google.maps.LatLng(lat, lng);
                
                var marker_obj = {
                    position: latlng,
                    map: map,
                    draggable: false,
                    animation: google.maps.Animation.DROP
                };
                if ($item.data('marker_icon')) {
                    marker_obj.icon = $item.data('marker_icon');
                }
                var marker = new google.maps.Marker(marker_obj);
            });
        },
        initWOW: function() {
            if (typeof WOW !== "undefined") {
                var wow = new WOW({
                    boxClass:     'wow',      // default
                    animateClass: 'animated', // default
                    offset:       0,          // default
                    mobile:       true,       // default
                    live:         true        // default
                });
                wow.init();
            }
        },
        initPopupNewsletter: function() {
            var self = this;

            if ($('.popupnewsletter').length > 0) {
                setTimeout(function(){
                    var hiddenmodal = self.getCookie('hidde_popup_newsletter');
                    if (hiddenmodal == "") {
                        var popup_content = $('.popupnewsletter').html();
                        $.magnificPopup.open({
                            mainClass: 'apus-mfp-zoom-in popupnewsletter-wrapper',
                            modal:true,
                            items    : {
                                src : popup_content,
                                type: 'inline'
                            },
                            callbacks: {
                                close: function() {
                                    var dont = $('.close-dont-show').attr('data-dont');
                                    if ( dont === 'yes' ) {
                                        self.setCookie('hidde_popup_newsletter', 1, 30);
                                    }
                                }
                            }
                        });
                    }
                }, 3000);
            }
            $('.apus-mfp-close').on('click', function(){
                $.magnificPopup.close();
            });
            $('body').on('click', '.close-dont-show', function(e){
                e.preventDefault();
                $(this).attr('data-dont', 'yes');
                $.magnificPopup.close();
            });
        },
        initMobileMenu: function() {
            var self = this;
            // mobile menu
            $('.btn-toggle-canvas').on('click', function (e) {
                e.stopPropagation();
                $('.apus-offcanvas').toggleClass('active');           
                $('.over-dark').toggleClass('active');      
                self.layzyLoadImage();

                var $slick = $("[data-carousel=slick]", $('.apus-offcanvas'));
                if ($slick.length > 0 && $slick.hasClass('slick-initialized')) {
                    $slick.slick('refresh');
                }
            });
            $('body').on('click', function() {
                if ($('.apus-offcanvas').hasClass('active')) {
                    $('.apus-offcanvas').toggleClass('active');
                    $('.over-dark').toggleClass('active');
                }
            });
            $('.apus-offcanvas').on('click', function(e) {
                e.stopPropagation();
            });

            $(".main-mobile-menu .icon-toggle").on('click', function(){
                $(this).parent().find('> .sub-menu').slideToggle();
                self.layzyLoadImage();
                var $slick = $("[data-carousel=slick]", $('.apus-offcanvas'));
                if ($slick.length > 0 && $slick.hasClass('slick-initialized')) {
                    $slick.slick('refresh');
                }
                if ( $(this).find('i').hasClass('icon-plus') ) {
                    $(this).find('i').removeClass('icon-plus').addClass('icon-minus');
                } else {
                    $(this).find('i').removeClass('icon-minus').addClass('icon-plus');
                }
                return false;
            } );

            // sidebar mobile
            $('.sidebar-right, .sidebar-left').perfectScrollbar();
            $('body').on('click', '.mobile-sidebar-btn', function(){
                if ( $('.sidebar-left').length > 0 ) {
                    $('.sidebar-left').toggleClass('active');
                } else if ( $('.sidebar-right').length > 0 ) {
                    $('.sidebar-right').toggleClass('active');
                }
                $('.mobile-sidebar-panel-overlay').toggleClass('active');
                self.layzyLoadImage();
                var $slick = $("[data-carousel=slick]", $('.apus-offcanvas'));
                if ($slick.length > 0 && $slick.hasClass('slick-initialized')) {
                    $slick.slick('refresh');
                }
            });
            $('body').on('click', '.mobile-sidebar-panel-overlay, .close-sidebar-btn', function(){
                if ( $('.sidebar-left').length > 0 ) {
                    $('.sidebar-left').removeClass('active');
                } else if ( $('.sidebar-right').length > 0 ) {
                    $('.sidebar-right').removeClass('active');
                }
                $('.mobile-sidebar-panel-overlay').removeClass('active');
            });
        },
        initHeader: function() {
            $('.btn-main-menu-right').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                $(this).find('.icon-show-menu').toggleClass('active');
                $('.site-header-mainmenu').toggleClass('show').outerWidth('370px');
                var overlay_left = 370;
                
                if ( $('.site-header-mainmenu').hasClass('show') ) {
                    if ( $('html').attr('dir') == 'rtl' ) {
                       $('#wrapper-container, .apus-header .sticky-header,.header_transparent .apus-header').css({
                            'padding-left': '370px','overflow': 'visible'
                        });
                        $('.over-dark').toggleClass('show').css({'left' : overlay_left});
                    } else {
                        $('#wrapper-container, .apus-header .sticky-header, .header_transparent .apus-header').css({
                            'padding-right': '370px','overflow': 'visible'
                        });
                        $('.over-dark').toggleClass('show').css({'right' : overlay_left});
                    }
                    $('body').addClass('scroll-hidden');
                } else {
                    $('#wrapper-container, .apus-header .sticky-header, .header_transparent .apus-header').attr('style', '');
                    $('.over-dark').toggleClass('show').attr('style', '');
                    $('body').removeClass('scroll-hidden');
                }
            });

            $('body').on('click', function() {
                if ($('.site-header-mainmenu').hasClass('show')) {
                    $('.site-header-mainmenu').toggleClass('show');
                    $(this).find('.icon-show-menu').toggleClass('active');

                    $('body').removeClass('scroll-hidden');
                    $('#wrapper-container').attr('style', '');
                }
            });
            $('.site-header-mainmenu').on('click', function(e) {
                e.stopPropagation();
            });
            $('.btn-main-menu-left').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                $(this).find('.icon-show-menu').toggleClass('active');
                $('.site-header-mainmenu').toggleClass('show').outerWidth('370px');
                var overlay_left = 370;
                
                if ( $('.site-header-mainmenu').hasClass('show') ) {
                    if ( $('html').attr('dir') == 'rtl' ) {
                        $('#wrapper-container, .apus-header .sticky-header').css({
                            'padding-right': '370px','overflow': 'visible'
                        });
                        $('.over-dark').toggleClass('show').css({'right' : overlay_left});
                    } else {
                        $('#wrapper-container, .apus-header .sticky-header').css({
                            'padding-left': '370px','overflow': 'visible'
                        });
                        $('.over-dark').toggleClass('show').css({'left' : overlay_left});
                    }
                    $('body').addClass('scroll-hidden');
                } else {
                    $('#wrapper-container, .apus-header .sticky-header').attr('style', '');
                    $('.over-dark').toggleClass('show').attr('style', '');
                    $('body').attr('style', '');
                    $('body').removeClass('scroll-hidden');
                }
            });

            $('.btn-main-menu').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                $(this).find('.icon-show-menu').toggleClass('active');
                $('.header-v6,.header-v6 .main-menu').toggleClass('active');
                $('.header-v12,.header-v12 .main-menu').toggleClass('active');
                // header 9, 11
                $('.header-v9 .site-header-mainmenu,.header-v11 .site-header-mainmenu').toggleClass('show').outerWidth('320px');
            });

            $('.show-header-left').on('click', function(e){
                e.stopPropagation();
                $('.header-left-inner').toggleClass('active');
            });

        },
        setCookie: function(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + "; " + expires+";path=/";
        },
        getCookie: function(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
            }
            return "";
        }
    }

    $.apusThemeCore = ApusThemeCore.prototype;
    
    
    $.fn.wrapStart = function(numWords){
        return this.each(function(){
            var $this = $(this);
            var node = $this.contents().filter(function(){
                return this.nodeType == 3;
            }).first(),
            text = node.text().trim(),
            first = text.split(' ', 1).join(" ");
            if (!node.length) return;
            node[0].nodeValue = text.slice(first.length);
            node.before('<b>' + first + '</b>');
        });
    };

    $(document).ready(function() {
        // Initialize script
        new ApusThemeCore();

        $('.mod-heading .widget-title > span').wrapStart(1);
    });

})(jQuery);


<?php

if ( function_exists('vc_map') && class_exists('WPBakeryShortCode') ) {
	
	if ( !function_exists('mella_load_load_theme_element')) {
		function mella_load_load_theme_element() {
			$columns = array(1,2,3,4,6);
			// Heading Text Block
			vc_map( array(
				'name'        => esc_html__( 'Apus Widget Heading','mella'),
				'base'        => 'apus_title_heading',
				"class"       => "",
				"category" => esc_html__('Apus Elements', 'mella'),
				'description' => esc_html__( 'Create title for one Widget', 'mella' ),
				"params"      => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'mella' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter heading title.', 'mella' ),
						"admin_label" => true,
					),
					array(
						"type" => "textarea",
						'heading' => esc_html__( 'Description', 'mella' ),
						"param_name" => "des",
						"value" => '',
						'description' => esc_html__( 'Enter description for title.', 'mella' )
				    ),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Style", 'mella'),
						"param_name" => "style",
						'value' 	=> array(
							esc_html__('Default Center', 'mella') => 'center', 
						),
						'std' => ''
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'mella' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'mella' )
					)
				),
			));
			
			// calltoaction
			vc_map( array(
				'name'        => esc_html__( 'Apus Call To Action','mella'),
				'base'        => 'apus_call_action',
				"category" => esc_html__('Apus Elements', 'mella'),
				'description' => esc_html__( 'Create title for one Widget', 'mella' ),
				"params"      => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Sub title', 'mella' ),
						'param_name' => 'sub_title',
						'value'       => esc_html__( 'Title', 'mella' ),
						'description' => esc_html__( 'Enter Sub heading title.', 'mella' ),
						"admin_label" => true
					),
					array(
						"type" => "textarea_html",
						'heading' => esc_html__( 'Widget title', 'mella' ),
						"param_name" => "content",
						"value" => '',
						'description' => esc_html__( 'Enter Widget title.', 'mella' )
				    ),

				    array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Text Button', 'mella' ),
						'param_name' => 'textbutton1',
						'description' => esc_html__( 'Text Button', 'mella' ),
						"admin_label" => true
					),

					array(
						'type' => 'textfield',
						'heading' => esc_html__( ' Link Button', 'mella' ),
						'param_name' => 'linkbutton1',
						'description' => esc_html__( 'Link Button', 'mella' ),
						"admin_label" => true
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Button Style", 'mella'),
						"param_name" => "buttons1",
						'value' 	=> array(
							esc_html__('Default ', 'mella') => 'btn-default ', 
							esc_html__('Primary ', 'mella') => 'btn-primary ', 
							esc_html__('Success ', 'mella') => 'btn-success radius-0 ', 
							esc_html__('Info ', 'mella') => 'btn-info ', 
							esc_html__('Warning ', 'mella') => 'btn-warning ', 
							esc_html__('Theme Color ', 'mella') => 'btn-theme',
							esc_html__('Theme Gradient Color ', 'mella') => 'btn-theme btn-gradient',
							esc_html__('Second Color ', 'mella') => 'btn-theme-second',
							esc_html__('Danger ', 'mella') => 'btn-danger ', 
							esc_html__('Pink ', 'mella') => 'btn-pink ', 
							esc_html__('White Gradient ', 'mella') => 'btn-white btn-gradient', 
							esc_html__('Primary Outline', 'mella') => 'btn-primary btn-outline', 
							esc_html__('White ', 'mella') => 'btn-white',
							esc_html__('Theme Outline ', 'mella') => 'btn-theme btn-outline ',
						),
						'std' => ''
					),
					
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Style", 'mella'),
						"param_name" => "style",
						'value' 	=> array(
							esc_html__('Default', 'mella') => 'default',
						),
						'std' => ''
					),

					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'mella' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'mella' )
					)
				),
			));
			// Our Team
			vc_map( array(
	            "name" => esc_html__("Apus Our Team",'mella'),
	            "base" => "apus_ourteam",
	            'description'=> esc_html__('Display Our Team In FrontEnd', 'mella'),
	            "class" => "",
	            "category" => esc_html__('Apus Elements', 'mella'),
	            "params" => array(
	              	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => '',
					),
	              	array(
						'type' => 'param_group',
						'heading' => esc_html__('Members Settings', 'mella' ),
						'param_name' => 'members',
						'description' => '',
						'value' => '',
						'params' => array(
							array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Name','mella'),
				                "param_name" => "name",
				            ),
				            array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Job','mella'),
				                "param_name" => "job",
				            ),
							array(
								"type" => "attach_image",
								"heading" => esc_html__("Image", 'mella'),
								"param_name" => "image"
							),
				            array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Facebook','mella'),
				                "param_name" => "facebook",
				            ),

				            array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Twitter Link','mella'),
				                "param_name" => "twitter",
				            ),

				            array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Google plus Link','mella'),
				                "param_name" => "google",
				            ),

				            array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Linkin Link','mella'),
				                "param_name" => "linkin",
				            ),

						),
					),
					array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Columns','mella'),
		                "param_name" => 'columns',
		                "value" => array(1,2,3,4,5,6),
		            ),
		            array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
	            )
	        ));

	        // Our Team
			vc_map( array(
	            "name" => esc_html__("Apus List Store",'mella'),
	            "base" => "apus_store",
	            'description'=> esc_html__('Display List Store In FrontEnd', 'mella'),
	            "class" => "",
	            "category" => esc_html__('Apus Elements', 'mella'),
	            "params" => array(
	              	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => '',
					),
	              	array(
						'type' => 'param_group',
						'heading' => esc_html__('Members Settings', 'mella' ),
						'param_name' => 'members',
						'description' => '',
						'value' => '',
						'params' => array(
							array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Name Store','mella'),
				                "param_name" => "name",
				            ),
				            array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Location','mella'),
				                "param_name" => "location",
				            ),
				            array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Phone','mella'),
				                "param_name" => "phone",
				            ),
				            array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Email','mella'),
				                "param_name" => "email",
				            ),
						),
					),
					array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Columns','mella'),
		                "param_name" => 'columns',
		                "value" => array(1,2,3,4,5,6),
		            ),
		            array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
	            )
	        ));

			// Apus Counter
			vc_map( array(
			    "name" => esc_html__("Apus Counter",'mella'),
			    "base" => "apus_counter",
			    "class" => "",
			    "description"=> esc_html__('Counting number with your term', 'mella'),
			    "category" => esc_html__('Apus Elements', 'mella'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"value" => '',
						"admin_label"	=> true
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Number", 'mella'),
						"param_name" => "number",
						"value" => ''
					),
					array(
						"type" => "colorpicker",
						"heading" => esc_html__("Color Number", 'mella'),
						"param_name" => "text_color",
						'value' 	=> '',
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
			   	)
			));
			// Banner CountDown
			vc_map( array(
				'name'        => esc_html__( 'Apus Banner CountDown','mella'),
				'base'        => 'apus_banner_countdown',
				"category" => esc_html__('Apus Elements', 'mella'),
				'description' => esc_html__( 'Show CountDown with banner', 'mella' ),
				"params"      => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Sub Title', 'mella' ),
						'param_name' => 'subtitle',
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget Title', 'mella' ),
						'param_name' => 'title',
					),
					array(
						'type' => 'textarea_html',
						'heading' => esc_html__( 'Price', 'mella' ),
						'param_name' => 'content',
					),
					array(
						"type" => "textarea",
						'heading' => esc_html__( 'Description', 'mella' ),
						"param_name" => "des",
						"value" => '',
						'description' => esc_html__( 'Enter description for title.', 'mella' )
				    ),
					array(
					    'type' => 'textfield',
					    'heading' => esc_html__( 'Date Expired', 'mella' ),
					    'param_name' => 'input_datetime'
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Button Url', 'mella' ),
						'param_name' => 'btn_url',
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Style", 'mella'),
						"param_name" => "style_widget",
						'value' 	=> array(
							esc_html__('Default', 'mella') => '',
						),
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'mella' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'mella' )
					),
				),
			));
			// Banner
			vc_map( array(
				'name'        => esc_html__( 'Apus Banner Collections','mella'),
				'base'        => 'apus_banner_collections',
				"category" => esc_html__('Apus Elements', 'mella'),
				'description' => esc_html__( 'Show banner Collections in FrontEnd', 'mella' ),
				"params"      => array(
					array(
						'type' => 'textarea',
						'heading' => esc_html__( 'Title', 'mella' ),
						'param_name' => 'title',
					),
				    array(
						"type" => "attach_image",
						"heading" => esc_html__("Banner Image", 'mella'),
						"param_name" => "image"
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Url', 'mella' ),
						'param_name' => 'url',
					),
					array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Style','mella'),
		                "param_name" => 'style',
		                'value' 	=> array(
							esc_html__('Normal ', 'mella') => 's_normal', 
							esc_html__('Large ', 'mella') => 's_large',
						),
						'std' => ''
		            ),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'mella' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'mella' )
					),
				),
			));
			// Banner
			vc_map( array(
				'name'        => esc_html__( 'Apus Banner','mella'),
				'base'        => 'apus_banner',
				"category" => esc_html__('Apus Elements', 'mella'),
				'description' => esc_html__( 'Show banner in FrontEnd', 'mella' ),
				"params"      => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Sub Title', 'mella' ),
						'param_name' => 'subtitle',
					),
					array(
						'type' => 'textarea_html',
						'heading' => esc_html__( 'Widget Title', 'mella' ),
						'param_name' => 'content',
					),
					array(
						'type' => 'textarea',
						'heading' => esc_html__( 'Description', 'mella' ),
						'param_name' => 'des',
					),
				    array(
						"type" => "attach_image",
						"heading" => esc_html__("Banner Image", 'mella'),
						"param_name" => "image"
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Url', 'mella' ),
						'param_name' => 'url',
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Text Url', 'mella' ),
						'param_name' => 'text_url',
					),
					array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Position','mella'),
		                "param_name" => 'style',
		                'value' 	=> array(
							esc_html__('Left ', 'mella') => 'p_left ', 
							esc_html__('Right ', 'mella') => 'p_right ',
							esc_html__('Left Bottom', 'mella') => 'p_left p_bottom', 
							esc_html__('Right Bottom', 'mella') => 'p_right p_bottom',
						),
						'std' => ''
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Font Size','mella'),
		                "param_name" => 'size',
		                'value' 	=> array(
							esc_html__('Small', 'mella') => 'f_small ', 
							esc_html__('Normal', 'mella') => 'f_normal ', 
							esc_html__('Large', 'mella') => 'f_large ',
							esc_html__('Big', 'mella') => 'f_big ',
							esc_html__('Big Thin', 'mella') => 'f_big f_thin',
						),
						'std' => ''
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Align','mella'),
		                "param_name" => 'align',
		                'value' 	=> array(
							esc_html__('Left ', 'mella') => 'a_left ', 
							esc_html__('Right ', 'mella') => 'a_right ',
							esc_html__('Center ', 'mella') => 'a_center ',
						),
						'std' => ''
		            ),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'mella' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'mella' )
					),
				),
			));
			// Banner
			vc_map( array(
				'name'        => esc_html__( 'Apus Banner Sale Of','mella'),
				'base'        => 'apus_banner_sale',
				"category" => esc_html__('Apus Elements', 'mella'),
				'description' => esc_html__( 'Show banner Sale in FrontEnd', 'mella' ),
				"params"      => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Sub Title', 'mella' ),
						'param_name' => 'subtitle',
					),
					array(
						'type' => 'textarea_html',
						'heading' => esc_html__( 'Widget Title', 'mella' ),
						'param_name' => 'content',
					),
					array(
						'type' => 'textarea',
						'heading' => esc_html__( 'Description', 'mella' ),
						'param_name' => 'des',
					),
				    array(
						"type" => "attach_image",
						"heading" => esc_html__("Banner Image", 'mella'),
						"param_name" => "image"
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Url', 'mella' ),
						'param_name' => 'url',
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'mella' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'mella' )
					),
				),
			));
			// Banner
			vc_map( array(
				'name'        => esc_html__( 'Apus Lookbook Banner','mella'),
				'base'        => 'apus_lookbook',
				"category" => esc_html__('Apus Elements', 'mella'),
				'description' => esc_html__( 'Show Lookbook banner in FrontEnd', 'mella' ),
				"params"      => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget Title', 'mella' ),
						'param_name' => 'title',
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Url', 'mella' ),
						'param_name' => 'url',
					),
				    array(
						"type" => "attach_image",
						"heading" => esc_html__("Banner Image", 'mella'),
						"param_name" => "image"
					),
					array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Style','mella'),
		                "param_name" => 'style',
		                'value' 	=> array(
							esc_html__('Normal ', 'mella') => 'normal ', 
							esc_html__('Large ', 'mella') => 'large ',
						),
						'std' => ''
		            ),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'mella' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'mella' )
					),
				),
			));
			// Apus Brands
			vc_map( array(
			    "name" => esc_html__("Apus Brands",'mella'),
			    "base" => "apus_brands",
			    "class" => "",
			    "description"=> esc_html__('Display brands on front end', 'mella'),
			    "category" => esc_html__('Apus Elements', 'mella'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"value" => '',
						"admin_label"	=> true
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Number", 'mella'),
						"param_name" => "number",
						"value" => ''
					),
				 	array(
						"type" => "dropdown",
						"heading" => esc_html__("Layout Type", 'mella'),
						"param_name" => "layout_type",
						'value' 	=> array(
							esc_html__('Carousel', 'mella') => 'carousel', 
							esc_html__('Grid', 'mella') => 'grid'
						),
						'std' => ''
					),
					array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Columns','mella'),
		                "param_name" => 'columns',
		                "value" => array(1,2,3,4,5,6,8),
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Style','mella'),
		                "param_name" => 'style',
		                'value' 	=> array(
							esc_html__('Default ', 'mella') => '', 
						),
						'std' => ''
		            ),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
			   	)
			));
			
			vc_map( array(
			    "name" => esc_html__("Apus Socials link",'mella'),
			    "base" => "apus_socials_link",
			    "description"=> esc_html__('Show socials link', 'mella'),
			    "category" => esc_html__('Apus Elements', 'mella'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"value" => '',
						"admin_label"	=> true
					),
					array(
						"type" => "textarea",
						"heading" => esc_html__("Description", 'mella'),
						"param_name" => "description",
						"value" => '',
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Facebook Page URL", 'mella'),
						"param_name" => "facebook_url",
						"value" => '',
						"admin_label"	=> true
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Twitter Page URL", 'mella'),
						"param_name" => "twitter_url",
						"value" => '',
						"admin_label"	=> true
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Youtube Page URL", 'mella'),
						"param_name" => "youtube_url",
						"value" => '',
						"admin_label"	=> true
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Pinterest Page URL", 'mella'),
						"param_name" => "pinterest_url",
						"value" => '',
						"admin_label"	=> true
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Google Plus Page URL", 'mella'),
						"param_name" => "google-plus_url",
						"value" => '',
						"admin_label"	=> true
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Instagram Page URL", 'mella'),
						"param_name" => "instagram_url",
						"value" => '',
						"admin_label"	=> true
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Align", 'mella'),
						"param_name" => "align",
						'value' 	=> array(
							esc_html__('Left', 'mella') => 'left', 
							esc_html__('Right', 'mella') => 'right',
							esc_html__('Center', 'mella') => 'center'
						),
						'std' => ''
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Style", 'mella'),
						"param_name" => "style",
						'value' 	=> array(
							esc_html__('Icon', 'mella') => '', 
							esc_html__('Text', 'mella') => 'st_text', 
						),
						'std' => ''
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
			   	)
			));
			// newsletter
			vc_map( array(
			    "name" => esc_html__("Apus Newsletter",'mella'),
			    "base" => "apus_newsletter",
			    "class" => "",
			    "description"=> esc_html__('Show newsletter form', 'mella'),
			    "category" => esc_html__('Apus Elements', 'mella'),
			    "params" => array(
			    	array(
						'type' => 'textarea_html',
						'heading' => esc_html__( 'Widget Title', 'mella' ),
						'param_name' => 'content',
					),
					array(
						"type" => "textarea",
						"heading" => esc_html__("Description", 'mella'),
						"param_name" => "description",
						"value" => '',
					),
					array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Layout','mella'),
		                "param_name" => 'style',
		                'value' 	=> array(
							esc_html__('Style 1 ', 'mella') => 'style1',
							esc_html__('Style 2 ', 'mella') => 'style2',
							esc_html__('Style 3 ', 'mella') => 'style3',
							esc_html__('Style 4 ', 'mella') => 'style4',
						),
						'std' => ''
		            ),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
			   	)
			));
			
			// google map
			$map_styles = array( esc_html__('Choose a map style', 'mella') => '' );
			if ( is_admin() && class_exists('Mella_Google_Maps_Styles') ) {
				$styles = Mella_Google_Maps_Styles::styles();
				foreach ($styles as $style) {
					$map_styles[$style['title']] = $style['slug'];
				}
			}
			vc_map( array(
			    "name" => esc_html__("Apus Google Map",'mella'),
			    "base" => "apus_googlemap",
			    "description" => esc_html__('Diplay Google Map', 'mella'),
			    "category" => esc_html__('Apus Elements', 'mella'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => '',
					),
					array(
		                "type" => "textarea",
		                "class" => "",
		                "heading" => esc_html__('Description','mella'),
		                "param_name" => "des",
		            ),
		            array(
		                'type' => 'googlemap',
		                'heading' => esc_html__( 'Location', 'mella' ),
		                'param_name' => 'location',
		                'value' => ''
		            ),
		            array(
		                'type' => 'hidden',
		                'heading' => esc_html__( 'Latitude Longitude', 'mella' ),
		                'param_name' => 'lat_lng',
		                'value' => '21.0173222,105.78405279999993'
		            ),
		            array(
						"type" => "textfield",
						"heading" => esc_html__("Map height", 'mella'),
						"param_name" => "height",
						"value" => '',
					),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Map Zoom", 'mella'),
						"param_name" => "zoom",
						"value" => '13',
					),
		            array(
		                'type' => 'dropdown',
		                'heading' => esc_html__( 'Map Type', 'mella' ),
		                'param_name' => 'type',
		                'value' => array(
		                    esc_html__( 'roadmap', 'mella' ) 		=> 'ROADMAP',
		                    esc_html__( 'hybrid', 'mella' ) 	=> 'HYBRID',
		                    esc_html__( 'satellite', 'mella' ) 	=> 'SATELLITE',
		                    esc_html__( 'terrain', 'mella' ) 	=> 'TERRAIN',
		                )
		            ),
		            array(
						"type" => "attach_image",
						"heading" => esc_html__("Custom Marker Icon", 'mella'),
						"param_name" => "marker_icon"
					),
					array(
		                'type' => 'dropdown',
		                'heading' => esc_html__( 'Custom Map Style', 'mella' ),
		                'param_name' => 'map_style',
		                'value' => $map_styles
		            ),
		            
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
			   	)
			));
			// Testimonial
			vc_map( array(
	            "name" => esc_html__("Apus Testimonials",'mella'),
	            "base" => "apus_testimonials",
	            'description'=> esc_html__('Display Testimonials In FrontEnd', 'mella'),
	            "class" => "",
	            "category" => esc_html__('Apus Elements', 'mella'),
	            "params" => array(
	              	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => '',
					),
	              	array(
		              	"type" => "textfield",
		              	"heading" => esc_html__("Number", 'mella'),
		              	"param_name" => "number",
		              	"value" => '4',
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Columns','mella'),
		                "param_name" => 'columns',
		                "value" => array(1,2,3,4,5,6),
		            ),
		            array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
	            )
	        ));
	        
	        // Gallery Images
			vc_map( array(
	            "name" => esc_html__("Apus Gallery",'mella'),
	            "base" => "apus_gallery",
	            'description'=> esc_html__('Display Gallery In FrontEnd', 'mella'),
	            "class" => "",
	            "category" => esc_html__('Apus Elements', 'mella'),
	            "params" => array(
	              	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => '',
					),
					array(
						'type' => 'param_group',
						'heading' => esc_html__('Images', 'mella'),
						'param_name' => 'images',
						'params' => array(
							array(
								"type" => "attach_image",
								"param_name" => "image",
								'heading'	=> esc_html__('Image', 'mella')
							),
							array(
				                "type" => "textfield",
				                "heading" => esc_html__('Title','mella'),
				                "param_name" => "title",
				            ),
				            array(
				                "type" => "textarea",
				                "heading" => esc_html__('Description','mella'),
				                "param_name" => "description",
				            ),
						),
					),
					array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Columns','mella'),
		                "param_name" => 'columns',
		                "value" => $columns,
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Layout Type','mella'),
		                "param_name" => 'layout_type',
		                'value' 	=> array(
							esc_html__('Grid', 'mella') => 'grid', 
							esc_html__('Mansory 1', 'mella') => 'mansory',
							esc_html__('Mansory 2', 'mella') => 'mansory2',
						),
						'std' => 'grid'
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Gutter Elements','mella'),
		                "param_name" => 'gutter',
		                'value' 	=> array(
							esc_html__('Default ', 'mella') => '', 
							esc_html__('Gutter 30', 'mella') => 'gutter30',
						),
						'std' => ''
		            ),
		            array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
	            )
	        ));
	        // Gallery Video
			vc_map( array(
	            "name" => esc_html__("Apus Video",'mella'),
	            "base" => "apus_video",
	            'description'=> esc_html__('Display Video In FrontEnd', 'mella'),
	            "class" => "",
	            "category" => esc_html__('Apus Elements', 'mella'),
	            "params" => array(
	              	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => '',
					),
					array(
						"type" => "textarea",
						'heading' => esc_html__( 'Description', 'mella' ),
						"param_name" => "des",
						"value" => '',
						'description' => esc_html__( 'Enter description for title.', 'mella' )
				    ),
	              	array(
						"type" => "attach_image",
						"heading" => esc_html__("Background Play Image", 'mella'),
						"param_name" => "image"
					),
					array(
						"type" => "attach_image",
						"heading" => esc_html__("Icon Play", 'mella'),
						"param_name" => "image_icon"
					),
					array(
		                "type" => "textfield",
		                "heading" => esc_html__('Youtube Video Link','mella'),
		                "param_name" => 'video_link'
		            ),
		            array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
	            )
	        ));
	        // Features Box
			vc_map( array(
	            "name" => esc_html__("Apus Features Box",'mella'),
	            "base" => "apus_features_box",
	            'description'=> esc_html__('Display Features In FrontEnd', 'mella'),
	            "class" => "",
	            "category" => esc_html__('Apus Elements', 'mella'),
	            "params" => array(
	            	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => '',
					),
					array(
						'type' => 'param_group',
						'heading' => esc_html__('Members Settings', 'mella' ),
						'param_name' => 'items',
						'description' => '',
						'value' => '',
						'params' => array(
							array(
								"type" => "attach_image",
								"description" => esc_html__("Image for box.", 'mella'),
								"param_name" => "image",
								"value" => '',
								'heading'	=> esc_html__('Image', 'mella' )
							),
							array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Title','mella'),
				                "param_name" => "title",
				            ),
				            array(
				                "type" => "textarea",
				                "class" => "",
				                "heading" => esc_html__('Description','mella'),
				                "param_name" => "description",
				            ),
							array(
								"type" => "textfield",
								"heading" => esc_html__("Material Design Icon and Awesome Icon", 'mella'),
								"param_name" => "icon",
								"value" => '',
								'description' => esc_html__( 'This support display icon from Material Design and Awesome Icon, Please click', 'mella' )
												. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://zavoloklom.github.io/material-design-iconic-font/icons.html" target="_blank">'
												. esc_html__( 'here to see the list', 'mella' ) . '</a>'
							),
						),
					),
		           	array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Style Layout','mella'),
		                "param_name" => 'style',
		                'value' 	=> array(
							esc_html__('Default', 'mella') => '', 
						),
						'std' => ''
		            ),
		            array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
	            )
	        ));
	        // Location
			vc_map( array(
	            "name" => esc_html__("Apus Location",'mella'),
	            "base" => "apus_location",
	            'description'=> esc_html__('Display Features In FrontEnd', 'mella'),
	            "class" => "",
	            "category" => esc_html__('Apus Elements', 'mella'),
	            "params" => array(
					array(
						'type' => 'param_group',
						'heading' => esc_html__('Members Settings', 'mella' ),
						'param_name' => 'items',
						'description' => '',
						'value' => '',
						'params' => array(
							array(
								"type" => "attach_image",
								"description" => esc_html__("Image for box.", 'mella'),
								"param_name" => "image",
								"value" => '',
								'heading'	=> esc_html__('Image', 'mella' )
							),
							array(
				                "type" => "textfield",
				                "class" => "",
				                "heading" => esc_html__('Title','mella'),
				                "param_name" => "title",
				            ),
				            array(
				                "type" => "textarea",
				                "class" => "",
				                "heading" => esc_html__('Description','mella'),
				                "param_name" => "description",
				            ),
						),
					),
		            array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
	            )
	        ));

			$custom_menus = array();
			if ( is_admin() ) {
				$menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );
				if ( is_array( $menus ) && ! empty( $menus ) ) {
					foreach ( $menus as $single_menu ) {
						if ( is_object( $single_menu ) && isset( $single_menu->name, $single_menu->slug ) ) {
							$custom_menus[ $single_menu->name ] = $single_menu->slug;
						}
					}
				}
			}
			// Menu
			vc_map( array(
			    "name" => esc_html__("Apus Custom Menu",'mella'),
			    "base" => "apus_custom_menu",
			    "class" => "",
			    "description"=> esc_html__('Show Custom Menu', 'mella'),
			    "category" => esc_html__('Apus Elements', 'mella'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"value" => '',
						"admin_label"	=> true
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Menu', 'mella' ),
						'param_name' => 'nav_menu',
						'value' => $custom_menus,
						'description' => empty( $custom_menus ) ? esc_html__( 'Custom menus not found. Please visit Appearance > Menus page to create new menu.', 'mella' ) : esc_html__( 'Select menu to display.', 'mella' ),
						'admin_label' => true,
						'save_always' => true,
					),
					array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Align','mella'),
		                "param_name" => 'align',
		                'value' 	=> array(
							esc_html__('Inherit', 'mella') => '', 
							esc_html__('Left', 'mella') => 'left', 
							esc_html__('Right', 'mella') => 'right', 
							esc_html__('Center', 'mella') => 'center', 
							esc_html__('Inline Left', 'mella') => 'inline left', 
							esc_html__('Inline Right', 'mella') => 'inline right', 
						),
						'std' => ''
		            ),
					array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
			   	)
			));

			vc_map( array(
	            "name" => esc_html__("Apus Instagram",'mella'),
	            "base" => "apus_instagram",
	            'description'=> esc_html__('Display Instagram In FrontEnd', 'mella'),
	            "class" => "",
	            "category" => esc_html__('Apus Elements', 'mella'),
	            "params" => array(
	            	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'mella'),
						"param_name" => "title",
						"admin_label" => true,
						"value" => '',
					),
					array(
		              	"type" => "textfield",
		              	"heading" => esc_html__("Instagram Username", 'mella'),
		              	"param_name" => "username",
		            ),
					array(
		              	"type" => "textfield",
		              	"heading" => esc_html__("Number", 'mella'),
		              	"param_name" => "number",
		              	'value' => '1',
		            ),
	             	array(
		              	"type" => "textfield",
		              	"heading" => esc_html__("Number Columns", 'mella'),
		              	"param_name" => "columns",
		              	'value' => '1',
		            ),
		           	array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Layout Type','mella'),
		                "param_name" => 'layout_type',
		                'value' 	=> array(
							esc_html__('Grid', 'mella') => 'grid', 
							esc_html__('Carousel', 'mella') => 'carousel', 
						)
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Photo size','mella'),
		                "param_name" => 'size',
		                'value' 	=> array(
							esc_html__('Thumbnail', 'mella') => 'thumbnail', 
							esc_html__('Small', 'mella') => 'small', 
							esc_html__('Large', 'mella') => 'large', 
							esc_html__('Original', 'mella') => 'original', 
						)
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Open links in','mella'),
		                "param_name" => 'target',
		                'value' 	=> array(
							esc_html__('Current window (_self)', 'mella') => '_self', 
							esc_html__('New window (_blank)', 'mella') => '_blank',
						)
		            ),
		            array(
		                "type" => "dropdown",
		                "heading" => esc_html__('Layout For Widget','mella'),
		                "param_name" => 'layout_widget',
		                'value' 	=> array(
							esc_html__('Style 1', 'mella') => '', 
							esc_html__('Style 2', 'mella') => 'style2', 
						)
		            ),
		            array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'mella'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
					)
	            )
	        ));
		}
	}
	add_action( 'vc_after_set_mode', 'mella_load_load_theme_element', 99 );

	class WPBakeryShortCode_apus_title_heading extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_call_action extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_brands extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_socials_link extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_newsletter extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_googlemap extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_testimonials extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_banner_countdown extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_banner extends WPBakeryShortCode {}

	class WPBakeryShortCode_apus_counter extends WPBakeryShortCode {
		public function __construct( $settings ) {
			parent::__construct( $settings );
			$this->load_scripts();
		}

		public function load_scripts() {
			wp_register_script('jquery-counterup', get_template_directory_uri().'/js/jquery.counterup.min.js', array('jquery'), false, true);
		}
	}
	class WPBakeryShortCode_apus_gallery extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_video extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_features_box extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_custom_menu extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_instagram extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_ourteam extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_location extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_lookbook extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_banner_collections extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_banner_sale extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_store extends WPBakeryShortCode {}
}
<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	do_action( 'woocommerce_before_single_product' );

	if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	}
    $thumbs_pos = mella_get_config('product_thumbs_position', 'thumbnails-bottom');
    $layout = mella_get_config('product_single_version', 'default');
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'details-product layout-'.$layout, $product ); ?>>
	<?php if ( $layout == 'default' || $layout == 'layout-image-1-column' ) {
		$thumb_class = 'col-sm-6 col-xs-12 left-detail';
		$content_class = 'column-information col-sm-6 col-xs-12';
		if ( $layout == 'default' ) {
			if ( $thumbs_pos == 'thumbnails-left' || $thumbs_pos == 'thumbnails-right' ) {
				$thumb_class = 'col-lg-7 col-sm-6 col-xs-12 left-detail';
				$content_class = 'column-information col-lg-5 col-sm-6 col-xs-12';
			}
		} elseif ( $layout == 'layout-image-1-column' ) {
			$content_class = 'column-information col-sm-6 col-xs-12 sticky-this';
		}
	?>
		<!-- layout: default|layout-2 -->
		<div class="row top-content product-v-wrapper">
			<div class="<?php echo esc_attr($thumb_class); ?>">
				<div class="image-mains <?php echo esc_attr($layout == 'default' ? $thumbs_pos : ''); ?>">
					<?php
						/**
						 * woocommerce_before_single_product_summary hook
						 *
						 * @hooked woocommerce_show_product_sale_flash - 10
						 * @hooked woocommerce_show_product_images - 20
						 */
						remove_action('woocommerce_before_single_product_summary','woocommerce_show_product_sale_flash',10);
						do_action( 'woocommerce_before_single_product_summary');
					?>
				</div>
			</div>
			<div class="<?php echo esc_attr($content_class); ?>">
				<div class="information">
					<div class="summary entry-summary">
						<?php
							/**
							 * woocommerce_single_product_summary hook
							 *
							 * @hooked woocommerce_template_single_title - 5
							 * @hooked woocommerce_template_single_rating - 10
							 * @hooked woocommerce_template_single_price - 10
							 * @hooked woocommerce_template_single_excerpt - 20
							 * @hooked woocommerce_template_single_add_to_cart - 30
							 * @hooked woocommerce_template_single_meta - 40
							 * @hooked woocommerce_template_single_sharing - 50
							 */
							remove_action('woocommerce_single_product_summary','woocommerce_template_single_rating',10);
							add_action('woocommerce_single_product_summary','woocommerce_template_single_rating',8);

							do_action( 'woocommerce_single_product_summary' );
						?>
					</div><!-- .summary -->
					
				</div>
			</div>
		</div>
	<?php } elseif($layout == 'layout-image-bg') {
		$bg_color = mella_get_config('product_image_breadcrumb_color');
		$style = '';
		if ( $bg_color ) {
			$style = 'style="background-color:'.$bg_color.'"';
		}
	?>	
		<div class="row top-content">
			<div class="col-xs-12 wrapper-image-bg" <?php echo trim($style); ?>>
				<div class="row">
					<div class="col-md-6">
						<div class="image-mains">
							<?php
								/**
								 * woocommerce_before_single_product_summary hook
								 *
								 * @hooked woocommerce_show_product_sale_flash - 10
								 * @hooked woocommerce_show_product_images - 20
								 */
								do_action( 'woocommerce_before_single_product_summary' );
							?>
						</div>
					</div>
					<div class="col-md-6 hidden-xs hidden-sm"></div>
				</div>
				<div class="info-content">
					<div class="container">
						<div class="row">
							<div class="col-md-6 hidden-xs hidden-sm"></div>
							<div class="col-md-6">
								<div class="information">
									<div class="summary entry-summary ">
										<?php
											/**
											 * woocommerce_single_product_summary hook
											 *
											 * @hooked woocommerce_template_single_title - 5
											 * @hooked woocommerce_template_single_rating - 10
											 * @hooked woocommerce_template_single_price - 10
											 * @hooked woocommerce_template_single_excerpt - 20
											 * @hooked woocommerce_template_single_add_to_cart - 30
											 * @hooked woocommerce_template_single_meta - 40
											 * @hooked woocommerce_template_single_sharing - 50
											 */
											remove_action('woocommerce_single_product_summary','woocommerce_template_single_rating',10);
											add_action('woocommerce_single_product_summary','woocommerce_template_single_rating',8);

											do_action( 'woocommerce_single_product_summary' );
										?>
									</div><!-- .summary -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<!-- layout 1 -->
		<div class="row top-content">
			<div class="col-xs-12 left-detail">
				<div class="image-mains">
					<?php
						/**
						 * woocommerce_before_single_product_summary hook
						 *
						 * @hooked woocommerce_show_product_sale_flash - 10
						 * @hooked woocommerce_show_product_images - 20
						 */
						do_action( 'woocommerce_before_single_product_summary' );
					?>
				</div>

				<div class="information">
					<div class="summary entry-summary ">
						<?php
							/**
							 * woocommerce_single_product_summary hook
							 *
							 * @hooked woocommerce_template_single_title - 5
							 * @hooked woocommerce_template_single_rating - 10
							 * @hooked woocommerce_template_single_price - 10
							 * @hooked woocommerce_template_single_excerpt - 20
							 * @hooked woocommerce_template_single_add_to_cart - 30
							 * @hooked woocommerce_template_single_meta - 40
							 * @hooked woocommerce_template_single_sharing - 50
							 */
							remove_action('woocommerce_single_product_summary','woocommerce_template_single_rating',10);
							add_action('woocommerce_single_product_summary','woocommerce_template_single_rating',8);

							do_action( 'woocommerce_single_product_summary' );
						?>
					</div><!-- .summary -->
					
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if ( $layout == 'layout-image-bg' ) { ?>
		<div class="container">
	<?php } ?>
	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>
	<?php if ( $layout == 'layout-image-bg' ) { ?>
		</div>
	<?php } ?>
	<meta itemprop="url" content="<?php the_permalink(); ?>" />
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
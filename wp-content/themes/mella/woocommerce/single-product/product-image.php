<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $post, $product;

$layout = mella_get_config('product_single_version', 'default');
$thumbnail_size    = apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' );
$post_thumbnail_id = $product->get_image_id();
$attachment_ids = $product->get_gallery_image_ids();
$count_thumbs = (!empty($attachment_ids) && has_post_thumbnail()) ? count($attachment_ids) + 1 : 1;
if ( $layout == 'default' || $layout == 'layout-image-bg' ) {
	$nb_columns = mella_get_config('number_product_thumbs',3);
	
	$thumbs_pos = mella_get_config('product_thumbs_position', 'thumbnails-bottom');
	if ($layout == 'layout-image-bg') {
		$thumbs_pos = 'thumbnails-left';
	}

?>
	<div class="apus-woocommerce-product-gallery-wrapper">

		<?php if ( $product->is_on_sale() ) : ?>
			<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'mella' ) . '</span>', $post, $product ); ?>
		<?php endif; ?>
		
	    <?php mella_product_video(); ?>

		<div class="slick-carousel apus-woocommerce-product-gallery" data-carousel="slick" data-items="1" data-smallmedium="1" data-extrasmall="1" data-pagination="false" data-nav="false" data-slickparent="true">
			<?php
			
			if ( has_post_thumbnail() ) {
				$html  = wc_get_gallery_image_html( $post_thumbnail_id, true );
			} else {
				$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
				$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_attr__( 'Awaiting product image', 'mella' ) );
				$html .= '</div>';
			}

			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id );

			do_action( 'woocommerce_product_thumbnails' );
			?>
		</div>
	</div>
	<?php if ( $attachment_ids && has_post_thumbnail() ) { ?>
		<div class="wrapper-thumbs">
			<div class="slick-carousel apus-woocommerce-product-gallery-thumbs <?php echo esc_attr($thumbs_pos == 'thumbnails-left' || $thumbs_pos == 'thumbnails-right' ? 'vertical' : ''); ?>" data-carousel="slick" data-items="<?php echo esc_attr($nb_columns); ?>" data-smallmedium="<?php echo esc_attr($nb_columns); ?>" data-extrasmall="3" data-smallest="<?php echo esc_attr($nb_columns); ?>" data-pagination="false" data-nav="true" data-asnavfor=".apus-woocommerce-product-gallery" data-slidestoscroll="1" data-focusonselect="true" <?php echo trim($thumbs_pos == 'thumbnails-left' || $thumbs_pos == 'thumbnails-right' ? 'data-vertical="true"' : ''); ?>>
				<?php

				if ( has_post_thumbnail() ) {
					$html  = '<div class="woocommerce-product-gallery__image"><div class="thumbs-inner">';
					$html .= get_the_post_thumbnail( $post->ID, 'woocommerce_gallery_thumbnail' );
					$html .= '</div></div>';
				} else {
					$html  = '<div class="woocommerce-product-gallery__image--placeholder"><div class="thumbs-inner">';
					$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_attr__( 'Awaiting product image', 'mella' ) );
					$html .= '</div></div>';
				}

				echo apply_filters( 'mella_woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id );

				
				foreach ( $attachment_ids as $attachment_id ) {
					$html  = '<div class="woocommerce-product-gallery__image"><div class="thumbs-inner">';
					$html .= wp_get_attachment_image( $attachment_id, 'woocommerce_gallery_thumbnail', false );
			 		$html .= '</div></div>';

					echo apply_filters( 'mella_woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
				}

				?>
			</div>
		</div>
	<?php } ?>
<?php } elseif ( $layout == 'layout-image-1-column' ) { ?>
	<div class="apus-woocommerce-product-gallery-wrapper clearfix">
		<?php if ( $product->is_on_sale() ) : ?>
			<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'mella' ) . '</span>', $post, $product ); ?>
		<?php endif; ?>
	    <?php mella_product_video(); ?>

		<div class="apus-woocommerce-product-gallery">
			<?php
			
			if ( has_post_thumbnail() ) {
				$html  = wc_get_gallery_image_html( $post_thumbnail_id, true );
			} else {
				$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
				$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_attr__( 'Awaiting product image', 'mella' ) );
				$html .= '</div>';
			}

			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id );

			do_action( 'woocommerce_product_thumbnails' );
			?>
		</div>
	</div>
<?php } elseif ( $layout == 'layout-center' ) {
	$thumb_layout = mella_get_config('product_thumbs_layout-center_version', 'carousel');
	$nb_images = mella_get_config('number_product_images_per_row', '3');
	if ( $thumb_layout == 'carousel' ) {
?>
		<div class="apus-woocommerce-product-gallery-wrapper">
			<?php if ( $product->is_on_sale() ) : ?>
				<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'mella' ) . '</span>', $post, $product ); ?>
			<?php endif; ?>
		    <?php mella_product_video(); ?>

			<div class="slick-carousel apus-woocommerce-product-gallery" data-carousel="slick" data-items="<?php echo esc_attr($nb_images); ?>" data-smallmedium="<?php echo esc_attr($nb_images); ?>" data-extrasmall="1" data-pagination="false" data-nav="true" data-slickparent="true" <?php echo trim($count_thumbs > $nb_images ? 'data-infinite="true"' : ''); ?>>
				<?php
					$ids = array();
					if ( $post_thumbnail_id ) {
						$ids = array($post_thumbnail_id);
					}
					if ( $attachment_ids ) {
						$ids = array_merge($ids, $attachment_ids);
					}

					$i=0; foreach ( $ids as $attachment_id ) {
						$full_size_image = wp_get_attachment_image_src( $attachment_id, $thumbnail_size );
						$thumbnail       = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );
						$attributes      = array(
							'title'                   => get_post_field( 'post_title', $attachment_id ),
							'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
							'data-src'                => $full_size_image[0],
							'data-large_image'        => $full_size_image[0],
							'data-large_image_width'  => $full_size_image[1],
							'data-large_image_height' => $full_size_image[2],
						);
						$thumb_size = 'mella-shop-normal1';
						$html  = '<div data-thumb="' . esc_url( $thumbnail[0] ) . '" class="woocommerce-product-gallery__image"><a href="' . esc_url( $full_size_image[0] ) . '">';
						$html .= wp_get_attachment_image( $attachment_id, $thumb_size, false, $attributes );
				 		$html .= '</a></div>';

						echo trim($html);

					$i++; }
					?>
			</div>
		</div>

	<?php } else { ?>
		<div class="apus-woocommerce-product-gallery-wrapper <?php echo esc_attr($thumb_layout); ?>">
			<?php if ( $product->is_on_sale() ) : ?>
				<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'mella' ) . '</span>', $post, $product ); ?>
			<?php endif; ?>
		    <?php mella_product_video(); ?>

			<div class="row apus-woocommerce-product-gallery st_gallery">
				<?php
				$ids = array();
				if ( $post_thumbnail_id ) {
					$ids = array($post_thumbnail_id);
				}
				if ( $attachment_ids ) {
					$ids = array_merge($ids, $attachment_ids);
				}

				$i=0; foreach ( $ids as $attachment_id ) {
					$full_size_image = wp_get_attachment_image_src( $attachment_id, $thumbnail_size );
					$thumbnail       = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );
					$attributes      = array(
						'title'                   => get_post_field( 'post_title', $attachment_id ),
						'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
						'data-src'                => $full_size_image[0],
						'data-large_image'        => $full_size_image[0],
						'data-large_image_width'  => $full_size_image[1],
						'data-large_image_height' => $full_size_image[2],
					);
					if ( $i%4 == 0 ) {
						$class = 'col-sm-6';
						$thumb_size = 'mella-shop-normal1';
					} elseif ( $i%4 == 1 ) {
						$class = 'col-sm-6';
						$thumb_size = 'mella-single-horizontal';
					} else {
						$class = 'col-sm-3';
						$thumb_size = 'mella-shop-normal1';
					}
					$html  = '<div class="'.esc_attr($class).'">';
					$html  .= '<div data-thumb="' . esc_url( $thumbnail[0] ) . '" class="woocommerce-product-gallery__image"><a href="' . esc_url( $full_size_image[0] ) . '">';
					$html .= wp_get_attachment_image( $attachment_id, $thumb_size, false, $attributes );
			 		$html .= '</a></div>';
			 		$html .= '</div>';

					echo trim($html);

				$i++; }
				?>

			</div>
		</div>
	<?php } ?>
<?php } ?>
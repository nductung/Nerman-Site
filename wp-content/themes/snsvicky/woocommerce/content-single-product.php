<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<?php
$have_three_col = 0; $col1_class = ' col-xs-6 col-phone-12'; $col2_class = ' col-xs-6 col-phone-12'; $col3_class = '';
if( snsvicky_getoption('single_product_sidebar') != 1 && snsvicky_getoption('woo_gallery_type', 'h') == 'h' && snsvicky_getoption('primary_block_three_col') !='' ){
	$have_three_col = 1;
	$col1_class = ' col-md-5 col-xs-6 col-phone-12'; $col2_class = ' col-md-4 col-xs-6 col-phone-12'; $col3_class = ' col-md-3';
}
?>
<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="second_block row<?php echo ($have_three_col==1)?" three-cols":""; ?> clearfix gallery_type_<?php echo esc_attr(snsvicky_getoption('woo_gallery_type', 'h')); ?>">
		<div class="col-sm-12">
			<div class="primary_block row clearfix">
				<div class="entry-img<?php echo $col1_class; ?>">
					<div class="inner">
	<?php
		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>
					</div>
				</div>
				<div class="summary entry-summary<?php echo $col2_class; ?>">

		<?php
			/**
			 * woocommerce_single_product_summary hook.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 */
			do_action( 'woocommerce_single_product_summary' );
		?>
				</div><!-- .summary -->
				<?php
				if ($have_three_col == 1) :?>
				<div class="inner-sidebar<?php echo $col3_class;?>">
					<?php echo do_shortcode('[snsvicky_postwcode name="'.esc_attr(snsvicky_getoption('primary_block_three_col', 'product-inner-sidebar')).'"]'); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		// Upsells Product
	    remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	    if (snsvicky_themeoption('woo_upsells') == 1 && snsvicky_layouttype('layouttype', 'm') == 'm' ) {
	        add_action( 'woocommerce_after_single_product_summary', 'snsvicky_woo_output_upsells', 15 );
	    }
	    if ( ! function_exists( 'snsvicky_woo_output_upsells' ) ) {
	        function snsvicky_woo_output_upsells() {
	            woocommerce_upsell_display( snsvicky_themeoption('woo_upsells_num', 6), 1 ); // Display n products in rows of 1
	        }
	    }
	    // Related Product
	    if (snsvicky_themeoption('woo_related') != 1){
	        remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
	    }else{
	        add_filter( 'woocommerce_output_related_products_args', 'snsvicky_woo_output_related' );
	          function snsvicky_woo_output_related( $args ) {
	            $args['posts_per_page'] = snsvicky_themeoption('woo_related_num', 6); // n related products
	            $args['columns'] = 1; // arranged in 1 columns
	            return $args;
	        }
	    }
		do_action( 'woocommerce_after_single_product_summary' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
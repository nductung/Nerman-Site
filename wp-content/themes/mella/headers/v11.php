<header id="apus-header" class="apus-header header-v11 hidden-sm hidden-xs hidden-md" role="banner">
    <div class="header-left">
        <?php if ( has_nav_menu( 'primary' ) ) { ?>
            <div class="show-menu">
                <a class="btn-main-menu" href="#main-menu">
                    <span class="icon-show-menu">
                        <span class="line1"></span>
                        <span class="line2"></span>
                        <span class="line3"></span>
                    </span>
                </a>
            </div>
        <?php } ?>
    </div>
    <div class="header-top">
        <div class="inner-header">
            <div class="logo-in-theme text-center">
                <?php get_template_part( 'template-parts/logo/logo' ); ?>
            </div>
        </div>
    </div>
    <div class="header-right-warpper cart-horizontal st_right">
        <div class="header-right clearfix">
            <?php if ( mella_get_config('show_user_info', true) ) { ?>
                <div class="clearfix">
                    <div class="top-wrapper-menu">
                        <a class="drop-dow"><i class="icon-user-1"></i></a>
                        <?php if( is_user_logged_in()){ ?>
                            <?php if ( has_nav_menu( 'top-menu' ) ) {
                                    $args = array(
                                        'theme_location' => 'top-menu',
                                        'container_class' => 'inner-top-menu',
                                        'menu_class' => 'nav navbar-nav topmenu-menu',
                                        'fallback_cb' => '',
                                        'menu_id' => '',
                                        'walker' => new Mella_Nav_Menu()
                                    );
                                    wp_nav_menu($args);
                                }
                            ?>
                        <?php } else { ?>
                            <?php get_template_part( 'template-parts/login' ); ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <div class="clearfix">
            <?php if ( mella_is_woocommerce_activated() && mella_get_config('show_cartbtn') && !mella_get_config( 'enable_shop_catalog' ) ): ?>
                
                    <?php get_template_part( 'woocommerce/cart/mini-cart-button' ); ?>
                
            <?php endif; ?>
            </div>
            <div class="clearfix">
            <?php if ( class_exists( 'YITH_WCWL' ) && mella_get_config('show_wishlist', true) ):
                $wishlist_url = YITH_WCWL()->get_wishlist_url();
            ?>
                
                    <a class="wishlist-icon" href="<?php echo esc_url($wishlist_url);?>" title="<?php esc_attr_e( 'View Your Wishlist', 'mella' ); ?>"><i class="icon-heart3"></i>
                        <?php if ( function_exists('yith_wcwl_count_products') ) { ?>
                            <span class="count"><?php echo yith_wcwl_count_products(); ?></span>
                        <?php } ?>
                    </a>
                
            <?php endif; ?>
            </div>
            <div class="clearfix">
            <?php if ( mella_get_config('show_searchform') ){ ?>
                
                    <a class="btn-search-top"><i class="icon-magnifying-glass"></i></a>
                
            <?php } ?>
            </div>
        </div>
    </div>

    <?php if ( has_nav_menu( 'primary-sidebar' ) ) : ?>
        <div class="site-header-mainmenu st-brown">
            <div class="main-menu">
                <nav data-duration="400" class="hidden-xs hidden-sm apus-megamenu slide animate navbar p-static" role="navigation">
                <?php   $args = array(
                        'theme_location' => 'primary-sidebar',
                        'container_class' => 'collapse navbar-collapse no-padding',
                        'menu_class' => 'nav navbar-nav megamenu',
                        'fallback_cb' => '',
                        'menu_id' => 'primary-menu',
                        'walker' => new Mella_Nav_Menu()
                    );
                    wp_nav_menu($args);
                ?>
                </nav>
            </div>
        </div>
    <?php endif; ?>
</header>
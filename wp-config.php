<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'nerman' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
//define( 'DB_PASSWORD', '/RyXen2f@:fkh9"5' );
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
//define( 'DB_HOST', '54.179.126.81' );
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

//define('WP_HOME','https://nerman.com.vn');
//define('WP_SITEURL','https://nerman.com.vn');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0

 */
define( 'AUTH_KEY',         '0a-EEq&,s3)qfo&Ks!RWxG@:w%(mm4?~)[5<;vc1y,r#m-GlbOZ[5>)bvLt:|-&f' );
define( 'SECURE_AUTH_KEY',  'Ht%/8[4VIp7yzr][*0T/8x-V5~K:FIM[`>d~,aS6]KPX&S7HB/TJ@k`g|@3?*J<O' );
define( 'LOGGED_IN_KEY',    'CPU=$fEGhz/y*(j`agEjd 0$==p|lAaR^nxqY`M@={D>,j7YS1R1TV@<mdo;&B|%' );
define( 'NONCE_KEY',        'w`xOE%GyVto/l@J~.kk#:0ker7k0.#_)0aJ+]@LK<^v9^d`<I yBX0bn| yIx&1=' );
define( 'AUTH_SALT',        'cs|Nim7:I0csj/ONH]- wrc qWqL;0I;IM:dKUD}=uTBF>!=D<_L.sqi7H&,*,D%' );
define( 'SECURE_AUTH_SALT', 'OC#Q6vE$ag;55&d)?/Ff]qOPpaNOh~432_bR9uriaoJ~+}ONG^/ZPJ{W<nN^68 t' );
define( 'LOGGED_IN_SALT',   'QmjgUuR_usk~P3$ed59ePW+@qn2bE!nN <9d+DRhA)i|4[d}8z!dVIw4Cp6f8opz' );
define( 'NONCE_SALT',       ']1^z*idx{[4k?y]2~hFiBV<voxMoe=-uq0c/vef%?e{IE?Yqy]Od>u>@PTgOgEVo' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/*** FTP login settings ***/
define("FTP_HOST", "localhost");
define("FTP_USER", "ftp-user");
define("FTP_PASS", "ftp-password");
define('FS_METHOD', 'direct');
define('WP_LOAD_IMPORTERS', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

//define('FORCE_SSL_ADMIN', true);
define('FORCE_SSL_LOGIN', true);
//define('WP_CACHE', true); 
//define('WP_HOME','https://nerman.com.vn');
//define('WP_SITEURL','https://nerman.com.vn');

/*
if ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && 'https' == $_SERVER['HTTP_X_FORWARDED_PROTO'] ) {
        $_SERVER['HTTPS'] = 'on';
    }
*/

if (
    isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && 
    $_SERVER["HTTP_X_FORWARDED_PROTO"] == "https"
) {
    $_SERVER["HTTPS"] = "on";
}

<div id="sns_respmenu" class="menu-offcanvas hidden-lg hidden-md">
	<span class="btn2 btn-navbar leftsidebar">
	    <span class="overlay"></span>
	</span>
	<span class="btn2 btn-navbar offcanvas">
	    <span class="overlay"></span>
	</span>
	<span class="btn2 btn-navbar rightsidebar">
	    <span class="overlay"></span>
	</span>
	
	<div id="menu_offcanvas" class="offcanvas">
		<?php
		$main_menu = '';
		if(is_page() && ($menu_selected = get_post_meta(get_the_ID(), 'snsvicky_main_menu', true))){
			$main_menu = $menu_selected;
		}
		
        if(has_nav_menu('main_navigation')):
           wp_nav_menu( array(
           				'theme_location' => 'main_navigation',
           				'container' => false,
           				'menu'		=> $main_menu,
           				'menu_id' => 'res_main_nav',
           				'menu_class' => 'resp-nav'
           	) ); 
		else:
			esc_html_e('Please sellect menu for Main navigation', 'snsvicky');
		endif;
		?>
	</div>
</div>
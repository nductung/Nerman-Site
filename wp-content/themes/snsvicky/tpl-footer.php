<?php 
	$footer_layout = snsvicky_getoption('footer_layout', 'style_1'); ?>	
	<div id="sns_footer" class="sns-footer <?php echo $footer_layout; ?>">
		<div class="container">
	<?php
	$default_html = '<div class="row"><div class="col-md-12 copyright">'.esc_html__('© 2017 Vicky Online. WordPress Themes by SNSTheme', 'snsvicky').'</div></div>';
	if ( !is_404() ) {
		switch ($footer_layout):
			case 'style_2':
				if ( function_exists('dynamic_sidebar') && is_active_sidebar('Footer Box #2') ) {
					dynamic_sidebar('Footer Box #2');
				}else{
					echo $default_html;
				}
				break;
			case 'style_3':
				if ( function_exists('dynamic_sidebar') && is_active_sidebar('Footer Box #3') ) {
					dynamic_sidebar('Footer Box #3');
				}else{
					echo $default_html;
				}
				break;
			case 'style_4':
				if ( function_exists('dynamic_sidebar') && is_active_sidebar('Footer Box #4') ) {
					dynamic_sidebar('Footer Box #4');
				}else{
					echo $default_html;
				}
				break;
			case 'style_5':
				if ( function_exists('dynamic_sidebar') && is_active_sidebar('Footer Box #5') ) {
					dynamic_sidebar('Footer Box #5');
				}else{
					echo $default_html;
				}
				break;
			case 'style_1':
				if ( function_exists('dynamic_sidebar') && is_active_sidebar('Footer Box #1') ) {
					dynamic_sidebar('Footer Box #1');
				}else{
					echo $default_html;
				}
				break;
			default:
				echo $default_html;
		endswitch;
	}else{
		echo $default_html;
	}
	?>
		</div>
	</div>
	<?php
	$advance_scrolltotop = snsvicky_themeoption('advance_scrolltotop', 1);
	$advance_cpanel = snsvicky_themeoption('advance_cpanel', 0);
	if ( $advance_scrolltotop == 1 || $advance_cpanel == 1 ) : ?>
	<div id="sns_tools">
		<?php 
		if ( $advance_scrolltotop == 1 ) : ?>
		<div class="sns-croll-to-top">
			<a href="#" id="sns-totop"></a>
		</div>
		<?php
		endif;
		if ( $advance_cpanel == 1 ) : 
			get_template_part( 'tpl-cpanel');
		endif;
		?>
	</div>
	<?php endif; ?>
</div>
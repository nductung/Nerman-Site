<?php
if ( function_exists('vc_map') && class_exists('WPBakeryShortCode') ) {
	
	if ( !function_exists('mella_vc_get_product_object')) {
		function mella_vc_get_product_object($term) {
			$vc_taxonomies_types = vc_taxonomies_types();

			return array(
				'label' => $term->post_title,
				'value' => $term->post_name,
				'group_id' => $term->post_name,
				'group' => isset( $vc_taxonomies_types[ $term->taxonomy ], $vc_taxonomies_types[ $term->taxonomy ]->labels, $vc_taxonomies_types[ $term->taxonomy ]->labels->name ) ? $vc_taxonomies_types[ $term->taxonomy ]->labels->name : esc_html__( 'Taxonomies', 'mella' ),
			);
		}
	}

	if ( !function_exists('mella_product_field_search')) {
		function mella_product_field_search( $search_string ) {
			$data = array();
			$loop = mella_get_products( array( 'product_type' => 'deals' ) );

			if ( !empty($loop->posts) ) {

				foreach ( $loop->posts as $t ) {
					if ( is_object( $t ) ) {
						$data[] = mella_vc_get_product_object( $t );
					}
				}
			}
			
			return $data;
		}
	}

	if ( !function_exists('mella_product_render')) {
		function mella_product_render( $query ) {
			$args = array(
			  'name'        => $query['value'],
			  'post_type'   => 'product',
			  'post_status' => 'publish',
			  'numberposts' => 1
			);
			$products = get_posts($args);
			if ( ! empty( $query ) && $products ) {
				$product = $products[0];
				$data = array();
				$data['value'] = $product->post_name;
				$data['label'] = $product->post_title;
				return ! empty( $data ) ? $data : false;
			}
			return false;
		}
	}
	add_filter( 'vc_autocomplete_apus_product_deal_product_slugs_callback', 'mella_product_field_search', 10, 1 );
	add_filter( 'vc_autocomplete_apus_product_deal_product_slugs_render', 'mella_product_render', 10, 1 );
	
	if ( !function_exists('mella_vc_get_term_object')) {
		function mella_vc_get_term_object($term) {
			$vc_taxonomies_types = vc_taxonomies_types();

			return array(
				'label' => $term->name,
				'value' => $term->slug,
				'group_id' => $term->taxonomy,
				'group' => isset( $vc_taxonomies_types[ $term->taxonomy ], $vc_taxonomies_types[ $term->taxonomy ]->labels, $vc_taxonomies_types[ $term->taxonomy ]->labels->name ) ? $vc_taxonomies_types[ $term->taxonomy ]->labels->name : esc_html__( 'Taxonomies', 'mella' ),
			);
		}
	}

	if ( !function_exists('mella_category_field_search')) {
		function mella_category_field_search( $search_string ) {
			$data = array();
			$vc_taxonomies_types = array('product_cat');
			$vc_taxonomies = get_terms( $vc_taxonomies_types, array(
				'hide_empty' => false,
				'search' => $search_string
			) );
			if ( is_array( $vc_taxonomies ) && ! empty( $vc_taxonomies ) ) {
				foreach ( $vc_taxonomies as $t ) {
					if ( is_object( $t ) ) {
						$data[] = mella_vc_get_term_object( $t );
					}
				}
			}
			return $data;
		}
	}

	if ( !function_exists('mella_category_render')) {
		function mella_category_render($query) {  
			$category = get_term_by('slug', $query['value'], 'product_cat');
			if ( ! empty( $query ) && !empty($category)) {
				$data = array();
				$data['value'] = $category->slug;
				$data['label'] = $category->name;
				return ! empty( $data ) ? $data : false;
			}
			return false;
		}
	}

	$bases = array( 'apus_products' );
	foreach( $bases as $base ){   
		add_filter( 'vc_autocomplete_'.$base .'_categories_callback', 'mella_category_field_search', 10, 1 );
	 	add_filter( 'vc_autocomplete_'.$base .'_categories_render', 'mella_category_render', 10, 1 );
	}

	function mella_load_woocommerce_element() {
		$categories = array();
		if ( is_admin() ) {
			$categories = mella_woocommerce_get_categories();
		}
		$types = array(
			esc_html__('Recent Products', 'mella' ) => 'recent_product',
			esc_html__('Best Selling', 'mella' ) => 'best_selling',
			esc_html__('Featured Products', 'mella' ) => 'featured_product',
			esc_html__('Top Rate', 'mella' ) => 'top_rate',
			esc_html__('On Sale', 'mella' ) => 'on_sale',
			esc_html__('Recent Review', 'mella' ) => 'recent_review'
		);

		vc_map( array(
			'name' => esc_html__( 'Apus Products', 'mella' ),
			'base' => 'apus_products',
			'icon' => 'icon-wpb-woocommerce',
			'category' => esc_html__( 'Apus Woocommerce', 'mella' ),
			'description' => esc_html__( 'Display products in frontend', 'mella' ),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Title', 'mella' ),
					'param_name' => 'title',
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Subtitle', 'mella' ),
					'param_name' => 'subtitle',
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Get Products By", 'mella'),
					"param_name" => "type",
					"value" => $types,
				),
				array(
				    'type' => 'autocomplete',
				    'heading' => esc_html__( 'Get Products By Categories', 'mella' ),
				    'param_name' => 'categories',
				    'settings' => array(
				     	'multiple' => true,
				     	'unique_values' => true
				    ),
			   	),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Number Products', 'mella' ),
					'value' => 10,
					'param_name' => 'number',
					'description' => esc_html__( 'Number products per page to show', 'mella' ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( 'Layout Type', 'mella' ),
					"param_name" => "layout_type",
					"value" => array(
						esc_html__( 'Grid', 'mella' ) => 'grid',
						esc_html__( 'Carousel', 'mella' ) => 'carousel',
						esc_html__( 'Mansory V1', 'mella' ) => 'mansory-v1',
						esc_html__( 'Mansory V2', 'mella' ) => 'mansory-v2',
					)
				),
				array(
	                "type" => "dropdown",
	                "heading" => esc_html__('Columns', 'mella'),
	                "param_name" => 'columns',
	                "value" => array(1,2,3,4,5,6),
	                'dependency' => array(
						'element' => 'layout_type',
						'value' => array('carousel', 'grid'),
					),
	            ),
	            array(
	                "type" => "dropdown",
	                "heading" => esc_html__('Rows', 'mella'),
	                "param_name" => 'rows',
	                "value" => array(1,2,3,4,5),
	                'dependency' => array(
						'element' => 'layout_type',
						'value' => array('carousel'),
					),
	            ),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Show Navigation', 'mella' ),
					'param_name' => 'show_nav',
					'value' => array( esc_html__( 'Yes, to show navigation', 'mella' ) => 'yes' ),
					'dependency' => array(
						'element' => 'layout_type',
						'value' => array('carousel'),
					),
				),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Show Pagination', 'mella' ),
					'param_name' => 'show_pagination',
					'value' => array( esc_html__( 'Yes, to show Pagination', 'mella' ) => 'yes' ),
					'dependency' => array(
						'element' => 'layout_type',
						'value' => array('carousel'),
					),
				),
	            array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name",'mella'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'mella')
				)
			)
		) );

		vc_map( array(
			'name' => esc_html__( 'Apus Products Tabs', 'mella' ),
			'base' => 'apus_products_tabs',
			'icon' => 'icon-wpb-woocommerce',
			'category' => esc_html__( 'Apus Woocommerce', 'mella' ),
			'description' => esc_html__( 'Display products in Tabs', 'mella' ),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Widget Title', 'mella' ),
					'param_name' => 'title',
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Subtitle', 'mella' ),
					'param_name' => 'subtitle',
				),
				array(
					'type' => 'param_group',
					'heading' => esc_html__( 'Product Tabs', 'mella' ),
					'param_name' => 'tabs',
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Tab Title', 'mella' ),
							'param_name' => 'title',
						),
						array(
							"type" => "dropdown",
							"heading" => esc_html__("Get Products By",'mella'),
							"param_name" => "type",
							"value" => $types,
						),
						array(
							"type" => "dropdown",
							"heading" => esc_html__( 'Category', 'mella' ),
							"param_name" => "category",
							"value" => $categories
						),
					)
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Number Products', 'mella' ),
					'value' => 10,
					'param_name' => 'number',
					'description' => esc_html__( 'Number products per page to show', 'mella' ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( 'Layout Type', 'mella' ),
					"param_name" => "layout_type",
					"value" => array(
						esc_html__( 'Grid', 'mella' ) => 'grid',
						esc_html__( 'Carousel', 'mella' ) => 'carousel',
					)
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( 'Style', 'mella' ),
					"param_name" => "style",
					"value" => array(
						esc_html__( 'Normal', 'mella' ) => '',
						esc_html__( 'Center', 'mella' ) => 'st_center',
						esc_html__( 'Background Transparent', 'mella' ) => 'st_bg',
					)
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Link all Products', 'mella' ),
					'value' => '',
					'param_name' => 'linkall',
					'description' => esc_html__( 'Link all Products', 'mella' ),
				),
				array(
	                "type" => "dropdown",
	                "heading" => esc_html__('Columns', 'mella'),
	                "param_name" => 'columns',
	                "value" => array(1,2,3,4,5,6),
	            ),
	            array(
	                "type" => "dropdown",
	                "heading" => esc_html__('Rows', 'mella'),
	                "param_name" => 'rows',
	                "value" => array(1,2,3,4),
	                'dependency' => array(
						'element' => 'layout_type',
						'value' => array('carousel'),
					),
	            ),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Show Navigation', 'mella' ),
					'param_name' => 'show_nav',
					'value' => array( esc_html__( 'Yes, to show navigation', 'mella' ) => 'yes' ),
					'dependency' => array(
						'element' => 'layout_type',
						'value' => array('carousel'),
					),
				),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Show Pagination', 'mella' ),
					'param_name' => 'show_pagination',
					'value' => array( esc_html__( 'Yes, to show Pagination', 'mella' ) => 'yes' ),
					'dependency' => array(
						'element' => 'layout_type',
						'value' => array('carousel'),
					),
				),
	            array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name",'mella'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'mella')
				)
			)
		) );

		vc_map( array(
			'name' => esc_html__( 'Apus Category Banners', 'mella' ),
			'base' => 'apus_category_banner',
			'icon' => 'icon-wpb-woocommerce',
			'category' => esc_html__( 'Apus Woocommerce', 'mella' ),
			'description' => esc_html__( 'Display category banner', 'mella' ),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Description', 'mella' ),
					'param_name' => 'des',
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Title', 'mella' ),
					'param_name' => 'title',
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__( 'Category', 'mella' ),
					"param_name" => "category",
					"value" => $categories
				),
				array(
					"type" => "attach_image",
					"heading" => esc_html__("Category Image", 'mella'),
					"param_name" => "image"
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'mella'),
					"param_name" => "style",
					'value' 	=> array(
						esc_html__('Style 1', 'mella') => 'style1',
						esc_html__('Style 2 (left)', 'mella') => 'style2 st_left',
						esc_html__('Style 3 (right)', 'mella') => 'style2 st_right',
						esc_html__('Style 4 (Center)', 'mella') => 'style4',
					),
				),
	            array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'mella'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
				)
			)
		) );

		vc_map( array(
			'name' => esc_html__( 'Apus Category Slider', 'mella' ),
			'base' => 'apus_category_slider',
			'icon' => 'icon-wpb-woocommerce',
			'category' => esc_html__( 'Apus Woocommerce', 'mella' ),
			'description' => esc_html__( 'Display category banner slider', 'mella' ),
			'params' => array(
				array(
					'type' => 'param_group',
					'heading' => esc_html__( 'Categories', 'mella' ),
					'param_name' => 'categories',
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Title', 'mella' ),
							'param_name' => 'title',
						),
						array(
							"type" => "dropdown",
							"heading" => esc_html__( 'Category', 'mella' ),
							"param_name" => "category",
							"value" => $categories
						),
						array(
							"type" => "attach_image",
							"heading" => esc_html__("Category Image", 'mella'),
							"param_name" => "image"
						),
					)
				),
				array(
	                "type" => "dropdown",
	                "heading" => esc_html__('Columns', 'mella'),
	                "param_name" => 'columns',
	                "value" => array(1,2,3,4,5,6),
	            ),
	            array(
					"type" => "dropdown",
					"heading" => esc_html__( 'Layout Type', 'mella' ),
					"param_name" => "layout_type",
					"value" => array(
						esc_html__( 'Carousel', 'mella' ) => 'carousel',
						esc_html__( 'Grid', 'mella' ) => 'grid',
					)
				),
	            array(
	                "type" => "dropdown",
	                "heading" => esc_html__('Rows', 'mella'),
	                "param_name" => 'rows',
	                "value" => array(1,2,3,4),
	                'dependency' => array(
						'element' => 'layout_type',
						'value' => array('carousel'),
					),
	            ),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Show Navigation', 'mella' ),
					'param_name' => 'show_nav',
					'value' => array( esc_html__( 'Yes, to show navigation', 'mella' ) => 'yes' ),
					'dependency' => array(
						'element' => 'layout_type',
						'value' => array('carousel'),
					),
				),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Show Pagination', 'mella' ),
					'param_name' => 'show_pagination',
					'value' => array( esc_html__( 'Yes, to show Pagination', 'mella' ) => 'yes' ),
					'dependency' => array(
						'element' => 'layout_type',
						'value' => array('carousel'),
					),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'mella'),
					"param_name" => "style",
					'value' 	=> array(
						esc_html__('Style 1', 'mella') => 'style1',
						esc_html__('Style 2', 'mella') => 'style2',
					),
				),
	            array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'mella'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'mella')
				)
			)
		) );
	}
	add_action( 'vc_after_set_mode', 'mella_load_woocommerce_element', 99 );

	class WPBakeryShortCode_apus_products extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_products_tabs extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_category_banner extends WPBakeryShortCode {}
	class WPBakeryShortCode_apus_category_slider extends WPBakeryShortCode {}
}
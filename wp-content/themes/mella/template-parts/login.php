<div class="header_customer_login">
	<form method="post" class="login" role="form">
		<?php do_action( 'woocommerce_login_form_start' ); ?>
		<p class="form-group form-row form-row-wide">
			<input type="text" placeholder="<?php esc_attr_e( 'Username or email', 'mella' ); ?>" class="input-text form-control" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
		</p>
		<p class="form-row-wide">
			<input class="input-text form-control" placeholder="<?php esc_attr_e( 'Password', 'mella' ); ?>" type="password" name="password" id="password" />
		</p>

		<?php do_action( 'woocommerce_login_form' ); ?>
		<div class="space-10">
			<span class="inline">
				<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php esc_html_e( 'Remember me', 'mella' ); ?>
			</span>
		</div>
		<div class="form-group form-row">
			<?php wp_nonce_field( 'woocommerce-login' ); ?>
			
			<input type="submit" class="btn btn-theme btn-block btn-sm" name="login" value="<?php esc_attr_e( 'sign in', 'mella' ); ?>" />
		</div>

		<?php do_action( 'woocommerce_login_form_end' ); ?>

		<div class="clearfix">
			<ul class="topmenu-menu">
				<li class="lost_password">
					<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><i class="fa fa-repeat"></i> <?php esc_html_e( 'Lost your password?', 'mella' ); ?></a>
				</li>
				<li class="register">
					<a class="register" href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>?ac=register" title="<?php esc_attr_e('Register','mella'); ?>"><i class="fa fa-user-plus"></i><?php esc_html_e('Register', 'mella'); ?></a>
				</li>
				<li class="lost_password">
					<a href="<?php echo get_permalink( wc_get_page_id( 'checkout' ) ); ?>"><i class="fa fa-shopping-basket" aria-hidden="true"></i><?php esc_html_e( 'Checkout', 'mella' ); ?></a>
				</li>
			</ul>
		</div>
	</form>
</div>
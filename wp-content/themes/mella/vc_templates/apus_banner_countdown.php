<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$time = strtotime( $input_datetime );
?>
<div class="widget banner-countdown-widget <?php echo esc_attr($el_class.' '.$style_widget); ?>">
    <?php if ($title!=''): ?>
        <h3 class="widget-title">
            <?php echo trim( $title ); ?>
        </h3>
    <?php endif; ?>
    <?php if ($subtitle!=''): ?>
        <div class="sub-title">
            <?php echo trim( $subtitle ); ?>
        </div>
    <?php endif; ?>
    <?php if (wpb_js_remove_wpautop( $content, true )): ?>
        <div class="price text-theme">
            <?php echo wpb_js_remove_wpautop( $content, true ); ?>
        </div>
    <?php endif; ?>
    <?php if ($des!=''): ?>
        <div class="des">
            <?php echo trim( $des ); ?>
        </div>
    <?php endif; ?>
	<div class="countdown-wrapper">
	    <div class="apus-countdown" data-time="timmer"
	         data-date="<?php echo date('m',$time).'-'.date('d',$time).'-'.date('Y',$time).'-'. date('H',$time) . '-' . date('i',$time) . '-' .  date('s',$time) ; ?>">
	    </div>
	</div>
	<?php if ( !empty($btn_url)) { ?>
        <div class="more">
	       <a class="btn-readmore" href="<?php echo esc_attr($btn_url); ?>" ><?php echo esc_html__('SHOP NOW','mella'); ?></a>
        </div>
    <?php } ?>
</div>
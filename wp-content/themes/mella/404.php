<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Mella
 * @since Mella 1.0
 */
/*
*Template Name: 404 Page
*/
get_header();
$icon = mella_get_config('icon-img');
$bg_img = mella_get_config('bg-img');
if( !empty($bg_img) && !empty($bg_img['url'])) {
	$style_bg= 'style="background-image:url('.esc_url_raw( $bg_img['url']).') " ';
}else{
	$style_bg= 'style="background-image:url('.esc_url_raw( get_template_directory_uri().'/images/bg-error.jpg').') " ';
}
?>
<section class="page-404" <?php echo trim($style_bg); ?>>
	<div id="main-container" class="inner">
		<div id="main-content" class="main-page">
			<section class="error-404 not-found clearfix">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-xs-12">	
							<?php if( !empty($icon) && !empty($icon['url'])) { ?>
								<img src="<?php echo esc_url( $icon['url']); ?>" alt="<?php echo esc_attr(get_bloginfo( 'name' )); ?>">
							<?php }else{ ?>
								<img src="<?php echo esc_url_raw( get_template_directory_uri().'/images/error.png'); ?>" alt="<?php echo esc_attr(get_bloginfo( 'name' )); ?>">
							<?php } ?>
							<div class="slogan">
								<?php if(!empty(mella_get_config('404_title', '404')) ) { ?>
									<h4 class="title-big"><?php echo mella_get_config('404_title', 'This page not be found'); ?></h4>
								<?php } ?>
							</div>
							<div class="page-content">
								<div class="description">
									<?php echo mella_get_config('404_description', 'We are really sorry, but the page you requested is missing.. Perhaps searching again can help. Or back to home page'); ?>
										<a class="text-theme" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo esc_html__('home page','mella') ?></a>
								</div>
								<div class="search-error">
									<?php get_search_form(); ?>
								</div>
							</div><!-- .page-content -->
						</div>
						<div class="col-lg-7 hidden-sm hidden-md hidden-xs"></div>
					</div>
				</div>
			</section><!-- .error-404 -->
		</div><!-- .content-area -->
	</div>
</section>
<?php get_footer(); ?>
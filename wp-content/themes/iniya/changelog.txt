*** Iniya WordPress Theme Changelog ***

2020.02.01 - version 1.9

* Compatible with wordpress 5.3.2
* Updated: All premium plugins
* Updated: All wordpress theme standards
* Updated: Privacy and Cookies concept
* Updated: Gutenberg editor support for custom post types

* Fixed: Google Analytics issue
* Fixed: Mailchimp email client issue
* Fixed: Privacy Button Issue
* Fixed: Gutenberg check for old wordpress version

* Improved: Tags taxonomy added for portfolio
* Improved: Single product breadcrumb section
* Improved: Revisions options added for all custom posts

2019.11.20 - version 1.8

* Updated WordPress Standards
* Gutenberg Update
* Other Minor Fixes

2019.10.25 - version 1.7
* Compatible with wordpress 5.2.4
* Updated: All WordPress Standards for WP Requirements Compliant
* Updated: Gutenberg editor support fixes

2019.08.22 - version 1.6
* Compatible with wordpress 5.2.2
* Updated: All premium plugins
* Updated: Revisions added to all custom post types
* Updated: Gutenberg editor support for custom post types
* Updated: Link for phone number module
* Updated: Online documentation link, check readme file

* Fixed: Google Analytics issue
* Fixed: Mailchimp email client issue
* Fixed: Gutenberg check for old wordpress version
* Fixed: Edit with Visual Composer for portfolio
* Fixed: Site title color
* Fixed: Privacy popup bg color

* Improved: Single product breadcrumb section
* Improved: Tags taxonomy added for portfolio
* Improved: Woocommerce cart module added with custom class option

* New: Whatsapp Shortcode

2019.05.22 - version 1.5
 * Gutenberg Latest update compatible
 * Portfolio Video option
 * Coming Soon page fix
 * GDPR product single page fix
 * Codestar framework update
 * wpml xml file updated
 * disable options for likes and views in single post page
 * Updated latest version of all third party plugins
 * Image Caption module link
 * RTL Design Fix
 * Some design tweaks

2019.01.19 - version 1.4
 * Gutenberg compatible
 * Latest WordPress version 5.0.3 compatible
 * Updated latest version of all third party plugins
 * Some design tweaks

2018.11.10 - version 1.3
 * Gutenberg plugin compatible
 * Latest wordpress version 4.9.8 compatible
 * Updated latest version of all third party plugins
 * Updated documentation

2018.08.04 - version 1.2
 * GDPR Compliant update in comment form, mailchimp form etc.
 * Compatible with wordpress 4.9.8
 * Google Map Api Key Fix
 * Fix - Bulk plugins install issue
 * Fix - Iphone sidebar issue
 * Fix - Add target attribute for social media
 * Fix - Option for change the site title color
 * Fix - Unyson Page Bilder Conflict
 * Fix - Nav Menu Role Plugin compatible.
 * Fix - Added the smile fonts folder
 * Fix - Woocommerce custom sidebar issue
 * Updated language files
 * Updated all third party plugins
 * Updated designthemes core features plugin
 * Fix - Buddypress issue
 * Fix - youtube and vimeo video issue in https
 * Fix - Twitter feeds links issue

2017.12.01 - version 1.1
 * Optimized Dummy Content Updated

2017.10.07 - version 1.0
 * First release!
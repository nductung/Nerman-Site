<div class="sns-grid-item">
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	    <?php
	    // Post Quote
	    if ( get_post_format() == 'quote' && function_exists('rwmb_meta') && rwmb_meta('snsvicky_post_quotecontent') && rwmb_meta('snsvicky_post_quoteauthor') ) :
	    	$uq  = rand().time();
        	?>
	        <div class="quote-info quote-info-<?php echo $uq; ?>">
	            <?php if ( rwmb_meta('snsvicky_post_quotecontent') ) : ?>
	            <div class="quote-content gfont"><i class="fa fa-quote-left"> </i> <?php echo esc_html(rwmb_meta('snsvicky_post_quotecontent')); ?> <i class="fa fa-quote-right"></i></div>
	            <?php endif; ?>
	             <?php if ( rwmb_meta('snsvicky_post_quoteauthor') ) : ?>
	            <div class="quote-author">- <?php echo esc_html(rwmb_meta('snsvicky_post_quoteauthor')); ?></div>
	            <?php endif; ?>
	        </div>
	    <?php
	    // Post Link
	    elseif ( get_post_format() == 'link' && function_exists('rwmb_meta') && rwmb_meta('snsvicky_post_linkurl') ) : ?>
	        <div class="link-info">
	            <a title="<?php echo esc_attr(rwmb_meta('snsvicky_post_linktitle')) ?>" href="<?php echo esc_url( rwmb_meta('snsvicky_post_linkurl') ) ?>"><?php echo esc_html(rwmb_meta('snsvicky_post_linktitle')) ?></a>
	           
	        </div>
	    <?php
	    // Post Video
	    elseif ( get_post_format() == 'video' && function_exists('rwmb_meta') && rwmb_meta('snsvicky_post_video') ) : ?>
	        <div class="video-thumb video-responsive">
	            <?php
	            echo rwmb_meta('snsvicky_post_video');
	            ?>
	            
	        </div>
	    <?php
	    // Post audio
        elseif ( get_post_format() == 'audio' && function_exists('rwmb_meta') && rwmb_meta('snsvicky_post_audio') ) : ?>
            <div class="audio-thumb audio-responsive">
                <?php
                echo wp_oembed_get(esc_attr(rwmb_meta('snsvicky_post_audio')));
                ?>
            </div>
        <?php
	    // Post Gallery
	    elseif ( get_post_format() == 'gallery' && function_exists('rwmb_meta') && rwmb_meta('snsvicky_post_gallery') ) : 
			wp_enqueue_script('owlcarousel');
	    ?>
	        <div class="gallery-thumb">
	            <div class="thumb-container owl-carousel">
	            <?php
	            foreach (rwmb_meta('snsvicky_post_gallery', 'type=image') as $image) {?>
	               <div class="item"><img alt="<?php echo esc_attr($image['alt']); ?>" src="<?php echo esc_attr($image['full_url']); ?>"/></div>
	            <?php
	            }
	            ?>
	            </div>
	        </div>
	    <?php
	    // Post Image
	    elseif ( has_post_thumbnail() ) : ?>
	        <div class="post-thumb">
	            <?php
	            $blog_type = snsvicky_themeoption('blog_type');
	            $img_size = 'full';
	            switch ($blog_type){
	            	case 'grid-2-col';
	            		$img_size = 'snsvicky_blog_list_thumb';
	            		break;
	            	case 'grid-3-col';
	            		$img_size = 'snsvicky_blog_grid_thumb';
	            		break;
	            	case 'masonry';
	            		$img_size = 'full';
	            		break;
	            	default: // standard post
	            		$img_size = 'full';
	            		break;
	            }?>
		            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( esc_html__( '%s', 'snsvicky' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
		            <?php
		            	the_post_thumbnail($img_size);
		            ?>
		            </a>
	        </div>
	    <?php
	    endif;?>
	    <div class="post-content">      
	        <div class="content">
	            <h3 class="post-title">
	              <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to %s', 'snsvicky' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	            </h3>
	            <div class="post-meta">
	                <?php if( snsvicky_themeoption('show_author', 1) == 1 ): ?>
	                <span class="post-author">
	                <?php
	                printf( wp_kses(__( '<a class="author-link" href="%s" ref="author">%s%s</a>', 'snsvicky' ), array(
	                                'a' => array(
	                                    'href' => array(),
	                                    'class' => array(),
	                                    'ref' => array()
	                                ),
	                                ) ), esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), get_avatar( get_the_author_meta( 'ID' ), 32 ), get_the_author_meta('display_name') ); ?>
	                </span>
	                <?php endif; ?>
	                <?php
	                // Date
	                if (snsvicky_themeoption('show_date', 1) == 1) :
	                    printf( '<span class="entry-date"><a href="%1$s" rel="bookmark"><time class="entry-date published" datetime="%2$s">%3$s</time></a></span>',
	                        get_permalink(),
	                        esc_attr( get_the_date( 'c' ) ),
	                        get_the_date()
	                    );
	                endif;
	                ?>
	                <?php if( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ): ?>
	                    <span class="post-comment-count">
	                    <?php
	                        echo '<span class="comments-link">';
	                        comments_popup_link( esc_html__( '0 Comments','snsvicky' ),  esc_html__( '1 Comment','snsvicky' ), '%' . esc_html__(' Comments','snsvicky'));
	                        echo '</span>';
	                    ?>
	                    </span>
	                <?php endif; ?>
	                <?php
	                // Edit link
	                edit_post_link(esc_html__('Edit','snsvicky'), '<span class="edit-post">', '</span>'); ?>
	            </div>
            </div>
        </div>
	</article>
</div>
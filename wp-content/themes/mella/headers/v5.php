<header id="apus-header" class="apus-header header-v5 hidden-sm hidden-md hidden-xs" role="banner">
    <div class="<?php echo (mella_get_config('keep_header') ? 'main-sticky-header-wrapper' : ''); ?>">
        <div class="<?php echo (mella_get_config('keep_header') ? 'main-sticky-header' : ''); ?>">
            <div class="inner-header p-relative">
            <div class="container">
                    <div class="row flex-middle">
                        <div class="col-md-2">
                            <div class="logo-in-theme ">
                                <?php get_template_part( 'template-parts/logo/logo' ); ?>
                            </div>
                        </div>
                        <?php if ( has_nav_menu( 'primary' ) ) : ?>
                        <div class="col-md-8 p-static">
                            <div class="main-menu">
                                <nav data-duration="400" class="hidden-xs hidden-sm apus-megamenu slide animate navbar p-static" role="navigation">
                                <?php   $args = array(
                                        'theme_location' => 'primary',
                                        'container_class' => 'collapse navbar-collapse no-padding',
                                        'menu_class' => 'nav navbar-nav megamenu',
                                        'fallback_cb' => '',
                                        'menu_id' => 'primary-menu',
                                        'walker' => new Mella_Nav_Menu()
                                    );
                                    wp_nav_menu($args);
                                ?>
                                </nav>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="col-md-2">
                            <div class="header-right clearfix">
                                <?php if ( mella_get_config('show_user_info', true) ) { ?>
                                    <div class="pull-right">
                                        <div class="top-wrapper-menu">
                                            <?php if( is_user_logged_in()){ ?>
                                                <?php if ( has_nav_menu( 'top-menu' ) ) { ?>
                                                    <a class="drop-dow"><i class="icon-user-1"></i></a>
                                                <?php } ?>
                                                <?php if ( has_nav_menu( 'top-menu' ) ) {
                                                        $args = array(
                                                            'theme_location' => 'top-menu',
                                                            'container_class' => 'inner-top-menu',
                                                            'menu_class' => 'nav navbar-nav topmenu-menu',
                                                            'fallback_cb' => '',
                                                            'menu_id' => '',
                                                            'walker' => new Mella_Nav_Menu()
                                                        );
                                                        wp_nav_menu($args);
                                                    }
                                                ?>
                                            <?php } else { ?>
                                                <a class="drop-dow"><i class="icon-user-1"></i></a>
                                                <?php get_template_part( 'template-parts/login' ); ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ( mella_is_woocommerce_activated() && mella_get_config('show_cartbtn') && !mella_get_config( 'enable_shop_catalog' ) ): ?>
                                    <div class="pull-right">
                                        <?php get_template_part( 'woocommerce/cart/mini-cart-button' ); ?>
                                    </div>
                                <?php endif; ?>

                                <?php if ( class_exists( 'YITH_WCWL' ) && mella_get_config('show_wishlist', true) ):
                                    $wishlist_url = YITH_WCWL()->get_wishlist_url();
                                ?>
                                    <div class="pull-right">
                                        <a class="wishlist-icon" href="<?php echo esc_url($wishlist_url);?>" title="<?php esc_attr_e( 'View Your Wishlist', 'mella' ); ?>"><i class="icon-heart3"></i>
                                            <?php if ( function_exists('yith_wcwl_count_products') ) { ?>
                                                <span class="count"><?php echo yith_wcwl_count_products(); ?></span>
                                            <?php } ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                
                                <?php if ( mella_get_config('show_searchform') ){ ?>
                                    <div class="pull-right">
                                        <a class="btn-search-top"><i class="icon-magnifying-glass"></i></a>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>   
            </div>
            </div>
        </div>
    </div>
</header>

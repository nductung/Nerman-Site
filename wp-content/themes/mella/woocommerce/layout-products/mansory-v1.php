<?php
wp_enqueue_script( 'isotope-pkgd', get_template_directory_uri().'/js/isotope.pkgd.min.js', array( 'jquery', 'imagesloaded' ) );
?>
<div class="mansory-wrapper isotope-items row row-products" data-isotope-duration="400" data-columnwidth=".col-sm-4">
	
		<?php wc_set_loop_prop( 'loop', 0 ); ?>

		<?php $i=0; while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

			<?php if ( $i%7 == 0 ) { ?>
				<div class="col-sm-8 isotope-item">
	 				<?php wc_get_template( 'item-product/inner-v1.php', array('image_size' => 'mella-shop-large') ); ?>
	 			</div>
			<?php } elseif ( in_array($i%7, array(4,5)) ) { ?>
				<div class="col-sm-8 isotope-item">
					<?php wc_get_template( 'item-product/inner-v1.php', array('image_size' => 'mella-shop-horizontal') ); ?>
				</div>
			<?php } else { ?>
				<div class="col-sm-4 isotope-item">
					<?php wc_get_template( 'item-product/inner-v1.php', array('image_size' => 'mella-shop-normal') ); ?>
				</div>
			<?php } ?>


		<?php $i++; endwhile; ?>

</div>
<?php wp_reset_postdata(); ?>
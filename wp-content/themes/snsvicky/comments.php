<div class="sns-comments<?php echo (have_comments() && comments_open() )?' have-comments':' no-comment'; ?>">
    <?php 
    if ( post_password_required() ) { ?>
        <p class="no-comments">
        <?php esc_html_e('This post is password protected. Enter the password to view comments.', 'snsvicky'); ?>
        </p>
        <?php 
        return; 
    }
    if ( have_comments() ) : ?>
        <?php if ( is_singular() ) wp_enqueue_script( "comment-reply" ); ?>
        <h3 id="comments">
            <span>
                <?php
                $comments_number = get_comments_number();
                if ( '1' === $comments_number ) {
                    /* translators: %s: post title */
                    printf( _x( 'One Reply to &ldquo;%s&rdquo;', 'comments title', 'snsvicky' ), get_the_title() );
                } else {
                    printf(
                        /* translators: 1: number of comments, 2: post title */
                        _nx(
                            '%1$s Reply to &ldquo;%2$s&rdquo;',
                            '%1$s Replies to &ldquo;%2$s&rdquo;',
                            $comments_number,
                            'comments title',
                            'snsvicky'
                        ),
                        number_format_i18n( $comments_number ),
                        get_the_title()
                    );
                }
                ?>
            </span>
        </h3>
        <ul class="commentlist">
            <?php wp_list_comments('callback=snsvicky_comment'); ?>
        </ul>
        <div class="navigation"><div class="pagination">
            <?php paginate_comments_links(); ?> 
        </div></div>
    <?php
    endif;
    // If comments are closed and there are comments, let's leave a little note, shall we?
    if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
        <p class="no-comments"><?php echo esc_html__( 'Comments are closed.', 'snsvicky' ); ?></p>
    <?php
    endif;

    $req = get_option( 'require_name_email' );
    $aria_req = ( $req ? " aria-required='true'" : '' );
    //Custom Fields
    $fields =  array(
        'author'=> '<div class="text-field"><div><input name="author" type="text" placeholder="' . esc_html__('Name *', 'snsvicky') . '" size="30"' . $aria_req . ' /></div>',
        'email' => '<div><input name="email" type="text" placeholder="' . esc_html__('E-Mail *', 'snsvicky') . '" size="30"' . $aria_req . ' /></div>',
        'url'   => '<div><input name="url" type="text" placeholder="' . esc_html__('Your website', 'snsvicky') . '" size="30" /></div></div>',
    );
    //Comment Form Args
    $comments_args = array(
        'fields' => $fields,
        'title_reply'=>'<span>'. esc_html__('Leave a comment', 'snsvicky') .'</span>',
        'comment_field' => '<div class="your-comment"><div><textarea id="comment" name="comment" placeholder="' . esc_html__('Your comment *', 'snsvicky') . '" aria-required="true" cols="58" rows="8" tabindex="4"></textarea></div></div>',
        'label_submit' => esc_html__('Submit comment','snsvicky') ,
    	'comment_notes_before' => '<p class="comment-notes"><span id="email-notes">' . esc_html__( 'Your email address will not be published.', 'snsvicky' ) . '</span>' . esc_html__( ' Required fields are marked ', 'snsvicky' ) . '<span class="required">*</span></p>',
        'comment_notes_after' => ''
    );
    comment_form($comments_args);
    ?>
</div>
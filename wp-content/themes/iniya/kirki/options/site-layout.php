<?php
$config = iniya_kirki_config();

INIYA_Kirki::add_section( 'dt_site_layout_section', array(
	'title' => esc_html__( 'Site Layout', 'iniya' ),
	'priority' => 20
) );

	# site-layout
	INIYA_Kirki::add_field( $config, array(
		'type'     => 'radio-image',
		'settings' => 'site-layout',
		'label'    => esc_html__( 'Site Layout', 'iniya' ),
		'section'  => 'dt_site_layout_section',
		'default'  => iniya_defaults('site-layout'),
		'choices' => array(
			'boxed' =>  INIYA_THEME_URI.'/kirki/assets/images/site-layout/boxed.png',
			'wide' => INIYA_THEME_URI.'/kirki/assets/images/site-layout/wide.png',
		)
	));

	# site-boxed-layout
	INIYA_Kirki::add_field( $config, array(
		'type'     => 'switch',
		'settings' => 'site-boxed-layout',
		'label'    => esc_html__( 'Customize Boxed Layout?', 'iniya' ),
		'section'  => 'dt_site_layout_section',
		'default'  => '1',
		'choices'  => array(
			'on'  => esc_attr__( 'Yes', 'iniya' ),
			'off' => esc_attr__( 'No', 'iniya' )
		),
		'active_callback' => array(
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
		)			
	));

	# body-bg-type
	INIYA_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-type',
		'label'    => esc_html__( 'Background Type', 'iniya' ),
		'section'  => 'dt_site_layout_section',
		'multiple' => 1,
		'default'  => 'none',
		'choices'  => array(
			'pattern' => esc_attr__( 'Predefined Patterns', 'iniya' ),
			'upload' => esc_attr__( 'Set Pattern', 'iniya' ),
			'none' => esc_attr__( 'None', 'iniya' ),
		),
		'active_callback' => array(
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-pattern
	INIYA_Kirki::add_field( $config, array(
		'type'     => 'radio-image',
		'settings' => 'body-bg-pattern',
		'label'    => esc_html__( 'Predefined Patterns', 'iniya' ),
		'description'    => esc_html__( 'Add Background for body', 'iniya' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-image' )
		),
		'choices' => array(
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern1.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern1.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern2.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern2.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern3.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern3.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern4.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern4.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern5.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern5.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern6.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern6.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern7.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern7.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern8.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern8.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern9.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern9.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern10.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern10.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern11.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern11.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern12.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern12.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern13.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern13.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern14.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern14.jpg',
			INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern15.jpg'=> INIYA_THEME_URI.'/kirki/assets/images/site-layout/pattern15.jpg',
		),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => '==', 'value' => 'pattern' ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)						
	));

	# body-bg-image
	INIYA_Kirki::add_field( $config, array(
		'type' => 'image',
		'settings' => 'body-bg-image',
		'label'    => esc_html__( 'Background Image', 'iniya' ),
		'description'    => esc_html__( 'Add Background Image for body', 'iniya' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-image' )
		),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => '==', 'value' => 'upload' ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-position
	INIYA_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-position',
		'label'    => esc_html__( 'Background Position', 'iniya' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-position' )
		),
		'default' => 'center',
		'multiple' => 1,
		'choices' => iniya_image_positions(),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => 'contains', 'value' => array( 'pattern', 'upload') ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-repeat
	INIYA_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-repeat',
		'label'    => esc_html__( 'Background Repeat', 'iniya' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-repeat' )
		),
		'default' => 'repeat',
		'multiple' => 1,
		'choices' => iniya_image_repeats(),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => 'contains', 'value' => array( 'pattern', 'upload' ) ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));	
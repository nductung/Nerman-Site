<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

do_action( 'woocommerce_before_customer_login_form' );

$args = array('#customer_login', '#customer_register');
$action = isset($_COOKIE['mella_login_register']) && in_array($_COOKIE['mella_login_register'], $args) ? $_COOKIE['mella_login_register'] : '#customer_login';
if (isset($_GET['ac']) && $_GET['ac'] == 'register') {
	$action = '#customer_register';
}
?>

<div class="user">

	<div id="customer_login" class="register_login_wrapper <?php echo trim($action == '#customer_login' ? 'active' : ''); ?>">
		<div class="acction-tab">
			<a class="login-account register-login-action active" href="#customer_login"><?php echo esc_html__('LOGIN','mella'); ?></a>
			<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
				<a class="creat-account register-login-action" href="#customer_register"><?php echo esc_html__('REGISTER','mella'); ?></a>
			<?php endif; ?>
		</div>
		<form method="post" class="login" role="form">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="form-group  row-user">
				<input type="text" class="form-control" placeholder="<?php esc_attr_e( 'Username or email address', 'mella' ); ?>" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
			</p>
			<p class="form-group row-password">
				<input class="form-control" placeholder="<?php esc_attr_e( 'Password', 'mella' ); ?>" type="password" name="password" id="password" />
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<div class="form-group form-row">
				<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
				<div class="form-group clearfix">
					<span class="inline pull-left">
						<input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php esc_html_e( 'Remember me', 'mella' ); ?>
					</span>
					<span class="lost_password pull-right">
						<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'mella' ); ?></a>
					</span>
				</div>
				<div class="text-center action-login">
					<input type="submit" class="btn btn-theme" name="login" value="<?php esc_attr_e( 'login now', 'mella' ); ?>" />
				</div>
			</div>

			<?php do_action( 'woocommerce_login_form_end' ); ?>

		</form>

	</div>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

	<div id="customer_register" class="content-register register_login_wrapper <?php echo trim($action == '#customer_register' ? 'active' : ''); ?>">
		<div class="acction-tab">
			<a class="login-account register-login-action" href="#customer_login"><?php echo esc_html__('LOGIN','mella'); ?></a>
			<a class="creat-account register-login-action active" href="#customer_register"><?php echo esc_html__('REGISTER','mella'); ?></a>
		</div>
		<form method="post" class="register widget" <?php do_action( 'woocommerce_register_form_tag' ); ?>>

			<?php do_action( 'woocommerce_register_form_start' ); ?>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<p class="form-group row-user">
					<input type="text" class="form-control" placeholder="<?php esc_attr_e( 'Username', 'mella' ); ?>" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
				</p>

			<?php endif; ?>

			<p class="form-group row-email">
				<input type="email" class="form-control" name="email" placeholder="<?php esc_attr_e( 'Email address', 'mella' ); ?>" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />
			</p>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

				<p class="form-group row-password">
					<input type="password" class="form-control" placeholder="<?php esc_attr_e( 'Password', 'mella' ); ?>" name="password" id="reg_password" />
				</p>

			<?php else : ?>

				<p><?php esc_html_e( 'A password will be sent to your email address.', 'mella' ); ?></p>

			<?php endif; ?>


			<?php do_action( 'woocommerce_register_form' ); ?>

			<div class="form-group form-row wrapper-submit">
				<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
				<div class="text-center action-login">
					<input type="submit" class="btn btn-theme" name="register" value="<?php esc_attr_e( 'Register', 'mella' ); ?>" />
				</div>
			</div>

			<?php do_action( 'woocommerce_register_form_end' ); ?>

		</form>
	</div>

<?php endif; ?>
</div>
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
=== Highcompress Image Compressor ===
Contributors: himalayaSaxena
Tags: compress image, optimization , images compressor, SEO, performance,,Boost GT Metrix score
Donate link: https://www.paypal.me/gammerson
Requires at least: 3.7.0
Tested up to: 5.4
Stable tag : 3.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Now Image compression powered by A.I. that dramatically reduce image file sizes without losing quality, make your website load faster, boost your SEO and save space on your server.

== Description ==
Highcompress is a tool to compress images without loosing quality powered by Deep learning.We have added many feature other than compression on our web app.
Now We decided to add wordpress plugin for it so here it is and it is powered by A.I.
Speed up your website with lighter images without losing quality with the power of A.I.

Highcompress is the most advanced image compression tool ever that uses power of Deep learning and machine learning, you can now use this power directly in WordPress.

= What is Image Compression? =

Learn more about image compression, check that: [https://www.highcompress.com](https://www.highcompress.com/)

= Why use Highcompress to optimize your images? =

Highcompress is the first in the marked to used A.I ( Deeplearnig to compress images). High Compress is useful because it’s the only widely supported format that can store partially transparent images. The format uses compression, but the files can still be large. Use High Compress to shrink images for your apps and sites. It will use less bandwidth and load faster.

Highcompress can directly compress your images, **you won't have to lose time anymore on resizing your images before uploading them**.

Three level of compression are available:

- Normal, This mode provides lossless compression; your images will be compressed without any visible change up to 60%. Use this when quality is your priority rather than size. Note: The file size reduction will be less, compared to other modes.
- High, This mode provides balanced compression of your images without any significant quality loss as well as less size, up to 70%. This will provide drastic savings on the initial weight, with a small reduction in image quality. Most of the time it's not even noticeable.
- Super, This mode will do maximum image compression. If your image size is big, we recommend you to use this mode. This will provide massive size reduction on images. Sometimes quality would be degraded a little. If you want to compress your image up to 90% and agree with some loss in quality of the image, use this mode.
With the backup option, you can change your mind whenever you want by restoring your images to their original version or optimize them to another compression level.

= Is Highcompress Free? =

You can optimize for free up to 200 images using fist time API key after that you have to buy another key to continue our service.

Need more? Have a look at our plans: [https://www.highcompress.com](https://www.highcompress.com).

= Who we are? =

We are Developer behind this awesome Wordpress Plugin to optimize your site.

= Get in touch! =

* Website: [highcompresss.com](https://highcompress.com)
* Contact Us: [https://highcompress.com/contact](https://highcompress.com/contact.html)
* Facebook: [https://facebook.com/himalayasaxena](https://facebook.com/himalayasaxena)

License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Installation ==

= WordPress Admin Method =
1. Go to you administration area in WordPress `Plugins > Add`
2. Look for `Highcompress` (use search form)
3. Click on Install and activate the plugin
4. Find the Plugin in administration area in WordPress page -> `Highcompress`

= FTP Method =
1. Upload the complete `Highcompress` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Find the Plugin in administration area in WordPress page -> `Highcompress`

== Frequently Asked Questions ==

= Which formats can be optimized? =

Highcompress can optimize jpg and png formats.

= Can I use the plugin with a free account? =

Absolutely. You are limited to a quota of 25 MB of images per month with a free account. Once this quota is reached, you cannot optimize new images until your quota is renewed or you purchase credits.

= On how many websites can I use the plugin? =

You can use the plugin on as many sites as you wish. The only limit is the optimization quota of your account.

= I used Kraken, Shortpixel, Optimus, EWWW or WP Smush, will Highcompress further optimize my images? =

Absolutely. Most of the time, Highcompress will still be able to optimize your images even if you have already compressed them with another tool.

= What are the three modes normal, high and Ultra? =

These Three Modes Give You The Choice Of Choosing The Compression Ratio With Ultra Being The Highest And Normal Being The Lowest.

= Will the original images be deleted? =

Yes. Highcompress automatically replaces the images with an optimized image.

= Does using HighCompress is safe? =

High Compress Is A Trusted Site With Well Defined Privacy Policies.The Image You Upload For Compression Are Secure With No Leakage And Accessibility By External Sources.

= What is the maximum size that i can upload ? =

The Maximum Size Of Image That Can Be Uploaded In This Site Is 30mb.

== Screenshots ==

1. High Compress API Page.

2. High Compress Settings.

3. High Compress Progress and Statics Page.

3. High Compress Output Page.

== Changelog ==
= 3.6 =
* Minor bug fix
* Increased user experience.

= 3.5 =
* Minor bug fix
* Increased user experience.

= 3.4 =
* Minor bug fix
* Increased user experience.

= 3.3 =
* Newer Platform with improvement in performance.
* Increased user experience.

= 3.1 =
* Huge improvement in performance.
* Increased user experience.

= 3.0 =
* Power of AI and Speed up compression.
* Huge improvement in performance.
* Increased user experience.


= 2.5 =
* Now Images are compress wiht the power of Deep learning.
* Huge improvement in performance.
* Increased user experience.

= 2.3 =
* Minor bug fixes.
* Performance improved.
* Increased user experience.


= 2.1.1 =
* Minor bug fixes.
* Performance improved.
* Increased user experience.

= 2.1 =
* Added File Sizes selection.
* Performance improved.
* Increased user experience.

= 2.0 =
* Minor Bug Fixes
* Performance improved.
* Increased user experience.

= 1.9 =
* Minor Bug Fixes
* Performance improved.
* Increased user experience.

= 1.8 =
* Added auto compress on upload.
* Performance improved.
* Increased user experience.


= 1.7 =
* Added one-click auto backup feature Added.
* Subscription Status Fixed.
* Payment Link Fixed.

= 1.6 =
* Now it calculate size in realtime.
* Performance Highly improved.
* Increased user experience.

= 1.5 =
* Calculate total image in few sec.
* Performance improved.
* Increased user experience.

= 1.4 =
* Fixed ajax 505() error.
* Performance improved.
* Increased user experience.

= 1.3 =
* Unsupported images format will not be counted in the Plan.
* Performance improved.
* Increased user experience.

= 1.2 =
* Show statistic of before and after compression.
* Performance improved.
* Increased user experience.

= 1.1 =
* Added feature to control compression.
* Performance improved.
* Better user experience.

= 1.0 =
* Initial release.

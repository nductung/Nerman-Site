<?php
$config = iniya_kirki_config();

# Footer Copyright
	INIYA_Kirki::add_section( 'dt_footer_copyright', array(
		'title'	=> esc_html__( 'Copyright', 'iniya' ),
		'description' => esc_html__('Footer Copyright Settings','iniya'),
		'panel' 		 => 'dt_footer_copyright_panel',
		'priority' => 1
	) );

		# show-copyright-text
		INIYA_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'show-copyright-text',
			'label'    => esc_html__( 'Show Copyright Text ?', 'iniya' ),
			'section'  => 'dt_footer_copyright',
			'default'  =>  iniya_defaults('show-copyright-text'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'iniya' ),
				'off' => esc_attr__( 'No', 'iniya' )
			)
		) );

		# copyright-text
		INIYA_Kirki::add_field( $config, array(
			'type'     => 'textarea',
			'settings' => 'copyright-text',
			'label'    => esc_html__( 'Add Content', 'iniya' ),
			'section'  => 'dt_footer_copyright',
			'default'  =>  iniya_defaults('copyright-text'),
			'active_callback' => array(
				array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' )
			)
		) );

		# enable-copyright-darkbg
		INIYA_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'enable-copyright-darkbg',
			'label'    => esc_html__( 'Enable Copyright Dark BG ?', 'iniya' ),
			'section'  => 'dt_footer_copyright',
			'default'  =>  iniya_defaults('enable-copyright-darkbg'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'iniya' ),
				'off' => esc_attr__( 'No', 'iniya' )
			)
		) );		

		# copyright-next
		INIYA_Kirki::add_field( $config, array(
			'type'     => 'select',
			'settings' => 'copyright-next',
			'label'    => esc_html__( 'Show Sociable / menu ?', 'iniya' ),
			'description'    => esc_html__( 'Add description here.', 'iniya' ),
			'section'  => 'dt_footer_copyright',
			'default'  => iniya_defaults('copyright-next'),
			'choices'  => array(
				'hidden'  => esc_attr__( 'Hide', 'iniya' ),
				'disable'  => esc_attr__( 'Disable', 'iniya' ),
				'sociable' => esc_attr__( 'Show sociable', 'iniya' ),
				'footer-menu' => esc_attr__( 'Show menu', 'iniya' ),
			)
		) );

# Footer Social
	INIYA_Kirki::add_section( 'dt_footer_social', array(
		'title'	=> esc_html__( 'Social', 'iniya' ),
		'description' => esc_html__('Footer Social Icons Settings','iniya'),
		'panel' 		 => 'dt_footer_copyright_panel',
		'priority' => 2
	) );

		INIYA_Kirki::add_field( $config, array(
			'type'     => 'sortable',
			'settings' => 'footer-sociables',
			'label'    => esc_html__( 'Social Icons Order', 'iniya' ),
			'section'  => 'dt_footer_social',
			'default'  => iniya_defaults('footer-sociables'),
			'choices'  => array(
				"delicious"		=>	esc_attr__( 'Delicious', 'iniya' ),
				"deviantart"	=>	esc_attr__( 'Deviantart', 'iniya' ),
				"digg"			=>	esc_attr__( 'Digg', 'iniya' ),
				"dribbble"		=>	esc_attr__( 'Dribbble', 'iniya' ),
				"envelope-open"	=>	esc_attr__( 'Envelope', 'iniya' ),
				"facebook"		=>	esc_attr__( 'Facebook', 'iniya' ),
				"flickr"		=>	esc_attr__( 'Flickr', 'iniya' ),
				"google-plus"	=>	esc_attr__( 'Google Plus', 'iniya' ),
				"comment"		=>	esc_attr__( 'GTalk', 'iniya' ),
				"instagram"		=>	esc_attr__( 'Instagram', 'iniya' ),
				"lastfm"		=>	esc_attr__( 'Lastfm', 'iniya' ),
				"linkedin"		=>	esc_attr__( 'Linkedin', 'iniya' ),
				"picasa"		=>  esc_attr__( 'Picasa', 'iniya' ),
				"myspace"		=>	esc_attr__( 'Myspace', 'iniya' ),
				"pinterest"		=>	esc_attr__( 'Pinterest', 'iniya' ),
				"reddit"		=>	esc_attr__( 'Reddit', 'iniya' ),
				"rss"			=>	esc_attr__( 'RSS', 'iniya' ),
				"skype"			=>	esc_attr__( 'Skype', 'iniya' ),
				"stumbleupon"	=>	esc_attr__( 'Stumbleupon', 'iniya' ),
				"technorati"	=>	esc_attr__( 'Technorati', 'iniya' ),
				"tumblr"		=>	esc_attr__( 'Tumblr', 'iniya' ),
				"twitter"		=>	esc_attr__( 'Twitter', 'iniya' ),
				"viadeo"		=>	esc_attr__( 'Viadeo', 'iniya' ),
				"vimeo"			=>	esc_attr__( 'Vimeo', 'iniya' ),
				"yahoo"			=>	esc_attr__( 'Yahoo', 'iniya' ),
				"youtube"		=>	esc_attr__( 'Youtube', 'iniya' ),
			),
			'active_callback' => array(
				array( 'setting' => 'copyright-next', 'operator' => '==', 'value' => 'sociable' ),
			)
		) );

# Footer Copyright Background		
	INIYA_Kirki::add_section( 'dt_footer_copyright_bg', array(
		'title'	=> esc_html__( 'Background', 'iniya' ),
		'panel' => 'dt_footer_copyright_panel',
		'priority' => 3,
	) );

		# customize-footer-copyright-bg
		INIYA_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'customize-footer-copyright-bg',
			'label'    => esc_html__( 'Customize Background ?', 'iniya' ),
			'section'  => 'dt_footer_copyright_bg',
			'default'  => iniya_defaults('customize-footer-copyright-bg'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'iniya' ),
				'off' => esc_attr__( 'No', 'iniya' )
			),
			'active_callback' => array(
				array(
					array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' ),
					array( 'setting' => 'copyright-next', 'operator' => 'in', 'value' =>  array( 'sociable', 'footer-menu') )
				)
			)
		));

		# footer-copyright-bg-color
		INIYA_Kirki::add_field( $config, array(
			'type' => 'color',
			'settings' => 'footer-copyright-bg-color',
			'label'    => esc_html__( 'Background Color', 'iniya' ),
			'section'  => 'dt_footer_copyright_bg',
			'output' => array(
				array( 'element' => '.footer-copyright' , 'property' => 'background-color' )
			),
			'choices' => array( 'alpha' => true ),
			'active_callback' => array(
				array( 'setting' => 'customize-footer-copyright-bg', 'operator' => '==', 'value' => '1' )
			)
		));

		# footer-copyright-bg-image
		INIYA_Kirki::add_field( $config, array(
			'type' => 'image',
			'settings' => 'footer-copyright-bg-image',
			'label'    => esc_html__( 'Background Image', 'iniya' ),
			'description'    => esc_html__( 'Add Background Image for footer', 'iniya' ),
			'section'  => 'dt_footer_copyright_bg',
			'output' => array(
				array( 'element' => '.footer-copyright' , 'property' => 'background-image' )
			),
			'active_callback' => array(
				array( 'setting' => 'customize-footer-copyright-bg', 'operator' => '==', 'value' => '1' )
			)
		));

		# footer-copyright-bg-position
		INIYA_Kirki::add_field( $config, array(
			'type' => 'select',
			'settings' => 'footer-copyright-bg-position',
			'label'    => esc_html__( 'Background Image Position', 'iniya' ),
			'section'  => 'dt_footer_copyright_bg',
			'output' => array(),
			'default' => 'center',
			'multiple' => 1,
			'choices' => iniya_image_positions(),
			'active_callback' => array(
				array( 'setting' => 'customize-footer-copyright-bg', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'footer-copyright-bg-image', 'operator' => '!=', 'value' => '' )				
			)			
		));

		# footer-copyright-bg-repeat
		INIYA_Kirki::add_field( $config, array(
			'type' => 'select',
			'settings' => 'footer-copyright-bg-repeat',
			'label'    => esc_html__( 'Background Image Repeat', 'iniya' ),
			'section'  => 'dt_footer_copyright_bg',
			'output' => array(),
			'default' => 'repeat',
			'multiple' => 1,
			'choices' => iniya_image_repeats(),
			'active_callback' => array(
				array( 'setting' => 'customize-footer-copyright-bg', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'footer-copyright-bg-image', 'operator' => '!=', 'value' => '' )
			)			
		));

# Footer Copyright Typography
	INIYA_Kirki::add_section( 'dt_footer_copyright_typo', array(
		'title'	=> esc_html__( 'Typography', 'iniya' ),
		'panel' => 'dt_footer_copyright_panel',
		'priority' => 4,
	) );

		# customize-footer-copyright-text-typo
		INIYA_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'customize-footer-copyright-text-typo',
			'label'    => esc_html__( 'Customize Copyright Text ?', 'iniya' ),
			'section'  => 'dt_footer_copyright_typo',
			'default'  => iniya_defaults('customize-footer-copyright-text-typo'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'iniya' ),
				'off' => esc_attr__( 'No', 'iniya' )
			),
			'active_callback' => array(
				array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' )
			)			
		));

		# footer-copyright-text-typo
		INIYA_Kirki::add_field( $config, array(
			'type'     => 'typography',
			'settings' => 'footer-copyright-text-typo',
			'label'    => esc_html__( 'Text Typography', 'iniya' ),
			'section'  => 'dt_footer_copyright_typo',
			'output' => array(
				array( 'element' => '.footer-copyright' )
			),
			'default' => iniya_defaults( 'footer-copyright-text-typo' ),
			'active_callback' => array(
				array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'customize-footer-copyright-text-typo', 'operator' => '==', 'value' => '1' )
			)		
		));

		# Divider
		INIYA_Kirki::add_field( $config ,array(
			'type'=> 'custom',
			'settings' => 'footer-copyright-text-typo-divider',
			'section'  => 'dt_footer_copyright_typo',
			'default'  => '<div class="customize-control-divider"></div>',
			'active_callback' => array(
				array( 'setting' => 'show-copyright-text', 'operator' => '==', 'value' => '1' ),
				array( 'setting' => 'copyright-next', 'operator' => '==', 'value' => 'footer-menu' )
			)			
		));		

		# customize-footer-menu-typo
		INIYA_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'customize-footer-menu-typo',
			'label'    => esc_html__( 'Customize Footer Menu ?', 'iniya' ),
			'section'  => 'dt_footer_copyright_typo',
			'default'  => iniya_defaults('customize-footer-menu-typo'),
			'choices'  => array(
				'on'  => esc_attr__( 'Yes', 'iniya' ),
				'off' => esc_attr__( 'No', 'iniya' )
			),
			'active_callback' => array(
				array( 'setting' => 'copyright-next', 'operator' => '==', 'value' => 'footer-menu' )
			)			
		));

		# footer-menu-typo
		INIYA_Kirki::add_field( $config, array(
			'type'     => 'typography',
			'settings' => 'footer-menu-typo',
			'label'    => esc_html__( 'Menu Typography', 'iniya' ),
			'section'  => 'dt_footer_copyright_typo',
			'output' => array(
				array( 'element' => '' )
			),
			'default' => iniya_defaults( 'footer-menu-typo' ),
			'active_callback' => array(
				array( 'setting' => 'copyright-next', 'operator' => '==', 'value' => 'footer-menu' ),
				array( 'setting' => 'customize-footer-menu-typo', 'operator' => '==', 'value' => '1' )
			)		
		));		
<?php

if ( !function_exists( 'mella_page_metaboxes' ) ) {
	function mella_page_metaboxes(array $metaboxes) {
		global $wp_registered_sidebars;
        $sidebars = array();

        if ( !empty($wp_registered_sidebars) ) {
            foreach ($wp_registered_sidebars as $sidebar) {
                $sidebars[$sidebar['id']] = $sidebar['name'];
            }
        }
        $headers = array_merge( array('global' => esc_html__( 'Global Setting', 'mella' )), mella_get_header_layouts() );
        $footers = array_merge( array('global' => esc_html__( 'Global Setting', 'mella' )), mella_get_footer_layouts() );

		$prefix = 'apus_page_';
	    $fields = array(
			array(
				'name' => esc_html__( 'Select Layout', 'mella' ),
				'id'   => $prefix.'layout',
				'type' => 'select',
				'options' => array(
					'main' => esc_html__('Main Content Only', 'mella'),
					'left-main' => esc_html__('Left Sidebar - Main Content', 'mella'),
					'main-right' => esc_html__('Main Content - Right Sidebar', 'mella')
				)
			),
			array(
                'id' => $prefix.'fullwidth',
                'type' => 'select',
                'name' => esc_html__('Is Full Width?', 'mella'),
                'default' => 'no',
                'options' => array(
                    'no' => esc_html__('No', 'mella'),
                    'yes' => esc_html__('Yes', 'mella')
                )
            ),
            array(
                'id' => $prefix.'left_sidebar',
                'type' => 'select',
                'name' => esc_html__('Left Sidebar', 'mella'),
                'options' => $sidebars
            ),
            array(
                'id' => $prefix.'right_sidebar',
                'type' => 'select',
                'name' => esc_html__('Right Sidebar', 'mella'),
                'options' => $sidebars
            ),
            array(
                'id' => $prefix.'show_breadcrumb',
                'type' => 'select',
                'name' => esc_html__('Show Breadcrumb?', 'mella'),
                'options' => array(
                    'no' => esc_html__('No', 'mella'),
                    'yes' => esc_html__('Yes', 'mella')
                ),
                'default' => 'yes',
            ),
            
            array(
                'id' =>  $prefix.'breadcrumb_center',
                'type' => 'select',
                'name' => esc_html__('Breadcrumbs Align', 'mella'),
                'options' => array(
                    'st_inherit' => esc_html__('Inherit', 'mella'),
                    'st_center' => esc_html__('Center', 'mella'),
                    'st_stretch' => esc_html__('Stretch', 'mella'),
                ),
                'default' => 'st_center',
            ),

            array(
                'id' => $prefix.'breadcrumb_style',
                'type' => 'select',
                'name' => esc_html__('Breadcrumbs Style', 'mella'),
                'options' => array(
                    'text-black' => esc_html__('Text Black', 'mella'),
                    'text-white' => esc_html__('Text White', 'mella'),
                ),
                'default' => 'text-black'
            ),
            array(
                'id' => $prefix.'breadcrumb_color',
                'type' => 'colorpicker',
                'name' => esc_html__('Breadcrumb Background Color', 'mella')
            ),
            array(
                'id' => $prefix.'breadcrumb_image',
                'type' => 'file',
                'name' => esc_html__('Breadcrumb Background Image', 'mella')
            ),
            array(
                'id' => $prefix.'header_type',
                'type' => 'select',
                'name' => esc_html__('Header Layout Type', 'mella'),
                'description' => esc_html__('Choose a header for your website.', 'mella'),
                'options' => $headers,
                'default' => 'global'
            ),
            array(
                'id' => $prefix.'header_transparent',
                'type' => 'select',
                'name' => esc_html__('Header Transparent', 'mella'),
                'description' => esc_html__('Choose a header for your website.', 'mella'),
                'options' => array(
                    'no' => esc_html__('No', 'mella'),
                    'yes' => esc_html__('Yes', 'mella')
                ),
                'default' => 'global'
            ),
            array(
                'id' => $prefix.'footer_type',
                'type' => 'select',
                'name' => esc_html__('Footer Layout Type', 'mella'),
                'description' => esc_html__('Choose a footer for your website.', 'mella'),
                'options' => $footers,
                'default' => 'global'
            ),
            array(
                'id' => $prefix.'extra_class',
                'type' => 'text',
                'name' => esc_html__('Extra Class', 'mella'),
                'description' => esc_html__('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'mella')
            )
    	);
		
	    $metaboxes[$prefix . 'display_setting'] = array(
			'id'                        => $prefix . 'display_setting',
			'title'                     => esc_html__( 'Display Settings', 'mella' ),
			'object_types'              => array( 'page' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => $fields
		);

	    return $metaboxes;
	}
}
add_filter( 'cmb2_meta_boxes', 'mella_page_metaboxes' );

if ( !function_exists( 'mella_cmb2_style' ) ) {
	function mella_cmb2_style() {
		wp_enqueue_style( 'mella-cmb2-style', get_template_directory_uri() . '/inc/vendors/cmb2/assets/style.css', array(), '1.0' );
	}
}
add_action( 'admin_enqueue_scripts', 'mella_cmb2_style' );



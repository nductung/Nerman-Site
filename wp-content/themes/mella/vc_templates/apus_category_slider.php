<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

if (isset($categories) && !empty($categories)):
    $categories = (array) vc_param_group_parse_atts( $categories );
	
	$bcol = 12/$columns;
?>
	<div class="widget-category-slider <?php echo esc_attr($el_class.' '.$style); ?>">

		<?php if ( $layout_type == 'grid' ) { ?>
			<div class="row">
				<?php foreach ($categories as $category) :
					if ( !empty($category['category']) ) {
						$term = get_term_by( 'slug', $category['category'], 'product_cat' );
						if ( !empty($term) ) {
				?>
							<div class="col-lg-<?php echo esc_attr($bcol); ?> col-xs-12 category-wrapper">
					        	<a class="link-action clearfix" href="<?php echo esc_url(get_term_link($term)); ?>">
					                <?php
						                if ( !empty($category['image']) ) {
						                	echo trim(mella_get_attachment_thumbnail($category['image'], 'full'));
						                }
					                ?>
				                	<h2 class="title">
				                		<?php if ( !empty($category['title']) ) { ?>
			                                <?php echo trim($category['title']); ?>
			                            <?php } else { ?>
			                                <?php echo trim($term->name); ?>
			                            <?php } ?>
			                        </h2>
				                </a>
					        </div>
				    	<?php } ?>
				    <?php } ?>
				<?php endforeach; ?>
			</div>
		<?php } else { ?>
			<div class="slick-carousel" data-carousel="slick" data-items="<?php echo esc_attr($columns); ?>" data-smallmedium="3" data-extrasmall="2" data-smallest="1" data-pagination="<?php echo esc_attr( $show_pagination ? 'true' : 'false' ); ?>" data-nav="<?php echo esc_attr( $show_nav ? 'true' : 'false' ); ?>" data-rows="<?php echo esc_attr( $rows ); ?>">
				<?php foreach ($categories as $category) :
					if ( !empty($category['category']) ) {
						$term = get_term_by( 'slug', $category['category'], 'product_cat' );
						if ( !empty($term) ) {
				?>
							<div class="category-wrapper">
					        	<a class="link-action clearfix" href="<?php echo esc_url(get_term_link($term)); ?>">
					                <?php
						                if ( !empty($category['image']) ) {
						                	echo trim(mella_get_attachment_thumbnail($category['image'], 'full'));
						                }
					                ?>
				                	<h2 class="title">
				                		<?php if ( !empty($category['title']) ) { ?>
			                                <?php echo trim($category['title']); ?>
			                            <?php } else { ?>
			                                <?php echo trim($term->name); ?>
			                            <?php } ?>
			                        </h2>
				                </a>
					        </div>
				    	<?php } ?>
				    <?php } ?>
				<?php endforeach; ?>
			</div>
		<?php } ?>
	</div>
<?php endif; ?>
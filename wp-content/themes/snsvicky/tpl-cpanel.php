<?php
$theme_color = snsvicky_themeoption('theme_color', '#f89b72');

// Get page meta data
if (function_exists('rwmb_meta') && rwmb_meta('snsvicky_page_themecolor') == 1) {
	$theme_color = rwmb_meta('snsvicky_theme_color') != '' ? rwmb_meta('snsvicky_theme_color') : $theme_color;
}

$theme_color = str_replace('#', '', $theme_color);
$boxedlayout =  snsvicky_themeoption('use_boxedlayout');
$stickymenu = snsvicky_themeoption('use_stickmenu');

?>
<div id="sns_cpanel">
    <div class="cpanel-head">
    	<a class="button btn-buy" href="#" title="<?php echo esc_html__('Buy Theme Now', 'snsvicky'); ?>"><?php echo esc_html__('Buy Theme Now', 'snsvicky'); ?></a>
    </div>
    <div class="cpanel-set">
    	<div class="form-group">
		<?php
		$scss_compile = snsvicky_themeoption('advance_scss_compile');
		?>
		<div class="form-group">
			<div class="col-xs-12">
				<label><?php echo esc_html__('Always Compile SCSS', 'snsvicky'); ?></label>
				<div class="content scss_compile">
					<a class="<?php echo ($scss_compile == 2)?'active menu':'menu'; ?>" href="#" data-scsscompile="2">Yes</a>
					<a class="<?php echo ($scss_compile == 1)?'active menu':'menu'; ?>" href="#" data-scsscompile="1">No</a>
					<input type="hidden" name="scss_compile" value="<?php echo esc_attr($scss_compile); ?>"/>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<label><?php echo esc_html__('Enable sticky menu', 'snsvicky'); ?></label>
				<div class="content sticky_menu">
					<a class="<?php echo ($stickymenu == 1)?'active menu':'menu'; ?>" href="#" data-sticky="1">Yes</a>
					<a class="<?php echo ($stickymenu == 0)?'active menu':'menu'; ?>" href="#" data-sticky="0">No</a>
					<input type="hidden" name="sticky_menu" value="<?php echo esc_attr($stickymenu); ?>"/>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<label><?php echo esc_html__('Use boxed Layout', 'snsvicky'); ?></label>
				<div class="content boxed_layout">
					<a class="<?php echo ($boxedlayout == 1)?'active layout':'layout'; ?>" href="#" data-boxed="1">Yes</a>
					<a class="<?php echo ($boxedlayout == 0)?'active layout':'layout'; ?>" href="#" data-boxed="0">No</a>
					<input type="hidden" name="sns_boxed_layout" value="<?php echo esc_attr($boxedlayout); ?>" />
				</div>
			</div>
		</div>
    </div>
    <div class="cpanel-bottom">
    	<div class="form-group">
			<div class="col-xs-12">
				<div class="button-action">
					<a class="button btn-reset" href="#" data-url="<?php echo site_url('/'); ?>"><?php echo esc_html__('Reset Options', 'snsvicky'); ?></a>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-12">
				<p><?php echo esc_html__('That is some options to demo for you.', 'snsvicky'); ?></p>
			</div>
		</div>
	</div>
    <div id="sns_cpanel_btn">
        <i class="fa fa-cog fa-spin"></i>
    </div>
</div>
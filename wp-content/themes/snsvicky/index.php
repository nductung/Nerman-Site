<?php get_header(); ?>
<!-- Content -->
<div id="sns_content">
	<div class="container">
		<div class="row sns-content">
			<!-- left sidebar -->
			<?php snsvicky_leftcol(); ?>
			<!-- Main content -->
			<div class="<?php echo snsvicky_maincolclass(); ?>">
			    <?php
			    if ( have_posts() ) :
			        get_template_part( 'framework/tpl/blog/blog', snsvicky_themeoption('blog_type','') );
			    else:
			        get_template_part( 'content', 'none' );
			    endif; ?>
			</div>
			<!-- Right sidebar -->
			<?php snsvicky_rightcol(); ?>
		</div>
	</div>
</div>
<!-- End Content -->
<?php get_footer(); ?>
<?php

$GLOBALS['comment'] = $comment;
$add_below = '';

?>
<li <?php comment_class(); ?> id="comment-<?php comment_ID() ?>">

	<div class="the-comment clearfix">
		<div class="text-center pull-left left-infor">
			<?php if(get_avatar($comment, 90)){ ?>
				<div class="avatar">
					<?php echo get_avatar($comment, 90); ?>
				</div>
			<?php } ?>
			<?php comment_reply_link(array_merge( $args, array( 'reply_text' => '<i class="fa fa-reply" aria-hidden="true"></i>', 'add_below' => 'comment', 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</div>
		<div class="comment-box">
			<div class="comment-author meta">
				<div class="name-author"><?php echo get_comment_author_link() ?></div>
				<span class="date"><i class="fa fa-calendar" aria-hidden="true"></i><?php printf(esc_html__('%1$s', 'mella'), get_comment_date()) ?></span>
				<?php edit_comment_link(esc_html__('Edit', 'mella'),'  ','') ?>
			</div>
			<div class="comment-text">
				<?php if ($comment->comment_approved == '0') : ?>
				<em><?php esc_html_e('Your comment is awaiting moderation.', 'mella') ?></em>
				<br />
				<?php endif; ?>
				<?php comment_text() ?>
			</div>
			
		</div>
	</div>
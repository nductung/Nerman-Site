<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<div class="widget-collections <?php echo esc_attr($el_class); ?>">
    <div class="widget-collections-item <?php echo esc_attr($style); ?>">
        <?php if ( $url ) { ?>
            <a class="link-img" href="<?php echo esc_url($url); ?>">
        <?php } ?>
            <?php echo trim(mella_get_attachment_thumbnail($image, 'full')); ?>
            <?php if ($title!=''): ?>
                <div class="content-inner">
                    <h3 class="title">
                        <?php echo trim( $title ); ?>
                    </h3>
                </div>
            <?php endif; ?>
        <?php if ( $url ) { ?>
            </a>
        <?php } ?>
    </div>
</div>
<?php
/**
 * The template part for displaying results in search pages
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Mella
 * @since Mella 1.0
 */
?>
<article <?php post_class('post post-grid post-grid-v1'); ?>>
    <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
        <span class="post-sticky"><?php echo esc_html__('Featured','mella'); ?></span>
    <?php endif; ?>
    <div class="content">
        <?php if (get_the_title()) { ?>
            <h4 class="title">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h4>
        <?php } ?>
        <div class="bottom-info">
            <div class="categories"><?php echo esc_html__('in ','mella') ?><?php mella_post_categories($post); ?></div> 
            <div class="date-post">
                <span class="suffix"><?php echo esc_html__('on','mella') ?></span>
                <a href="<?php the_permalink(); ?>"><?php the_time( get_option('date_format', 'd M, Y') ); ?></a>
            </div>
        </div>
    </div>
    <?php if( is_search() || has_excerpt() ){?>
        <div class="description">
            <?php the_excerpt(); ?>
        </div>
    <?php } ?>
</article>
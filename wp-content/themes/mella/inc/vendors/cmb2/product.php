<?php

if ( !function_exists( 'mella_product_metaboxes' ) ) {
	function mella_product_metaboxes(array $metaboxes) {
		$prefix = 'apus_product_';
	    $fields = array(
	    	array(
				'name' => esc_html__( 'Review Video', 'mella' ),
				'id'   => $prefix.'review_video',
				'type' => 'text',
				'description' => esc_html__( 'You can enter a video youtube or vimeo', 'mella' ),
			),
			array(
				'id'          => $prefix.'custom_tabs',
				'type'        => 'group',
				'description' => esc_html__( 'Add your custom tabs here', 'mella' ),
				'options'     => array(
					'group_title'   => esc_html__( 'Tab {#}', 'mella' ),
					'add_button'    => esc_html__( 'Add Another Tab', 'mella' ),
					'remove_button' => esc_html__( 'Remove Tab', 'mella' ),
					'sortable'      => true,
				),
				'fields' => array(
					array(
						'name' => esc_html__( 'Tab Title', 'mella' ),
						'id'   => 'title',
						'type' => 'text',
						'description' => esc_html__( 'Enter Tab Title', 'mella' ),
					),
					array(
						'name' => esc_html__( 'Tab Content', 'mella' ),
						'id'   => 'content',
						'type' => 'wysiwyg',
						'description' => esc_html__( 'Enter Tab Content', 'mella' ),
					),
				)
			),
    	);
		
	    $metaboxes[$prefix . 'display_setting'] = array(
			'id'                        => $prefix . 'display_setting',
			'title'                     => esc_html__( 'More Information', 'mella' ),
			'object_types'              => array( 'product' ),
			'context'                   => 'normal',
			'priority'                  => 'low',
			'show_names'                => true,
			'fields'                    => $fields
		);

	    return $metaboxes;
	}
}
add_filter( 'cmb2_meta_boxes', 'mella_product_metaboxes' );

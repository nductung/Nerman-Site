<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) || $woocommerce_loop['columns'] == 0 ){
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', snsvicky_woo_shop_columns() );
}
// Extra post classes
$classes = array();
if ( !is_product() && !is_cart() ) :
	$col = ( !empty($woocommerce_loop['columns']) && $woocommerce_loop['columns'] > 0 ) ? 12/$woocommerce_loop['columns'] : 4 ;
	$col2 = ( !empty($woocommerce_loop['columns']) && $woocommerce_loop['columns'] > 0 ) ? 12/$woocommerce_loop['columns'] : 4 ;
	if ( $woocommerce_loop['columns'] >= 4 ) {
		$col3 = ( !empty($woocommerce_loop['columns']) && $woocommerce_loop['columns']-1 > 0 ) ? 12/($woocommerce_loop['columns']-1) : 12 ;
	}else{
		$col3 = ( !empty($woocommerce_loop['columns']) && $woocommerce_loop['columns'] > 0 ) ? 12/$woocommerce_loop['columns'] : 12 ;
	}
	$classes[] = 'col-lg-'.$col.' col-md-'.$col2.' col-sm-'.$col3.' col-xs-12';
	if ( snsvicky_themeoption('woo_mobile_portrait_per_row', false) == 1 ) $classes[] =  ' col-phone-6';
endif;
?>
<div <?php post_class($classes); ?>>
	<div class="block-product-inner grid-view product-inner">
		<div class="item-inner">
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );
	?>
			<div class="item-img clearfix">
				<div class="item-img-info">
					<a class="product-image" href="<?php the_permalink(); ?>">
	<?php
	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );
	?>
					</a>
				</div>
			</div>
			<div class="item-info">
				<div class="item-content">
					<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<div class="cat-links">', '</div>' ); ?>
					<h3 class="item-title">
					 	<a href="<?php the_permalink(); ?>">
					 		<?php the_title(); ?>
					 	</a>
					</h3>
	<?php
	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	// do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
	do_action( 'woocommerce_after_shop_loop_item_title' );
	do_action( 'snsvicky_variations_color_loop_item' );
	?>
				</div>
				<div class="item-box-hover"><div class="item-box-hover-wrap">
	<?php
	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
				</div></div>
			</div>
		</div>
	</div>
<?php if ( !is_product() && !is_cart() ) : ?>
	<div class="list-view product-inner">
		<?php do_action( 'woocommerce_before_shop_loop_item' ); ?>
		<div class="item-img">
			<a class="product-image" href="<?php the_permalink(); ?>">
	<?php
	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );
	?>
			</a>
		</div>
		<div class="product-shop">
			<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<div class="cat-links">', '</div>' ); ?>
			<h3 class="item-title">
			 	<a href="<?php the_permalink(); ?>">
			 		<?php the_title(); ?>
			 	</a>
			</h3>
			<?php
			add_action( 'snsvicky_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 ); 
			add_action( 'snsvicky_after_shop_loop_item_title', 'woocommerce_template_single_excerpt', 6 );
			add_action( 'snsvicky_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 ); 
			do_action( 'snsvicky_after_shop_loop_item_title' );
			?>
			<?php do_action( 'snsvicky_variations_color_loop_item' ); ?>
			<div class="prd-buttons">
				<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
			</div>
		</div>
	</div>
<?php endif; ?>
</div>

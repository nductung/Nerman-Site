<?php 
get_header();
?>
<div id="sns_content" class="is-notfound">
	<div class="container sns-notfound-page">
		<div class="sns-notfound-content">
        <h1 class="notfound-title"><?php echo esc_html( snsvicky_themeoption('notfound_title') ); ?></h1>
        <p>
        	<?php echo esc_html( snsvicky_themeoption('notfound_content') ); ?>
        	<?php 
        		echo sprintf( wp_kses(__( '. Would you like to go to <a href="%s" title="Home Page">homepage</a> instead?', 'snsvicky' ), array(
                    'a' => array(
                        'href' => array(),
                        'class' => array(),
                        'title' => array()
                    ),
                )), esc_url(home_url('/')) );
        	?>
        </p>
        <p class="or"><?php echo esc_html('Or', 'snsvicky'); ?></p>
        </div>
        <?php get_search_form(); ?>
	</div>
</div>
<?php get_footer(); ?>
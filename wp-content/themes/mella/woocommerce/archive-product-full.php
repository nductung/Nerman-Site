<?php

get_header();
$sidebar_configs = mella_get_woocommerce_layout_configs();

$display_mode = mella_woocommerce_get_display_mode();
$layout_type = $display_mode;
if ( $display_mode == 'mansory' || $display_mode == 'metro' ) {
	wp_enqueue_script( 'isotope-pkgd', get_template_directory_uri().'/js/isotope.pkgd.min.js', array( 'jquery', 'imagesloaded' ) );
}
?>

<?php do_action( 'mella_woo_template_main_before' ); ?>
<section id="main-container" class="page-shop <?php echo apply_filters('mella_woocommerce_content_class', 'container');?>">

	<?php do_action('mella_woocommerce_archive_description'); ?>
	
	<?php mella_before_content( $sidebar_configs ); ?>

	
	<div class="row">
		<?php mella_display_sidebar_left( $sidebar_configs ); ?>

		<div id="main-content" class="archive-shop col-xs-12 <?php echo esc_attr($sidebar_configs['main']['class']); ?>">

			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">

					
					<div id="apus-shop-products-wrapper" class="apus-shop-products-wrapper <?php echo esc_attr($layout_type); ?>" data-layout_type="<?php echo esc_attr($layout_type); ?>">
						
                        <!-- product content -->
						<?php
							$show_taxonomy_description = is_product_taxonomy() ? true : false;
							if ( $show_taxonomy_description ) {
								/**
								 * woocommerce_archive_description hook
								 *
								 * @hooked woocommerce_taxonomy_archive_description - 10
								 * @hooked woocommerce_product_archive_description - 10
								 */
								do_action( 'woocommerce_archive_description' );
							}

						?>

						<?php if ( have_posts() ) : ?>
							<div class="top-archive-shop">
								<?php if ( mella_get_config('product_archive_top_categories') && mella_get_config('product_archive_top_filter') ) : ?>
									<!-- header for full -->
									<?php
										wc_get_template_part( 'content', 'product_header' );
										
										remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
										remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
										remove_action( 'woocommerce_before_shop_loop', 'mella_filter_before' , 1 );
										remove_action( 'woocommerce_before_shop_loop', 'mella_filter_after' , 40 );
										remove_action( 'woocommerce_before_shop_loop', 'mella_woocommerce_display_modes' , 2 );

										do_action( 'woocommerce_before_shop_loop' );
									?>
								<?php endif; ?>

								<?php if ( !mella_get_config('product_archive_top_categories') ) { ?>
									<div class="before-shop-header-wrapper clearfix">
										<div class="before-shop-loop-fillter pull-left">
											<?php 
												remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering' , 30 );
												remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count' , 20 );
											?>
											<?php do_action( 'woocommerce_before_shop_loop' ); ?>
										</div>
										<?php wc_get_template_part( 'content', 'product_header' ); ?>
									</div>
								<?php } ?>

								<?php
		                            // Results bar/button
									if ( mella_get_config('product_archive_top_filter') ) {
			                            wc_get_template_part( 'content', 'product_results_bar' );
									}
		                        ?>
	                        </div>
							<?php woocommerce_product_loop_start(); ?>
								
								<?php woocommerce_product_subcategories( array( 'before' => '<div class="row subcategories-wrapper">', 'after' => '</div>' ) ); ?>
								
								<?php
									$attr = 'class="products-wrapper-'.esc_attr($display_mode).'"';
									if ( $display_mode == 'mansory' ) {
										$attr = 'class="products-wrapper-mansory isotope-items row" data-isotope-duration="400" data-columnwidth=".col-sm-4"';
									} elseif ( $display_mode == 'metro' ) {
										$attr = 'class="products-wrapper-mansory isotope-items row" data-isotope-duration="400" data-columnwidth=".col-sm-3"';
									}
								?>
								<div <?php echo trim($attr); ?>>
									<?php if ( $display_mode == 'list' || $display_mode == 'grid' ) { ?>
										<div class="row row-products-wrapper">
											<?php while ( have_posts() ) : the_post(); ?>
												<?php wc_get_template_part( 'content', 'product' ); ?>
											<?php endwhile; // end of the loop. ?>
										</div>
									<?php } else { ?>
										<?php while ( have_posts() ) : the_post(); ?>
											<?php wc_get_template_part( 'content', 'product' ); ?>
										<?php endwhile; // end of the loop. ?>
									<?php } ?>
								</div>

							<?php woocommerce_product_loop_end(); ?>

							<?php do_action( 'woocommerce_after_shop_loop' ); ?>

						<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
							<?php do_action( 'woocommerce_no_products_found' ); ?>
						<?php endif; ?>

					</div>
				</div><!-- #content -->
			</div><!-- #primary -->
		</div><!-- #main-content -->
		
		<?php mella_display_sidebar_right( $sidebar_configs ); ?>
		
	</div>
</section>
<?php

get_footer();

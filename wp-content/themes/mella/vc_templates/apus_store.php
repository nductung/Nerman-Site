<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$members = (array) vc_param_group_parse_atts( $members );
if ( !empty($members) ):
?>
	<div class="widget-store <?php echo esc_attr($el_class); ?>">
	    <?php if ($title!=''): ?>
	        <h3 class="widget-title">
	            <?php echo esc_attr( $title ); ?>
	        </h3>
	    <?php endif; ?>
    	<div class="row">
    		<?php foreach ($members as $item): ?>
    			<div class="col-sm-6 col-xs-12 col-md-<?php echo esc_attr(12/$columns); ?>">	
					<div class="item-store">
	                    <div class="inner-content text-center">
		                    <?php if ( isset($item['name']) && !empty($item['name']) ): ?>
		                    	<h3 class="name-team"><?php echo trim($item['name']); ?></h3>
		                    <?php endif; ?>
		                    <?php if ( isset($item['location']) && !empty($item['location']) ): ?>
		                    	<div class="location"><?php echo trim($item['location']); ?></div>
		                    <?php endif; ?>
		                    <?php if ( isset($item['phone']) && !empty($item['phone']) ): ?>
		                    	<div class="phone"><?php echo trim($item['phone']); ?></div>
		                    <?php endif; ?>
		                    <?php if ( isset($item['email']) && !empty($item['email']) ): ?>
		                    	<div class="email"><?php echo trim($item['email']); ?></div>
		                    <?php endif; ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>
(function ($) {
	"use strict";
	$(document).ready(function() {
		// Filter hoz
		if ( $('.toolbar .filter-hoz').length ){
			$('.toolbar .filter-hoz').click(function(){
				if ( $(this).hasClass('active') ){
					$(this).removeClass('active');
					if ( $('.horizontal-filter').length ) {
						$('.horizontal-filter').slideUp(300);
					}
				}else{
					$(this).addClass('active');
					if ( $('.horizontal-filter').length ) {
						$('.horizontal-filter').slideDown(300);
					}
				}
			});
		}
		// Product image, gallery, zoom
		if( $('body.single-product .product.type-product > .gallery_type_h').length ){
			SnsSinglePrdIHG.init('.gallery_type_h');
		}
		if( $('body.single-product .product.type-product > .gallery_type_v').length ){
			SnsSinglePrdIVG.init('.gallery_type_v');
		}
		if( $('body.single-product .product.type-product > .gallery_type_none').length ){
			SnsSinglePrdING.init('.gallery_type_none');
			//var sns_sticky = new Sticky('body.single-product .product.type-product > .gallery_type_none .sticky-content');
			var sidebar_sticky = new StickySidebar('body.single-product .product.type-product > .gallery_type_none .entry-summary .sticky-content', {
		        containerSelector: 'body.single-product .product.type-product > .gallery_type_none',
		        innerWrapperSelector: 'body.single-product .product.type-product > .gallery_type_none .entry-summary .sticky-content .inner',
		        topSpacing: 30,
		        bottomSpacing: 30
		    });
		}
		// Variations for Variable Product
		SnsVariationForm.reDesignVariationForm();
		SnsVariationForm.init();
		// Shortcode: SNS Products
		$('.sns-products').each(function(){
			if ( $(this).length > 0 && $(this).find('div.product_list').find('> *').length > 0 ){
				var wrapc = $(this).find('div.product_list');
				var c_param = {
					loop: ( $(this).find('div.product_list').find('> *').length > $(this).data('desktop')) ? true : false,
					dots: false,
					nav: ( $(this).data('usenav')=='1' ) ? true : false,
	                items : $(this).data('desktop'),
	                responsive : {
	                    0 : { items: $(this).data('mobilep') },
	                    480 : { items: $(this).data('mobilel') },
	                    768 : { items: $(this).data('tabletp') },
	                    992 : { items: $(this).data('tabletl') },
	                    1200 : { items: $(this).data('desktop') }
	                },
				}
				if ( $(this).hasClass('special-carousel') ) {
					var carousel_p = setTimeout(function(){
						wrapc.owlCarousel(c_param);
					}, 600);
				}else{
					wrapc.owlCarousel(c_param);
				}
			}
		});
		// Shortcode: SNS Custom Deal
		$('.sns-custom-deal').each(function(){
			if ( $(this).length > 0 && $(this).find('div.product_list').find('> *').length > 0 ){
				var wrapc = $(this).find('div.product_list');
				wrapc.owlCarousel({
					loop: ( $(this).find('div.product_list').find('> *').length > $(this).attr('data-desktop')) ? true : false,
					dots: false,
					nav: true,
	                items : $(this).attr('data-desktop'),
	                responsive : {
	                    0 : { items: $(this).attr('data-mobilep')},
	                    480 : { items: $(this).attr('data-mobilel') },
	                    768 : { items: $(this).attr('data-tablet') },
	                    992 : { items: $(this).attr('data-tablet') },
	                    1200 : { items: $(this).attr('data-desktop') }
	                },
				});
			}
		});
		// sns-sale-count-downt
		$('.sns-sale-count-down').each(function(){
			if ( $(this).length > 0 && $(this).find('div.owl-carousel').find('> *').length > 0 ){
				var wrapc = $(this).find('div.owl-carousel');
				wrapc.owlCarousel({
					loop: ( $(this).find('div.owl-carousel').find('> *').length > $(this).data('desktop')) ? true : false,
					dots: false,
					nav: false,
					autoplay: true, 
	                items : $(this).data('desktop'),
	                responsive : {
	                    0 : { items: $(this).data('mobilep') },
	                    480 : { items: $(this).data('mobilel') },
	                    768 : { items: $(this).data('tabletp') },
	                    992 : { items: $(this).data('tabletl') },
	                    1200 : { items: $(this).data('desktop') }
	                },
				});
			}
		});
		// Move Compare & Wishlist in Product Page
		if ( $('body.single-product .entry-summary form.variations_form').length || $('body.single-product .entry-summary form.cart table.group_table').length ) {
			var wl_c_el = 'body.single-product .entry-summary .yith-wcwl-add-to-wishlist, body.single-product .entry-summary .compare';
			$(wl_c_el).each(function(){
				if ( $(this).length ) {
					$('body.single-product .entry-summary form.cart').append($(this).clone());
					$(this).remove();
				}
			});
		}
		// Carousel Related Product
		if ( $('.related.products .product_list').length ) {
			var number_lg = 4;
			if ( !$('body').hasClass('layout-type-m') ){
				number_lg = 3;
			}
			$('.related.products .product_list').addClass('owl-carousel');
			$('.related.products .product_list').owlCarousel({
                loop: ( $('.related.products .product_list').find('> *').length > number_lg ) ? true : false, 
                autoplay : false, dots: false, nav: true,items : number_lg,
                responsive : {
                    0 : { items: 1},
                    480 : { items: 2 },
                    768 : { items: 3 },
                    992 : { items: number_lg },
                    1200 : { items: number_lg }
                },
            });
		}
		
		// Carousel Upsell Product
		if ( $('.sns-main .upsells.products .product_list').length ) {
			$('.sns-main .upsells.products .product_list').addClass('owl-carousel');
			$('.sns-main .upsells.products .product_list').owlCarousel({
                loop: ( $('.sns-main .upsells.products .product_list').find('> *').length > 4 ) ? true : false, 
                autoplay : false, dots: false, nav: true, items : 4,
                responsive : {
                    0 : { items: 1},
                    480 : { items: 2 },
                    768 : { items: 3 },
                    992 : { items: 4 },
                    1200 : { items: 4 }
                },
            });
		}
		// Carousel Cross sell Product
		if ( $('.cross-sells .products.product_list').length ) {
			$('.cross-sells .products.product_list').addClass('owl-carousel');
			$('.cross-sells .products.product_list').owlCarousel({
                loop: ( $('.cross-sells .products.product_list').find('> *').length > 4 ) ? true : false, 
                autoplay : false, dots: false, nav: true, items : 4,
                responsive : {
                    0 : { items: 1},
                    480 : { items: 2 },
                    768 : { items: 2 },
                    992 : { items: 3 },
                    1200 : { items: 3 }
                },
            });
		}
		// lazyload for woo product
		if( $('body').hasClass('use_lazyload') ){
			$('.product_list, .sns-products-list, .special-product').each(function(){
				if( $(this).length > 0 ){
					$(this).find('img.lazy').lazyload();
				}
			})
		}
		// Click mode view
		$('.mode-view a').click(function(){
            var mode = $(this).data('mode');
            if(!$(this).hasClass('active')){
	            $.ajax({
	                url: ajaxurl,
	                data:{
	                	action : 'sns_setmodeview',
	                	mode : mode
	                },
	                type: 'POST'
	            });
	        }else{
	        	return false;
	        }
	        $('.mode-view a').removeClass('active');
            $('.mode-view a').each(function(){
            	if ( $(this).hasClass(mode) ) $(this).addClass('active');
            })   
            return false;
        });
		$('.mode-view .list').click(function(){         
        	$('#sns_woo_list.product_list').removeClass('grid');
        	$('#sns_woo_list.product_list').addClass('list');
            return false;
        });
		$('.mode-view .grid').click(function(){
        	$('#sns_woo_list.product_list').removeClass('list');
        	$('#sns_woo_list.product_list').addClass('grid');
          
            return false;
        });
		// YITH Filter
        $(document).off('click', '.yith-wcan a').on('click', '.yith-wcan a', function (e) {
        	e.preventDefault();
        	$('html, body').stop().animate({
                scrollTop: $(yith_wcan.scroll_top).offset().top - 20
            }, 600);
            $(this).yith_wcan_ajax_filters(e, this);
        });
		// Rating
		$('.star-rating .value').each(function(){
			if( typeof $(this).attr('data-width') !== typeof undefined ){
				$(this).css('width', $(this).attr('data-width'));
			}
		})
        // Accordion for category
		$('.product-categories').SnsAccordion({
			btn_open: '<span class="ac-tongle open">+</span>',
			btn_close: '<span class="ac-tongle close">-</span>',
		});
		// Click add to cart
		$('.grid-view a.add_to_cart_button.product_type_simple').click(function(e) {
			var $this = $(this);
		});
		$('.list-view a.add_to_cart_button.product_type_simple').each(function() {
		});
		// Added Wishlist
		$('.block-product-inner .yith-wcwl-wishlistaddedbrowse a, .block-product-inner .yith-wcwl-wishlistexistsbrowse a').each(function(){
			$(this).attr('data-toggle', 'tooltip').attr('data-original-title', $(this).text().trim());
		});
	    // Tab information
	    if ( $('#sns_tab_informations').length ) {
		    if (window.location.href.indexOf('#comments') > 0 ) {
				$('#sns_tab_informations .nav-tabs').find("li.reviews_tab").addClass("active");
	    		$('#sns_tab_informations .tab-content').find("#tab-reviews").addClass("active in");
			}else{
	    		$('#sns_tab_informations .nav-tabs').find("li").first().addClass("active");
	    		$('#sns_tab_informations .tab-content').find(".tab-pane").first().addClass("active in");
	    	}
	    	$('#sns_tab_informations .nav-tabs').tabdrop();
	    }
	    // Click next/prev show lazyload
	    $('.owl-carousel .owl-nav div').each(function(){
			$(this).on('click', function(){
				if( $('body').hasClass('use_lazyload') ){
					var parent_owl = $(this).parents('.owl-carousel');
					var timeout = setTimeout(function() {
				        parent_owl.find("img.lazy:not(.loaded)").trigger("appear")
				    }, 1000);
				}
			});
		});
	    // Click filter on handle device
	    $('.yith-wcan a').on('click', function (e) {
	        $('.sns-icon-nav .btn2').each(function(){
	        	if( $(this).hasClass('active') ){
	        		$('#sns_content .sns-left, #sns_content .sns-right').removeClass('active');
	        		$(this).find('.overlay').fadeOut(250); $(this).removeClass('active');
	        	}
	        })
	    });
	    // Product Tabs Naviation
		if( $('#sns-producttabs-navigation').length && $('.sns-ajaxtab').length ){
			$('#sns-producttabs-navigation ul.product-tab-nav-list').html('');
			var len = $('.sns-ajaxtab').length;
			$('.sns-ajaxtab').each(function(index, value){
				// Check is first and last element
				if(index == 0){
					$(this).addClass('first');
				}else if(index == len - 1){
					$(this).addClass('last');
				}
				var $_this_id, $_this_iconnav, $content_li = '';

				$_this_id = $(this).attr('id');
				$_this_iconnav = $(this).attr('data-iconnav');
				// append nav list
				if( $_this_iconnav !== undefined ){
					$content_li = '<li>'
								+ '<a href="#'+ $_this_id +'" title="" data-id="'+ $_this_id +'" style="background:'+ $(this).attr('data-color') +'; color:'+ $(this).attr('data-color') +'">'
								+ '<span class="icon-nav"><img src="'+ $_this_iconnav +'" alt=""/></span>'
								+ '</a>'
								+ '</li>';
					$('#sns-producttabs-navigation ul.product-tab-nav-list').append($content_li);
				}
			});
			// Detect scroll
			$(window).scroll(function() {
				var scrollTop = $(this).scrollTop();
				$('.sns-ajaxtab').each(function(){
					var topDistance = $(this).offset().top;
					// Display #sns-producttabs-navigation
					if ( (topDistance - 200) < scrollTop) {
						if( $(this).hasClass('first') ){
			        		$('#sns-producttabs-navigation').removeClass('active').addClass('active');
			        	}
					}else if((topDistance - 200) > scrollTop){
						if( $(this).hasClass('first') ){
			        		$('#sns-producttabs-navigation').removeClass('active');
			        	}
					}
					if((topDistance + 100) < scrollTop){
						if( $(this).hasClass('last') ){
			        		$('#sns-producttabs-navigation').removeClass('active');
			        	}
					}
					//
			        if ( (topDistance - 40) < scrollTop) {
			            var $_this_id = $(this).attr('id');
						$('#sns-producttabs-navigation ul.product-tab-nav-list li').each(function(){
							// remove class active
							$(this).find('a').removeClass('active');
							if( $_this_id == $(this).find('a').attr('data-id') ){
								$(this).find('a').addClass('active');
							}
						});
			        }
				});
			});
			//
			$('#sns-producttabs-navigation ul.product-tab-nav-list li').each(function(){
				var $this = $(this);
				$this.find('a').click(function(){
					// remove class active
					$this.find('a').removeClass('active');
					var $_id = $(this).attr('href');
					$("html, body").animate({ scrollTop: $($_id).offset().top - 20}, 800);
					// $(this).addClass('active');
					return false;
				});
			});
		}

		//
		$('.variations-product-wrap .variable-item a.option').each(function(){
			if ( !$(this).length ) return;
			var $img_src = '';
			var $variable_prd = $(this).parents('.product-inner');
			var $product_image = $variable_prd.find('a.product-image img');
			if ( $product_image.hasClass('lazy') ) {
				$img_src = $product_image.data('original');
			}else{
				$img_src = $product_image.attr('src');
			}
			if ( $img_src == $(this).attr('data-image-src') ) {
				$(this).addClass('active');
			}
			$(this).on('click', function(e){
				e.preventDefault();
				if ($(this).hasClass('active') ) return false;
				$variable_prd.find('.variations-product-wrap .variable-item a.option').removeClass('active');
				var $data_image_src = $(this).attr('data-image-src');
				if( $data_image_src != ''){
					$variable_prd.find('.product-image img').attr('src', $data_image_src);
					$variable_prd.find('.product-image img').attr('srcset', '');
					$variable_prd.find('.product-image img').attr('sizes', '');
				}else{
				 	$variable_prd.find('.product-image img').attr('src', $img_src);
				}
				$(this).addClass('active');
				return false;
			});
		});
	   	
		// Ajax complete
		jQuery(document).ajaxComplete(function(e, xhr, settings) {
			// Click Quick View
			if ( typeof settings.data != 'undefined' && settings.data.match(/yith_load_product_quick_view/i) ) {
				if( $('#yith-quick-view-modal .product.type-product > .gallery_type_h').length ){
					SnsSinglePrdIHG.init('#yith-quick-view-modal .gallery_type_h');
				}
				if( $('#yith-quick-view-modal .product.type-product > .gallery_type_v').length ){
					SnsSinglePrdIVG.init('#yith-quick-view-modal .gallery_type_v');
				}
				SnsVariationForm.init();
			}
			// Click Wishlist
			if ( typeof settings.data != 'undefined' && settings.data.match(/add_to_wishlist/i) ) {
				if ( $('.mini-wishlist .tongle .number').length ) {
					var num_wl = $('.mini-wishlist .tongle .number').html() - 1;
					if ( $('body').hasClass('header-style2') ) {
						$('.mini-wishlist .tongle .number').html('('+ num_wl +')');
					}else{
						$('.mini-wishlist .tongle .number').html(num_wl);
					}
				}
			}
			// lazyload for woo product
			if( $('body').hasClass('use_lazyload') ){
				var timeout = setTimeout(function() {
					$(".sns-main img.lazy:not(.loaded)").lazyload();
				}, 1000);
			}
			// Wishlist & compare
			$('.block-product-inner .yith-wcwl-wishlistaddedbrowse a, .block-product-inner .yith-wcwl-wishlistexistsbrowse a').each(function(){
				$(this).attr('data-toggle', 'tooltip').attr('data-original-title', $(this).text().trim());
			});
			// View cart
			$('.products .added_to_cart').each(function(){
				if( $(this).text().trim() != '') $(this).attr('data-toggle', 'tooltip').attr('data-original-title', $(this).text().trim());
			});
			// Mini cart number
			if ( $('.sns-ajaxcart .sns-cart-number').length ) {
				if ( $('body').hasClass('header-style2') ) {
					$('.sns-ajaxcart .tongle .number').html('('+$('.sns-ajaxcart .sns-cart-number').html().trim()+')');
				}else{
					$('.sns-ajaxcart .tongle .number').html($('.sns-ajaxcart .sns-cart-number').html().trim());
				}
			}
			// Click filter on handle device
		    $('.yith-wcan a').on('click', function (e) {
		        $('.sns-icon-nav .btn2').each(function(){
		        	if( $(this).hasClass('active') ){
		        		$('#sns_content .sns-left, #sns_content .sns-right').removeClass('active');
		        		$(this).find('.overlay').fadeOut(250); $(this).removeClass('active');
		        	}
		        })
		    });
			//
			$('.variations-product-wrap .variable-item a.option').each(function(){
				if ( !$(this).length ) return;
				var $img_src = '';
				var $variable_prd = $(this).parents('.product-inner');
				var $product_image = $variable_prd.find('a.product-image img');
				if ( $product_image.hasClass('lazy') ) {
					$img_src = $product_image.data('original');
				}else{
					$img_src = $product_image.attr('src');
				}
				if ( $img_src == $(this).attr('data-image-src') ) {
					$(this).addClass('active');
				}
				$(this).on('click', function(e){
					e.preventDefault();
					if ($(this).hasClass('active') ) return false;
					$variable_prd.find('.variations-product-wrap .variable-item a.option').removeClass('active');
					var $data_image_src = $(this).attr('data-image-src');
					if( $data_image_src != ''){
						$variable_prd.find('.product-image img').attr('src', $data_image_src);
						$variable_prd.find('.product-image img').attr('srcset', '');
						$variable_prd.find('.product-image img').attr('sizes', '');
					}else{
					 	$variable_prd.find('.product-image img').attr('src', $img_src);
					}
					$(this).addClass('active');
					return false;
				});
			});
		});
	});
    
	// Begin Class: SnsVariationForm
	var SnsVariationForm = {
        init: function() {
            var self = this;
            //
            if ( $('form.variations_form').length ) {
	            var $product        = $('.variations_form').closest( '.product' );
	            var $product_img    = $product.find( 'div.product-images .woocommerce-main-image' );
	            $product.data('img-osrc', $product_img.attr('src'));
	            $product.data('img-title', $product_img.attr('title'));
	            $product.data('img-src', $product_img.attr('data-src'));
	        }
            $( document ).on( 'reset_image', '.variations_form', function(event) { // alert('dddd');
                var $product        = $(this).closest( '.product' );
                var $product_images = $product.find('.product-images');
                var $product_thumbs = $product.find('.product-thumbs');
                // Begin: Switch to first Image
                if ($product_images.length) {
                    $product_images.trigger('to.owl.carousel', [0, 300, true]);
                }
                // Swap Image Zoom
                var $product_img    = $product.find( 'div.product-images .woocommerce-main-image' );
                var o_src           = $product_img.attr('data-o_src');
                var o_title         = $product_img.attr('data-o_title');
                var src          = $product_img.attr('data-src');
                if ( o_src && o_title && src ) {
                    $product_img.each(function() {
                    	var elevateZoom = $(this).data('elevateZoom');
                    	if (typeof elevateZoom != 'undefined') {
                        	elevateZoom.swaptheimage($(this).attr( 'src' ), $(this).attr( 'data-src' ));
                        }
                    });
                    // Switch Button Popup to first Image
                	var imgsrc = $product_images.data('imgsrc'), imgtitle = $product_images.data('imgtitle');
                    if (typeof imgsrc != 'undefined' && typeof imgtitle != 'undefined') {
                        imgsrc[0] = src; imgtitle[0] = o_title;
                    }
                    $product_images.data('imgsrc', imgsrc); $product_images.data('imgtitle', imgtitle);
                }
                // Begin: Switch to first Thumb
                if ($product_thumbs.length) {
                    $product_thumbs.trigger('to.owl.carousel', [0, 300, true]);
                    $product_thumbs.find('.owl-item:eq(0)').click();
                }
                // Replace first thumb src
                var $thumb_img      = $product.find( '.woocommerce-main-thumb' );
                var o_thumb_src     = $thumb_img.attr('data-o_src');
                if (o_thumb_src) {
                    $thumb_img.attr( 'src', o_thumb_src ).attr( 'srcset', '' );
                }
            });
            $( document ).on( 'click', '.variations_form .reset_variations', function(event) {
            	var $product 			= $(this).closest( '.product' );
            	var $product_images 	= $product.find('.product-images');
                var $product_img     	= $product.find( 'div.product-images .woocommerce-main-image' );
                // var $thumb_img 			= $product.find( '.woocommerce-main-thumb');
                $product_img.each(function() {
                	$product_img.attr( 'src', $product.data('img-osrc') )
                				.attr( 'data-o_src', $product.data('img-osrc') )
                				.attr( 'srcset', '' )
                				.attr( 'alt', $product.data('img-title') )
                				.attr( 'data-src', $product.data('img-src') );
                	var elevateZoom = $(this).data('elevateZoom');
                	if (typeof elevateZoom != 'undefined') {
                    	elevateZoom.swaptheimage($(this).attr( 'src' ), $(this).attr( 'data-src' ));
                    }
                });
                var imgsrc = $product_images.data('imgsrc'), imgtitle = $product_images.data('imgtitle');
                imgsrc[0] = $product.data('img-src'); imgtitle[0] = $product.data('img-title');
                $product_images.data('imgsrc', imgsrc); $product_images.data('imgtitle', imgtitle);
            });
            $( document ).on( 'found_variation', '.variations_form', function(event, variation) {
                if (typeof variation == 'undefined') {
                    return;
                }
                var $product 			= $(this).closest( '.product' );
                var $product_images 	= $product.find('.product-images');
                var $product_thumbs 	= $product.find('.product-thumbs');
                // Begin: Switch to first Image
                if ($product_images.length) {
                    $product_images.trigger('to.owl.carousel', [0, 300, true]);
                }
                // Begin: Switch to first Thumb
                if ($product_thumbs.length) {
                    $product_thumbs.trigger('to.owl.carousel', [0, 300, true]);
                    $product_thumbs.find('.owl-item:eq(0)').click();
                }
                var $product_img     		= $product.find( 'div.product-images .woocommerce-main-image' );
                var $thumb_img 				= $product.find( '.woocommerce-main-thumb');
                var variation_image 		= variation.image.full_src; //alert(variation.image.full_src); console.log(variation);
                var variation_link 			= variation.image.src;
                var variation_title 		= variation.image.caption;
                var variation_thumb 		= ( typeof variation.image.thumb_src != 'undefined' ) ? variation.image.thumb_src : variation.image.src ;
                
                // Replace Image & Thumb Src
                var o_src = $product_img.attr('data-o_src');
                if ( ! o_src ) {
                    o_src = ( ! $product_img.attr('src') ) ? '' : $product_img.attr('src');
                    $product_img.attr('data-o_src', o_src );
                }
                var src = $product_img.attr('data-src');
                if ( ! src ) {
                    src = ( ! $product_img.attr('data-src') ) ? '' : $product_img.attr('data-src');
                    $product_img.attr('data-src', src );
                }
                var o_title = $product_img.attr('data-o_title');
                if ( ! o_title ) {
                    o_title = ( ! $product_img.attr('alt') ) ? '' : $product_img.attr('alt');
                    $product_img.attr('data-o_title', o_title );
                }
                var o_thumb_src = $thumb_img.attr('data-o_src');
                if ( ! o_thumb_src ) {
                    o_thumb_src = ( ! $thumb_img.attr('src') ) ? '' : $thumb_img.attr('src');
                    $thumb_img.attr('data-o_src', o_thumb_src );
                }
                var imgsrc = $product_images.data('imgsrc'), imgtitle = $product_images.data('imgtitle');
                if ( variation_link ) {
                    $product_img.attr( 'src', variation_link ).attr( 'data-o_src', variation_link ).attr( 'srcset', '' ).attr( 'alt', variation_title ).attr( 'data-src', variation_image );
                    $thumb_img.attr( 'src', variation_thumb ).attr( 'srcset', '' );
                    if (typeof imgsrc != 'undefined' && typeof imgtitle != 'undefined') {
                        imgsrc[0] = variation_image; imgtitle[0] = variation_title;
                    }
                } else {
                    $product_img.attr( 'src', o_src ).attr( 'data-o_src', o_src ).attr( 'srcset', '' ).attr( 'alt', o_title ).attr( 'data-src', src );
                    $thumb_img.attr( 'src', o_thumb_src ).attr( 'srcset', '' );
                    if (typeof imgsrc != 'undefined' && typeof imgtitle != 'undefined') {
                        imgsrc[0] = src; imgtitle[0] = o_title;
                    }
                }
                // Switch Button Popup to first Image
                $product_images.data('imgsrc', imgsrc); $product_images.data('imgtitle', imgtitle);
                // Swap Image Zoom
                $product_img.each(function() {
                	var elevateZoom = $(this).data('elevateZoom');
                	if (typeof elevateZoom != 'undefined') {
                    	elevateZoom.swaptheimage($(this).attr( 'src' ), $(this).attr( 'data-src' ));
                    }
                });
            });
        },
        reDesignVariationForm: function( $wrap ) {
        	if ( typeof $wrap == 'undefined') var $wrap = '';
        	if ( $wrap != '' ) {
	    		$wrap = $wrap+' .type-product.product-type-variable';
	    	}else{
	    		$wrap = '.type-product.product-type-variable';
	    	}
			if ( $($wrap).length  && typeof sns_arr_attr !== 'undefined' ) { 
				$($wrap).find('.variations select').each(function(){
					var select = $(this), select_div, var_attr = sns_arr_attr[select.attr('name')];
					if ( typeof var_attr == 'undefined' ) { return false; } 
					select_div = $('<div />', {
				                	'class': 'sellect-wrap'
				            	}).insertAfter(select);
					select.hide();
					select.find( 'option' ).each(function (){
						var option_old = $(this), option;
						if ( option_old.attr('value')!='' ) {
							var inner_opt, class_sellect, val_opt = var_attr.key_val[option_old.attr('value')];
							if (var_attr.type == 'color') {
								if (val_opt.indexOf("http:") == 0 || val_opt.indexOf("https:") == 0) {
									inner_opt = $('<span/>', {
												'style':'background-image:url("' + val_opt + '")'
											});
									class_sellect = ' image';
								}else if (val_opt.indexOf("#") == 0 ){
				                    inner_opt = $('<span/>', {
													'style':'background:' + val_opt
												});
									class_sellect = ' color';
								}else{
									inner_opt = $('<span/>', {
												'html': val_opt
											});
									class_sellect = ' text';
								}
			                }else if (var_attr.type == 'text') {
			                    inner_opt = $('<span/>', {
												'html': val_opt
											});
								class_sellect = ' text';
			                }
			                option = $('<div/>', {
				                        'class': 'option'+class_sellect,
				                        'data-toggle':'tooltip',
				                        'data-original-title':option_old.text(),
				                        'data-value': option_old.attr('value')
				                    }).appendTo(select_div);
		                    inner_opt.appendTo(option);
							if ( option_old.val() == select.val() ){
								option.addClass('selected');
							}
		                    option.on('click', function () {
		                    	// Update variation values
		                        if ( $(this).hasClass('selected') ) {
		                            select.val('').change();
		                        } else {
		                            select.val( option_old.val() ).change();
		                        }
		                        SnsVariationForm.setSelectedOpt( $(this) );
		                    });
		                }
					});
				});
				$( document ).on( 'click', '.variations_form .reset_variations', function(event) {
					$('.variations_form .sellect-wrap .option').removeClass('selected');
				});
			}
		},
		setSelectedOpt: function( option ) {
	        option.toggleClass('selected');
	        option.siblings().removeClass('selected');
	    },
    }
    // End Class: SnsVariationForm
    // Begin Class: SnsSinglePrdING
    var SnsSinglePrdING = {
		init: function( $wrap ) {
			var self = this, flag_switch = false;
			if ( typeof sns_sp_var == 'undefined' ) return false;
	    	//if ( sns_sp_var['zoom'] == '1' && ( !('ontouchstart' in document) || ( ('ontouchstart' in document) && sns_sp_var['zoommobile'] == '1' ) ) ) {
	    		var zoomcfg = {
		            responsive: true, zoomType: sns_sp_var['zoomtype'], cursor: 'grab'
		        };
		        if ( sns_sp_var['zoomtype'] == 'inner' ) {
		        	zoomcfg.borderSize = 0;
		        }else if ( sns_sp_var['zoomtype'] == 'lens' ) {
		        	zoomcfg.lensSize = sns_sp_var['lenssize']; zoomcfg.lensShape = sns_sp_var['lensshape'];
		        }
	    	//}
	    	if ( typeof $wrap == 'undefined') var $wrap = '';
	    	if ( $wrap != '' ) {
	    		$wrap = $wrap+' .product-images';
	    	}else{
	    		$wrap = '.product-images';
	    	}
	        $($wrap).each(function() {
	            var $this = $(this),
	                $product = $this.closest('.product'),
	                image_active = 0;
	                // Button click to popup Images
	                if ( sns_sp_var['poup'] == '1' ) {
	                    var imgsrc = [], imgtitle = [], i = 0;
	                    var $popup_btn = $product.find('.popup-image');
	                    var $popup_video_btn = $product.find('.popup-video');
	                    $this.find('img').each(function() {
	                        imgsrc[i] = $(this).attr('data-src'); imgtitle[i] = $(this).attr('alt');
	                        i++;
	                    });
	                    $this.data('imgsrc', imgsrc); $this.data('imgtitle', imgtitle);
	                    if (typeof $.fn.prettyPhoto !== 'undefined') {
		                    $($popup_btn).off('click').on('click', function(e) {
		                        e.preventDefault();
		                        $.prettyPhoto.open($this.data('imgsrc'), [], $this.data('imgtitle'), image_active);
		                    });
		                    $popup_video_btn.prettyPhoto({
								hook: 'data-rel', default_width: 750, default_height: 600, social_tools: false, theme: 'pp_woocommerce', horizontal_padding: 20, opacity: 0.8, deeplinking: false
							});
		                }
	                }
	            //if ( sns_sp_var['zoom'] == '1' && ( !('ontouchstart' in document) || ( ('ontouchstart' in document) && sns_sp_var['zoommobile'] == '1' ) ) ) {
	                $this.find('img').each(function() {
                    	zoomcfg.zoomContainer = $(this).parent();
    					$(this).elevateZoom(zoomcfg);
    					$(window).on('resize', function () {
	                            $this.find('img').each(function() {
	                                var elevateZoom = $(this).data('elevateZoom');
	                                if (typeof elevateZoom != 'undefined') {
	                                    elevateZoom.startZoom();
	                                    elevateZoom.swaptheimage($(this).attr('src'), $(this).attr('data-src'));
	                                }
	                            });
	                    });
	                });
	            //}
	        });
	    },
	};
    // End Class: SnsSinglePrdING
    // Begin Class: SnsSinglePrdIHG
	var SnsSinglePrdIHG = {
		init: function( $wrap ) {
			var self = this, flag_switch = false;
			if ( typeof sns_sp_var == 'undefined' ) return false;
	    	if ( sns_sp_var['zoom'] == '1' && ( !('ontouchstart' in document) || ( ('ontouchstart' in document) && sns_sp_var['zoommobile'] == '1' ) ) ) {
	    		var zoomcfg = {
		            responsive: true, zoomType: sns_sp_var['zoomtype'], cursor: 'grab'
		        };
		        if ( sns_sp_var['zoomtype'] == 'inner' ) {
		        	zoomcfg.borderSize = 0;
		        }else if ( sns_sp_var['zoomtype'] == 'lens' ) {
		        	zoomcfg.lensSize = sns_sp_var['lenssize']; zoomcfg.lensShape = sns_sp_var['lensshape'];
		        }
	    	}
	    	if ( typeof $wrap == 'undefined') var $wrap = '';
	    	if ( $wrap != '' ) {
	    		$wrap = $wrap+' .product-images';
	    	}else{
	    		$wrap = '.product-images';
	    	}
	        $($wrap).each(function() {
	            var $this = $(this),
	                $product = $this.closest('.product'),
	                $product_thumbs = $product.find('.product-thumbs'),
	                image_active = 0;
	            $this.find('img:first-child').waitForImages(true).done(function() {
		            // Carousel for Thumbs
	                $product_thumbs.owlCarousel({
	                    loop: false, autoplay : false, rewind: true, dots: false, nav: true,
	                    items : sns_sp_var['thumbnum'],
	                    responsive : {
		                    0 : { items: sns_sp_var['thumbnum']-1},
		                    480 : { items: sns_sp_var['thumbnum']-1 },
		                    768 : { items: sns_sp_var['thumbnum']-1 },
		                    992 : { items: sns_sp_var['thumbnum'] },
		                    1200 : { items: sns_sp_var['thumbnum'] }
		                },
	                    onInitialized: function() {
	                        self.switchImageThumb(null, $product_thumbs, 0);
	                        if ($product_thumbs.find('.owl-item').length < sns_sp_var['thumbnum']) {
	                        	$product_thumbs.find('.owl-nav').hide();
	                        }	
	                    }
	                }).on('click', '.owl-item', function() {
	                    self.switchImageThumb($this, $product_thumbs, $(this).index());
	                });
	                $product_thumbs.on('click', '.owl-prev', function(e) {
	                    self.switchImageThumb($this, $product_thumbs, $product_thumbs.data('thumbselected') - 1);
	                });
	                $product_thumbs.on('click', '.owl-next', function(e) {
	                    self.switchImageThumb($this, $product_thumbs, $product_thumbs.data('thumbselected') + 1);
	                });
	                // Button click to popup Images
	                if ( sns_sp_var['poup'] == '1' ) {
	                    var imgsrc = [], imgtitle = [], i = 0;
	                    var $popup_btn = $product.find('.popup-image');
	                    var $popup_video_btn = $product.find('.popup-video');
	                    $this.find('img').each(function() {
	                        imgsrc[i] = $(this).attr('data-src'); imgtitle[i] = $(this).attr('alt');
	                        i++;
	                    });
	                    $this.data('imgsrc', imgsrc); $this.data('imgtitle', imgtitle);
	                    if (typeof $.fn.prettyPhoto !== 'undefined') {
		                    $($popup_btn).off('click').on('click', function(e) {
		                        e.preventDefault();
		                        $.prettyPhoto.open($this.data('imgsrc'), [], $this.data('imgtitle'), image_active);
		                    });
		                    $popup_video_btn.prettyPhoto({
								hook: 'data-rel', default_width: 750, default_height: 600, social_tools: false, theme: 'pp_woocommerce', horizontal_padding: 20, opacity: 0.8, deeplinking: false
							});
		                }
	                }
	                // Carousel for Images
	                $this.owlCarousel({
	                    autoplay : false, items : 1, rewind: true, nav: true, navText: ["", ""], dots: false, loop : false,
	                    onInitialized : function() {
	                        if ( sns_sp_var['zoom'] == '1' && ( !('ontouchstart' in document) || ( ('ontouchstart' in document) && sns_sp_var['zoommobile'] == '1' ) ) ) {
	                            $this.find('img').each(function() {
	                                var $this = $(this);
	                                zoomcfg.zoomContainer = $this.parent();
	                                $this.elevateZoom(zoomcfg);
	                            });
	                        }
	                    },
	                    onTranslate : function(e) {
	                        image_active = this._current - $this.find('.cloned').length / 2;
	                        self.switchImageThumb(null, $product_thumbs, image_active);
	                    },
	                    onRefreshed: function() {
	                        if ( sns_sp_var['zoom'] == '1' && ( !('ontouchstart' in document) || ( ('ontouchstart' in document) && sns_sp_var['zoommobile'] == '1' ) ) ) {
	                            $this.find('img').each(function() {
	                                var elevateZoom = $(this).data('elevateZoom');
	                                if (typeof elevateZoom != 'undefined') {
	                                    elevateZoom.startZoom();
	                                    elevateZoom.swaptheimage($(this).attr('src'), $(this).attr('data-src'));
	                                }
	                            });
	                        }
	                    }
	                });
	            });
	        });
	    },
	    switchImageThumb: function($product_images, $product_thumbs, index) {
	        if (self.flag_switch) return;
	        self.flag_switch = true;
	        var thumb_leng = $product_thumbs.find('.owl-item').length, thumbs_active = [], i = 0;
	        index = (index + thumb_leng) % thumb_leng;
	        $product_thumbs.find('.owl-item').removeClass('selected');
	        $product_thumbs.find('.owl-item:eq(' + index + ')').addClass('selected');
	        $product_thumbs.data('thumbselected', index);
	        $product_thumbs.find('.owl-item.active').each(function() {
	            thumbs_active[i++] = $(this).index();
	        });
	        // Switch Thumb
	        if ($.inArray(index, thumbs_active) == -1) {
	            if (Math.abs(index - thumbs_active[0]) > Math.abs(index - thumbs_active[thumbs_active.length - 1])) {
	                $product_thumbs.trigger('to.owl.carousel', [(index - thumbs_active.length + 1) % thumb_leng, 300, true]);
	            } else {
	                $product_thumbs.trigger('to.owl.carousel', [index % thumb_leng, 300, true]);
	            }
	        }
	        // Switch Image
	        if ($product_images) {
	            $product_images.trigger('to.owl.carousel', [index, 300, true]);
	        }
	        self.flag_switch = false;
	    }
	};
	// End Class: SnsSinglePrdIHG
	// Begin Class: SnsSinglePrdIVG
	var SnsSinglePrdIVG = {
		init: function( $wrap ) {
			if ( typeof sns_sp_var == 'undefined' ) return false;
	    	if ( sns_sp_var['zoom'] == '1' && ( !('ontouchstart' in document) || ( ('ontouchstart' in document) && sns_sp_var['zoommobile'] == '1' ) ) ) {
	    		var zoomcfg = {
		            responsive: true, zoomType: sns_sp_var['zoomtype'], cursor: 'grab'
		        };
		        if ( sns_sp_var['zoomtype'] == 'inner' ) {
		        	zoomcfg.borderSize = 0;
		        }else if ( sns_sp_var['zoomtype'] == 'lens' ) {
		        	zoomcfg.lensSize = sns_sp_var['lenssize']; zoomcfg.lensShape = sns_sp_var['lensshape'];
		        }
	    	}
	    	if ( typeof $wrap == 'undefined') var $wrap = '';
	    	if ( $wrap != '' ) {
	    		$wrap = $wrap+' .product-images';
	    	}else{
	    		$wrap = '.product-images';
	    	}
	        $($wrap).each(function() {
	            var $this = $(this),
	                $product = $this.closest('.product'),
	                $product_thumbs = $product.find('.product-thumbs'),
	                image_active = 0;
	            $this.find('img:first-child').waitForImages(true).done(function() {
		            // Slider for Thumbs
	                $product_thumbs.slick({
						vertical: true,
						asNavFor: '.entry-img .images .product-images',
						focusOnSelect: true,
						infinite: false,
						arrows: true,
						slidesToScroll: 1,
						slidesToShow: ($product_thumbs.find('.img').size()>=4) ? 4 : $product_thumbs.find('.img').size() ,
					});
	                // Button click to popup Images
	                if ( sns_sp_var['poup'] == '1' ) {
	                    var imgsrc = [], imgtitle = [], i = 0;
	                    var $popup_btn = $product.find('.popup-image');
	                    var $popup_video_btn = $product.find('.popup-video');
	                    $this.find('img').each(function() {
	                        imgsrc[i] = $(this).attr('data-src'); imgtitle[i] = $(this).attr('alt'); i++;
	                    });
	                    $this.data('imgsrc', imgsrc); $this.data('imgtitle', imgtitle);
	                    if (typeof $.fn.prettyPhoto !== 'undefined') {
		                    $($popup_btn).off('click').on('click', function(e) {
		                        e.preventDefault();
		                        $.prettyPhoto.open($this.data('imgsrc'), [], $this.data('imgtitle'), image_active);
		                    });
		                    $popup_video_btn.prettyPhoto({
								hook: 'data-rel', default_width: 750, default_height: 600, social_tools: false, theme: 'pp_woocommerce', horizontal_padding: 20, opacity: 0.8, deeplinking: false
							});
		                }
	                }
	                // Zoom for image
	                if ( sns_sp_var['zoom'] == '1' && ( !('ontouchstart' in document) || ( ('ontouchstart' in document) && sns_sp_var['zoommobile'] == '1' ) ) ) {
                        $this.find('img').each(function() {
                            var $this = $(this);
                            zoomcfg.zoomContainer = $this.parent();
                            $this.elevateZoom(zoomcfg);
                        });
                    }
                    // Slider for Images
	                $this.slick({
						vertical: false,
						asNavFor: '.entry-img .thumbnails .product-thumbs',
						focusOnSelect: true,
						infinite: false,
						slidesToScroll: 1,
						slidesToShow: 1,
						arrows: false
					}).on('afterChange', function(event, slick, currentSlide){
						image_active = $this.slick('slickCurrentSlide');
					});
	            });
	        });
	    }
	};
	// End Class: SnsSinglePrdIVG
})(jQuery);
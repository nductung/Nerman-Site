<?php get_header(); ?>
<!-- Content -->
<div id="sns_content">
	<div class="container">
		<div class="row sns-content sns-woocommerce-page">
			<!-- left sidebar -->
			<?php snsvicky_leftcol(); ?>
			<!-- Main content -->
			<div class="<?php echo snsvicky_maincolclass(); ?>">
			    <?php
		    	if( is_product() ){
					wc_get_template( 'single-product.php' );
				}else{
					wc_get_template( 'archive-product.php' );
				}
				?>
			</div>
			<!-- Right sidebar -->
			<?php snsvicky_rightcol(); ?>
		</div>
	</div>
</div>
<!-- End Content -->
<?php get_footer(); ?>
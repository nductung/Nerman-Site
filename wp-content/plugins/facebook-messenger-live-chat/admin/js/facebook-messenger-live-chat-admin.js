jQuery(document).ready(function($) {
	 	
    $('#mwb_fmlcrt_display_select').select2(
		{
		    minimumResultsForSearch: -1
		}
	);
	$('#mwb_fmlcrt_display_pages_except_select').select2();
	$('#mwb_fmlcrt_display_pages_only_select').select2();
	$('#mwb_fmlcrt_device_display_select').select2(
		{
		    minimumResultsForSearch: -1
		}
	);
	$('#mwb_fmlcrt_min_behaviour_select').select2(
		{
		    minimumResultsForSearch: -1
		}
	);

	$('#mwb_fmlcrt_lang_select').select2();


	$('#mwb_fmlcrt_display_select').change( function(){

		var select_option = $(this).val();

		if( select_option == 'eve'){
			$('#mwb_fmlcrt_display_pages_eve').removeClass('hidden');
			$('#mwb_fmlcrt_display_pages_except').addClass('hidden');
			$('#mwb_fmlcrt_display_pages_only').addClass('hidden');
			$('#mwb_fmlcrt_display_pages_home').addClass('hidden');
		}
		else if( select_option == 'eve_except'){
			$('#mwb_fmlcrt_display_pages_eve').addClass('hidden');
			$('#mwb_fmlcrt_display_pages_except').removeClass('hidden');
			$('#mwb_fmlcrt_display_pages_only').addClass('hidden');
			$('#mwb_fmlcrt_display_pages_home').addClass('hidden');
		}
		else if( select_option == 'only_these'){
			$('#mwb_fmlcrt_display_pages_eve').addClass('hidden');
			$('#mwb_fmlcrt_display_pages_except').addClass('hidden');
			$('#mwb_fmlcrt_display_pages_only').removeClass('hidden');
			$('#mwb_fmlcrt_display_pages_home').addClass('hidden');
		}
		else if( select_option == 'only_home'){
			$('#mwb_fmlcrt_display_pages_eve').addClass('hidden');
			$('#mwb_fmlcrt_display_pages_except').addClass('hidden');
			$('#mwb_fmlcrt_display_pages_only').addClass('hidden');
			$('#mwb_fmlcrt_display_pages_home').removeClass('hidden');
		}
	});

	$('#mwb_fmlcrt_min_behaviour_select').change( function(){

		var select_option = $(this).val();

		if( select_option == ''){
			$('#mwb_fmlcrt_min_behaviour_default').removeClass('hidden');
			$('#mwb_fmlcrt_min_behaviour_true').addClass('hidden');
			$('#mwb_fmlcrt_min_behaviour_false').addClass('hidden');
		}
		else if( select_option == 'true'){
			$('#mwb_fmlcrt_min_behaviour_default').addClass('hidden');
			$('#mwb_fmlcrt_min_behaviour_true').removeClass('hidden');
			$('#mwb_fmlcrt_min_behaviour_false').addClass('hidden');
		}
		else if( select_option == 'false'){
			$('#mwb_fmlcrt_min_behaviour_default').addClass('hidden');
			$('#mwb_fmlcrt_min_behaviour_true').addClass('hidden');
			$('#mwb_fmlcrt_min_behaviour_false').removeClass('hidden');
		}
	});

});

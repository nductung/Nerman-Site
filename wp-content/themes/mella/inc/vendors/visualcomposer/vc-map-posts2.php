<?php

if ( function_exists('vc_map') && class_exists('WPBakeryShortCode') ) {

    function mella_get_post_categories() {
        $return = array( esc_html__(' --- Choose a Category --- ', 'mella') => '' );

        $args = array(
            'type' => 'post',
            'child_of' => 0,
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => false,
            'hierarchical' => 1,
            'taxonomy' => 'category'
        );

        $categories = get_categories( $args );
        mella_get_post_category_childs( $categories, 0, 0, $return );

        return $return;
    }

    function mella_get_post_category_childs( $categories, $id_parent, $level, &$dropdown ) {
        foreach ( $categories as $key => $category ) {
            if ( $category->category_parent == $id_parent ) {
                $dropdown = array_merge( $dropdown, array( str_repeat( "- ", $level ) . $category->name => $category->slug ) );
                unset($categories[$key]);
                mella_get_post_category_childs( $categories, $category->term_id, $level + 1, $dropdown );
            }
        }
	}

	function mella_load_post2_element() {
		$layouts = array(
			esc_html__('Grid', 'mella') => 'grid',
			esc_html__('List', 'mella') => 'list',
			esc_html__('Carousel', 'mella') => 'carousel',
		);
		$columns = array(1,2,3,4,6);
		$categories = array();
		if ( is_admin() ) {
			$categories = mella_get_post_categories();
		}
		vc_map( array(
			'name' => esc_html__( 'Apus Grid Posts', 'mella' ),
			'base' => 'apus_gridposts',
			'icon' => 'icon-wpb-news-12',
			"category" => esc_html__('Apus Post', 'mella'),
			'description' => esc_html__( 'Create Post having blog styles', 'mella' ),
			'params' => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Title', 'mella' ),
					'param_name' => 'title',
					'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'mella' ),
					"admin_label" => true
				),
				array(
	                "type" => "dropdown",
	                "heading" => esc_html__('Category','mella'),
	                "param_name" => 'category',
	                "value" => $categories
	            ),
	            array(
	                "type" => "dropdown",
	                "heading" => esc_html__('Order By','mella'),
	                "param_name" => 'orderby',
	                "value" => array(
	                	esc_html__('Date', 'mella') => 'date',
	                	esc_html__('ID', 'mella') => 'ID',
	                	esc_html__('Author', 'mella') => 'author',
	                	esc_html__('Title', 'mella') => 'title',
	                	esc_html__('Modified', 'mella') => 'modified',
	                	esc_html__('Parent', 'mella') => 'parent',
	                	esc_html__('Comment count', 'mella') => 'comment_count',
	                	esc_html__('Menu order', 'mella') => 'menu_order',
	                	esc_html__('Random', 'mella') => 'rand',
	                )
	            ),
	            array(
	                "type" => "dropdown",
	                "heading" => esc_html__('Sort order','mella'),
	                "param_name" => 'order',
	                "value" => array(
	                	esc_html__('Descending', 'mella') => 'DESC',
	                	esc_html__('Ascending', 'mella') => 'ASC',
	                )
	            ),
	            array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Limit', 'mella' ),
					'param_name' => 'posts_per_page',
					'description' => esc_html__( 'Enter limit posts.', 'mella' ),
					'std' => 4,
					"admin_label" => true
				),
				array(
					'type' => 'checkbox',
					'heading' => esc_html__( 'Show Pagination?', 'mella' ),
					'param_name' => 'show_pagination',
					'description' => esc_html__( 'Enables to show paginations to next new page.', 'mella' ),
					'value' => array( esc_html__( 'Yes, to show pagination', 'mella' ) => 'yes' )
				),
				array(
	                "type" => "dropdown",
	                "heading" => esc_html__('Grid Columns','mella'),
	                "param_name" => 'grid_columns',
	                "value" => $columns
	            ),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Layout Type", 'mella'),
					"param_name" => "layout_type",
					"value" => $layouts
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Thumbnail size', 'mella' ),
					'param_name' => 'thumbsize',
					'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'mella' )
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'mella' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'mella' )
				)
			)
		) );
	}

	add_action( 'vc_after_set_mode', 'mella_load_post2_element', 99 );

	class WPBakeryShortCode_apus_gridposts extends WPBakeryShortCode {}
}
<div id="apus-header-mobile" class="header-mobile hidden-lg clearfix">    
    <div class="container">
        <div class="row">
            <div class="flex-middle">
                <div class="col-xs-4">
                    <div class="box-left">
                        <button data-toggle="offcanvas" class="btn btn-offcanvas btn-toggle-canvas offcanvas pull-left" type="button">
                            <i class="fa fa-bars"></i>
                        </button>
                    </div>
                </div>
                <div class="col-xs-4 text-center">
                    <?php
                        $logo = mella_get_config('media-mobile-logo');
                    ?>
                    <?php if( isset($logo['url']) && !empty($logo['url']) ): ?>
                        <div class="logo">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" >
                                <img src="<?php echo esc_url( $logo['url'] ); ?>" alt="<?php echo esc_attr(get_bloginfo( 'name' )); ?>">
                            </a>
                        </div>
                    <?php else: ?>
                        <div class="logo logo-theme">
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" >
                                <img src="<?php echo esc_url_raw( get_template_directory_uri().'/images/logo.png'); ?>" alt="<?php echo esc_attr(get_bloginfo( 'name' )); ?>">
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
                
                <div class="col-xs-4 right-header-mobile">
                    <?php if ( mella_get_config('show_user_info', true) ) { ?>
                        <div class="pull-right">
                            <div class="top-wrapper-menu">
                                <a class="drop-dow"><i class="icon-user-1"></i></a>
                                <?php if( is_user_logged_in()){ ?>
                                    <?php if ( has_nav_menu( 'top-menu' ) ) {
                                            $args = array(
                                                'theme_location' => 'top-menu',
                                                'container_class' => 'inner-top-menu',
                                                'menu_class' => 'nav navbar-nav topmenu-menu',
                                                'fallback_cb' => '',
                                                'menu_id' => '',
                                                'walker' => new Mella_Nav_Menu()
                                            );
                                            wp_nav_menu($args);
                                        }
                                    ?>
                                <?php } else { ?>
                                    <div class="inner-top-menu">
                                        <ul class="nav navbar-nav topmenu-menu">
                                            <li><a class="login" href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>" title="<?php esc_attr_e('Sign in','mella'); ?>"><?php esc_html_e('Login in', 'mella'); ?></a></li>
                                            <li><a class="register" href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ); ?>?ac=register" title="<?php esc_attr_e('Register','mella'); ?>"><?php esc_html_e('Register', 'mella'); ?></a></li>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ( defined('MELLA_WOOCOMMERCE_ACTIVED') && mella_get_config('show_cartbtn') && !mella_get_config( 'enable_shop_catalog' ) ): ?>
                        <div class="box-right pull-right">
                            <!-- Setting -->
                            <div class="top-cart">
                                <?php get_template_part( 'woocommerce/cart/mini-cart-button' ); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ( class_exists( 'YITH_WCWL' ) && mella_get_config('show_wishlist', true) ):
                        $wishlist_url = YITH_WCWL()->get_wishlist_url();
                    ?>
                        <div class="pull-right">
                            <a class="wishlist-icon" href="<?php echo esc_url($wishlist_url);?>" title="<?php esc_attr_e( 'View Your Wishlist', 'mella' ); ?>"><i class="icon-heart3"></i>
                                <?php if ( function_exists('yith_wcwl_count_products') ) { ?>
                                    <span class="count"><?php echo yith_wcwl_count_products(); ?></span>
                                <?php } ?>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
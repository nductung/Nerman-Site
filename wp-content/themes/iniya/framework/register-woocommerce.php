<?php 
// WooCommerce Theme Support -------------------------------------------------
add_theme_support( 'woocommerce' );
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );

// Disable WooCommerce Styles & Sidebar --------------------------------------
add_filter( 'woocommerce_enqueue_styles', '__return_false' );
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

// To Remove Breadcrumb
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

// To Remove Page Title
add_filter( 'woocommerce_show_page_title', '__return_false' );

// To Remove Page wrapper Start
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
add_action( 'woocommerce_before_main_content', 'iniya_woo_output_content_wrapper', 11 );

// To Remove Page wrapper End
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
add_action( 'woocommerce_after_main_content', 'iniya_woo_output_content_wrapper_end', 11 );

// Remove Yith Buttons
iniya_woo_remove_anonymous_object_action('woocommerce_after_shop_loop_item', 'YITH_WCQV_Frontend', 'yith_add_quick_view_button' , 15 );
iniya_woo_remove_anonymous_object_action('woocommerce_after_shop_loop_item', 'YITH_Woocompare_Frontend', 'add_compare_link' , 20 );

// Product Category
add_action( 'woocommerce_before_subcategory', 'iniya_woo_product_style_start', 5 );
add_action( 'woocommerce_after_subcategory', 'iniya_woo_product_style_end', 10 );

add_action( 'woocommerce_before_subcategory_title', 'iniya_woo_before_subcategory_title', 5 );
remove_action('woocommerce_shop_loop_subcategory_title', 'woocommerce_template_loop_category_title', 10 );
add_action( 'woocommerce_after_subcategory_title', 'iniya_woo_after_subcategory_title', 10 );

// Product Wrapper
add_action( 'woocommerce_before_shop_loop_item', 'iniya_woo_product_style_start', 1 );
add_action( 'woocommerce_after_shop_loop_item', 'iniya_woo_product_style_end', 100 );

remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_after_shop_loop_item', 'iniya_woo_shop_overview_show_price', 10 );

// To Remove Pagination
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
add_action( 'woocommerce_after_shop_loop', 'iniya_woo_after_shop_loop', 10 );

add_filter( 'loop_shop_per_page', 'iniya_woo_posts_per_page' );
add_filter( 'loop_shop_columns', 'iniya_woo_loop_columns' );

// Single Product Page
	// Upsell Products
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	add_action( 'woocommerce_after_single_product_summary', 'iniya_woo_show_upsell', 16 );

	// Related Products
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
	add_action( 'woocommerce_after_single_product_summary', 'iniya_woo_show_related', 21 );

	// Sale Flash
	remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
	add_action( 'woocommerce_before_single_product_summary','iniya_woo_show_product_wrapper',10 );
	add_action( 'woocommerce_after_single_product_summary','iniya_woo_close_product_wrapper',10 );

	add_action( 'woocommerce_product_thumbnails', 'iniya_woo_show_product_sale_flash', 30 );

// Change Image Sizes --------------------------------------------------------
$pagenow = iniya_global_variables('pagenow');
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) add_action( 'init', 'iniya_woo_image_dimensions', 1 );
function iniya_woo_image_dimensions() {
	$catalog 	= 	array('width' => '500', 'height'	=> '500', 'crop' => 1);
    $single 	= 	array('width' => '500', 'height' 	=> '500', 'crop' => 1);
	$thumbnail 	= 	array('width' => '200', 'height'	=> '200', 'crop' => 1);

	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog );
	update_option( 'shop_single_image_size', $single );
	update_option( 'shop_thumbnail_image_size', $thumbnail );
}

// Before main content -------------------------------------------------------
function iniya_woo_output_content_wrapper() {

	if( is_shop() ):
		// Page Settings
		$tpl_default_settings = get_post_meta( get_option('woocommerce_shop_page_id') ,'_tpl_default_settings',TRUE);
		$tpl_default_settings = is_array($tpl_default_settings) ? $tpl_default_settings  : array();

		$page_layout  = array_key_exists("layout", $tpl_default_settings) ? $tpl_default_settings['layout'] : "content-full-width";

	elseif( is_product() ):
		$page_layout = cs_get_option( 'product-layout' );
		$page_layout = !empty($page_layout) ? $page_layout : "content-full-width";

	elseif( is_product_category() ):
		$page_layout = cs_get_option( 'product-category-layout' );
		$page_layout = !empty($page_layout) ? $page_layout : "content-full-width";

	elseif( is_product_tag() ):
		$page_layout = cs_get_option( 'product-tag-layout' );
		$page_layout = !empty($page_layout) ? $page_layout : "content-full-width";
	endif;

	if($page_layout == 'with-left-sidebar'):
	  echo '<section class="secondary-sidebar secondary-has-left-sidebar" id="secondary-left">';
		get_sidebar('left');
	  echo '</section>';
	elseif($page_layout == 'with-both-sidebar'):
	  echo '<section class="secondary-sidebar secondary-has-both-sidebar" id="secondary-left">';
		get_sidebar('left');
	  echo '</section>';
	endif;

	if($page_layout != 'content-full-width'):
		echo '<section id="primary" class="page-with-sidebar '.$page_layout.'">';
	else:
		echo '<section id="primary" class="content-full-width">';
	endif;
}

// Before shop loop item -----------------------------------------------------
function iniya_woo_product_style_start() {

	global $woocommerce_loop;

	$style = 'woo-type1';
	$class = '';

	if( is_shop() || is_product_category() || is_product_tag() ) {
		$pstyle = cs_get_option( 'product-style' );
		$style  = !is_null( $pstyle ) ? $pstyle : $style;
		$column = cs_get_option( 'shop-page-product-layout' );
	} else {
		$style = isset( $woocommerce_loop['product-style'] ) ? $woocommerce_loop['product-style'] : $style;
		$column = $woocommerce_loop['columns'];
	}

	if( is_null( $column ) ) {
		$column = '4';
	}

	switch($column) {

		case 1:
		case '1':
			$class = 'no-column';
		break;

		case 2:
		case '2':
			$class = 'column dt-sc-one-half';
		break;

		case 3:
		case '3':
			$class = 'column dt-sc-one-third';
		break;

		case 4:
		case '4':
			$class = 'column dt-sc-one-fourth';
		break;

		case 5:
		case '5':
			$class = 'column dt-sc-one-fifth';
		break;
	}

	echo '<div class="'.$style.'">';
	echo '	<div class="'.$class.'">';
	echo '		<div class="product-wrapper">';
}

// After shop loop item -----------------------------------------------------
function iniya_woo_shop_overview_show_price() {
	global $product;
	$output = "";

	$output .= "<div class='product-thumb'>";
	
		if( $product->is_on_sale() and $product->is_in_stock() )
			$output .= '<span class="onsale"><span>'.esc_html__('Sale','iniya').'</span></span>';

		elseif(!$product->is_in_stock())
			$output .= '<span class="out-of-stock"><span>'.esc_html__('Out of Stock','iniya').'</span></span>';

		if( $product->is_featured())
			$output .= '<div class="featured-tag"><div><i class="fa fa-thumb-tack"></i><span>'.esc_html__('Featured','iniya').'</span></div></div>';

		$output .= '<a class="image" href="'.get_permalink().'" title="'.get_the_title().'">';
		$id = $product->get_id();
			$image =  get_the_post_thumbnail( $id, 'shop_catalog' );
			$image = !empty( $image ) ? $image : "<img src='http://placehold.it/500' alt='product-thumb' />";
			$attachment_ids = $product->get_gallery_image_ids();
			$secondary_image_id = !empty( $attachment_ids ) ? $attachment_ids['0'] : '';
			$image1 = wp_get_attachment_image( $secondary_image_id, 'full', '', $attr = array( 'class' => 'secondary-image attachment-shop-catalog' ) );
			$output .= $image.$image1;
		$output .= '</a>';

		$output .= '<div class="product-buttons-wrapper">';

				$output .= '<div class="wc_inline_buttons">';

						ob_start();
						woocommerce_template_loop_add_to_cart();
						$add_to_cart = ob_get_clean();

						// Add to Cart
						if( !empty($add_to_cart) ) {
							$add_to_cart = str_replace(' class="',' class="dt-sc-button too-small ',$add_to_cart);
							$output .= '<div class="wc_cart_btn_wrapper wc_btn_inline">'.$add_to_cart.'</div>';
						}

						// YITH Wishlist 
						if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
							$output .= '<div class="wcwl_btn_wrapper wc_btn_inline">'.do_shortcode('[yith_wcwl_add_to_wishlist]').'</div>';
						}

				$output .= '</div>';

		$output .= '</div>';

	$output .= "</div>";

	ob_start();
	woocommerce_template_loop_price();
	$price = ob_get_clean();

	$output .= "<div class='product-details'>";
		$output .= '<h5><a href="'.get_permalink($product->get_id()).'">'.$product->get_name().'</a></h5>';
		$output .= '<span class="product-price">'.trim($price).'</span>';
		$output .= '<div class="product-rating-wrapper">'.wc_get_rating_html( $product->get_average_rating() ).'</div>';
	$output .= '</div>';
	echo "{$output}";
}

// After shop loop item -----------------------------------------------------
function iniya_woo_product_style_end() {
	echo '		</div> <!-- .product-wrapper -->';
	echo '	</div> <!-- .column -->';
	echo '</div> <!-- .style -->';
}

// After shop loop ----------------------------------------------------------
function iniya_woo_after_shop_loop() {
	echo '<div class="pagination">';
		if( function_exists( 'iniya_pagination' ) )
			echo iniya_pagination();
		else
			wc_get_template( 'loop/pagination.php' );
	echo '</div>';
}

// After main content -------------------------------------------------------
function iniya_woo_output_content_wrapper_end() {

	echo '</section>';

	if( is_shop() ):
		// Page Settings
		$tpl_default_settings = get_post_meta( get_option('woocommerce_shop_page_id') ,'_tpl_default_settings',TRUE);
		$tpl_default_settings = is_array($tpl_default_settings) ? $tpl_default_settings  : array();

		$page_layout  = array_key_exists("layout",$tpl_default_settings) ? $tpl_default_settings['layout'] : "content-full-width";

	elseif( is_product() ):
		$page_layout = cs_get_option( 'product-layout' );
		$page_layout = !empty($page_layout) ? $page_layout : "content-full-width";

	elseif( is_product_category() ):
		$page_layout = cs_get_option( 'product-category-layout' );
		$page_layout = !empty($page_layout) ? $page_layout : "content-full-width";

	elseif( is_product_tag() ):
		$page_layout = cs_get_option( 'product-tag-layout' );
		$page_layout = !empty($page_layout) ? $page_layout : "content-full-width";
	endif;

	if($page_layout == 'with-right-sidebar'):
		echo '<section class="secondary-sidebar secondary-has-right-sidebar" id="secondary-right">';
			get_sidebar('right');
		echo '</section>';
	elseif($page_layout == 'with-both-sidebar'):
		echo '<section class="secondary-sidebar secondary-has-both-sidebar" id="secondary-right">';
			get_sidebar('right');
		echo '</section>';
	endif;
}

// No.of products per page --------------------------------------------------
function iniya_woo_posts_per_page( $count ) {

	$count = cs_get_option( 'shop-product-per-page' );
	$count = !empty( $count )  ? $count : 12;

	return $count;
}

// Columns in products loop -------------------------------------------------
function iniya_woo_loop_columns( $columns ) {

	$columns = cs_get_option( 'shop-page-product-layout' );
	$columns = !empty( $columns )  ? $columns : 4;

	return $columns;
}

/* --------------------------------------------------------------------------
 * Single Product
 * Showing Releated Products
 * -------------------------------------------------------------------------- */
function iniya_woo_show_related() {
	global $woocommerce_loop;

	$show_related = cs_get_option( 'enable-related' );
	$output = '';

	if($show_related):

		$page_layout = cs_get_option( 'product-layout' );
		$page_layout = !empty($page_layout) ? $page_layout : "content-full-width";

		$related_products = ( $page_layout === "content-full-width" ) ? 4 : 3;
		$woocommerce_loop['product-style'] = cs_get_option( 'product-style' );

		ob_start();
		woocommerce_related_products(array('posts_per_page' => $related_products, 'columns' => $related_products, 'orderby' => 'rand')); // X products, X columns
		$content = ob_get_clean();
		if($content):
			$content =  str_replace('<h2>','<h2 class="border-title"><span>', $content);
			$output .= "<div class='related-products-container'>{$content}</div>";
		endif;

	endif;

	echo "{$output}";
}

/* --------------------------------------------------------------------------
 * Single Product
 * Showing Upsell Products
 * -------------------------------------------------------------------------- */
function iniya_woo_show_upsell() {
	global $woocommerce_loop;

	$output = '';

	$page_layout = cs_get_option( 'product-layout' );
	$page_layout = !empty($page_layout) ? $page_layout : "content-full-width";

	$upsell_products = ( $page_layout === "content-full-width" ) ? 4 : 3;
	$woocommerce_loop['product-style'] = cs_get_option( 'product-style' );

	ob_start();
	woocommerce_upsell_display($upsell_products, $upsell_products); // X products, X columns
	$content = ob_get_clean();
	if($content):
		$content =  str_replace('<h2>','<h2 class="border-title"><span>', $content);
		$output .= "<div class='upsell-products-container'>{$content}</div>";
	endif;

	echo "{$output}";
}

/* --------------------------------------------------------------------------
 * Single Product
 * Showing Product Thumb Wrapper
 * -------------------------------------------------------------------------- */
function iniya_woo_show_product_wrapper() {
	$out = '<div class="product-thumb-wrapper">';
	echo "{$out}";
}

/* --------------------------------------------------------------------------
 * Single Product
 * Closing Product Thumb Wrapper
 * -------------------------------------------------------------------------- */
function iniya_woo_close_product_wrapper() {
	$out = '</div>';
	echo "{$out}";
}

/* --------------------------------------------------------------------------
 * Single Product
 * Product Sale Flash
 * -------------------------------------------------------------------------- */
function iniya_woo_show_product_sale_flash() {
	global $product;

	$out = '<div class="product-status-labels">';
	if( $product->is_on_sale() and $product->is_in_stock() )
		$out .= '<span class="onsale"><span>'.esc_html__('Sale!','iniya').'</span></span>';

	elseif(!$product->is_in_stock())
		$out .= '<span class="out-of-stock">'.esc_html__('Out of Stock','iniya').'</span>';

	if($product->is_featured())
		$out .= '<div class="featured-tag"><div><i class="fa fa-thumb-tack"></i><span>'.esc_html__('Featured','iniya').'</span></div></div>';

	$out .= '</div>';

	echo "{$out}";
}

// Before Category Title ---------------------------------------------------
function iniya_woo_before_subcategory_title() {
	echo '<div class="product-thumb"><span class="image">';
}

// After Category Title ----------------------------------------------------
function iniya_woo_after_subcategory_title( $category ) {
	echo '</span></div>';
	echo '<div class="product-details"><h5>'.$category->name;
		if ( $category->count > 0 ) {
			echo apply_filters( 'woocommerce_subcategory_count_html', ' <mark class="count">(' . $category->count . ')</mark>', $category );
		}
	echo '</h5></div>';
}

// Remove action for Yith Buttons ------------------------------------------
function iniya_woo_remove_anonymous_object_action( $tag, $class, $method, $priority = null ){

	if( empty($GLOBALS['wp_filter'][ $tag ]) ){
		return;
	}

	foreach ( $GLOBALS['wp_filter'][ $tag ] as $filterPriority => $filter ){
		if( !($priority===null || $priority==$filterPriority) )
			continue;

		foreach ( $filter as $identifier => $function ){
			if( is_array( $function)
				and is_a( $function['function'][0], $class )
				and $method === $function['function'][1]
			){
				remove_action(
					$tag,
					array ( $function['function'][0], $method ),
					$filterPriority
				);
			}
		}
	}
}
<?php

// Shop Archive settings
function mella_woo_redux_config($sections, $sidebars, $columns) {
    $categories = array();
    $attributes = array();
    if ( is_admin() ) {
        $categories = mella_woocommerce_get_categories(false);

        $attrs = wc_get_attribute_taxonomies();
        if ( $attrs ) {
            foreach ( $attrs as $tax ) {
                $attributes[wc_attribute_taxonomy_name( $tax->attribute_name )] = $tax->attribute_label;
            }
        }
    }
    $sections[] = array(
        'icon' => 'el el-shopping-cart',
        'title' => esc_html__('Shop Settings', 'mella'),
        'fields' => array(
            array (
                'id' => 'products_general_total_setting',
                'icon' => true,
                'type' => 'info',
                'raw' => '<h3 style="margin: 0;"> '.esc_html__('General Setting', 'mella').'</h3>',
            ),
            array(
                'id' => 'enable_shop_catalog',
                'type' => 'switch',
                'title' => esc_html__('Enable Shop Catalog', 'mella'),
                'default' => 0,
                'subtitle' => esc_html__('Enable Catalog Mode for disable Add To Cart button, Cart, Checkout', 'mella'),
            ),
            array (
                'id' => 'products_watches_setting',
                'icon' => true,
                'type' => 'info',
                'raw' => '<h3 style="margin: 0;"> '.esc_html__('Swatches Variation Setting', 'mella').'</h3>',
            ),
            array(
                'id' => 'show_product_swatches_on_grid',
                'type' => 'switch',
                'title' => esc_html__('Show Swatches On Product Grid', 'mella'),
                'default' => 1
            ),
            array(
                'id' => 'product_swatches_attribute',
                'type' => 'select',
                'title' => esc_html__( 'Grid swatch attribute to display', 'mella' ),
                'subtitle' => esc_html__( 'Choose attribute that will be shown on products grid', 'mella' ),
                'options' => $attributes
            ),
            array(
                'id' => 'show_product_swatches_use_images',
                'type' => 'switch',
                'title' => esc_html__('Use images from product variations', 'mella'),
                'subtitle' => esc_html__( 'If enabled swatches buttons will be filled with images choosed for product variations and not with images uploaded to attribute terms.', 'mella' ),
                'default' => 1
            ),
            array (
                'id' => 'products_brand_setting',
                'icon' => true,
                'type' => 'info',
                'raw' => '<h3 style="margin: 0;"> '.esc_html__('Brands Setting', 'mella').'</h3>',
            ),
            array(
                'id' => 'product_brand_attribute',
                'type' => 'select',
                'title' => esc_html__( 'Brand Attribute', 'mella' ),
                'subtitle' => esc_html__( 'Choose a product attribute that will be used as brands', 'mella' ),
                'desc' => esc_html__( 'When you have choosed a brand attribute, you will be able to add brand image to the attributes', 'mella' ),
                'options' => $attributes
            ),
            array (
                'id' => 'products_breadcrumb_setting',
                'icon' => true,
                'type' => 'info',
                'raw' => '<h3 style="margin: 0;"> '.esc_html__('Breadcrumbs Setting', 'mella').'</h3>',
            ),
            array(
                'id' => 'show_product_breadcrumbs',
                'type' => 'switch',
                'title' => esc_html__('Breadcrumbs', 'mella'),
                'default' => 1
            ),

            array(
                'id' => 'woo_breadcrumb_center',
                'type' => 'select',
                'title' => esc_html__('Breadcrumbs Align', 'mella'),
                'options' => array(
                    'st_inherit' => esc_html__('Inherit', 'mella'),
                    'st_center' => esc_html__('Center', 'mella'),
                    'st_stretch' => esc_html__('Stretch', 'mella'),
                ),
                'default' => 'st_inherit'
            ),

            array(
                'id' => 'woo_breadcrumb_style',
                'type' => 'select',
                'title' => esc_html__('Breadcrumbs Style', 'mella'),
                'subtitle' => esc_html__('Choose a style for breadcrumbs.', 'mella'),
                'options' => array(
                    'text-black' => esc_html__('Text Black', 'mella'),
                    'text-white' => esc_html__('Text White', 'mella'),
                ),
                'default' => 'text-black'
            ),
            array(
                'title' => esc_html__('Breadcrumbs Background Color', 'mella'),
                'subtitle' => '<em>'.esc_html__('The breadcrumbs background color of the site.', 'mella').'</em>',
                'id' => 'woo_breadcrumb_color',
                'type' => 'color',
                'transparent' => false,
            ),
            array(
                'id' => 'woo_breadcrumb_image',
                'type' => 'media',
                'title' => esc_html__('Breadcrumbs Background', 'mella'),
                'subtitle' => esc_html__('Upload a .jpg or .png image that will be your breadcrumbs.', 'mella'),
            ),
        )
    );
    // Archive settings
    $sections[] = array(
        'title' => esc_html__('Product Archives', 'mella'),
        'subsection' => true,
        'fields' => array(
            array (
                'id' => 'products_top_setting',
                'icon' => true,
                'type' => 'info',
                'raw' => '<h3 style="margin: 0;"> '.esc_html__('Top Content Setting', 'mella').'</h3>',
            ),
            array(
                'id' => 'product_archive_top_categories',
                'type' => 'switch',
                'title' => esc_html__('Enable Top Categories', 'mella'),
                'default' => 1
            ),
            array(
                'id' => 'product_archive_top_filter',
                'type' => 'switch',
                'title' => esc_html__('Enable Filter Top', 'mella'),
                'default' => 1
            ),
            array (
                'id' => 'products_general_setting',
                'icon' => true,
                'type' => 'info',
                'raw' => '<h3 style="margin: 0;"> '.esc_html__('General Setting', 'mella').'</h3>',
            ),
            array(
                'id' => 'product_display_mode',
                'type' => 'select',
                'title' => esc_html__('Products Layout', 'mella'),
                'subtitle' => esc_html__('Choose a default layout archive product.', 'mella'),
                'options' => array(
                    'grid' => esc_html__('Grid', 'mella'),
                    'list' => esc_html__('List', 'mella'),
                    'mansory' => esc_html__('Mansory', 'mella'),
                    'metro' => esc_html__('Metro', 'mella'),
                ),
                'default' => 'grid'
            ),
            array(
                'id' => 'product_columns',
                'type' => 'select',
                'title' => esc_html__('Product Columns', 'mella'),
                'options' => $columns,
                'default' => 4,
                'required' => array('product_display_mode', '=', array('grid'))
            ),
            array(
                'id' => 'number_products_per_page',
                'type' => 'text',
                'title' => esc_html__('Number of Products Per Page', 'mella'),
                'default' => 12,
                'min' => '1',
                'step' => '1',
                'max' => '100',
                'type' => 'slider'
            ),
            array(
                'id' => 'show_quickview',
                'type' => 'switch',
                'title' => esc_html__('Show Quick View', 'mella'),
                'default' => 1
            ),
            array(
                'id' => 'enable_swap_image',
                'type' => 'switch',
                'title' => esc_html__('Enable Swap Image', 'mella'),
                'default' => 1
            ),
            array(
                'id' => 'product_pagination',
                'type' => 'select',
                'title' => esc_html__('Pagination Type', 'mella'),
                'options' => array(
                    'default' => esc_html__('Default', 'mella'),
                    'loadmore' => esc_html__('Load More Button', 'mella'),
                    'infinite' => esc_html__('Infinite Scrolling', 'mella'),
                ),
                'default' => 'default'
            ),
            array (
                'id' => 'products_sidebar_setting',
                'icon' => true,
                'type' => 'info',
                'raw' => '<h3 style="margin: 0;"> '.esc_html__('Sidebar Setting', 'mella').'</h3>',
            ),
            array(
                'id' => 'product_archive_fullwidth',
                'type' => 'switch',
                'title' => esc_html__('Is Full Width?', 'mella'),
                'default' => false
            ),
            array(
                'id' => 'product_archive_layout',
                'type' => 'image_select',
                'compiler' => true,
                'title' => esc_html__('Archive Product Layout', 'mella'),
                'subtitle' => esc_html__('Select the layout you want to apply on your archive product page.', 'mella'),
                'options' => array(
                    'main' => array(
                        'title' => esc_html__('Main Content', 'mella'),
                        'alt' => esc_html__('Main Content', 'mella'),
                        'img' => get_template_directory_uri() . '/inc/assets/images/screen1.png'
                    ),
                    'left-main' => array(
                        'title' => esc_html__('Left Sidebar - Main Content', 'mella'),
                        'alt' => esc_html__('Left Sidebar - Main Content', 'mella'),
                        'img' => get_template_directory_uri() . '/inc/assets/images/screen2.png'
                    ),
                    'main-right' => array(
                        'title' => esc_html__('Main Content - Right Sidebar', 'mella'),
                        'alt' => esc_html__('Main Content - Right Sidebar', 'mella'),
                        'img' => get_template_directory_uri() . '/inc/assets/images/screen3.png'
                    ),
                ),
                'default' => 'left-main'
            ),
            array(
                'id' => 'product_archive_left_sidebar',
                'type' => 'select',
                'title' => esc_html__('Archive Left Sidebar', 'mella'),
                'subtitle' => esc_html__('Choose a sidebar for left sidebar.', 'mella'),
                'options' => $sidebars
            ),
            array(
                'id' => 'product_archive_right_sidebar',
                'type' => 'select',
                'title' => esc_html__('Archive Right Sidebar', 'mella'),
                'subtitle' => esc_html__('Choose a sidebar for right sidebar.', 'mella'),
                'options' => $sidebars
            ),
        )
    );
    
    
    // Product Page
    $sections[] = array(
        'title' => esc_html__('Single Product', 'mella'),
        'subsection' => true,
        'fields' => array(
            array(
                'id' => 'product_general_setting',
                'icon' => true,
                'type' => 'info',
                'raw' => '<h3 style="margin: 0;"> '.esc_html__('General Setting', 'mella').'</h3>',
            ),
            array(
                'id' => 'product_single_version',
                'type' => 'select',
                'title' => esc_html__('Single Product Layout', 'mella'),
                'subtitle' => esc_html__('Select the layout you want to apply on your Single Product Page.', 'mella'),
                'options' => array(
                    'default' => esc_html__('Default', 'mella'),
                    'layout-image-1-column' => esc_html__('Layout Image 1 Column', 'mella'),
                    'layout-center' => esc_html__('Layout Center', 'mella'),
                    'layout-image-bg' => esc_html__('Layout Image Background', 'mella'),
                ),
                'default' => 'default',
            ),
            array(
                'id' => 'product_thumbs_position',
                'type' => 'select',
                'title' => esc_html__('Thumbnails Position', 'mella'),
                'options' => array(
                    'thumbnails-left' => esc_html__('Thumbnails Left', 'mella'),
                    'thumbnails-right' => esc_html__('Thumbnails Right', 'mella'),
                    'thumbnails-bottom' => esc_html__('Thumbnails Bottom', 'mella'),
                ),
                'default' => 'thumbnails-bottom',
                'required' => array('product_single_version', '=', array('default'))
            ),
            array(
                'id' => 'number_product_thumbs',
                'title' => esc_html__('Number Thumbnails Per Row', 'mella'),
                'default' => 4,
                'min' => '1',
                'step' => '1',
                'max' => '8',
                'type' => 'slider',
                'required' => array('product_single_version', '=', array('default', 'layout-image-bg'))
            ),
            array(
                'id' => 'product_thumbs_layout-center_version',
                'type' => 'select',
                'title' => esc_html__('Gallery Images Layout', 'mella'),
                'options' => array(
                    'carousel' => esc_html__('Carousel', 'mella'),
                    'gallery' => esc_html__('Gallery', 'mella'),
                ),
                'default' => 'carousel',
                'required' => array('product_single_version', '=', array('layout-center'))
            ),
            array(
                'id' => 'number_product_images_per_row',
                'title' => esc_html__('Number Images Per Row', 'mella'),
                'default' => 3,
                'min' => '1',
                'step' => '1',
                'max' => '8',
                'type' => 'slider',
                'required' => array('product_thumbs_layout-center_version', '=', array('carousel'))
            ),
            array(
                'title' => esc_html__('Background Color', 'mella'),
                'id' => 'product_image_breadcrumb_color',
                'type' => 'color',
                'transparent' => false,
                'required' => array('product_single_version', '=', array('layout-image-bg'))
            ),
            array(
                'id' => 'product_shipping_info',
                'type' => 'editor',
                'title' => esc_html__('Shipping Information', 'mella'),
                'default' => '',
            ),
            array(
                'id' => 'show_product_countdown_timer',
                'type' => 'switch',
                'title' => esc_html__('Show Product CountDown Timer', 'mella'),
                'default' => 1
            ),
            array(
                'id' => 'show_product_social_share',
                'type' => 'switch',
                'title' => esc_html__('Show Social Share', 'mella'),
                'default' => 1
            ),

            array(
                'id' => 'show_product_review_tab',
                'type' => 'switch',
                'title' => esc_html__('Show Product Review Tab', 'mella'),
                'default' => 1
            ),
            array(
                'id' => 'hidden_product_additional_information_tab',
                'type' => 'switch',
                'title' => esc_html__('Hidden Product Additional Information Tab', 'mella'),
                'default' => 1
            ),

            array (
                'id' => 'product_sidebar_setting',
                'icon' => true,
                'type' => 'info',
                'raw' => '<h3 style="margin: 0;"> '.esc_html__('Sidebar Setting', 'mella').'</h3>',
            ),
            array(
                'id' => 'product_single_layout',
                'type' => 'image_select',
                'compiler' => true,
                'title' => esc_html__('Single Product Sidebar Layout', 'mella'),
                'subtitle' => esc_html__('Select the layout you want to apply on your Single Product Page.', 'mella'),
                'options' => array(
                    'main' => array(
                        'title' => esc_html__('Main Only', 'mella'),
                        'alt' => esc_html__('Main Only', 'mella'),
                        'img' => get_template_directory_uri() . '/inc/assets/images/screen1.png'
                    ),
                    'left-main' => array(
                        'title' => esc_html__('Left - Main Sidebar', 'mella'),
                        'alt' => esc_html__('Left - Main Sidebar', 'mella'),
                        'img' => get_template_directory_uri() . '/inc/assets/images/screen2.png'
                    ),
                    'main-right' => array(
                        'title' => esc_html__('Main - Right Sidebar', 'mella'),
                        'alt' => esc_html__('Main - Right Sidebar', 'mella'),
                        'img' => get_template_directory_uri() . '/inc/assets/images/screen3.png'
                    ),
                ),
                'default' => 'left-main'
            ),
            array(
                'id' => 'product_single_fullwidth',
                'type' => 'switch',
                'title' => esc_html__('Is Full Width?', 'mella'),
                'default' => false
            ),
            array(
                'id' => 'product_single_left_sidebar',
                'type' => 'select',
                'title' => esc_html__('Single Product Left Sidebar', 'mella'),
                'subtitle' => esc_html__('Choose a sidebar for left sidebar.', 'mella'),
                'options' => $sidebars
            ),
            array(
                'id' => 'product_single_right_sidebar',
                'type' => 'select',
                'title' => esc_html__('Single Product Right Sidebar', 'mella'),
                'subtitle' => esc_html__('Choose a sidebar for right sidebar.', 'mella'),
                'options' => $sidebars
            ),
            array (
                'id' => 'product_block_setting',
                'icon' => true,
                'type' => 'info',
                'raw' => '<h3 style="margin: 0;"> '.esc_html__('Product Block Setting', 'mella').'</h3>',
            ),
            array(
                'id' => 'show_product_releated',
                'type' => 'switch',
                'title' => esc_html__('Show Products Releated', 'mella'),
                'default' => 1
            ),
            array(
                'id' => 'show_product_upsells',
                'type' => 'switch',
                'title' => esc_html__('Show Products upsells', 'mella'),
                'default' => 1
            ),
            array(
                'id' => 'number_product_releated',
                'title' => esc_html__('Number of related/upsells products to show', 'mella'),
                'default' => 4,
                'min' => '1',
                'step' => '1',
                'max' => '20',
                'type' => 'slider'
            ),
            array(
                'id' => 'releated_product_columns',
                'type' => 'select',
                'title' => esc_html__('Releated Products Columns', 'mella'),
                'options' => $columns,
                'default' => 4
            ),
        )
    );
    
    return $sections;
}
add_filter( 'mella_redux_framwork_configs', 'mella_woo_redux_config', 10, 3 );
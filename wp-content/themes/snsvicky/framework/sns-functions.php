<?php
// Set cookie theme option
add_action( 'wp_ajax_sns_setcookies', 'snsvicky_setcookies' );
add_action( 'wp_ajax_nopriv_sns_setcookies', 'snsvicky_setcookies' );
// Reset cookie theme option
add_action( 'wp_ajax_sns_resetcookies', 'snsvicky_resetcookies' );
add_action( 'wp_ajax_nopriv_sns_resetcookies', 'snsvicky_resetcookies' );
function snsvicky_setcookies(){
	setcookie('snsvicky_'.$_POST['key'], $_POST['value'], time()+3600*24*1, '/'); // 1 day
}
function snsvicky_resetcookies(){
	setcookie('snsvicky_advance_scss_compile', '', 0, '/');
	//setcookie('snsvicky_theme_color', '', 0, '/');
	setcookie('snsvicky_use_boxedlayout', '', 0, '/');
	setcookie('snsvicky_use_stickmenu', '', 0, '/');
}
/*
 * Get theme option
 */
function snsvicky_themeoption($option, $default = '', $key = ''){
	global $snsvicky_opt;
	$value = '';
	// Get config via theme option
	if($key !== ''){
		if ( isset($snsvicky_opt[$option][$key]) && $snsvicky_opt[$option][$key] ) $value = $snsvicky_opt[$option][$key];
	}else{
		if ( isset($snsvicky_opt[$option]) && $snsvicky_opt[$option] ) $value = $snsvicky_opt[$option];
	}
	// Get config via cookie
	if ( isset($_COOKIE['snsvicky_'.$option]) && $_COOKIE['snsvicky_'.$option] != '' ) {
		$value = $_COOKIE['snsvicky_'.$option];
	}
	// Return value when exists ReduxFramework
	if($value || class_exists( 'ReduxFramework' ))
		return $value; 
	// Return default value
	return $default;
}
/*
 * Set theme option
 */
function snsvicky_set_themeoption($key, $value){
	global $snsvicky_opt;
	if ( $key != '' && $value != '' )
		$snsvicky_opt[$key] = $value;
}
/*
 * Get theme & post/page option
 */
function snsvicky_getoption($option, $default = '', $type = null){
	global $post;
    $value = ''; $value1 = '';
    $value = snsvicky_themeoption($option, $default);
    if ( $type == 'image' ){
    	if ( $value['url'] == '' ) {
    		$value = $default;
    	}else{
    		$value = $value['url'];
    	}
    }
    // Get config via page/product config
    if ( function_exists('rwmb_meta') ){
    	// Just page/product have rwmb_meta 
    	if( is_woocommerce() || is_page() ){
    		$value1 = rwmb_meta('snsvicky_'.$option);
	    	if ( ( !is_array($value1) && trim($value1) == '' ) || ( is_array($value1) && empty($value1) ) ) {
	    		if ( class_exists('WooCommerce') ) {
		    		if ( is_product() ){
		    			$value1 = rwmb_meta('snsvicky_'.$option, array(), $post->ID);
		    		}elseif( is_shop() ) { // for shop page
		    			$value1 = rwmb_meta('snsvicky_'.$option, array(), get_option('woocommerce_shop_page_id'));
		    		}
		    	}
	    	}else{
	    		if ( $type == 'image' ){
			    	foreach ( $value1 as $img ) {
			    		$value1 = $img['full_url'];
			    	}
			    }
	    	}
    	}
	}
	if ( ( ( !is_array($value1) && trim($value1) == '' ) || ( is_array($value1) && empty($value1) )  ) && $default ) {
		if ( $value != '' ) {
			return $value;
		}else{
			return $default;
		}
	}
    if ( $value1 != '' ) return $value1;
    if ( $value != '' ) return $value;
	return $default;
}
/*
 * Get meta box data
 */
function snsvicky_metabox($field_id, $args = array()){
	if( !function_exists('rwmb_meta') ){
		return '';
	}
	$field_id = 'snsvicky_'.$field_id;
	if( function_exists('is_shop') && is_woocommerce() ) {
		return rwmb_meta($field_id, $args, get_option('woocommerce_shop_page_id'));
	}
	return rwmb_meta($field_id, $args);
}
function snsvicky_get_term_byid($term_id, $key, $default = ''){
	$value = get_term_meta( $term_id, $key );
    $value = ( is_array($value) && isset($value[0]) && $value[0] != '' ) ? $value[0] : $default; //var_dump($key.': '.$value. ' & default: '.$default);
    return $value;
}
/*
 * Css file
 */
function snsvicky_css_file(){
	$compile_obj = new SnsBiaMucCompileSass();
	$optimize = '';
	$theme_color = snsvicky_getoption('theme_color', '#ffb197');
	// Get page meta data
	if ( is_page() && function_exists('rwmb_meta') && rwmb_meta('snsvicky_page_themecolor') == 1) {
		$theme_color = rwmb_meta('snsvicky_theme_color') != '' ? rwmb_meta('snsvicky_theme_color') : $theme_color;
	}
	$body_color = snsvicky_themeoption('body_font', '#666666', 'color');
	$scss_compile = snsvicky_themeoption('advance_scss_compile', '1');
	$scss_format = snsvicky_themeoption('advance_scss_format', 'scss_formatter_compressed');
	// Compile scss and get css file name
	$css_file = $compile_obj->snsvicky_getstyle(
		$scss_compile,
		array('dir' => SNSVICKY_THEME_DIR . '/assets/scss/', 'name' => 'theme'),
		array('dir' => SNSVICKY_THEME_DIR . '/assets/css/', 'name' => 'theme'),
		$scss_format,
		array(
			'$color1' => $theme_color,
			'$color' => $body_color,
		)
	);
	return $css_file;
}
/**
 * Layout type
**/
function snsvicky_layouttype($option='layouttype', $default=''){
	$layouttype = snsvicky_getoption($option, $default);
	if( class_exists('WooCommerce') && is_woocommerce() ){
		$layouttype = snsvicky_metabox('snsvicky_layouttype', $default);
		if( is_product_category() ){
			$cate = get_queried_object();
			$layouttype = snsvicky_get_term_byid($cate->term_id, 'snsvicky_product_cat_layout');
		}
		if ( trim($layouttype) == '' ) $layouttype = 'l-m';
		if( is_product_tag() ){
			$layouttype = 'l-m';
		}
		if( is_product() ){ 
			if ( snsvicky_getoption('single_product_sidebar') == 1 ) {
				$layouttype = 'm-r';
			}else{
				$layouttype = 'm';
			}
		}
	}
	return $layouttype;
}
/**
 * Left Column
**/
function snsvicky_leftcol(){
	$layouttype = snsvicky_layouttype('layouttype', 'l-m');
	if ( $layouttype == '' || $layouttype == 'm-r' || $layouttype == 'm' ) 
		return '';
	?>
	<div class="col-md-3 sns-left">
		<?php
	    if( class_exists('WooCommerce') && is_woocommerce() ){
	        if( is_product() ){
	    		dynamic_sidebar('product-sidebar');
	    	}else{
	        	if( is_active_sidebar( 'woo-sidebar' ) ) {
		        	dynamic_sidebar( 'woo-sidebar' ); 
		        }else{
		        	dynamic_sidebar('widget-area');
		        }
	        }
	    }else{
	        if( snsvicky_getoption('leftsidebar') != '' && is_active_sidebar( snsvicky_getoption('leftsidebar') ) ) {
	        	dynamic_sidebar( snsvicky_getoption('leftsidebar') ); 
	        }else{
	        	dynamic_sidebar('widget-area');
	        }
	    } ?>
	</div>
	<?php
 }
/**
 * Right Column
**/
 function snsvicky_rightcol(){
 	$layouttype = snsvicky_layouttype('layouttype', 'm');
	if ( $layouttype == '' || $layouttype == 'l-m' || $layouttype == 'm' ) 
		return '';
	?>
	<div class="col-md-3 sns-right">
		<?php
	    if( class_exists('WooCommerce') && is_woocommerce() ){
	    	if( is_product() ){
	    		dynamic_sidebar('product-sidebar');
	    	}else{
	        	if( is_active_sidebar( 'woo-sidebar' ) ) {
		        	dynamic_sidebar( 'woo-sidebar' ); 
		        }else{
		        	dynamic_sidebar('widget-area');
		        }
	        }
	    }else{
	        if( snsvicky_getoption('rightsidebar') != '' && is_active_sidebar( snsvicky_getoption('rightsidebar') ) ) {
	        	dynamic_sidebar( snsvicky_getoption('rightsidebar') ); 
	        }else{
	        	dynamic_sidebar('widget-area');
	        }
	    } ?>
	</div>
	<?php
 }
/**
 * Main class
**/
function snsvicky_maincolclass(){
	$layouttype = snsvicky_layouttype('layouttype', 'l-m');
	$mclass = 'sns-main';
	if ( $layouttype == 'l-m' ) {
		$mclass .= ' col-md-9 main-left';
	}elseif( $layouttype == 'm-r' ){
		$mclass .= ' col-md-9 main-right';
	}elseif ( $layouttype == 'l-m-r' ) {
		$mclass .= ' col-md-6 main-center';
	}else{
		$mclass .= ' col-md-12';
	}
	return $mclass;
}
/**
 * Return number of published sticky posts
**/
function snsvicky_get_sticky_posts_count(){
	global $wpdb;
	$sticky_posts = array_map('absint', (array)get_option('sticky_posts') );
	return count($sticky_posts) > 0 ? $wpdb->get_var( $wpdb->prepare( "SELECT COUNT( 1 ) FROM $wpdb->posts WHERE post_type = 'post' AND post_status = 'publish' AND ID IN (".implode(',', $sticky_posts).")" ) ) : 0;
}

/**
 * Display Ajax loading
 */
function snsvicky_paging_nav_ajax( $content_div = '#snsmain', $template = '' ){
	// Don't print empty markup if there is only one page.
	if( $GLOBALS['wp_query']->max_num_pages < 2 ){
		return;
	}
	
	?>
	<nav class="navigation-ajax" role="navigation">
		<a href="<?php echo esc_url('javascript:void(0)'); ?>" data-target="<?php echo esc_attr($content_div);?>" data-template="<?php echo esc_attr($template); ?>" id="navigation-ajax" class="load-more">
			<span><?php echo esc_html__('Load More', 'snsvicky');?></span>
			<div class="sns-navloading"><div class="sns-navloader"></div></div>
		</a>
	</nav>
	<?php
}

/**
 * Display navigation to next/previous post when applicable.
 */
function snsvicky_post_nav() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	if ( ! $next && ! $previous ) {
		return;
	}?>
	<div class="post-standard-navigation navigation post-navigation" role="navigation">
    	<?php 
    	if( $previous ):?>
        <div class="nav-previous">
            <div class="area-content">
            	<?php
            	previous_post_link('%link',''); // link overlay
                ?>
                <div class="nav-content">
                	<?php 
                		previous_post_link( '<div class="nav-post-link">%link</div>', _x( 'Previous post', 'Previous post link', 'snsvicky' ) );
						previous_post_link( '<div class="nav-post-title">%link</div>');
					?>	
                </div>
            </div>
        </div>
        <?php endif; ?>
        <?php if( $next ): ?>
        <div class="nav-next">
            <div class="area-content ">
            	<?php
            	next_post_link( '%link',''); // link overlay
                ?>
                <div class="nav-content">
                	<?php 
                		next_post_link( '<div class="nav-post-link">%link</div>', _x( 'Next post', 'Next post link', 'snsvicky' ) );
						next_post_link( '<div class="nav-post-title">%link</div>');
					?>
                </div>
            </div>
        </div>
        <?php endif; ?>
	</div>
	<?php
}

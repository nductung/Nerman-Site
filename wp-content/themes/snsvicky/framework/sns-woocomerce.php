<?php
// Declare WooCommerce support in theme
add_action( 'after_setup_theme', 'snsvicky_woocommerce_support' );
function snsvicky_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
if(class_exists('WooCommerce')){
    // Archive Product
    remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
    remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
    add_action( 'woocommerce_before_main_content', 'snsvicky_arichive_wrapper_start', 10 );
    add_action( 'woocommerce_after_main_content', 'snsvicky_arichive_wrapper_end', 10 );
    function snsvicky_arichive_wrapper_start() {
        if ( is_shop() || is_product_category() || is_product_tag() ) echo '<div class="listing-product-main"><div class="listing-product-wrap">';
    }
    function snsvicky_arichive_wrapper_end() {
        if ( is_shop() || is_product_category() || is_product_tag() ) echo '</div></div>';
    }
    // Number product per page
    add_filter( 'loop_shop_columns', 'snsvicky_woo_shop_columns');
    function snsvicky_woo_shop_columns() {
        if(is_product_category()){
            $cat = get_queried_object();
            $snsvicky_product_cat_grid_col = snsvicky_get_term_byid( $cat->term_id, 'snsvicky_product_cat_grid_col' );
            if( trim($snsvicky_product_cat_grid_col) != '' )
                return $snsvicky_product_cat_grid_col;
        }
        return snsvicky_themeoption('woo_grid_col', 3);
    }
    add_action( 'woocommerce_before_shop_loop', 'snsvicky_arichive_begin_toolbar_top', 1);
    add_action( 'woocommerce_before_shop_loop', 'snsvicky_arichive_modeview', 2);
    add_action( 'woocommerce_before_shop_loop', 'snsvicky_arichive_end_toolbar', 31);
    add_action( 'woocommerce_after_shop_loop', 'snsvicky_arichive_begin_toolbar_bottom', 1);
    add_action( 'woocommerce_after_shop_loop', 'snsvicky_arichive_end_toolbar', 31);
    add_action( 'woocommerce_before_shop_loop', 'snsvicky_filter_hoz', 19 );
    function snsvicky_filter_hoz(){
        if ( snsvicky_themeoption('woo_usefilterhoz') && is_active_sidebar('woo-sidebar-horizontal') && snsvicky_layouttype() == 'm' )
            echo '<div class="filter-hoz">'.esc_html__('Filter', 'snsvicky').'</div>';
    }
    function snsvicky_arichive_begin_toolbar_top(){
        echo '<div class="toolbar toolbar-top">';
    }
    function snsvicky_arichive_begin_toolbar_bottom(){
        echo '<div class="toolbar toolbar-bottom">';
    }
    function snsvicky_arichive_end_toolbar(){
        echo '</div>';
    }
    function snsvicky_arichive_modeview(){
        $modeview = 'grid';
        if (isset($_COOKIE['sns_woo_list_modeview']) && $_COOKIE['sns_woo_list_modeview']== 'list') {
            $modeview = 'list';
        }?>
        <ul class="mode-view pull-left">
            <li class="grid1">
                <a class="grid<?php echo ($modeview=='grid')?' active':''; ?>" data-mode="grid" href="#" title="<?php echo esc_html__('Grid', 'snsvicky'); ?>">
                    <span><?php echo esc_html__('Grid', 'snsvicky'); ?></span>
                </a>
            </li>
            <li class="list1">
                <a class="list<?php echo ($modeview=='list')?' active':''; ?>" data-mode="list" href="#" title="<?php echo esc_html__('List', 'snsvicky'); ?>">
                    <span><?php echo esc_html__('List', 'snsvicky'); ?></span>
                </a>
            </li>
        </ul>
        <?php
    }
    // Remove sidebar
    remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
    //
    remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10);
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
    add_action( 'woocommerce_after_shop_loop_item', 'snsvicky_item_addtocart', 10);
    add_action( 'woocommerce_after_shop_loop_item', 'snsvicky_item_wishlist', 11);
    add_action( 'woocommerce_after_shop_loop_item', 'snsvicky_item_compare', 12);
    function snsvicky_item_addtocart( $args = array() ) {
        global $product;
        if ( $product ) {
            $defaults = array(
                'quantity'   => 1,
                'class'      => implode( ' ', array_filter( array(
                    'button',
                    'product_type_' . $product->get_type(),
                    $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
                    $product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : '',
                ) ) ),
                'attributes' => array(
                    'data-product_id'  => $product->get_id(),
                    'data-product_sku' => $product->get_sku(),
                    'aria-label'       => $product->add_to_cart_description(),
                    'rel'              => 'nofollow',
                ),
            );
            $args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( $args, $defaults ), $product );
            ?>
            <div class="cart-wrap">
            <?php 
            wc_get_template( 'loop/add-to-cart.php', $args );
            ?>
            </div>
            <?php
        }
    }
    function snsvicky_item_wishlist(){
        if( class_exists( 'YITH_WCWL' ) ) {
            echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
        }
    }
    function snsvicky_item_compare(){
        global $product;
        if( class_exists( 'YITH_Woocompare' ) ) {
            $action_add = 'yith-woocompare-add-product';
            $url_args = array(
                'action' => $action_add,
                'id' => $product->get_id()
            );
            ?>
            <a href="<?php echo esc_url( wp_nonce_url( add_query_arg( $url_args ), $action_add ) ); ?>" class="compare btn btn-primary-outline" data-product_id="<?php echo esc_attr( $product->get_id() ); ?>">
            <?php echo esc_html__( 'Add to compare', 'snsvicky' ); ?>
            </a>
            <?php
        }
    }
    
    // Share box in product page
    if ( snsvicky_themeoption('woo_sharebox') == 1 ) add_action('woocommerce_share', 'snsvicky_sharebox', 22);
    function snsvicky_woo_category_image() {
        if ( is_product_category() ){
            global $wp_query;
            $cat = $wp_query->get_queried_object();
           
            $thumbnail_id = snsvicky_get_term_byid( $cat->term_id, 'thumbnail_id', true );
            
            $image = wp_get_attachment_image_src($thumbnail_id, 'full');
            $image = isset($image[0]) ? $image[0] : '';
            if ( $image ) {
                echo '<p class="cate-img"><img src="' . esc_url($image) . '" alt="" /></p>';
            }
        }
    }
    // Override thumbnail
    function snsvicky_post_thumbnail_html($html, $post_id, $post_thumbnail_id, $size, $attr) {
        if(snsvicky_themeoption('woo_uselazyload') == 1){
            $id = get_post_thumbnail_id();
            $src = wp_get_attachment_image_src($id, $size);
            $alt = get_the_title($id);
            $class = ( isset($attr['class']) ) ? $attr['class'] : '';
            if (strpos($class, 'lazy') !== false) {
                $html = '<img src="'.SNSVICKY_THEME_URI.'/assets/img/prod_loading.gif" alt="'.$alt.'" data-original="' . $src[0] . '" class="' . $class . '" />';
            }
        }
        return $html;
    }
    add_filter('post_thumbnail_html', 'snsvicky_post_thumbnail_html', 99, 5);
    function snsvicky_product_thumbnail(){
        global $post;
        $size = 'shop_catalog';
        if ( has_post_thumbnail() ) {
            if( snsvicky_themeoption('woo_uselazyload') == 1 ){
                echo get_the_post_thumbnail( $post->ID, $size, array('class' => "lazy attachment-$size") );
            }else{
                echo get_the_post_thumbnail( $post->ID, $size );
            }
        } elseif ( wc_placeholder_img_src() ) {
            echo wc_placeholder_img( $size );
        }
    }
    function snsvicky_get_woocommerce_categories(){
        $args = array(
            'taxonomy' => 'product_cat'
        );
         return get_categories($args);
    }

    add_action( 'woocommerce_before_main_content', 'snsvicky_woo_cat_slideshow', 11 );
    function snsvicky_woo_cat_slideshow(){
        $cat_slideshow = '';
        if ( is_product_category() ) {
            $cate = get_queried_object();
            $cat_slideshow = snsvicky_get_term_byid( $cate->term_id, 'snsvicky_product_cat_slideshow' );
        } elseif ( is_shop() ) {
            $cat_slideshow = snsvicky_getoption('revolutionslider');
        }
        if( !empty($cat_slideshow) ) { ?>
            <div class="cat-slideshow">
                <?php
                    echo do_shortcode('[rev_slider '.esc_attr($cat_slideshow).' ]');
                ?>
            </div>
        <?php
        }
    }
    remove_filter( 'woocommerce_product_loop_start', 'woocommerce_maybe_show_product_subcategories' );
    add_action( 'woocommerce_before_main_content', 'snsvicky_archive_subcategories', 12);
    function snsvicky_archive_subcategories(){ ?>
        <ul class="sub-cats">
        <?php
        $display_type = woocommerce_get_loop_display_mode();
        if ( 'subcategories' === $display_type || 'both' === $display_type ) {
            woocommerce_output_product_categories( array(
                'parent_id' => is_product_category() ? get_queried_object_id() : 0,
            ) );
        }
        ?>
        </ul>
        <?php
    }
    add_filter( 'woocommerce_product_tabs', 'snsvicky_woo_rename_tabs', 98 );
    function snsvicky_woo_rename_tabs( $tabs ) {
        $tabs['description']['title'] = esc_html__( 'Description', 'snsvicky' );
        if ( isset($tabs['additional_information']) ) {
            $tabs['additional_information']['title'] = esc_html__( 'Additional','snsvicky' );   // Rename the additional information tab
        }
        return $tabs;
    }

    if( !function_exists('sns_yith_woocompare') ){
        function sns_yith_woocompare(){
            global $product;
            if( class_exists( 'YITH_Woocompare' ) ) {
                    $action_add = 'yith-woocompare-add-product';
                    $url_args = array(
                        'action' => $action_add,
                        'id' => $product->get_id()
                    );
                    ?>
                    <a data-original-title="<?php echo esc_html( get_option('yith_woocompare_button_text') ); ?>" data-toggle="tooltip" href="<?php echo esc_url( wp_nonce_url( add_query_arg( $url_args ), $action_add ) ); ?>" class="compare btn btn-primary-outline" data-product_id="<?php echo esc_attr( $product->get_id() ); ?>">
                    </a>
            <?php }
        }
    }

    if( !function_exists('sns_yith_wcwl_add_to_wishlist') ){
        function sns_yith_wcwl_add_to_wishlist(){
            if( class_exists( 'YITH_WCWL' ) ) {
                echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
            }
        }
    }
    /**
     * For Single Product
     **/
    // Clear 
    add_action( 'woocommerce_single_product_summary', 'snsvicky_woo_addclear', 35 );
    function snsvicky_woo_addclear() {
        echo '<div class="clear"></div>';
    }
    
    // Cross sells
    add_filter( 'woocommerce_cross_sells_total', 'limit_woocommerce_cross_sells_total' );
    function limit_woocommerce_cross_sells_total() {
        return 6;
    }

	class snsvicky_Woocommerce{
        public $revsliders = array();

		public static function getInstance(){
			static $_instance;
			if( !$_instance ){
				$_instance = new snsvicky_Woocommerce();
			}
			return $_instance;
		}
		public function __construct(){
            global $wpdb;
            $this->revsliders[0] = 'Select a slider';
            if ( ! function_exists( 'is_plugin_active' ) ) {
                require_once ABSPATH . 'wp-admin/includes/plugin.php';
            }
            if (is_plugin_active('revslider/revslider.php')) {
                $query = $wpdb->prepare("
                    SELECT * 
                    FROM {$wpdb->prefix}revslider_sliders 
                    ORDER BY %s"
                    , "ASC");
                $get_sliders = $wpdb->get_results($query);
                if($get_sliders) {
                    foreach($get_sliders as $slider) {
                       $this->revsliders[$slider->alias] = $slider->title;
                   }
                }
            }
			// Grid cols per row
			add_filter( 'loop_shop_per_page', array($this, 'snsvicky_woo_shop_perpage') );
			// Remove each style one by one
			add_filter( 'woocommerce_enqueue_styles', array($this, 'snsvicky_dequeue_woostyles') );
			// Set modeview
			add_action( 'wp_ajax_sns_setmodeview', array($this,'snsvicky_set_modeview') );
			add_action( 'wp_ajax_nopriv_sns_setmodeview', array($this,'snsvicky_set_modeview') );
			// Ajax load more
			add_action('wp_ajax_sns_wooloadmore', array($this, 'snsvicky_woo_loadmore'));
			add_action('wp_ajax_nopriv_sns_wooloadmore', array($this, 'snsvicky_woo_loadmore'));
			
			// Ajax load more
			add_action('wp_ajax_sns_wootabloadproducts', array($this, 'snsvicky_woo_tab_load_products'));
			add_action('wp_ajax_nopriv_sns_wootabloadproducts', array($this, 'snsvicky_woo_tab_load_products'));

			remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
            add_action( 'woocommerce_before_shop_loop_item_title', 'snsvicky_product_thumbnail', 11 );

            // Add Taxonomy product_cat custom meta fields
            add_action('product_cat_add_form_fields', array(&$this, 'snsvicky_product_cat_add_new_meta_field'), 20, 3);
            add_action('product_cat_edit_form_fields', array(&$this, 'snsvicky_product_cat_edit_meta_field'), 20, 3);
            // Save extra taxonomy fields callback function
            add_action( 'edit_term', array(&$this,'snsvicky_save_product_cat_custom_meta'), 10, 3 );
            add_action( 'created_term', array(&$this,'snsvicky_save_product_cat_custom_meta'), 10, 3 );

            // Add Taxonomy product_attributes custom meta fields
            $attribute_taxonomies = wc_get_attribute_taxonomies();
            if ( $attribute_taxonomies ) {
                foreach ( $attribute_taxonomies as $attribute) {
                    add_action( 'pa_' . $attribute->attribute_name . '_add_form_fields', array(&$this, 'snsvicky_product_attribute_add_new_meta_field'), 20, 3 );
                    add_action( 'pa_' . $attribute->attribute_name . '_edit_form_fields', array(&$this, 'snsvicky_product_attribute_edit_meta_field'), 20, 3);
                    add_action( 'pa_' . $attribute->attribute_name . '_edit_form_fields', array(&$this, 'snsvicky_termattr_js'));
                    add_action( 'pa_' . $attribute->attribute_name . '_add_form_fields', array(&$this, 'snsvicky_termattr_js'));
                    add_action( 'edit_term', array(&$this,'snsvicky_product_attribute_save_custom_meta'), 10, 3 );
                    add_action( 'created_term', array(&$this,'snsvicky_product_attribute_save_custom_meta'), 10, 3 );
                }
            }
            // Add product variation select anwhere.
			remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
			remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
		}
		
		public function snsvicky_woo_loadmore(){
			$orderby        = $_POST['order'];
			$number_query   = $_POST['numberquery'];
          //  $show_button   = $_POST['show_button'];
			$start          = $_POST['start'];
			$list_cat       = $_POST['cat'];
			$col            = $_POST['col'];
			$eclass         = $_POST['eclass'];
			$loop = snsvicky_woo_query($orderby, $number_query, $list_cat, $start);
             $i = 0;
			while ( $loop->have_posts() ) : $loop->the_post();
            $i++; 
			wc_get_template( 'vc/item-grid.php', array('col' => $col, 'animate' => 1, 'eclass' => $eclass) );
            //  if(($i) % ($col-2) == 0) echo '<div class="clearfix visible-xs"></div>';  
            // if(($i) % ($col-1) == 0) echo '<div class="clearfix visible-sm"></div>';
            // if($i % ($col) == 0) echo '<div class="clearfix visible-md"></div>';
            // if($i % $col == 0) echo '<div class="clearfix visible-lg"></div>';   
			endwhile;
            wp_reset_postdata(); // Because snsvicky_woo_query return WP_Query
			wp_die();
		}
		
		public function snsvicky_woo_tab_load_products(){
			
			$tab_args = array(
					'data_type'		=> $_POST['data_type'],
					'wrap_id'		=> $_POST['wrap_id'],
					'tab_id'		=> $_POST['tab_id'],
					'cat'			=> $_POST['cat'],
					'template'		=> $_POST['template'],
					'orderby'		=> $_POST['orderby'],
					'number_query'	=> $_POST['number_query'],
					'number_display'=> $_POST['number_display'],
                    'number_display_1200'=> $_POST['number_display_1200'],
                    'number_display_992'=> $_POST['number_display_992'],
                    'number_display_768'=> $_POST['number_display_768'],
                    'number_display_480'=> $_POST['number_display_480'],
					'number_limit'	=> $_POST['number_limit'],
					'effect_load'	=> $_POST['effect_load'],
					'col'			=> $_POST['col'],
					'uq'			=> $_POST['uq'],
					'number_load'	=> $_POST['number_load'],
                   // 'show_button'   => $_POST['show_button'],
					'eclass'		=> $_POST['eclass'],
					'animate'		=> 'yes',
				);
			
			wc_get_template( 'vc/template-tab.php', array('tab_args' => $tab_args) );
			wp_die();
		}

		public function snsvicky_set_modeview(){
			setcookie('sns_woo_list_modeview', $_POST['mode'] , time()+3600*24*100, '/');
		}
		
		public function snsvicky_woo_shop_perpage( $columns ) {
			global $snsvicky_opt;
             if(is_product_category()){
                $cat = get_queried_object();
                $snsvicky_number_perpage = snsvicky_get_term_byid( $cat->term_id, 'snsvicky_number_perpage' );
                if( $snsvicky_number_perpage != '' )
                    return $snsvicky_number_perpage;
            }
            
		    return $snsvicky_opt['woo_number_perpage'];
		}
		
		public function snsvicky_dequeue_woostyles( $enqueue_styles ) {
			unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
			unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
			unset( $enqueue_styles['woocommerce-smallscreen'] );	// Remove the smallscreen optimisation
			return $enqueue_styles;
		}
         // Add term page
        public function snsvicky_product_cat_add_new_meta_field(){
            // This will add the custom meta field  to the add new term page
            $revsliders = $this->revsliders;
            ?>
            <div class="form-field term-snsvicky_product_cat_slideshow">
                <label for="snsvicky_product_cat_slideshow"><?php esc_html_e( 'Slideshow', 'snsvicky' ); ?></label>
                <select name="snsvicky_product_cat_slideshow" id="snsvicky_product_cat_slideshow">
                    <?php
                    foreach ($revsliders as $key => $value):?>
                       <option value="<?php  echo $key; ?>"><?php  echo $value; ?></option>
                    <?php
                    endforeach;
                    ?>
                </select>
                <p class="description"><?php esc_html_e( 'Select Slideshow.','snsvicky' ); ?></p>
            </div>

            <div class="form-field term-snsvicky_product_cat_layout">
                <label for="snsvicky_product_cat_layout"><?php esc_html_e( 'Layout', 'snsvicky' ); ?></label>
                <select name="snsvicky_product_cat_layout" id="snsvicky_product_cat_layout">
                    <option value=""><?php esc_html_e('Default (Layout of Shop page)', 'snsvicky');?></option>
                    <option value="m"><?php esc_html_e('Fullwidth', 'snsvicky');?></option>
                    <option value="l-m"><?php esc_html_e('Left Sidebar', 'snsvicky');?></option>
                    <option value="m-r"><?php esc_html_e('Right Sidebar', 'snsvicky');?></option>
                </select>
                <p class="description"><?php esc_html_e( 'Set the layout fullwidth or has sidebar (Woo Sidebar).','snsvicky' ); ?></p>
            </div>

            <div class="form-field term-snsvicky_product_cat_grid_col">
                <label for="snsvicky_product_cat_grid_col"><?php esc_html_e( 'Grid columns', 'snsvicky' ); ?></label>
                <select name="snsvicky_product_cat_grid_col" id="snsvicky_product_cat_grid_col">
                    <option value=""><?php esc_html_e('Default', 'snsvicky');?></option>
                    <option value="1">1</option>
                    <option value="1">2</option>
                    <option value="1">3</option>
                    <option value="1">4</option>
                    <option value="1">6</option>
                </select>
                <p class="description"><?php esc_html_e( 'We are using grid bootstap - 12 cols layout. Default use in Theme Options.','snsvicky' ); ?></p>
            </div>
             <div class="form-field term-snsvicky_number_perpage">
                <label for="snsvicky_number_perpage"><?php esc_html_e( 'Number products per listing page', 'snsvicky' ); ?></label>
                <input name="snsvicky_number_perpage" id="snsvicky_number_perpage" type="text" value="12" size="40">
            </div>
            
            <?php
        }
        // Edit term page
        public function snsvicky_product_cat_edit_meta_field($term, $taxonomy){
            $revsliders = $this->revsliders;
            $snsvicky_product_cat_slideshow = snsvicky_get_term_byid( $term->term_id, 'snsvicky_product_cat_slideshow' );

            $snsvicky_product_cat_layout = snsvicky_get_term_byid( $term->term_id, 'snsvicky_product_cat_layout' );
            $snsvicky_product_cat_grid_col = snsvicky_get_term_byid( $term->term_id, 'snsvicky_product_cat_grid_col' );
            $snsvicky_number_perpage = snsvicky_get_term_byid( $term->term_id, 'snsvicky_number_perpage' );
            ?>
            <tr class="form-field snsvicky_product_cat_slideshow">
                <th scope="row"><label for="snsvicky_product_cat_slideshow"><?php esc_html_e( 'Slideshow', 'snsvicky' ); ?></label></th>
                <td>
                    <select name="snsvicky_product_cat_slideshow" id="snsvicky_product_cat_slideshow">
                        <?php
                        foreach ($revsliders as $key => $value):?>
                           <option value="<?php  echo $key; ?>" <?php selected($snsvicky_product_cat_slideshow, $key, true)?>><?php  echo $value; ?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                    
                    <p class="description"><?php esc_html_e( 'Select Slideshow.','snsvicky' ); ?></p>
                </td>
            </tr>
            
            <tr class="form-field snsvicky_product_cat_layout">
                <th scope="row"><label for="snsvicky_product_cat_layout"><?php esc_html_e( 'Layout', 'snsvicky' ); ?></label></th>
                <td>
                    <select name="snsvicky_product_cat_layout" id="snsvicky_product_cat_layout">
                        <option value="" <?php selected($snsvicky_product_cat_layout, '', true)?>><?php esc_html_e('Default (Layout of Shop page)', 'snsvicky');?></option>
                        <option value="m" <?php selected($snsvicky_product_cat_layout, 'm', true)?>><?php esc_html_e('Fullwidth', 'snsvicky');?></option>
                        <option value="l-m" <?php selected($snsvicky_product_cat_layout, 'l-m', true)?>><?php esc_html_e('Left Sidebar', 'snsvicky');?></option>
                        <option value="m-r" <?php selected($snsvicky_product_cat_layout, 'm-r', true)?>><?php esc_html_e('Right Sidebar', 'snsvicky');?></option>
                    </select>
                    
                    <p class="description"><?php esc_html_e( 'Set the layout fullwidth or has sidebar (Woo Sidebar).','snsvicky' ); ?></p>
                </td>
            </tr>

            <tr class="form-field snsvicky_product_cat_grid_col">
                <th scope="row"><label for="snsvicky_product_cat_grid_col"><?php esc_html_e( 'Grid columns', 'snsvicky' ); ?></label></th>
                <td>
                    <select name="snsvicky_product_cat_grid_col" id="snsvicky_product_cat_grid_col">
                        <option value="" <?php selected($snsvicky_product_cat_grid_col, '', true)?>><?php esc_html_e('Default', 'snsvicky');?></option>
                        <option value="1" <?php selected($snsvicky_product_cat_grid_col, '1', true)?>>1</option>
                        <option value="2" <?php selected($snsvicky_product_cat_grid_col, '2', true)?>>2</option>
                        <option value="3" <?php selected($snsvicky_product_cat_grid_col, '3', true)?>>3</option>
                        <option value="4" <?php selected($snsvicky_product_cat_grid_col, '4', true)?>>4</option>
                        <option value="5" <?php selected($snsvicky_product_cat_grid_col, '5', true)?>>5</option>
                        <option value="6" <?php selected($snsvicky_product_cat_grid_col, '6', true)?>>6</option>
                    </select>
                    
                    <p class="description"><?php esc_html_e( 'We are using grid bootstap - 12 cols layout. Default use in Theme Options.','snsvicky' ); ?></p>
                </td>
            </tr>
             <tr class="form-field snsvicky_number_perpage">
                <th scope="row"><label for="snsvicky_number_perpage"><?php esc_html_e( 'Number products per listing page', 'snsvicky' ); ?></label></th>
                <td>
                    <input name="snsvicky_number_perpage" id="snsvicky_number_perpage" type="text" value="<?php echo $snsvicky_number_perpage;?>" size="40">
                </td>
            </tr>
            <?php
        }
        // Save term page
        public function snsvicky_save_product_cat_custom_meta($term_id, $tt_id, $taxonomy){
            $fields = array(
                'snsvicky_product_cat_slideshow',
                'snsvicky_product_cat_layout',
                'snsvicky_product_cat_grid_col',
                'snsvicky_number_perpage',
                'snsvicky_woo_onsale_layout',
                'snsvicky_woo_onsale_limit'
            );
            
            foreach ($fields as $field){
                if( isset($_POST[$field]) ){
                    $value = !empty($_POST[$field]) ? $_POST[$field] : '';
                    
                    update_woocommerce_term_meta($term_id, $field, $value);
                }
            }
        }

        /*
        *   Add term page for product_attribute
        */
        public function snsvicky_product_attribute_add_new_meta_field(){
            // This will add the custom meta field  to the add new term page
            ?>
            <div class="form-field term-snsvicky_product_attribute_type">
                <label for="snsvicky_product_attribute_type"><?php esc_html_e( 'Type', 'snsvicky' ); ?></label>
                <select name="snsvicky_product_attribute_type" id="snsvicky_product_attribute_type">
                    <option value="text"><?php esc_html_e('Text', 'snsvicky');?></option>
                    <option value="color"><?php esc_html_e('Color Picker', 'snsvicky');?></option>
                </select>
                <p class="description"></p>
            </div>

            <div class="form-field term-snsvicky_product_attribute_color">
                <label for="snsvicky_product_attribute_color"><?php esc_html_e( 'Color Picker', 'snsvicky' ); ?></label>
                <input type="text" class="colorpicker sns-colorpicker" value="" name="snsvicky_product_attribute_color"/>
                <p class="description"></p>
            </div>
            
            <?php
        }
        // Term attribute js
        function snsvicky_termattr_js(){
            wp_enqueue_script('snsvicky-termattr', SNSVICKY_THEME_URI . '/assets/js/sns-woo-termattr.js', array('jquery'), '', true);
        }
        // Edit term page
        public function snsvicky_product_attribute_edit_meta_field($term, $taxonomy){
            $snsvicky_product_attribute_type = snsvicky_get_term_byid( $term->term_id, 'snsvicky_product_attribute_type' );
            $snsvicky_product_attribute_color = snsvicky_get_term_byid( $term->term_id, 'snsvicky_product_attribute_color' );
            ?>
            
            <tr class="form-field snsvicky_product_attribute_type">
                <th scope="row"><label for="snsvicky_product_attribute_type"><?php esc_html_e( 'Type', 'snsvicky' ); ?></label></th>
                <td>
                    <select name="snsvicky_product_attribute_type" id="snsvicky_product_attribute_type">
                        <option value="text" <?php selected($snsvicky_product_attribute_type, 'text', true)?>><?php esc_html_e('Text', 'snsvicky');?></option>
                        <option value="color" <?php selected($snsvicky_product_attribute_type, 'color', true)?>><?php esc_html_e('Color Picker', 'snsvicky');?></option>
                    </select>
                    
                    <p class="description"></p>
                </td>
            </tr>

            <tr class="form-field term-snsvicky_product_attribute_color">
                <th scope="row"><label for="snsvicky_product_attribute_color"><?php esc_html_e( 'Color Picker', 'snsvicky' ); ?></label></th>
                <td>
                    <input type="text" class="colorpicker sns-colorpicker" value="<?php echo esc_attr( $snsvicky_product_attribute_color );?>" name="snsvicky_product_attribute_color"/>
                    <p class="description"></p>
                </td>
            </tr>

            <?php
        }
        // Save term page
        public function snsvicky_product_attribute_save_custom_meta($term_id, $tt_id, $taxonomy){
            $fields = array(
                'snsvicky_product_attribute_type',
                'snsvicky_product_attribute_color',
                'snsvicky_product_attribute_image_id'
            );
            
            foreach ($fields as $field){
                if( isset($_POST[$field]) ){
                    $value = !empty($_POST[$field]) ? $_POST[$field] : '';
                    
                    update_woocommerce_term_meta($term_id, $field, $value);
                }
            }
        }
		
	}

	snsvicky_Woocommerce::getInstance();
    function snsvicky_cart_url(){
        global $woocommerce;
        return esc_url( $woocommerce->cart->get_cart_url() );
    }
    
    function snsvicky_cart_total(){
        global $woocommerce;
        return $woocommerce->cart->get_cart_total();
    }
    // Re-render rating
    add_filter( 'woocommerce_product_get_rating_html', 'snsvicky_get_rating_html', 100, 2 );
    function snsvicky_get_rating_html(){
        global $woocommerce, $product;
        if( $product && $product->get_average_rating() ) $rating = $product->get_average_rating();
        if( isset($rating) && $rating > 0){
        	$rating_html  = '<div class="star-rating" title="' . sprintf( esc_html__( 'Rated %s out of 5', 'snsvicky' ), $rating ) . '">';
    		$rating_html .= '<span class="value" data-width="' . ( ( $rating / 5 ) * 100 ) . '%"><strong class="rating">' . $rating . '</strong> ' . esc_html__( 'out of 5', 'snsvicky' ) . '</span>';
        }else{
        	$rating_html  = '<div class="star-rating">';
        }
    	$rating_html .= '</div>';
    	return $rating_html;
    }
    // Display variations color
    add_action( 'snsvicky_variations_color_loop_item', 'snsvicky_display_variations_color', 15 );
    function snsvicky_display_variations_color(){
        global $product;
        if ( $product->get_type() != 'variable' ) { return; }
        $variations = $product->get_variation_attributes();
        if ( !empty($variations) && !isset($variations['pa_color']) && count($variations['pa_color']) <= 0 ) { return; }
        ?>
        <div class="variations-product-wrap">
            <div class="variable-item">
        <?php
        $available_variations = $product->get_available_variations();
        $terms = wc_get_product_terms( $product->get_id(), 'pa_color', array( 'fields' => 'all' ));
        foreach ($terms as $term) {
            foreach ($available_variations as $available_variation) {
                if($term->slug === $available_variation['attributes']["attribute_$term->taxonomy"]){
                    $image_src = get_post_thumbnail_id( $available_variation['variation_id'] ); 
                    $image_src = wp_get_attachment_image_src( $image_src, 'shop_catalog');
                    $image_src = isset($image_src['0']) ? $image_src['0'] : '';
                    $color = snsvicky_get_term_byid( $term->term_id, 'snsvicky_product_attribute_color' ); ?>
                    <a href="#" class="option color" title="<?php echo esc_attr( $term->name );?>" data-toggle="tooltip" data-original-title="<?php echo esc_attr( $term->name );?>" data-value="<?php echo esc_attr( $term->slug );?>" data-image-src="<?php echo esc_url( $image_src );?>"><span style="background:<?php echo esc_attr( $color );?>"></span></a>
                    <?php
                    break;
                }
            }
        }?>
        </div></div>
        <?php
    }
    function snsvicky_woo_display_special_imgprd ( $special_img ) {
        foreach ( $special_img as $img ) {
            if ( $img['full_url'] !='' ){
                if ( snsvicky_themeoption('woo_uselazyload') == 1 ) {
                    echo '<img src="'.SNSVICKY_THEME_URI.'/assets/img/prod_loading.gif" alt="" data-original="' . $img['full_url'] . '" class="lazy attachment-shop_catalog" />';
                } else {
                    echo '<img src="' . $img['full_url'] . '" alt="" class="attachment-shop_catalog" />';
                }
            }
            break;
        }
    }
    function snsvicy_scd_sold_bar(){
         global $product;
        ?>
        <div class="sold-bar">
            <div class="num-sold"><?php echo esc_html__('Already Sold: ', 'snsvicky') . $product->get_total_sales() ; ?></div>
            <div class="num-stock"><?php echo esc_html__('Available: ', 'snsvicky') . $product->get_stock_quantity() ; ?></div>
            <div class="the-bar">
                <span style="width: <?php echo ( $product->get_total_sales() < $product->get_stock_quantity() ) ? ($product->get_total_sales()/$product->get_stock_quantity())*100 : '0'; ; ?>%"></span>
            </div>            
        </div>
        <?php
    }
    function snsvicy_scd_time_countdown () {
        global $post, $product;
        // Get the Sale Price Date To of the product
        $sale_price_dates_to = ( $date = get_post_meta( $post->ID, '_sale_price_dates_to', true ) ) ? date_i18n( 'Y/m/d', $date ) : '';
        if(!empty($sale_price_dates_to)):
            // Set sale price date to default 10 days for http://demo.snstheme.com/
            if($_SERVER['SERVER_NAME'] == 'demo.snstheme.com' || $_SERVER['SERVER_NAME'] == 'dev.snsgroup.me' ){
                if($sale_price_dates_to == 0) $sale_price_dates_to = date('Y/m/d', strtotime('+10 days'));
            }
            wp_enqueue_script('countdown');
            $uq = rand().time();
            ?>
            <h4 class="count-down-heading"><?php echo esc_html__('Hurry Up! hotdeal ends in:', 'snsvicky'); ?></h4>
            <div class="time-count-down" id="sns-tcd-<?php echo $uq; ?>" data-date="<?php echo esc_attr($sale_price_dates_to); ?>">
                <div class="clock-digi">
                    <div><div class="day"></div><?php esc_html_e('Days', 'snsvicky');?></div>
                    <div><div class="hours"></div><?php esc_html_e('Hours', 'snsvicky');?></div>
                    <div><div class="minutes"></div><?php esc_html_e('Mins', 'snsvicky');?></div>
                    <div><div class="seconds"></div><?php esc_html_e('Secs', 'snsvicky');?></div>
                </div>
            </div>
        <?php endif;
    }
    function snsvicky_woo_single_product_images(){
        wc_get_template( 'single-product/product-image-nogallery.php' );
    }
    add_action('woocommerce_single_product_summary', 'snsvicky_woo_single_countdown', 25);
    function snsvicky_woo_single_countdown () {
        global $post, $product;
        // Get the Sale Price Date To of the product
        $sale_price_dates_to = ( $date = get_post_meta( $post->ID, '_sale_price_dates_to', true ) ) ? date_i18n( 'Y/m/d', $date ) : '';
        if(!empty($sale_price_dates_to)):
            // Set sale price date to default 10 days for http://demo.snstheme.com/
            if($_SERVER['SERVER_NAME'] == 'demo.snstheme.com' || $_SERVER['SERVER_NAME'] == 'dev.snsgroup.me' ){
                if($sale_price_dates_to == 0) $sale_price_dates_to = date('Y/m/d', strtotime('+10 days'));
            }
            wp_enqueue_script('countdown');
            $uq = rand().time();
            ?>
            <div class="time-count-down" id="sns-tcd-<?php echo $uq; ?>" data-date="<?php echo esc_attr($sale_price_dates_to); ?>">
                <div class="clock-digi">
                    <div><div class="day"></div><?php esc_html_e('Days', 'snsvicky');?></div>
                    <div><div class="hours"></div><?php esc_html_e('Hours', 'snsvicky');?></div>
                    <div><div class="minutes"></div><?php esc_html_e('Mins', 'snsvicky');?></div>
                    <div><div class="seconds"></div><?php esc_html_e('Secs', 'snsvicky');?></div>
                </div>
            </div>
        <?php endif;
    }
    function snsvicky_woo_query($type, $post_per_page=-1, $cat='', $offset=0, $paged=1){
        global $woocommerce;
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => $post_per_page,
            'post_status' => 'publish',
        	'offset'            => $offset,
            'paged' => $paged
        );
        switch ($type) {
            case 'best_selling':
                $args['meta_key']='total_sales';
                $args['orderby']='meta_value_num';
                $args['ignore_sticky_posts']   = 1;
                $args['meta_query'] = array();
                $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
                break;
            case 'featured_product':
                $args['ignore_sticky_posts']=1;
                $args['meta_query'] = array();
                $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
                $args['post__in'] = wc_get_featured_product_ids();
                break;
            case 'top_rate':
                $args['meta_key'] = '_wc_average_rating';
                $args['orderby']  = array(
                    'meta_value_num' => 'DESC',
                    'ID'             => 'ASC',
                );
                break;
            case 'recent':
                $args['meta_query'] = array();
                $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                break;
            case 'on_sale':
                $args['meta_query'] = array();
                $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
                $args['post__in'] = wc_get_product_ids_on_sale();
                break;
            case 'hot_deal':
                global $wpdb;
                $query = $wpdb->prepare("
                    SELECT posts.ID, posts.post_parent
                    FROM {$wpdb->prefix}posts posts
                    INNER JOIN {$wpdb->prefix}postmeta ON (posts.ID = {$wpdb->prefix}postmeta.post_id)
                    INNER JOIN {$wpdb->prefix}postmeta AS mt1 ON (posts.ID = mt1.post_id)
                    WHERE
                        posts.post_status = 'publish'
                        AND  (mt1.meta_key = '_sale_price_dates_to' AND mt1.meta_value >= '.time().') 
                        GROUP BY posts.ID 
                        ORDER BY posts.post_title ASC
                    ");
                $product_ids_raw = $wpdb->get_results($query);
                $product_ids_hotdeal = array();
                foreach ( $product_ids_raw as $product_raw ) {
                    if(!empty($product_raw->post_parent)){
                        $product_ids_hotdeal[] = $product_raw->post_parent;
                    }else{
                        $product_ids_hotdeal[] = $product_raw->ID;  
                    }
                }
                $product_ids_hotdeal = array_unique($product_ids_hotdeal);
                if ( is_array($product_ids_hotdeal) && count($product_ids_hotdeal) == 0 ) {
                    $product_ids_hotdeal = wc_get_product_ids_on_sale(); 
                }
                $args['meta_query'] = array();
                $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
                $args['post__in'] = $product_ids_hotdeal;
                break;
            case 'recent_review':
                if($post_per_page == -1) $_limit = 4;
                else $_limit = $post_per_page;
                global $wpdb;
                $query = $wpdb->prepare( "
                    SELECT c.comment_post_ID 
                    FROM {$wpdb->prefix}posts p, {$wpdb->prefix}comments c 
                    WHERE p.ID = c.comment_post_ID AND c.comment_approved > %d 
                    AND p.post_type = %s AND p.post_status = %s
                    AND p.comment_count > %d ORDER BY c.comment_date ASC" ,
                    0, 'product', 'publish', 0 );
                $results = $wpdb->get_results($query, OBJECT);
                $_pids = array();
                foreach ($results as $re) {
                    if(!in_array($re->comment_post_ID, $_pids))
                        $_pids[] = $re->comment_post_ID;
                    if(count($_pids) == $_limit)
                        break;
                }

                $args['meta_query'] = array();
                $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
                $args['post__in'] = $_pids;
                break;
        }

        if($cat!=''){
            $args['product_cat']= $cat;
        }
        return new WP_Query($args);
    }
    
}

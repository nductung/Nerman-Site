<?php
class snsvicky_Megamenu_Front extends Walker_Nav_Menu { 
    var $columns = 0;
    var $enablemega = 0;
    var $stylemega = '';
    var $sidebaremega = '';
    var $bgmegacol = '';
    var $customcolumnstyle = '';

    function start_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);
        $uq = rand(1, 1000);
        if($depth === 0 && $this->enablemega && $this->stylemega !=''){
            $left_content = $style_inline = $class = '';
            if($this->sidebaremega == 'menu_sidebar_1'){
                if (is_active_sidebar('menu_sidebar_1')){
                    $class .= ' has-left-content';
                }
            }elseif($this->sidebaremega == 'menu_sidebar_2'){
                if (is_active_sidebar('menu_sidebar_2')){
                    $class .= ' has-right-content';
                }
            }
            if($this->sidebaremega == 'menu_sidebar_1'){
                if (is_active_sidebar('menu_sidebar_1')){
                    ob_start();
                    dynamic_sidebar('menu_sidebar_1');
                    $left_content .= ob_get_clean();
                }
                if ( $left_content != '' ) {
                    $left_content = '<div class="menu-left-sidebar">'.$left_content.'</div>';
                } 
            }
            // Background image for Columns tyle
            if($this->stylemega == 'columns' && $this->bgmegacol != ''){
                $style_inline = 'style="background-image: url('.esc_url($this->bgmegacol).'); '. trim($this->customcolumnstyle) .'"';
                $class .= ' sub-content-background';
            }
            $output .= "\n$indent<div id=\"sub_content_$uq\" class=\"sub-content dropdownmenu $this->stylemega $class\" $style_inline>$left_content <ul class=\"". $this->stylemega ." {replace_class}\">\n";
        }else{
            $output .= "\n$indent<ul class=\"sub-menu {replace_class}\">\n";
        }
    }
    function end_lvl(&$output, $depth = 0, $args = array()) { 
        $indent = str_repeat("\t", $depth);
        // add menu sidebar
        if($depth === 0 && $this->enablemega && $this->stylemega !=''){
            if ($this->sidebaremega == 'menu_sidebar_2'){
                $right_content = '';
                if (is_active_sidebar('menu_sidebar_2')){
                    ob_start();
                    dynamic_sidebar('menu_sidebar_2');
                    $right_content .= ob_get_clean();
                }
                if ( $right_content != '' ) {
                    $right_content = '<div class="menu-right-sidebar">'.$right_content.'</div>';
                } 
                $output .= "$indent</ul>$right_content</div>\n";
            }elseif($this->sidebaremega == 'menu_sidebar_3'){
                $bottom_content = '';
                if (is_active_sidebar('menu_sidebar_3')){
                    ob_start();
                    dynamic_sidebar('menu_sidebar_3');
                    $bottom_content .= ob_get_clean();
                }
                if ( $bottom_content != '' ) {
                    $bottom_content = '<div class="menu-bottom-sidebar">'.$bottom_content.'</div>';
                } 
                $output .= "$indent</ul>$bottom_content</div>\n";
            }else{
                $output .= "$indent</ul></div>\n";
            }
        }
        else{
            $output .= "$indent</ul>\n";
        }
        if ($depth === 0) {
            if($this->enablemega && $this->columns > 0){
                // if($this->sidebaremega == 'menu_sidebar_2') $this->columns = $this->columns + 1; // Add a column for Right Sidebar Menu
                // if($this->sidebaremega == 'menu_sidebar_1') $this->columns = $this->columns + 1; // Add a column for Right Sidebar Menu
                $output = str_replace("{replace_class}", "enable-megamenu row-fluid col-".$this->columns."", $output);
                $this->columns = 0;
            }
            else{
                $output = str_replace("{replace_class}", "", $output);
            }
        }
    }    
    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
        global $wp_query;
        $item_output = $li_text_block_class = $column_class = "";
        if($depth === 0){   
            $this->enablemega = get_post_meta( $item->ID, '_sns_megamenu_item_enable', true);
            $this->stylemega = get_post_meta( $item->ID, '_sns_megamenu_item_style', true);
            $this->sidebaremega = get_post_meta( $item->ID, '_sns_megamenu_item_sidebar', true);
            $this->bgmegacol = get_post_meta( $item->ID, '_sns_megamenu_item_background', true);
            $this->customcolumnstyle = get_post_meta( $item->ID, '_sns_megamenu_item_customcolumnstyle', true);
        }
        if($depth === 1 && $this->enablemega) {
            $this->columns ++;
            if( $item->hidetitlemega != true && $this->stylemega == 'columns'){
                 $title = apply_filters( 'the_title', $item->title, $item->ID );
                if($title != "-" && $title != '"-"'){
                   
                   $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
                   $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
                   $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
                   $attributes .= ! empty( $item->url )        ? ' href="'   . esc_url( $item->url        ) .'"' : '';    
            
                   $item_output .= $args->before;
                   $item_output .= '<h4 class="megamenu-title">';
                   if ( get_post_meta( $item->ID, '_sns_megamenu_item_icon', true) != '' && get_post_meta( $item->ID, '_sns_megamenu_item_useicon', true) != '' ) {
                        if ( get_post_meta( $item->ID, '_sns_megamenu_item_useicon', true) == 'font' ) {
                            $item_output .='<i class="'.get_post_meta( $item->ID, '_sns_megamenu_item_icon', true).'"></i>';
                       }elseif( get_post_meta( $item->ID, '_sns_megamenu_item_useicon', true) == 'image' ){
                            $item_output .= '<img class="sns-megamenu-icon-img" src="'.get_post_meta( $item->ID, '_sns_megamenu_item_icon', true).'" alt=""/>';
                       }
                   }
                   $item_output .= '<a'. $attributes .'>';
                   $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
                   $item_output .= '</a></h4>';
                   $item_output .= $args->after;
               }
            }
        }else{
            $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
            $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
            $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
            $attributes .= ! empty( $item->url )        ? ' href="'   . esc_url( $item->url        ) .'"' : '';            
        
            $item_output .= $args->before;
            $item_output .= '<a'. $attributes .'>';
            if ( get_post_meta( $item->ID, '_sns_megamenu_item_icon', true) != '' && get_post_meta( $item->ID, '_sns_megamenu_item_useicon', true) != '' ) {
                if ( get_post_meta( $item->ID, '_sns_megamenu_item_useicon', true) == 'font' ) {
                    $item_output .='<i class="'.get_post_meta( $item->ID, '_sns_megamenu_item_icon', true).'"></i>';
               }elseif( get_post_meta( $item->ID, '_sns_megamenu_item_useicon', true) == 'image' ){
                    $item_output .= '<img class="sns-megamenu-icon-img" src="'.get_post_meta( $item->ID, '_sns_megamenu_item_icon', true).'" alt=""/>';
               }
            }
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            $item_output .= '</a>';
            $item_output .= $args->after;
        }
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $class_names = $value = '';

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        if( $depth == 0 && $this->enablemega && $this->stylemega !='') $class_names .= ' enable-mega';
        if ( get_post_meta( $item->ID, '_sns_megamenu_item_icon', true) != '' && get_post_meta( $item->ID, '_sns_megamenu_item_useicon', true) != '' ) $class_names .= ' have-icon';
        $class_names = ' class="'.$li_text_block_class. esc_attr( $class_names ) . $column_class.'"';
        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';
        $output .= $indent . '<li '.$id . $value . $class_names .'>'; 
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}
?>
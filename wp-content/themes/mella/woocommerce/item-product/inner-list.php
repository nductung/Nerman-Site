<?php 
global $product;
$product_id = $product->get_id();
$image_size = isset($image_size) ? $image_size : 'woocommerce_thumbnail';
?>
<div class="product-block product-block-list" data-product-id="<?php echo esc_attr($product_id); ?>">
	<div class="row">
		<div class="col-xs-5 col-sm-4 col-lg-4">	
			<div class="block-inner">
			    <figure class="image">
			        <a title="<?php echo esc_attr(get_the_title()); ?>" href="<?php the_permalink(); ?>" class="product-image">
			            <?php
			                /**
			                * woocommerce_before_shop_loop_item_title hook
			                *
			                * @hooked woocommerce_show_product_loop_sale_flash - 10
			                * @hooked woocommerce_template_loop_product_thumbnail - 10
			                */
			                do_action( 'woocommerce_before_shop_loop_item_title' );
			            ?>
			        </a>
			        <?php do_action('mella_woocommerce_before_shop_loop_item'); ?>
			    </figure>
			    <?php Mella_Woo_Swatches::swatches_list( $image_size ); ?>
			</div> 
		</div>
		<div class="col-lg-1"></div>
		<div class="col-sm-8 col-lg-7 col-xs-7">
		    <div class="wrapper-info">
		    	<div class="top-info clearfix">
			    	<div class="btn-wrapper pull-right">
	                    <?php if (mella_get_config('show_quickview', false)) { ?>
	                        <a href="#" class="quickview" data-product_id="<?php echo esc_attr($product_id); ?>" data-toggle="modal" data-target="#apus-quickview-modal">
	                            <i class="icon-enlarge"></i>
	                        </a>
	                    <?php } ?>
	                    <?php
	                        if ( class_exists( 'YITH_WCWL' ) ) {
	                            echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
	                        }
	                    ?>
	                </div>
	                <h3 class="name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
				</div>
		    	<?php
		            /**
		            * woocommerce_after_shop_loop_item_title hook
		            *
		            * @hooked woocommerce_template_loop_rating - 5
		            * @hooked woocommerce_template_loop_price - 10
		            */
		            remove_action('woocommerce_after_shop_loop_item_title','woocommerce_template_loop_rating', 5);
		            do_action( 'woocommerce_after_shop_loop_item_title');
		        ?>
	            <div class="rating clearfix">
	                <?php
	                    $rating_html = wc_get_rating_html( $product->get_average_rating() );
	                    $count = $product->get_rating_count();
	                    if ( $rating_html ) {
	                        echo trim( $rating_html );
	                    } else {
	                        echo '<div class="star-rating"></div>';
	                    }
	                    echo '<span class="counts">('.$count.')</span>';
	                ?>
	            </div>
	            <div class="product-excerpt">
		           <?php echo mella_substring( get_the_excerpt(), 50, '...' ); ?>
		        </div>
		        
		        <div class="cart-left">
	        		<?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
	        	</div>
			</div>
		</div>  
	</div>
</div>
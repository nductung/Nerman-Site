<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.5.1
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $post, $product;

$attachment_ids = $product->get_gallery_image_ids();

?>
<div class="thumbnails">
    <?php
    if( snsvicky_getoption('woo_gallery_type', 'h') == 'v' ){
        $html = '<div class="product-thumbs">';
    }else{
        $html = '<div class="product-thumbs owl-carousel">';
    }
    if ( has_post_thumbnail() ) {
        $attachment_id = get_post_thumbnail_id();
        $full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
        $thumbnail       = wp_get_attachment_image_src( $attachment_id, 'snsvicky_woo_110127_thumb' );

        $attributes = array(
            'title'                   => get_post_field( 'post_title', $attachment_id ),
            'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
            'data-zoom-image'         => $full_size_image[0],
            'data-src'                => $full_size_image[0],
            'data-large_image'        => $full_size_image[0],
            'data-large_image_width'  => $full_size_image[1],
            'data-large_image_height' => $full_size_image[2],
            'class'                   => "woocommerce-main-thumb img-responsive"
        );
        $html .= '<div data-thumb="' . esc_url( $thumbnail[0] ) . '" class="img woocommerce-product-gallery__image">';
        $html .= wp_get_attachment_image( $attachment_id, 'snsvicky_woo_110127_thumb', false, $attributes );
        $html .= '</div>';

    } else {
        $thumb_link = wc_placeholder_img_src();
        $html .= '<div class="img">';
        $html .= apply_filters( 'woocommerce_single_product_image_thumbnail_html', '<img class="woocommerce-main-thumb img-responsive" alt="" src="' . $thumb_link . '" />', false, $post->ID, '');
        $html .= '</div>';
    }
    if ( $attachment_ids && has_post_thumbnail() ) {
        foreach ( $attachment_ids as $attachment_id ) {
            $full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
            $thumbnail       = wp_get_attachment_image_src( $attachment_id, 'snsvicky_woo_110127_thumb' );
            $attributes = array(
                'title'                   => get_post_field( 'post_title', $attachment_id ),
                'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
                'data-zoom-image'         => $full_size_image[0],
                'data-src'                => $full_size_image[0],
                'data-large_image'        => $full_size_image[0],
                'data-large_image_width'  => $full_size_image[1],
                'data-large_image_height' => $full_size_image[2],
                'class'                   => "img-responsive"
            );
            $html .= '<div data-thumb="' . esc_url( $thumbnail[0] ) . '" class="img woocommerce-product-gallery__image">';
            $html .= wp_get_attachment_image( $attachment_id, 'snsvicky_woo_110127_thumb', false, $attributes );
            $html .= '</div>';
        }
    }
    $html .= '</div>';
    echo $html;
    ?>
</div>

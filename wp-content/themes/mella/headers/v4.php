<header id="apus-header" class="apus-header header-v4 hidden-sm hidden-md hidden-xs" role="banner">
    <div class="<?php echo (mella_get_config('keep_header') ? 'main-sticky-header-wrapper' : ''); ?>">
        <div class="<?php echo (mella_get_config('keep_header') ? 'main-sticky-header' : ''); ?>">
            <div class="container-fluid inner-header p-relative">
                    <div class="flex-middle row">
                        <div class="col-lg-4 col-md-4 group-button-header">
                            <?php if ( has_nav_menu( 'primary' ) ) { ?>
                                <div class="pull-left">
                                    <a class="btn-main-menu-left" href="#main-menu">
                                        <span class="icon-show-menu">
                                            <span class="line1"></span>
                                            <span class="line2"></span>
                                            <span class="line3"></span>
                                        </span>
                                        <span class="text"><?php esc_html_e('Menu', 'mella'); ?></span>
                                    </a>
                                </div>
                            <?php } ?>
                            <?php
                            if ( mella_is_woocommerce_activated() ) {
                            $page_id = wc_get_page_id( 'shop' );
                            $page_url = get_permalink( $page_id );
                            ?>
                                <div class="pull-left">
                                    <a class="btn-main-menu" href="<?php echo esc_url($page_url); ?>"><i class="fa fa-th-large" aria-hidden="true"></i> <?php esc_html_e('Shop', 'mella'); ?></a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="logo-in-theme text-center">
                                <?php get_template_part( 'template-parts/logo/logo-red' ); ?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="header-right clearfix">
                                <?php if ( mella_get_config('show_user_info', true) ) { ?>
                                    <div class="pull-right">
                                        <div class="top-wrapper-menu">
                                            <a class="drop-dow"><i class="icon-user-1"></i></a>
                                            <?php if( is_user_logged_in()){ ?>
                                                <?php if ( has_nav_menu( 'top-menu' ) ) {
                                                        $args = array(
                                                            'theme_location' => 'top-menu',
                                                            'container_class' => 'inner-top-menu',
                                                            'menu_class' => 'nav navbar-nav topmenu-menu',
                                                            'fallback_cb' => '',
                                                            'menu_id' => '',
                                                            'walker' => new Mella_Nav_Menu()
                                                        );
                                                        wp_nav_menu($args);
                                                    }
                                                ?>
                                            <?php } else { ?>
                                                <?php get_template_part( 'template-parts/login' ); ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ( mella_is_woocommerce_activated() && mella_get_config('show_cartbtn') && !mella_get_config( 'enable_shop_catalog' ) ): ?>
                                    <div class="pull-right">
                                        <?php get_template_part( 'woocommerce/cart/mini-cart-button' ); ?>
                                    </div>
                                <?php endif; ?>

                                <?php if ( class_exists( 'YITH_WCWL' ) && mella_get_config('show_wishlist', true) ):
                                    $wishlist_url = YITH_WCWL()->get_wishlist_url();
                                ?>
                                    <div class="pull-right">
                                        <a class="wishlist-icon" href="<?php echo esc_url($wishlist_url);?>" title="<?php esc_attr_e( 'View Your Wishlist', 'mella' ); ?>"><i class="icon-heart3"></i>
                                            <?php if ( function_exists('yith_wcwl_count_products') ) { ?>
                                                <span class="count"><?php echo yith_wcwl_count_products(); ?></span>
                                            <?php } ?>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                
                                <?php if ( mella_get_config('show_searchform') ){ ?>
                                    <div class="pull-right">
                                        <a class="btn-search-top"><i class="icon-magnifying-glass"></i></a>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>   
            </div>
        </div>
    </div>
</header>
<?php if ( has_nav_menu( 'primary-sidebar' ) ) : ?>
    <div class="site-header-mainmenu left st-dark">
        <div class="main-menu">
            <nav data-duration="400" class="hidden-xs hidden-sm apus-megamenu slide animate navbar p-static" role="navigation">
            <?php   $args = array(
                    'theme_location' => 'primary-sidebar',
                    'container_class' => 'collapse navbar-collapse no-padding',
                    'menu_class' => 'nav navbar-nav megamenu',
                    'fallback_cb' => '',
                    'menu_id' => 'primary-menu',
                    'walker' => new Mella_Nav_Menu()
                );
                wp_nav_menu($args);
            ?>
            </nav>
        </div>
        <?php
            $social_links = mella_get_config('header_social_links_link');
            $social_icons = mella_get_config('header_social_links_icon');
            if ( !empty($social_links) ) {
                ?>
                <ul class="socials-wrapper">
                    <?php foreach ($social_links as $key => $value) { ?>
                        <li class="social-item">
                            <a href="<?php echo esc_url($value); ?>">
                                <i class="<?php echo esc_attr($social_icons[$key]); ?>"></i>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                <?php
            }
        ?>
    </div>
<?php endif; ?>
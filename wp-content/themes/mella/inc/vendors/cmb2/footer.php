<?php
if ( !function_exists( 'mella_footer_metaboxes' ) ) {
	function mella_footer_metaboxes(array $metaboxes) {
		$prefix = 'apus_footer_';
	    $fields = array(
			array(
				'name' => esc_html__( 'Footer Style', 'mella' ),
				'id'   => $prefix.'style_class',
				'type' => 'select',
				'options' => array(
					'container' => esc_html__('Boxed', 'mella'),
					'full container-fluid' => esc_html__('Full Large', 'mella'),
					'full-medium container-fluid' => esc_html__('Full Medium', 'mella'),
				)
			),
			array(
				'name' => esc_html__( 'Footer Background Color', 'mella' ),
				'id'   => $prefix.'background_class',
				'type' => 'select',
				'options' => array(
					'' => esc_html__('White', 'mella'),
					'dark' => esc_html__('Dark', 'mella'),
				)
			),
    	);
		
	    $metaboxes[$prefix . 'display_setting'] = array(
			'id'                        => $prefix . 'display_setting',
			'title'                     => esc_html__( 'Display Settings', 'mella' ),
			'object_types'              => array( 'apus_footer' ),
			'context'                   => 'normal',
			'priority'                  => 'high',
			'show_names'                => true,
			'fields'                    => $fields
		);

	    return $metaboxes;
	}
}
add_filter( 'cmb2_meta_boxes', 'mella_footer_metaboxes' );
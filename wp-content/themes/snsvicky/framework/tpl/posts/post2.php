<article id="post-<?php the_ID(); ?>" class="post type-post post-list" >
	<div <?php post_class('row'); ?>>
	    <?php
	    $post_media = false;
	    // Post Quote
	    if ( get_post_format() == 'quote' && function_exists('rwmb_meta') && rwmb_meta('snsvicky_post_quotecontent') && rwmb_meta('snsvicky_post_quoteauthor') ) :
	    	$post_media = true;
	        $uq  = rand().time();
	        ?>
	        <div class="col-md-5 part-left">
		        <div class="quote-info quote-info-<?php echo $uq; ?>">
		            <?php if ( rwmb_meta('snsvicky_post_quotecontent') ) : ?>
		            <div class="quote-content"><i class="fa fa-quote-left"></i> <?php echo esc_html(rwmb_meta('snsvicky_post_quotecontent')); ?> <i class="fa fa-quote-right"></i></div>
		            <?php endif; ?>
		             <?php if ( rwmb_meta('snsvicky_post_quoteauthor') ) : ?>
		            <div class="quote-author">- <?php echo esc_html(rwmb_meta('snsvicky_post_quoteauthor')); ?></div>
		            <?php endif; ?>
		        </div>
	        </div>
	    <?php
	    // Post Link
	    elseif ( get_post_format() == 'link' && function_exists('rwmb_meta') && rwmb_meta('snsvicky_post_linkurl') ) : 
	    	$post_media = true;
	    ?>	
	    	<div class="col-md-5 part-left">
		        <div class="link-info">
		            <a title="<?php echo esc_attr(rwmb_meta('snsvicky_post_linktitle')) ?>" href="<?php echo esc_url( rwmb_meta('snsvicky_post_linkurl') ) ?>"><?php echo esc_html(rwmb_meta('snsvicky_post_linkurl')) ?></a>
		           
		        </div>
	        </div>
	    <?php
	    // Post Video
	    elseif ( get_post_format() == 'video' && function_exists('rwmb_meta') && rwmb_meta('snsvicky_post_video') ) : 
	    	$post_media = true;
	    ?>
	    	<div class="col-md-5 part-left">
		        <div class="video-thumb video-responsive">
		            <?php
		            echo rwmb_meta('snsvicky_post_video');
		            ?>
		        </div>
	        </div>
	    <?php
	    // Post audio
	    elseif ( get_post_format() == 'audio' && function_exists('rwmb_meta') && rwmb_meta('snsvicky_post_audio') ) : 
	        	$post_media = true;
	        ?>
	        	<div class="col-md-5 part-left">
		            <div class="audio-thumb audio-responsive">
		                <?php
		                echo wp_oembed_get(esc_attr(rwmb_meta('snsvicky_post_audio')));
		                ?>
		            </div>
	            </div>
	        <?php
	    // Post Gallery
	    elseif ( get_post_format() == 'gallery' && function_exists('rwmb_meta') && rwmb_meta('snsvicky_post_gallery') ) :
			wp_enqueue_script('owlcarousel'); 
	    	$post_media = true;
	    ?>
	    	<div class="col-md-5 part-left">
		        <div class="gallery-thumb">
		            <div class="thumb-container owl-carousel">
		            <?php
		            foreach (rwmb_meta('snsvicky_post_gallery', 'type=image') as $image) {?>
		               <div class="item"><img alt="<?php echo esc_attr($image['alt']); ?>" src="<?php echo esc_attr($image['full_url']); ?>"/></div>
		            <?php
		            }
		            ?>
		            </div>
		        </div>
	        </div>
	    <?php
	    // Post Image
	    elseif ( has_post_thumbnail() ) : $post_media = true; ?>
	        <div class="col-md-5 part-left">
		        <div class="post-thumb">
		            <?php
		            $blog_type = snsvicky_themeoption('blog_type');
		            $img_size = 'full';
		            switch ($blog_type){
		            	case 'layout2';
		            		$img_size = 'snsvicky_blog_list_thumb';
		            		break;
		            	case 'masonry';
		            		$img_size = 'snsvicky_blog_grid_thumb';
		            		break;
		            	default: // standard post
		            		$img_size = 'full';
		            		break;
		            }?>
		            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( esc_html__( '%s', 'snsvicky' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark">
		            <?php
		            	the_post_thumbnail($img_size);
		            ?>
		            </a>
		        </div>
	        </div>
	    <?php
	    endif;?>
	    <?php if( $post_media ): ?>
	    <div class="col-md-7 part-right">
	    <?php endif; ?>
	    <div class="post-content">      
	        <div class="content">
	            <?php if(snsvicky_themeoption('show_categories', 1) == 1 && get_the_category_list() ):?>
	                <div class="cat-links">
	                    <?php //echo esc_html__('In: ', 'snsvicky'); ?>
	                    <?php echo get_the_category_list(); ?>
	                </div>
	            <?php endif;?>
	            <h3 class="post-title">
	              <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( esc_html__( 'Permalink to %s', 'snsvicky' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
	            </h3>
	            <div class="post-excerpt">
	                <?php 
	                if( empty( $post->post_excerpt ) ) {
	                    $readmore = '<span>'.esc_html__('Read More', 'snsvicky').'</span><span class="meta-nav">&#8594;</span>';
	                    if ( is_search() && $post->post_type == 'page' ) {
	                        // Trip shortcodes for post type is page on search result page
	                        echo strip_shortcodes(get_the_content($readmore));
	                    }else{
	                        the_content($readmore);
	                    }
	                    wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'snsvicky' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) );
	                } else { ?>
	                    <p class="excerpt"><?php echo snsvicky_excerpt( (int)snsvicky_themeoption('excerpt_length', 55) ); ?></p>
	                <?php } ?>
	            </div>
	            <div class="post-meta">
	                <?php if( snsvicky_themeoption('show_author', 1) == 1 ): ?>
	                <span class="post-author">
	                <?php
	                printf( wp_kses(__( '<a class="author-link" href="%s" ref="author">%s%s</a>', 'snsvicky' ), array(
	                                'a' => array(
	                                    'href' => array(),
	                                    'class' => array(),
	                                    'ref' => array()
	                                ),
	                                ) ), esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), get_avatar( get_the_author_meta( 'ID' ), 32 ), get_the_author_meta('display_name') ); ?>
	                </span>
	                <?php endif; ?>
	                <?php
	                // Date
	                if (snsvicky_themeoption('show_date', 1) == 1 && !is_sticky()) :
	                    printf( '<span class="entry-date"><a href="%1$s" rel="bookmark"><time class="entry-date published" datetime="%2$s">%3$s</time></a></span>',
	                        get_permalink(),
	                        esc_attr( get_the_date( 'c' ) ),
	                        get_the_date()
	                    );
	                endif;
	                ?>
	                <?php
	                // Is sticky or not
	                if ( is_sticky() && ! is_paged() ) { ?>
	                    <span class="sticky-post"><?php echo esc_html__( 'Is sticky', 'snsvicky' ) ; ?></span>
	                <?php
	                } ?>
	                <?php if( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ): ?>
	                    <span class="post-comment-count">
	                    <?php
	                        echo '<span class="comments-link">';
	                        comments_popup_link( esc_html__( '0 Comments','snsvicky' ),  esc_html__( '1 Comment','snsvicky' ), '%' . esc_html__(' Comments','snsvicky'));
	                        echo '</span>';
	                    ?>
	                    </span>
	                <?php endif; ?>
	                <?php if( snsvicky_themeoption('show_tags', 1) == 1 && get_the_tag_list() ): ?>
	                    <span class="tags-links"><?php the_tags(esc_html__('Tags: ', 'snsvicky'),', '); ?></span>
	                <?php endif; ?>
	                <?php
	                // Edit link
	                edit_post_link(esc_html__('Edit','snsvicky'), '<span class="edit-post">', '</span>'); ?>
	            </div>
	            
	        </div>
	    </div>
	    <?php if( $post_media ): ?>
	    </div><!-- /.col-md-7 .post-content -->
	    <?php endif; ?>
	</div>
</article>
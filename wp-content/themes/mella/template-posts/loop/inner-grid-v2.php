<?php $thumbsize = !isset($thumbsize) ? mella_get_config( 'blog_item_thumbsize', 'full' ) : $thumbsize;?>
<article <?php post_class('post post-grid post-grid-v2'); ?>>
    <?php
        $thumb = mella_display_post_thumb($thumbsize);
        echo trim($thumb);
    ?>
    <div class="content">
        <?php if (get_the_title()) { ?>
            <h4 class="title">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h4>
        <?php } ?>
        <div class="bottom-info">
            <div class="categories"><?php echo esc_html__('in ','mella') ?><?php mella_post_categories($post); ?></div> 
            <div class="date-post">
                <span class="suffix"><?php echo esc_html__('on','mella') ?></span>
                <a href="<?php the_permalink(); ?>"><?php the_time( get_option('date_format', 'd M, Y') ); ?></a>
            </div>
        </div>
    </div>
</article>
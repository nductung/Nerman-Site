<?php get_header(); ?>
<!-- Content -->
<div id="sns_content">
	<div class="container">
		<div class="row sns-content">
			<!-- left sidebar -->
			<?php snsvicky_leftcol(); ?>
			<!-- Main content -->
			<div class="<?php echo snsvicky_maincolclass(); ?>">
			    <?php
			    if ( have_posts() ) : ?>
	            <div id="snsmain" class="blog-standard posts sns-blog-posts">
	            	<?php
	                // Theloop
	                while ( have_posts() ) : the_post();
	                    get_template_part( 'framework/tpl/posts/post', get_post_format() );
	                endwhile;
	            	?>
	            </div>
	            <?php
		            // Paging
		            get_template_part('tpl-paging');
	            else:
	               echo esc_html__('No post were found matching your selection', 'snsvicky');
	            endif; ?>
			</div>
			<!-- Right sidebar -->
			<?php snsvicky_rightcol(); ?>
		</div>
	</div>
</div>
<!-- End Content -->
<?php get_footer(); ?>
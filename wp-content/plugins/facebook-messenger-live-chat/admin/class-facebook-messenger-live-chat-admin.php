<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://makewebbetter.com/
 * @since      1.0.0
 *
 * @package    facebook-messenger-live-chat
 * @subpackage facebook-messenger-live-chat/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    facebook-messenger-live-chat
 * @subpackage facebook-messenger-live-chat/admin
 * @author     makewebbetter <webmaster@makewebbetter.com>
 */
class Facebook_Messenger_Live_Chat_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles($hook) {

		if( $hook != 'toplevel_page_mwb_fmlcrt_options_menu' ){

			return;
		}

		wp_enqueue_style( $this->plugin_name . 'admin-css', MWB_FMLCRT_URL . 'admin/css/facebook-messenger-live-chat-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name . 'select-css', MWB_FMLCRT_URL . 'admin/css/facebook-messenger-live-chat-select.css', array(), $this->version, 'all' );

		// Enueue Color picker only on General options tab.
		$active_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'general_options';

		if ( 'general_options' == $active_tab ) {

			wp_enqueue_style( 'wp-color-picker' );
		}
		
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts($hook) {

		if( $hook != 'toplevel_page_mwb_fmlcrt_options_menu' ){

			return;
		}

		wp_enqueue_script( $this->plugin_name. 'admin-js', MWB_FMLCRT_URL . 'admin/js/facebook-messenger-live-chat-admin.js', array( 'jquery' ), $this->version, false );
		
		wp_enqueue_script( $this->plugin_name . 'select-js', MWB_FMLCRT_URL . 'admin/js/facebook-messenger-live-chat-select.js', array( 'jquery' ), $this->version, false );

		// Enueue Color picker only on General options tab.
		$active_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'general_options';

		if ( 'general_options' == $active_tab ) {

			wp_enqueue_script( $this->plugin_name . 'color-picker', MWB_FMLCRT_URL . 'admin/js/facebook-messenger-live-chat-color-picker.js', array( 'jquery', 'wp-color-picker' ), $this->version, false );
		}
	}

	// Add settings menu for Facebook Messenger function.
	public function mwb_fmlcrt_options_menu(){

		add_menu_page(
		__('Facebook Messenger', MWB_FMLCRT_TXT_DOMAIN ),
		__('Facebook Messenger', MWB_FMLCRT_TXT_DOMAIN ),
		'manage_options',
		'mwb_fmlcrt_options_menu',
		array($this, 'mwb_fmlcrt_options_menu_html'),
		MWB_FMLCRT_URL . 'admin/resources/facebook-messenger-icon.png',
		85 
		);
	}

	// Callback function for settings menu.
	public function mwb_fmlcrt_options_menu_html(){

		if ( ! current_user_can( 'manage_options' ) ) {

			return;
		}

		?>

		<div class="mwb_fmlcrt_wrap">

		<h2><?php _e('Facebook Messenger Live Chat Options', MWB_FMLCRT_TXT_DOMAIN ); ?></h2>

		<?php 


		$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general_options';

		?>

		<h2 class="nav-tab-wrapper">
            <a href="?page=mwb_fmlcrt_options_menu&tab=general_options" class="nav-tab <?php echo $active_tab == 'general_options' ? 'nav-tab-active' : ''; ?>"><?php _e('General Options', MWB_FMLCRT_TXT_DOMAIN ); ?></a>

            <a href="?page=mwb_fmlcrt_options_menu&tab=facebook_options" class="nav-tab <?php echo $active_tab == 'facebook_options' ? 'nav-tab-active' : ''; ?>"><?php _e('Facebook Options', MWB_FMLCRT_TXT_DOMAIN ); ?></a>
		</h2>

		<?php

		if( $active_tab == 'general_options' ):

	   	 	echo '<form action="options.php" method="post">';

	        settings_errors();

			settings_fields( 'mwb_fmlcrt_gen_menu' );

			do_settings_sections( 'mwb_fmlcrt_gen_menu' );

			submit_button( __('Save Options', MWB_FMLCRT_TXT_DOMAIN ) );

			echo '</form>';

		elseif( $active_tab == 'facebook_options' ):

			echo '<form action="options.php" method="post">';

	        settings_errors();

			settings_fields( 'mwb_fmlcrt_fb_menu' );

			do_settings_sections( 'mwb_fmlcrt_fb_menu' );

			submit_button( __('Save Options', MWB_FMLCRT_TXT_DOMAIN ) );

			echo '</form>';	

		endif;	

		?>

        </div> <!-- mwb_fmlcrt_wrap -->

        <?php
    }

    // Settings API for settings menu function.
    public function mwb_fmlcrt_options_init(){

    	//General Options.

    	register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_plug_enable');

    	register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_plug_display');
    	register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_plug_display_except_pages');
    	register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_plug_display_only_pages');
		register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_home_for_except');
    	register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_home_for_only');

    	register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_plug_device_display');

		register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_plug_color_picker');

    	register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_plug_greeting_text');

    	register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_plug_min_behaviour');

    	register_setting('mwb_fmlcrt_gen_menu', 'mwb_fmlcrt_plug_messenger_lang');
    	
    	add_settings_section(
		    'mwb_fmlcrt_gen_menu_sec',
		    null,
			null,
		    'mwb_fmlcrt_gen_menu'
		);

		add_settings_field(
		    'mwb_fmlcrt_enable',
		    __('Enable', MWB_FMLCRT_TXT_DOMAIN ),
		    array($this, 'mwb_fmlcrt_enable_cb'),
		    'mwb_fmlcrt_gen_menu',
		    'mwb_fmlcrt_gen_menu_sec'
		);

		add_settings_field(
		    'mwb_fmlcrt_display',
		    __('Display', MWB_FMLCRT_TXT_DOMAIN ),
		    array($this, 'mwb_fmlcrt_Display_cb'),
		    'mwb_fmlcrt_gen_menu',
		    'mwb_fmlcrt_gen_menu_sec'
		);

		add_settings_field(
		    'mwb_fmlcrt_device_display',
		    __('Device Behaviour', MWB_FMLCRT_TXT_DOMAIN ),
		    array($this, 'mwb_fmlcrt_device_display_cb'),
		    'mwb_fmlcrt_gen_menu',
		    'mwb_fmlcrt_gen_menu_sec'
		);

		add_settings_field(
		    'mwb_fmlcrt_theme_color',
		    __('Messenger Theme', MWB_FMLCRT_TXT_DOMAIN ),
		    array($this, 'mwb_fmlcrt_theme_color_cb'),
		    'mwb_fmlcrt_gen_menu',
		    'mwb_fmlcrt_gen_menu_sec'
		);

		add_settings_field(
		    'mwb_fmlcrt_login_text',
		    __('Greeting Text', MWB_FMLCRT_TXT_DOMAIN ),
		    array($this, 'mwb_fmlcrt_greeting_text_cb'),
		    'mwb_fmlcrt_gen_menu',
		    'mwb_fmlcrt_gen_menu_sec'
		);

		add_settings_field(
		    'mwb_fmlcrt_min_behaviour',
		    __('Minimized Behaviour', MWB_FMLCRT_TXT_DOMAIN ),
		    array($this, 'mwb_fmlcrt_min_behaviour_cb'),
		    'mwb_fmlcrt_gen_menu',
		    'mwb_fmlcrt_gen_menu_sec'
		);

		add_settings_field(
		    'mwb_fmlcrt_messenger_lang',
		    __('Messenger Language', MWB_FMLCRT_TXT_DOMAIN ),
		    array($this, 'mwb_fmlcrt_messenger_lang_cb'),
		    'mwb_fmlcrt_gen_menu',
		    'mwb_fmlcrt_gen_menu_sec'
		);

		//Facebook Options.

		register_setting('mwb_fmlcrt_fb_menu', 'mwb_fmlcrt_plug_fb_pd_id');
		register_setting('mwb_fmlcrt_fb_menu', 'mwb_fmlcrt_plug_fb_app_id');

		add_settings_section(
		    'mwb_fmlcrt_fb_menu_sec',
		    null,
			null,
		    'mwb_fmlcrt_fb_menu'
		);

		add_settings_field(
		    'mwb_fmlcrt_fb_pg_id',
		    __('Facebook Page ID', MWB_FMLCRT_TXT_DOMAIN ),
		    array($this, 'mwb_fmlcrt_fb_pg_id_cb'),
		    'mwb_fmlcrt_fb_menu',
		    'mwb_fmlcrt_fb_menu_sec'
		);

		add_settings_field(
		    'mwb_fmlcrt_fb_white_list',
		    __('Whitelist your Domain', MWB_FMLCRT_TXT_DOMAIN ),
		    array($this, 'mwb_fmlcrt_fb_white_list_cb'),
		    'mwb_fmlcrt_fb_menu',
		    'mwb_fmlcrt_fb_menu_sec'
		);

		add_settings_field(
		    'mwb_fmlcrt_fb_app_id',
		    __('Facebook App ID', MWB_FMLCRT_TXT_DOMAIN ),
		    array($this, 'mwb_fmlcrt_fb_app_id_cb'),
		    'mwb_fmlcrt_fb_menu',
		    'mwb_fmlcrt_fb_menu_sec'
		);
	}

	// Callback for Enable Plugin.
	public function mwb_fmlcrt_enable_cb(){

		?>

		<div class="mwb_fmlcrt_option_sec">

		<label for="mwb_fmlcrt_plug_enable">
		<input type="checkbox" name="mwb_fmlcrt_plug_enable" value="yes" <?php checked('yes', get_option('mwb_fmlcrt_plug_enable', 'yes') ); ?> >
		<?php _e('Enable the Plugin.', MWB_FMLCRT_TXT_DOMAIN ); ?>
		</label>

		</div>

		<?php
	}

	// Callback for Display options. 
	public function mwb_fmlcrt_Display_cb(){

		$display = get_option('mwb_fmlcrt_plug_display', 'eve');

		?>

		<div class="mwb_fmlcrt_option_sec">

		<select id="mwb_fmlcrt_display_select" name="mwb_fmlcrt_plug_display">
			<option <?php selected('eve', $display ); ?> value="eve" ><?php _e('Show Everywhere', MWB_FMLCRT_TXT_DOMAIN); ?></option>
			<option <?php selected('eve_except', $display ); ?> value="eve_except"><?php _e('Show Everywhere except for these Pages', MWB_FMLCRT_TXT_DOMAIN); ?></option>
			<option <?php selected('only_these', $display ); ?> value="only_these"><?php _e('Only show on these Pages', MWB_FMLCRT_TXT_DOMAIN); ?></option>
			<option <?php selected('only_home', $display ); ?> value="only_home"><?php _e('Only show on Home Page', MWB_FMLCRT_TXT_DOMAIN); ?></option>
		</select>

		<div id="mwb_fmlcrt_display_pages_eve" class="<?php if($display != 'eve'){ echo 'hidden';}?>" >

			<p class="description"><?php _e('The Facebook Messenger will be displayed on all pages.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

		</div>

		<div id="mwb_fmlcrt_display_pages_except" class="<?php if($display != 'eve_except'){ echo 'hidden';}?>">

			<p class="description"><?php _e('The Facebook Messenger will be displayed on all pages, except for these selected pages.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

			<?php

			$pages = get_pages();

			$check_pages = (array)get_option('mwb_fmlcrt_plug_display_except_pages', '');

			?>

			<div class="mwb_fmlcrt_display_pages_sec">

			<?php if( is_array($pages) && !empty($pages) ): ?>

			<p class="mwb_fmlcrt_sel_pages"><?php _e('Select Pages: ', MWB_FMLCRT_TXT_DOMAIN ); ?></p>	

			<select id="mwb_fmlcrt_display_pages_except_select" name="mwb_fmlcrt_plug_display_except_pages[]" multiple="multiple">

			<?php

			foreach ($pages as $key => $page) {

				?>

				<option <?php selected( in_array( $page->post_name, $check_pages ) ); ?> value='<?php echo $page->post_name; ?>'><?php echo $page->post_title; ?></option>

				<?php

			}

			?>
			</select>

			<?php else: ?>

			<p><b><?php _e('Sorry No Pages were found.', MWB_FMLCRT_TXT_DOMAIN ); ?></b></p>	

			<?php endif; ?>

			<p class="mwb_fmlcrt_sel_home_page"><label for="mwb_fmlcrt_home_for_except">
			<input type="checkbox" name="mwb_fmlcrt_home_for_except" value="yes" <?php checked('yes', get_option('mwb_fmlcrt_home_for_except', 'yes')); ?> >
			<?php _e('Show on Home Page.', MWB_FMLCRT_TXT_DOMAIN ); ?>
			</label></p>

			</div>

		</div>

		<div id="mwb_fmlcrt_display_pages_only" class="<?php if($display != 'only_these'){ echo 'hidden';}?>">

			<p class="description"><?php _e('The Facebook Messenger will be displayed only on these selected pages.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

			<?php

			$pages = get_pages();

			$check_pages = (array)get_option('mwb_fmlcrt_plug_display_only_pages', '');

			?>

			<div class="mwb_fmlcrt_display_pages_sec">

			<?php if( is_array($pages) && !empty($pages) ): ?>

			<p class="mwb_fmlcrt_sel_pages"><?php _e('Select Pages: ', MWB_FMLCRT_TXT_DOMAIN ); ?></p>	

			<select id="mwb_fmlcrt_display_pages_only_select" name="mwb_fmlcrt_plug_display_only_pages[]" multiple="multiple">

			<?php

			foreach ($pages as $key => $page) {

				?>

				<option <?php selected( in_array( $page->post_name, $check_pages ) ); ?> value='<?php echo $page->post_name; ?>'><?php echo $page->post_title; ?></option>

				<?php
			}

			?>
			</select>

			<?php else: ?>

			<p><b><?php _e('Sorry No Pages were found.', MWB_FMLCRT_TXT_DOMAIN ); ?></b></p>	
			<?php endif; ?>	

			<p class="mwb_fmlcrt_sel_home_page"><label for="mwb_fmlcrt_home_for_only">
			<input type="checkbox" name="mwb_fmlcrt_home_for_only" value="yes" <?php checked('yes', get_option('mwb_fmlcrt_home_for_only', 'yes')); ?> >
			<?php _e('Show on Home Page.', MWB_FMLCRT_TXT_DOMAIN ); ?>
			</label></p>

			</div>

		</div>

		<div id="mwb_fmlcrt_display_pages_home" class="<?php if($display != 'only_home'){ echo 'hidden';}?>">

			<p class="description"><?php _e('The Facebook Messenger will be displayed only on the Home Page.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

		</div>

		</div>

		<?php
	}

	// Callback for Messenger device display options.
	public function mwb_fmlcrt_device_display_cb() {

		$device_display = get_option('mwb_fmlcrt_plug_device_display', 'device_all' );

		?>

		<div class="mwb_fmlcrt_option_sec">

			<select id="mwb_fmlcrt_device_display_select" name="mwb_fmlcrt_plug_device_display">
				<option <?php selected('device_all', $device_display ); ?> value="device_all" ><?php _e('Show on desktop and mobile both', MWB_FMLCRT_TXT_DOMAIN); ?></option>
				<option <?php selected('device_desk', $device_display ); ?> value="device_desk"><?php _e('Show on desktop only', MWB_FMLCRT_TXT_DOMAIN); ?></option>
				<option <?php selected('device_mob', $device_display ); ?> value="device_mob"><?php _e('Show on mobile only', MWB_FMLCRT_TXT_DOMAIN); ?></option>
					
			</select>

			<p class="description"><?php _e('Select devices where you want to display Facebook Messenger. Mobile devices include both Smartphones and Tablets.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

		</div>


		<?php
	}

	// Callback for Messenger theme color.
	public function mwb_fmlcrt_theme_color_cb() {

		$color = get_option('mwb_fmlcrt_plug_color_picker', '');

		?> 

		<div class="mwb_fmlcrt_option_sec">

			<p class="description"><?php _e('Select Facebook Messenger theme color, Facebook highly recommends to choose a color that has a high contrast to white.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

			<div class="mwb_fmlcrt_color_picker_div">

				<input type="text" id="mwb_fmlcrt_color_picker" value="<?php echo $color; ?>" name="mwb_fmlcrt_plug_color_picker">
				<input type="button" id="mwb_fmlcrt_clear_color" class="button button-small button-primary" value="<?php _e('Use Default', MWB_FMLCRT_TXT_DOMAIN ); ?>">

			</div>

			

		</div>

		<?php
	}

	// Callback for Messenger Greeting text.
	public function mwb_fmlcrt_greeting_text_cb() {

		$greeting_text = get_option('mwb_fmlcrt_plug_greeting_text', array() );

		$logged_in_text = !empty( $greeting_text['logged_in'] ) ? $greeting_text['logged_in'] : '';

		$logged_out_text = !empty( $greeting_text['logged_out'] ) ? $greeting_text['logged_out'] : '';

		?>

		<div class="mwb_fmlcrt_option_sec">

			<p class="description"><strong><label><?php  _e('For logged in users:', MWB_FMLCRT_TXT_DOMAIN ); ?></label></strong></p>
			<p><textarea placeholder="<?php  _e('Enter text here ( maximum 80 characters )', MWB_FMLCRT_TXT_DOMAIN ); ?>" cols="47" rows="3" maxlength="80" name="mwb_fmlcrt_plug_greeting_text[logged_in]"><?php echo $logged_in_text; ?></textarea></p>
			<p class="description"><strong><label><?php  _e('For logged out users:', MWB_FMLCRT_TXT_DOMAIN ); ?></label></strong></p>
			<p><textarea placeholder="<?php  _e('Enter text here ( maximum 80 characters )', MWB_FMLCRT_TXT_DOMAIN ); ?>" cols="47" rows="3" maxlength="80" name="mwb_fmlcrt_plug_greeting_text[logged_out]"><?php echo $logged_out_text; ?></textarea></p>

			<p class="description"><?php _e('Enter Facebook Messenger greeting text, leave blank to use default.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

		</div>

		<?php
	}

	// Callback for Minimized behaviour.
	public function mwb_fmlcrt_min_behaviour_cb(){

		$min_attr = get_option('mwb_fmlcrt_plug_min_behaviour', '');

		?>

		<div class="mwb_fmlcrt_option_sec">

		<select id="mwb_fmlcrt_min_behaviour_select" name="mwb_fmlcrt_plug_min_behaviour">
			<option <?php selected('', $min_attr ); ?> value="" ><?php _e('Default', MWB_FMLCRT_TXT_DOMAIN); ?></option>
			<option <?php selected('true', $min_attr ); ?> value="true"><?php _e('True', MWB_FMLCRT_TXT_DOMAIN); ?></option>
			<option <?php selected('false', $min_attr ); ?> value="false"><?php _e('False', MWB_FMLCRT_TXT_DOMAIN); ?></option>
			
		</select>

		<div id="mwb_fmlcrt_min_behaviour_default" class="<?php if($min_attr != ''){ echo 'hidden';}?>" >

			<p class="description"><?php _e('By default, Facebook Messenger will be minimized on mobile and not on desktop.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

		</div>

		<div id="mwb_fmlcrt_min_behaviour_true" class="<?php if($min_attr != 'true'){ echo 'hidden';}?>" >

			<p class="description"><?php _e('Facebook Messenger will be minimized on both mobile and desktop.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

		</div>

		<div id="mwb_fmlcrt_min_behaviour_false" class="<?php if($min_attr != 'false'){ echo 'hidden';}?>" >

			<p class="description"><?php _e('Facebook Messenger will be not be minimized on both mobile and desktop.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

		</div>

		</div>

		<?php
	}

	// Callback for Language settings.
	public function mwb_fmlcrt_messenger_lang_cb(){

		$lang_default = !empty( get_locale() ) ? get_locale() : 'en_US';

		$lang = get_option('mwb_fmlcrt_plug_messenger_lang', $lang_default );

		$lang_array = array(
                            // Afrikaans
                            'af_ZA' => __('Afrikaans', MWB_FMLCRT_TXT_DOMAIN ),
                            // Arabic
                            'ar_AR' => __('Arabic', MWB_FMLCRT_TXT_DOMAIN ),
                            // Azerbaijani
                            'az_AZ' => __('Azerbaijani', MWB_FMLCRT_TXT_DOMAIN ),
                            // Belarusian
                            'be_BY' => __('Belarusian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Bulgarian
                            'bg_BG' => __('Bulgarian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Bengali
                            'bn_IN' => __('Bengali', MWB_FMLCRT_TXT_DOMAIN ),
                            // Bosnian
                            'bs_BA' => __('Bosnian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Catalan
                            'ca_ES' => __('Catalan', MWB_FMLCRT_TXT_DOMAIN ),
                            // Czech
                            'cs_CZ' => __('Czech', MWB_FMLCRT_TXT_DOMAIN ),
                            // Welsh
                            'cy_GB' => __('Welsh', MWB_FMLCRT_TXT_DOMAIN ),
                            // Danish
                            'da_DK' => __('Danish', MWB_FMLCRT_TXT_DOMAIN ),
                            // German
                            'de_DE' => __('German', MWB_FMLCRT_TXT_DOMAIN ),
                            // Greek
                            'el_GR' => __('Greek', MWB_FMLCRT_TXT_DOMAIN ),
                            // English (UK)
                            'en_GB' => __('English (UK)', MWB_FMLCRT_TXT_DOMAIN ),
                            // English (Pirate)
                            'en_PI' => __('English (Pirate)', MWB_FMLCRT_TXT_DOMAIN ),
                            // English (Upside Down)
                            'en_UD' => __('English (Upside Down)', MWB_FMLCRT_TXT_DOMAIN ),
                            // English (US)
                            'en_US' => __('English (US)', MWB_FMLCRT_TXT_DOMAIN ),
                            // Esperanto
                            'eo_EO' => __('Esperanto', MWB_FMLCRT_TXT_DOMAIN ),
                            // Spanish (Spain)
                            'es_ES' => __('Spanish (Spain)', MWB_FMLCRT_TXT_DOMAIN ),
                            // Spanish
                            'es_LA' => __('Spanish', MWB_FMLCRT_TXT_DOMAIN ),
                            // Estonian
                            'et_EE' => __('Estonian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Basque
                            'eu_ES' => __('Basque', MWB_FMLCRT_TXT_DOMAIN ),
                            // Persian
                            'fa_IR' => __('Persian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Leet Speak
                            'fb_LT' => __('Leet Speak', MWB_FMLCRT_TXT_DOMAIN ),
                            // Finnish
                            'fi_FI' => __('Finnish', MWB_FMLCRT_TXT_DOMAIN ),
                            // Faroese
                            'fo_FO' => __('Faroese', MWB_FMLCRT_TXT_DOMAIN ),
                            // French (Canada)
                            'fr_CA' => __('French (Canada)', MWB_FMLCRT_TXT_DOMAIN ),
                            // French (France)
                            'fr_FR' => __('French (France)', MWB_FMLCRT_TXT_DOMAIN ),
                            // Frisian
                            'fy_NL' => __('Frisian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Irish
                            'ga_IE' => __('Irish', MWB_FMLCRT_TXT_DOMAIN ),
                            // Galician
                            'gl_ES' => __('Galician', MWB_FMLCRT_TXT_DOMAIN ),
                            // Hebrew
                            'he_IL' => __('Hebrew', MWB_FMLCRT_TXT_DOMAIN ),
                            // Hindi
                            'hi_IN' => __('Hindi', MWB_FMLCRT_TXT_DOMAIN ),
                            // Croatian
                            'hr_HR' => __('Croatian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Hungarian
                            'hu_HU' => __('Hungarian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Armenian
                            'hy_AM' => __('Armenian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Indonesian
                            'id_ID' => __('Indonesian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Icelandic
                            'is_IS' => __('Icelandic', MWB_FMLCRT_TXT_DOMAIN ),
                            // Italian
                            'it_IT' => __('Italian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Japanese
                            'ja_JP' => __('Japanese', MWB_FMLCRT_TXT_DOMAIN ),
                            // Georgian
                            'ka_GE' => __('Georgian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Khmer
                            'km_KH' => __('Khmer', MWB_FMLCRT_TXT_DOMAIN ),
                            // Korean
                            'ko_KR' => __('Korean', MWB_FMLCRT_TXT_DOMAIN ),
                            // Kurdish
                            'ku_TR' => __('Kurdish', MWB_FMLCRT_TXT_DOMAIN ),
                            // Latin
                            'la_VA' => __('Latin', MWB_FMLCRT_TXT_DOMAIN ),
                            // Lithuanian
                            'lt_LT' => __('Lithuanian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Latvian
                            'lv_LV' => __('Latvian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Macedonian
                            'mk_MK' => __('Macedonian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Malayalam
                            'ml_IN' => __('Malayalam', MWB_FMLCRT_TXT_DOMAIN ),
                            // Malay
                            'ms_MY' => __('Malay', MWB_FMLCRT_TXT_DOMAIN ),
                            // Norwegian (bokmal)
                            'nb_NO' => __('Norwegian (bokmal)', MWB_FMLCRT_TXT_DOMAIN ),
                            // Nepali
                            'ne_NP' => __('Nepali', MWB_FMLCRT_TXT_DOMAIN ),
                            // Dutch
                            'nl_NL' => __('Dutch', MWB_FMLCRT_TXT_DOMAIN ),
                            // Norwegian (nynorsk)
                            'nn_NO' => __('Norwegian (nynorsk)', MWB_FMLCRT_TXT_DOMAIN ),
                            // Punjabi
                            'pa_IN' => __('Punjabi', MWB_FMLCRT_TXT_DOMAIN ),
                            // Polish
                            'pl_PL' => __('Polish', MWB_FMLCRT_TXT_DOMAIN ),
                            // Pashto
                            'ps_AF' => __('Pashto', MWB_FMLCRT_TXT_DOMAIN ),
                            // Portuguese (Brazil)
                            'pt_BR' => __('Portuguese (Brazil)', MWB_FMLCRT_TXT_DOMAIN ),
                            // Portuguese (Portugal)
                            'pt_PT' => __('Portuguese (Portugal)', MWB_FMLCRT_TXT_DOMAIN ),
                            // Romanian
                            'ro_RO' => __('Romanian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Russian
                            'ru_RU' => __('Russian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Slovak
                            'sk_SK' => __('Slovak', MWB_FMLCRT_TXT_DOMAIN ),
                            // Slovenian
                            'sl_SI' => __('Slovenian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Albanian
                            'sq_AL' => __('Albanian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Serbian
                            'sr_RS' => __('Serbian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Swedish
                            'sv_SE' => __('Swedish', MWB_FMLCRT_TXT_DOMAIN ),
                            // Swahili
                            'sw_KE' => __('Swahili', MWB_FMLCRT_TXT_DOMAIN ),
                            // Tamil
                            'ta_IN' => __('Tamil', MWB_FMLCRT_TXT_DOMAIN ),
                            // Telugu
                            'te_IN' => __('Telugu', MWB_FMLCRT_TXT_DOMAIN ),
                            // Thai
                            'th_TH' => __('Thai', MWB_FMLCRT_TXT_DOMAIN ),
                            // Filipino
                            'tl_PH' => __('Filipino', MWB_FMLCRT_TXT_DOMAIN ),
                            // Turkish
                            'tr_TR' => __('Turkish', MWB_FMLCRT_TXT_DOMAIN ),
                            //
                            'uk_UA' => __('Ukrainian', MWB_FMLCRT_TXT_DOMAIN ),
                            // Vietnamese
                            'vi_VN' => __('Vietnamese', MWB_FMLCRT_TXT_DOMAIN ),
                            //
                            'zh_CN' => __('Simplified Chinese (China)', MWB_FMLCRT_TXT_DOMAIN ),
                            //
                            'zh_HK' => __('Traditional Chinese (Hong Kong)', MWB_FMLCRT_TXT_DOMAIN ),
                            //
                            'zh_TW' => __('Traditional Chinese (Taiwan)', MWB_FMLCRT_TXT_DOMAIN ),
                        );

		?>

		<div class="mwb_fmlcrt_option_sec">

		<select id="mwb_fmlcrt_lang_select" name="mwb_fmlcrt_plug_messenger_lang">

		<?php foreach ($lang_array as $key => $value): ?>

			<option <?php selected( $key, $lang ); ?> value="<?php echo $key; ?>" ><?php echo $value; ?></option>

		<?php endforeach; ?>	
			
		</select>

		<p class="description"><?php _e('Select language for Facebook Messenger.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

		</div>

		<?php
	}

	// Callback for Facebook Page ID.
	public function mwb_fmlcrt_fb_pg_id_cb(){

		$fb_page_id = get_option('mwb_fmlcrt_plug_fb_pd_id', '');

		$fb_page_id = esc_attr($fb_page_id);

		?>

		<div class="mwb_fmlcrt_option_sec">

		<input type="text" placeholder="<?php _e('Eg. XXXXXXXXXXXXXXX', MWB_FMLCRT_TXT_DOMAIN ); ?>" name="mwb_fmlcrt_plug_fb_pd_id" value="<?php echo $fb_page_id; ?>">
		<p class="description"><?php _e('Enter your Facebook Page ID here.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>

		<h4><?php _e('How to get Your Facebook Page ID?', MWB_FMLCRT_TXT_DOMAIN ); ?></h4>
		<ul>
			<li><?php printf( '%s<b>%s</b>%s<b>%s</b>%s', __('Go to your ', MWB_FMLCRT_TXT_DOMAIN ), __('Facebook Page', MWB_FMLCRT_TXT_DOMAIN ), __(' and at the left side click on ', MWB_FMLCRT_TXT_DOMAIN ), __('About', MWB_FMLCRT_TXT_DOMAIN ), __(' tab.', MWB_FMLCRT_TXT_DOMAIN ) ); ?></li>
			<li><?php printf( '%s<b>%s</b>%s<b>%s</b>%s', __('On ', MWB_FMLCRT_TXT_DOMAIN ), __('About page', MWB_FMLCRT_TXT_DOMAIN ), __(' scroll to the bottom, you will find your ', MWB_FMLCRT_TXT_DOMAIN ), __('Page ID', MWB_FMLCRT_TXT_DOMAIN ), __('.', MWB_FMLCRT_TXT_DOMAIN ) ); ?></li>
		</ul>

		<br>
		<p class="description"><?php _e('Click ', MWB_FMLCRT_TXT_DOMAIN ); ?>
			<a href="https://www.youtube.com/watch?v=bH3WnWk3OUc&feature=youtu.be" target="_blank"><?php _e('here', MWB_FMLCRT_TXT_DOMAIN ); ?></a>
			<?php _e('to see how to get Facebook Page ID and Whitelist your Domain.', MWB_FMLCRT_TXT_DOMAIN ); ?>
		</p>

		</div>

		<?php
	}

	// Callback for Whitelisting Facebook Page.
	public function mwb_fmlcrt_fb_white_list_cb(){

		?>

		<div class="mwb_fmlcrt_option_sec">

		<p>
		<?php printf( '%s<b>%s</b>%s<b>%s</b>%s',
			__('You need to ', MWB_FMLCRT_TXT_DOMAIN ),
			__('Whitelist', MWB_FMLCRT_TXT_DOMAIN ),
			__(' your ', MWB_FMLCRT_TXT_DOMAIN ),
			__('Website Domain', MWB_FMLCRT_TXT_DOMAIN ),
			__(' on your Facebook Page settings to make the Messenger Work.', MWB_FMLCRT_TXT_DOMAIN )
		); ?>
		</p>

		<ol>
			<li><?php printf( '%s<b>%s</b>%s', __('Click ', MWB_FMLCRT_TXT_DOMAIN ), __('Settings', MWB_FMLCRT_TXT_DOMAIN ), __(' at the top of your Facebook Page.', MWB_FMLCRT_TXT_DOMAIN ) ); ?></li>
			<li><?php printf( '%s<b>%s</b>%s', __('Click ', MWB_FMLCRT_TXT_DOMAIN ), __('Messenger Platform', MWB_FMLCRT_TXT_DOMAIN ), __(' on the left.', MWB_FMLCRT_TXT_DOMAIN ) ); ?></li>
			<li><?php printf( '%s<b>%s</b>%s<b>%s</b>%s', __('Scroll down to ', MWB_FMLCRT_TXT_DOMAIN ), __('Whitelisted Domains', MWB_FMLCRT_TXT_DOMAIN ), __(' section. Add your ', MWB_FMLCRT_TXT_DOMAIN ), __('Website Domain', MWB_FMLCRT_TXT_DOMAIN ), __(' and Save.', MWB_FMLCRT_TXT_DOMAIN ) ); ?></li>
		</ol>



		</div>

		<?php
	}

	// Callback for Facebook App ID.
	public function mwb_fmlcrt_fb_app_id_cb(){

		$fb_app_id = get_option('mwb_fmlcrt_plug_fb_app_id', '');

		$fb_app_id = esc_attr($fb_app_id);

		?>

		<div class="mwb_fmlcrt_option_sec">

		<input type="text" name="mwb_fmlcrt_plug_fb_app_id" value="<?php echo $fb_app_id; ?>">
		<p class="description"><?php _e('Enter your Facebook App ID that you want to use with Facebook Messenger, this is optional.', MWB_FMLCRT_TXT_DOMAIN ); ?></p>
		<p class="description"><a href="https://developers.facebook.com/apps/" target="_blank"><?php _e('Get App ID', MWB_FMLCRT_TXT_DOMAIN ); ?></a></p>

		</div>

		<?php
	}

}

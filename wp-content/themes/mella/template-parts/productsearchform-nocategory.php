<?php if ( mella_get_config('show_searchform') ):
	$class = mella_get_config('enable_autocompleate_search', true) ? ' apus-autocompleate-input' : '';
?>
	<div class="apus-search-form search-fix clearfix">
		<div class="inner-search">
			<div class="heading-search text-center">
				<span class="close-search-fix"> <img src="<?php echo esc_url_raw( get_template_directory_uri().'/images/close.png'); ?>" alt="'.esc_attr__('close' , 'mella').'"></span>
			</div>
			<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
				<div class="main-search">
					<?php if ( mella_get_config('enable_autocompleate_search', true) ) echo '<div class="twitter-typeahead">'; ?>
				  		<input type="text" placeholder="<?php esc_attr_e( 'Search products here...', 'mella' ); ?>" name="s" class="apus-search form-control <?php echo esc_attr($class); ?>"/>
					<?php if ( mella_get_config('enable_autocompleate_search', true) ) echo '</div>'; ?>
				</div>
				<button type="submit" class="btn btn-theme radius-0"><?php esc_html_e('SEARCH', 'mella'); ?></button>
				<input type="hidden" name="post_type" value="product" class="post_type" />
			</form>
		</div>
	</div>
<?php endif; ?>
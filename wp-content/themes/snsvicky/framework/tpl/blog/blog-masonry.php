<?php
wp_enqueue_script('masonry');
wp_enqueue_script('imagesloaded');
wp_enqueue_script('snsvicky-blog-ajax');
$wclass = '';
if ( snsvicky_themeoption('blog_class') ) {
	$wclass = snsvicky_themeoption('blog_class');
}
$pagination = snsvicky_themeoption('pagination', 'def'); // get theme option
?>
<div class="sns-grid posts sns-blog-posts sns-blog-masonry <?php echo esc_attr($wclass);?>">
	<div id="snsmain" class="sns-grid-masonry">
		<?php 
		while ( have_posts() ) : the_post();
		?>
			<?php get_template_part( 'framework/tpl/posts/post-masonry', get_post_format() )?>
		<?php
		endwhile;
		?>
	</div>
	<?php
	// Paging
	if( $pagination == 'def')
		get_template_part('tpl-paging');
	?>
</div>
<?php
if( $pagination == 'ajax')
	snsvicky_paging_nav_ajax('#snsmain', 'framework/tpl/posts/post-masonry' ); // This paging nav should be outside #snsmain div

echo '<input type="hidden" name="hidden_snsvicky_blog_layout" value="' . snsvicky_themeoption('blog_type') .  '">';
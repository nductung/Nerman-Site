<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

global $product, $woocommerce_loop;
	
$woo_display = mella_woocommerce_get_display_mode();

if ( $woo_display == 'list' ) { 	
	$classes[] = 'list-products col-xs-12';
?>
	<div <?php wc_product_class( $classes, $product ); ?>>
	 	<?php wc_get_template_part( 'item-product/inner', 'list' ); ?>
	</div>
<?php
} elseif ( $woo_display == 'mansory' ) {
	// Store loop count we're currently on
	if ( empty( $woocommerce_loop['loop'] ) ) {
		$woocommerce_loop['loop'] = 0;
		$nb_per_page = mella_get_config('number_products_per_page', 7);
		if ( isset($current_page) ) {
			$woocommerce_loop['loop'] = ((int)$current_page - 1) * $nb_per_page;
		}
	}

	$args = array();
	if ( $woocommerce_loop['loop']%7 == 0 ) {
		$classes[] = 'col-md-8 col-sm-8 col-xs-6 isotope-item';
		$args = array('image_size' => 'mella-shop-large');
	} elseif ( in_array($woocommerce_loop['loop']%7, array(4,5)) ) {
		$classes[] = 'col-md-8 col-sm-8 col-xs-6 isotope-item';
		$args = array('image_size' => 'mella-shop-horizontal');
	} else {
		$classes[] = 'col-sm-4 col-xs-6 isotope-item';
		$args = array('image_size' => 'mella-shop-normal');
	}
	?>

	<div <?php wc_product_class( $classes, $product ); ?>>
	 	<?php wc_get_template( 'item-product/inner-v1.php', $args ); ?>
	</div>
<?php
} elseif ( $woo_display == 'metro' ) {
	// Store loop count we're currently on
	if ( empty( $woocommerce_loop['loop'] ) ) {
		$woocommerce_loop['loop'] = 0;
		$nb_per_page = mella_get_config('number_products_per_page', 10);
		if ( isset($current_page) ) {
			$woocommerce_loop['loop'] = ((int)$current_page - 1) * $nb_per_page;
		}
	}

	$args = array();
	if ( $woocommerce_loop['loop']%10 == 0 || $woocommerce_loop['loop']%10 == 5 ) {
		$classes[] = 'col-md-6 col-sm-6 col-xs-6 isotope-item';
		$args = array('image_size' => 'mella-shop-metro-large');
	} else {
		$classes[] = 'col-sm-3 col-xs-6 isotope-item';
		$args = array('image_size' => 'mella-shop-metro');
	}
	?>

	<div <?php wc_product_class( $classes, $product ); ?>>
	 	<?php wc_get_template( 'item-product/inner.php', $args ); ?>
	</div>
<?php
} else {

	// Store loop count we're currently on
	if ( empty( $woocommerce_loop['loop'] ) ) {
		$woocommerce_loop['loop'] = 0;
	}
	// Store column count for displaying the grid
	
	$woocommerce_loop['columns'] = mella_woocommerce_shop_columns(4);
	
	$bcol = 12/$woocommerce_loop['columns'];
	if($woocommerce_loop['columns'] == 5){
		$bcol = 'col-md-cus-5';
	}
	if($woocommerce_loop['columns'] >=4 ){
		$classes[] = 'col-md-'.$bcol.' col-sm-4 col-xs-6 ';
	}else{
		$classes[] = 'col-md-'.$bcol.($woocommerce_loop['columns'] > 1 ? ' col-xs-6 ' : '').($woocommerce_loop['columns'] > 2 ? ' col-sm-4 ' : '');
	}
	
	?>
	<div <?php wc_product_class( $classes, $product ); ?>>
		<?php wc_get_template_part( 'item-product/inner' ); ?>
	</div>
<?php } ?>
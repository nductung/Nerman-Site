<?php

if ( !function_exists('mella_get_products') ) {
    function mella_get_products( $args = array() ) {
        global $woocommerce, $wp_query;

        $args = wp_parse_args( $args, array(
            'categories' => array(),
            'product_type' => 'recent_product',
            'paged' => 1,
            'post_per_page' => -1,
            'orderby' => '',
            'order' => '',
            'includes' => array(),
            'excludes' => array(),
            'author' => '',
        ));
        extract($args);
        
        $query_args = array(
            'post_type' => 'product',
            'posts_per_page' => $post_per_page,
            'post_status' => 'publish',
            'paged' => $paged,
            'orderby'   => $orderby,
            'order' => $order
        );

        if ( isset( $query_args['orderby'] ) ) {
            if ( 'price' == $query_args['orderby'] ) {
                $query_args = array_merge( $query_args, array(
                    'meta_key'  => '_price',
                    'orderby'   => 'meta_value_num'
                ) );
            }
            if ( 'featured' == $query_args['orderby'] ) {
                $query_args = array_merge( $query_args, array(
                    'meta_key'  => '_featured',
                    'orderby'   => 'meta_value'
                ) );
            }
            if ( 'sku' == $query_args['orderby'] ) {
                $query_args = array_merge( $query_args, array(
                    'meta_key'  => '_sku',
                    'orderby'   => 'meta_value'
                ) );
            }
        }

        switch ($product_type) {
            case 'best_selling':
                $query_args['meta_key']='total_sales';
                $query_args['orderby']='meta_value_num';
                $query_args['ignore_sticky_posts']   = 1;
                $query_args['meta_query'] = array();
                $query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                $query_args['meta_query'][] = $woocommerce->query->visibility_meta_query();
                break;
            case 'featured_product':
                $product_visibility_term_ids = wc_get_product_visibility_term_ids();
                $query_args['tax_query'][] = array(
                    'taxonomy' => 'product_visibility',
                    'field'    => 'term_taxonomy_id',
                    'terms'    => $product_visibility_term_ids['featured'],
                );
                break;
            case 'top_rate':
                add_filter( 'posts_clauses',  array( $woocommerce->query, 'order_by_rating_post_clauses' ) );
                $query_args['meta_query'] = array();
                $query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                $query_args['meta_query'][] = $woocommerce->query->visibility_meta_query();
                break;
            case 'recent_product':
                $query_args['meta_query'] = array();
                $query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                break;
            case 'deals':
                $query_args['meta_query'] = array();
                $query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                $query_args['meta_query'][] = $woocommerce->query->visibility_meta_query();
                $query_args['meta_query'][] =  array(
                    array(
                        'key'           => '_sale_price_dates_to',
                        'value'         => time(),
                        'compare'       => '>',
                        'type'          => 'numeric'
                    )
                );
                break;     
            case 'on_sale':
                $product_ids_on_sale    = wc_get_product_ids_on_sale();
                $product_ids_on_sale[]  = 0;
                $query_args['post__in'] = $product_ids_on_sale;
                break;
            case 'recent_review':
                if($post_per_page == -1) $_limit = 4;
                else $_limit = $post_per_page;
                global $wpdb;
                $query = "SELECT c.comment_post_ID FROM {$wpdb->prefix}posts p, {$wpdb->prefix}comments c
                        WHERE p.ID = c.comment_post_ID AND c.comment_approved > 0 AND p.post_type = 'product' AND p.post_status = 'publish' AND p.comment_count > 0
                        ORDER BY c.comment_date ASC";
                $results = $wpdb->get_results($query, OBJECT);
                $_pids = array();
                foreach ($results as $re) {
                    if(!in_array($re->comment_post_ID, $_pids))
                        $_pids[] = $re->comment_post_ID;
                    if(count($_pids) == $_limit)
                        break;
                }

                $query_args['meta_query'] = array();
                $query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                $query_args['meta_query'][] = $woocommerce->query->visibility_meta_query();
                $query_args['post__in'] = $_pids;

                break;
            case 'rand':
                $query_args['orderby'] = 'rand';
                break;
            case 'recommended':

                $query_args['meta_query'] = array();
                $query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
                $query_args['meta_query'][] = array(
                    'key' => '_apus_recommended',
                    'value' => 'yes',
                );
                $query_args['meta_query'][] = $woocommerce->query->visibility_meta_query();
                break;
            case 'recently_viewed':
                $viewed_products = ! empty( $_COOKIE['apus_woocommerce_recently_viewed'] ) ? (array) explode( '|', $_COOKIE['apus_woocommerce_recently_viewed'] ) : array();
                $viewed_products = array_reverse( array_filter( array_map( 'absint', $viewed_products ) ) );

                if ( empty( $viewed_products ) ) {
                    return false;
                }
                $query_args['post__in'] = $viewed_products;
                break;
        }

        if ( !empty($categories) && is_array($categories) ) {
            $query_args['tax_query'][] = array(
                'taxonomy'      => 'product_cat',
                'field'         => 'slug',
                'terms'         => implode(",", $categories ),
                'operator'      => 'IN'
            );
        }

        if (!empty($includes) && is_array($includes)) {
            $query_args['post__in'] = $includes;
        }
        
        if ( !empty($excludes) && is_array($excludes) ) {
            $query_args['post__not_in'] = $excludes;
        }

        if ( !empty($author) ) {
            $query_args['author'] = $author;
        }

        return new WP_Query($query_args);
    }
}

if ( !function_exists('mella_woocommerce_get_categories') ) {
    function mella_woocommerce_get_categories() {
        $return = array( esc_html__(' --- Choose a Category --- ', 'mella') => '' );

        $args = array(
            'type' => 'post',
            'child_of' => 0,
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => false,
            'hierarchical' => 1,
            'taxonomy' => 'product_cat'
        );

        $categories = get_categories( $args );
        mella_get_category_childs( $categories, 0, 0, $return );

        return $return;
    }
}

if ( !function_exists('mella_get_category_childs') ) {
    function mella_get_category_childs( $categories, $id_parent, $level, &$dropdown ) {
        foreach ( $categories as $key => $category ) {
            if ( $category->category_parent == $id_parent ) {
                $dropdown = array_merge( $dropdown, array( str_repeat( "- ", $level ) . $category->name => $category->slug ) );
                unset($categories[$key]);
                mella_get_category_childs( $categories, $category->term_id, $level + 1, $dropdown );
            }
        }
    }
}

// hooks
function mella_woocommerce_enqueue_styles() {
    wp_enqueue_style( 'mella-woocommerce', get_template_directory_uri() .'/css/woocommerce.css' , 'mella-woocommerce-front' , MELLA_THEME_VERSION, 'all' );
}
add_action( 'wp_enqueue_scripts', 'mella_woocommerce_enqueue_styles', 99 );

function mella_woocommerce_enqueue_scripts() {
    
    wp_enqueue_script( 'sticky-kit', get_template_directory_uri() . '/js/sticky-kit.js', array( 'jquery' ), '20150330', true );
    
    wp_register_script( 'mella-woocommerce', get_template_directory_uri() . '/js/woocommerce.js', array( 'jquery', 'jquery-unveil', 'slick' ), '20150330', true );

    $cart_url = function_exists('wc_get_cart_url') ? wc_get_cart_url() : site_url();
    $options = array(
        'ajaxurl' => admin_url( 'admin-ajax.php' ),
        'enable_search' => (mella_get_config('enable_autocompleate_search', true) ? '1' : '0'),
        'template' => apply_filters( 'mella_autocompleate_search_template', '<a href="{{url}}" class="media autocompleate-media"><div class="media-left media-middle"><img src="{{image}}" class="media-object" height="70" width="70"></div><div class="media-body media-middle"><h4>{{{title}}}</h4><p class="price">{{{price}}}</p></div></a>' ),
        'empty_msg' => apply_filters( 'mella_autocompleate_search_empty_msg', esc_html__( 'Unable to find any products that match the currenty query', 'mella' ) ),

        'success'       => sprintf( '<div class="woocommerce-message">%s <a class="button btn btn-primary btn-inverse wc-forward" href="%s">%s</a></div>', esc_html__( 'Products was successfully added to your cart.', 'mella' ), $cart_url, esc_html__( 'View Cart', 'mella' ) ),
        'empty'         => sprintf( '<div class="woocommerce-error">%s</div>', esc_html__( 'No Products selected.', 'mella' ) ),
        'nonce' => wp_create_nonce( 'ajax-nonce' ),
        'view_more_text' => esc_html__('View More', 'mella'),
        'view_less_text' => esc_html__('View Less', 'mella'),
        '_preset' => mella_get_demo_preset(),
        'is_swatches' => mella_is_woo_swatches_activated() ? 'on' : 'off',
    );
    wp_localize_script( 'mella-woocommerce', 'mella_woo_options', $options );
    wp_enqueue_script( 'mella-woocommerce' );
    
    wp_enqueue_script( 'wc-add-to-cart-variation' );

    if ( !mella_is_wc_quantity_increment_activated() ) {
        wp_enqueue_style( 'mella-quantity-increment', get_template_directory_uri() . '/css/wc-quantity-increment.css');
        wp_enqueue_script( 'mella-number-polyfill', get_template_directory_uri() . '/js/number-polyfill.min.js', array( 'jquery' ), '20150330', true );
        wp_enqueue_script( 'mella-quantity-increment', get_template_directory_uri() . '/js/wc-quantity-increment.js', array( 'jquery' ), '20150330', true );
    }
}
add_action( 'wp_enqueue_scripts', 'mella_woocommerce_enqueue_scripts', 10 );

// cart
if ( !function_exists('mella_woocommerce_header_add_to_cart_fragment') ) {
    function mella_woocommerce_header_add_to_cart_fragment( $fragments ){
        global $woocommerce;
        $fragments['.cart .count'] =  ' <span class="count"> '. $woocommerce->cart->cart_contents_count .' </span> ';
        $fragments['.footer-mini-cart .count'] =  ' <span class="count"> '. $woocommerce->cart->cart_contents_count .' </span> ';
        $fragments['.cart .total-minicart'] = '<div class="total-minicart">'. $woocommerce->cart->get_cart_total(). '</div>';
        return $fragments;
    }
}
add_filter('woocommerce_add_to_cart_fragments', 'mella_woocommerce_header_add_to_cart_fragment' );

// breadcrumb for woocommerce page
if ( !function_exists('mella_woocommerce_breadcrumb_defaults') ) {
    function mella_woocommerce_breadcrumb_defaults( $args ) {
        $breadcrumb_img = mella_get_config('woo_breadcrumb_image');
        $breadcrumb_color = mella_get_config('woo_breadcrumb_color');
        $style = array();
        $show_breadcrumbs = mella_get_config('show_product_breadcrumbs', true);
        $classes = array();

        $is_detail = '';
        if ( !$show_breadcrumbs ) {
            $style[] = 'display:none';
        }
        if( $breadcrumb_color  ){
            $style[] = 'background-color:'.$breadcrumb_color;
        }
        if ( isset($breadcrumb_img['url']) && !empty($breadcrumb_img['url']) ) {
            $style[] = 'background-image:url(\''.esc_url($breadcrumb_img['url']).'\')';
            $classes[] = 'has_bg';
        }
        $classes[] = mella_get_config('woo_breadcrumb_style');

        $estyle = !empty($style)? ' style="'.implode(";", $style).'"':"";
        if ( is_single() ) {
            $title = '<h2 class="bread-title">'.esc_html__('Products Detail', 'mella').'</h2>';
            $classes[] = ' woo-detail ';
            $layout = mella_get_config('product_single_version', 'default');
            if ( $layout == 'layout-image-bg' ) {
                $classes[] = 'no-margin';
            }
        } elseif ( is_product_category() ) {
            global $wp_query;
            $term = $wp_query->queried_object;
            $category_name = esc_html__('Products List', 'mella');
            if ( isset( $term->name) ) {
                $category_name = $term->name;
            }
            
            $title = '<h2 class="bread-title">'.$category_name.'</h2>';
        } else {
            $title = '<h2 class="bread-title">'.esc_html__('Products List', 'mella').'</h2>';
        }
        $full_width = apply_filters('mella_woocommerce_content_class', 'container');
        
        $center = mella_get_config('woo_breadcrumb_center');
        if ( $center ) {
            $classes[] = $center;
        }

        $args['wrap_before'] = '<section id="apus-breadscrumb" class="apus-breadscrumb woo-breadcrumb '.esc_attr(implode(' ', $classes)).'"'.$estyle.'><div class="'.$full_width.'"><div class="wrapper-breads"><div class="wrapper-breads-inner">'.$title.'
        <ol class="breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>';
        $args['wrap_after'] = '</ol></div></div></div></section>';

        return $args;
    }
}
add_filter( 'woocommerce_breadcrumb_defaults', 'mella_woocommerce_breadcrumb_defaults' );
add_action( 'mella_woo_template_main_before', 'woocommerce_breadcrumb', 30, 0 );
add_action( 'mella_woo_number_products', 'woocommerce_result_count', 1 );
// display woocommerce modes
if ( !function_exists('mella_woocommerce_display_modes') ) {
    function mella_woocommerce_display_modes(){
        global $wp;
        $current_url = mella_shop_page_link(true);

        $url_grid = add_query_arg( 'display_mode', 'grid', remove_query_arg( 'display_mode', $current_url ) );
        $url_list = add_query_arg( 'display_mode', 'list', remove_query_arg( 'display_mode', $current_url ) );
        $url_mansory = add_query_arg( 'display_mode', 'mansory', remove_query_arg( 'display_mode', $current_url ) );
        $url_metro = add_query_arg( 'display_mode', 'metro', remove_query_arg( 'display_mode', $current_url ) );

        $woo_mode = mella_woocommerce_get_display_mode();

        echo '<div class="display-mode ">';
        echo '<a href="'.  $url_grid  .'" class=" change-view '.($woo_mode == 'grid' ? 'active' : '').'"><i class="fa fa-th-large"></i></a>';
        echo '<a href="'.  $url_list  .'" class=" change-view '.($woo_mode == 'list' ? 'active' : '').'"><i class="fa fa-th-list"></i></a>';
        echo '<a href="'.  $url_mansory  .'" class=" change-view hidden '.($woo_mode == 'mansory' ? 'active' : '').'"><i class="fa fa-table"></i></a>';
        echo '<a href="'.  $url_metro  .'" class=" change-view hidden '.($woo_mode == 'metro' ? 'active' : '').'"><i class="fa fa-align-right"></i></a>';
        echo '</div>'; 
    }
}

if ( !function_exists('mella_woocommerce_get_display_mode') ) {
    function mella_woocommerce_get_display_mode() {
        $woo_mode = mella_get_config('product_display_mode', 'grid');
        $args = array( 'grid', 'list', 'mansory', 'metro' );
        if ( isset($_COOKIE['mella_woo_mode']) && in_array($_COOKIE['mella_woo_mode'], $args) ) {
            $woo_mode = $_COOKIE['mella_woo_mode'];
        }
        return $woo_mode;
    }
}

if(!function_exists('mella_shop_page_link')) {
    function mella_shop_page_link($keep_query = false ) {
        if ( defined( 'SHOP_IS_ON_FRONT' ) ) {
            $link = home_url();
        } elseif ( is_post_type_archive( 'product' ) || is_page( wc_get_page_id('shop') ) ) {
            $link = get_post_type_archive_link( 'product' );
        } else {
            $link = get_term_link( get_query_var('term'), get_query_var('taxonomy') );
        }

        if( $keep_query ) {
            // Keep query string vars intact
            foreach ( $_GET as $key => $val ) {
                if ( 'orderby' === $key || 'submit' === $key ) {
                    continue;
                }
                $link = add_query_arg( $key, $val, $link );

            }
        }
        return $link;
    }
}


if(!function_exists('mella_filter_before')){
    function mella_filter_before(){
        echo '<div class="wrapper-fillter"><div class="apus-filter">';
    }
}
if(!function_exists('mella_filter_after')){
    function mella_filter_after(){
        echo '</div></div>';
    }
}
add_action( 'woocommerce_before_shop_loop', 'mella_filter_before' , 1 );
add_action( 'woocommerce_before_shop_loop', 'mella_filter_after' , 40 );

// add filter to top archive
add_action( 'woocommerce_top_pagination', 'woocommerce_pagination', 1 );

if ( !function_exists('mella_before_woocommerce_init') ) {
    function mella_before_woocommerce_init() {
        // set display mode to cookie
        if( isset($_GET['display_mode']) && in_array($_GET['display_mode'], array('grid', 'list', 'mansory', 'metro')) ){  
            setcookie( 'mella_woo_mode', trim($_GET['display_mode']) , time()+3600*24*100,'/' );
            $_COOKIE['mella_woo_mode'] = trim($_GET['display_mode']);
        }

        if ( mella_get_config('show_quickview', false) ) {
            add_action( 'wp_ajax_mella_quickview_product', 'mella_woocommerce_quickview' );
            add_action( 'wp_ajax_nopriv_mella_quickview_product', 'mella_woocommerce_quickview' );
            add_action( 'wc_ajax_mella_quickview_product', 'mella_woocommerce_quickview' );
        }

        add_action( 'wp_ajax_mella_ajax_get_products', 'mella_woocommerce_get_ajax_products' );
        add_action( 'wp_ajax_nopriv_mella_ajax_get_products', 'mella_woocommerce_get_ajax_products' );
        add_action( 'wc_ajax_mella_ajax_get_products', 'mella_woocommerce_get_ajax_products' );
    }
}
add_action( 'init', 'mella_before_woocommerce_init' );

// Number of products per page
if ( !function_exists('mella_woocommerce_shop_per_page') ) {
    function mella_woocommerce_shop_per_page($number) {
        
        if ( isset( $_REQUEST['wppp_ppp'] ) ) :
            $number = intval( $_REQUEST['wppp_ppp'] );
            WC()->session->set( 'products_per_page', intval( $_REQUEST['wppp_ppp'] ) );
        elseif ( isset( $_REQUEST['ppp'] ) ) :
            $number = intval( $_REQUEST['ppp'] );
            WC()->session->set( 'products_per_page', intval( $_REQUEST['ppp'] ) );
        elseif ( WC()->session->__isset( 'products_per_page' ) ) :
            $number = intval( WC()->session->__get( 'products_per_page' ) );
        else :
            $value = mella_get_config('number_products_per_page', 12);
            $number = intval( $value );
        endif;
        
        return $number;

    }
}
add_filter( 'loop_shop_per_page', 'mella_woocommerce_shop_per_page', 30 );

// Number of products per row
if ( !function_exists('mella_woocommerce_shop_columns') ) {
    function mella_woocommerce_shop_columns($number) {
        $value = mella_get_config('product_columns');
        if ( in_array( $value, array(1, 2, 3, 4, 5, 6, 7, 8) ) ) {
            $number = $value;
        }
        return $number;
    }
}
add_filter( 'loop_shop_columns', 'mella_woocommerce_shop_columns' );

// share box
if ( !function_exists('mella_woocommerce_share_box') ) {
    function mella_woocommerce_share_box() {
        if ( mella_get_config('show_product_social_share', false) ) { ?>
            <div class="show-share">
                <span class="action"><i class="fa fa-share-alt" aria-hidden="true"></i></span>
                <?php get_template_part( 'template-parts/sharebox' ); ?>
            </div>
            <?php 
        }
    }
}
add_filter( 'woocommerce_single_product_summary', 'mella_woocommerce_share_box', 39 );

// add div top infor for detail
function mella_woo_before_detail_info() {
    ?>
    <div class="top-info-detail clearfix">
    <?php
}
function mella_woo_after_detail_info() {
    ?>
    </div>
    <?php
}
function mella_woo_clearfix_addcart() {
    ?>
    <div class="clearfix"></div>
    <?php
}

add_filter( 'woocommerce_single_product_summary', 'mella_woo_before_detail_info', 1 );
add_filter( 'woocommerce_single_product_summary', 'mella_woo_after_detail_info', 12 );

add_filter( 'woocommerce_single_product_summary', 'mella_woo_clearfix_addcart', 39 );

// add stock;
function mella_woo_stock() {
    global $product;
    echo wc_get_stock_html( $product );
    ?>
    <?php
}
add_filter( 'woocommerce_single_product_summary', 'mella_woo_stock', 9 );

function mella_woo_categories() {
    global $product;
    echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in"><span class="sub_title">' . _n( 'In:', 'In:', count( $product->get_category_ids() ), 'mella' ) . '</span> ', '</span>' ); 
    ?>
    <?php
}
add_filter( 'woocommerce_single_product_summary', 'mella_woo_categories', 7);

function mella_woo_categories_top() {
    global $product;
    echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in p-top hidden"><span class="sub_title">' . _n( 'In:', 'In:', count( $product->get_category_ids() ), 'mella' ) . '</span> ', '</span>' ); 
    ?>
    <?php
}
add_filter( 'woocommerce_single_product_summary', 'mella_woo_categories_top', 4);

// hook wrapper
function mella_woo_before_top_info_detail() {
    ?>
    <div class="top-info clearfix">
    <?php
}
function mella_woo_after_top_info_detail() {
    ?>
    </div>
    <?php
}
add_filter( 'woocommerce_single_product_summary', 'mella_woo_before_top_info_detail', 6 );
add_filter( 'woocommerce_single_product_summary', 'mella_woo_after_top_info_detail', 9 );

// shipping infomation
if ( !function_exists('mella_woocommerce_shipping_info') ) {
    function mella_woocommerce_shipping_info() {
        $shipping_info = mella_get_config('product_shipping_info');
        if ( !empty($shipping_info) ) {
            echo "<div class='shipping_info'>";
            echo trim($shipping_info);
            echo "</div>";
        }
    }
}
add_filter( 'woocommerce_after_single_product_summary', 'mella_woocommerce_shipping_info', 11 );

// quickview
if ( !function_exists('mella_woocommerce_quickview') ) {
    function mella_woocommerce_quickview() {
        if ( !empty($_GET['product_id']) ) {
            $args = array(
                'post_type' => 'product',
                'post__in' => array($_GET['product_id'])
            );
            $query = new WP_Query($args);
            if ( $query->have_posts() ) {
                while ($query->have_posts()): $query->the_post(); global $product;
                    wc_get_template_part( 'content', 'product-quickview' );
                endwhile;
            }
            wp_reset_postdata();
        }
        die;
    }
}

// swap effect
if ( !function_exists('mella_swap_images') ) {
    function mella_swap_images() {
        global $post, $product, $woocommerce;
        
        $thumb = 'woocommerce_thumbnail';
        $output = '';
        $class = "attachment-$thumb size-$thumb image-no-effect";
        if (has_post_thumbnail()) {
            $swap_image = mella_get_config('enable_swap_image', true);
            if ( $swap_image ) {
                $attachment_ids = $product->get_gallery_image_ids();
                if ($attachment_ids && isset($attachment_ids[0])) {
                    $class = "attachment-$thumb size-$thumb image-hover";
                    $swap_class = "attachment-$thumb size-$thumb image-effect";
                    $output .= mella_get_attachment_thumbnail( $attachment_ids[0], $thumb, false, array('class' => $swap_class), false);
                }
            }
            $output .= mella_get_attachment_thumbnail( get_post_thumbnail_id(), $thumb , false, array('class' => $class), false);
        } else {
            $image_sizes = get_option('shop_catalog_image_size');
            $placeholder_width = $image_sizes['width'];
            $placeholder_height = $image_sizes['height'];

            $output .= '<img src="'.wc_placeholder_img_src().'" alt="'.esc_attr__('Placeholder' , 'mella').'" class="'.$class.'" width="'.$placeholder_width.'" height="'.$placeholder_height.'" />';
        }
        echo trim($output);
    }
}
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action('woocommerce_before_shop_loop_item_title', 'mella_swap_images', 10);

if ( !function_exists('mella_product_image') ) {
    function mella_product_image($thumb = 'woocommerce_thumbnail') {
        $swap_image = (bool)mella_get_config('enable_swap_image', true);
        ?>
        <a title="<?php echo esc_attr(get_the_title()); ?>" href="<?php the_permalink(); ?>" class="product-image">
            <?php mella_product_get_image($thumb, $swap_image); ?>
        </a>
        <?php
    }
}
// get image
if ( !function_exists('mella_product_get_image') ) {
    function mella_product_get_image($thumb = 'woocommerce_thumbnail', $swap = true) {
        global $post, $product, $woocommerce;
        
        $output = '';
        $class = "attachment-$thumb size-$thumb image-no-effect";
        if (has_post_thumbnail()) {
            if ( $swap ) {
                $attachment_ids = $product->get_gallery_image_ids();
                if ($attachment_ids && isset($attachment_ids[0])) {
                    $class = "attachment-$thumb size-$thumb image-hover";
                    $swap_class = "attachment-$thumb size-$thumb image-effect";
                    $output .= mella_get_attachment_thumbnail( $attachment_ids[0], $thumb , false, array('class' => $swap_class), false);
                }
            }
            $output .= mella_get_attachment_thumbnail( get_post_thumbnail_id(), $thumb , false, array('class' => $class), false);
        } else {
            $image_sizes = get_option('shop_catalog_image_size');
            $placeholder_width = $image_sizes['width'];
            $placeholder_height = $image_sizes['height'];

            $output .= '<img src="'.wc_placeholder_img_src().'" alt="'.esc_attr__('Placeholder' , 'mella').'" class="'.$class.'" width="'.$placeholder_width.'" height="'.$placeholder_height.'" />';
        }
        echo trim($output);
    }
}

// layout class for woo page
if ( !function_exists('mella_woocommerce_content_class') ) {
    function mella_woocommerce_content_class( $class ) {
        $page = 'archive';
        if ( is_singular( 'product' ) ) {
            $page = 'single';
        }
        if( mella_get_config('product_'.$page.'_fullwidth') ) {
            return 'container-fluid';
        }
        return $class;
    }
}
add_filter( 'mella_woocommerce_content_class', 'mella_woocommerce_content_class' );

// get layout configs
if ( !function_exists('mella_get_woocommerce_layout_configs') ) {
    function mella_get_woocommerce_layout_configs() {
        $page = 'archive';
        if ( is_singular( 'product' ) ) {
            $page = 'single';
        }
        $left = mella_get_config('product_'.$page.'_left_sidebar');
        $right = mella_get_config('product_'.$page.'_right_sidebar');
        // check full width
        if( mella_get_config('product_'.$page.'_fullwidth') ) {
            $sidebar = 'col-lg-2';
            $main_full = 'col-lg-10';
        }else{
            $sidebar = 'col-lg-3';
            $main_full = 'col-lg-9';
        }
        switch ( mella_get_config('product_'.$page.'_layout') ) {
            case 'left-main':
                $configs['left'] = array( 'sidebar' => $left, 'class' => $sidebar.' col-md-3 col-sm-12 col-xs-12'  );
                $configs['main'] = array( 'class' => $main_full.' col-md-9 col-sm-12 col-xs-12' );
                break;
            case 'main-right':
                $configs['right'] = array( 'sidebar' => $right,  'class' => $sidebar.' col-md-3 col-sm-12 col-xs-12' ); 
                $configs['main'] = array( 'class' => $main_full.' col-md-9 col-sm-12 col-xs-12' );
                break;
            case 'main':
                $configs['main'] = array( 'class' => 'col-md-12 col-sm-12 col-xs-12' );
                break;
            case 'left-main-right':
                $configs['left'] = array( 'sidebar' => $left,  'class' => 'col-md-3 col-sm-12 col-xs-12'  );
                $configs['right'] = array( 'sidebar' => $right, 'class' => 'col-md-3 col-sm-12 col-xs-12' ); 
                $configs['main'] = array( 'class' => 'col-md-6 col-sm-12 col-xs-12' );
                break;
            default:
                $configs['main'] = array( 'class' => 'col-md-12 col-sm-12 col-xs-12' );
                break;
        }
        return $configs; 
    }
}

if ( !function_exists( 'mella_product_review_tab' ) ) {
    function mella_product_review_tab($tabs) {
        global $post;
        if ( !mella_get_config('show_product_review_tab', true) && isset($tabs['reviews']) ) {
            unset( $tabs['reviews'] ); 
        }

        if ( !mella_get_config('hidden_product_additional_information_tab', false) && isset($tabs['additional_information']) ) {
            unset( $tabs['additional_information'] ); 
        }
        
        return $tabs;
    }
}
add_filter( 'woocommerce_product_tabs', 'mella_product_review_tab', 90 );

function mella_woocommerce_get_ajax_products() {
    if ( mella_is_yith_woocompare_activated() ) {
        $compare_path = WP_PLUGIN_DIR.'/yith-woocommerce-compare/includes/class.yith-woocompare-frontend.php';
        if ( file_exists($compare_path) ) {
            require_once ($compare_path);
        }
    }

    $settings = isset($_POST['settings']) ? $_POST['settings'] : '';
    $tab = isset($_POST['tab']) ? $_POST['tab'] : '';
    
    if ( empty($settings) || empty($tab) ) {
        exit();
    }

    $categories = isset($tab['category']) ? $tab['category'] : '';
    $columns = isset($settings['columns']) ? $settings['columns'] : 4;
    $rows = isset($settings['rows']) ? $settings['rows'] : 1;
    $show_nav = isset($settings['show_nav']) ? $settings['show_nav'] : false;
    $show_pagination = isset($settings['show_pagination']) ? $settings['show_pagination'] : false;
    $number = isset($settings['number']) ? $settings['number'] : 4;
    $product_type = isset($tab['type']) ? $tab['type'] : 'recent_product';

    $layout_type = isset($settings['layout_type']) ? $settings['layout_type'] : 'grid';

    $categories = !empty($categories) ? array($categories) : array();
    $args = array(
        'categories' => $categories,
        'product_type' => $product_type,
        'paged' => 1,
        'post_per_page' => $number,
    );
    $loop = mella_get_products( $args );
    if ( $loop->have_posts() ) {
        $max_pages = $loop->max_num_pages;
        wc_get_template( 'layout-products/'.$layout_type.'.php' , array(
            'loop' => $loop,
            'columns' => $columns,
            'rows' => $rows,
            'show_nav' => $show_nav,
            'show_pagination' => $show_pagination,
        ) );
    }
    exit();
}

// Wishlist
add_filter( 'yith_wcwl_button_label', 'mella_woocomerce_icon_wishlist'  );
add_filter( 'yith-wcwl-browse-wishlist-label', 'mella_woocomerce_icon_wishlist_add' );
function mella_woocomerce_icon_wishlist( $value='' ){
    return '<i class="icon-heart"></i>'.'<span class="sub-title">'.esc_html__('Add to Wishlist','mella').'</span>';
}

function mella_woocomerce_icon_wishlist_add(){
    return '<i class="icon-heart"></i>'.'<span class="sub-title">'.esc_html__('Wishlisted','mella').'</span>';
}
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

function mella_get_all_subcategories_levels($parent_id, $parent_slug, &$return = array()) {
    $return[] = $parent_slug;
    $args = array(
        'hierarchical' => true,
        'show_option_none' => '',
        'hide_empty' => true,
        'parent' => $parent_id,
        'taxonomy' => 'product_cat'
    );
    $cats = get_categories($args);
    foreach ($cats as $cat) {
        mella_get_all_subcategories_levels($cat->term_id, $cat->slug, $return);
    }
    return $return;
}

function mella_woocommerce_accessories() {
    get_template_part( 'woocommerce/single-product/tabs/accessories' );
}

function mella_woocommerce_single_countdown() {
    if ( mella_get_config('show_product_countdown_timer') ) {
        get_template_part( 'woocommerce/single-product/countdown' );
    }
}
add_action('woocommerce_single_product_summary', 'mella_woocommerce_single_countdown', 21);


if ( ! function_exists( 'mella_wc_products_per_page' ) ) {
    function mella_wc_products_per_page() {
        global $wp_query;

        $action = '';
        $cat                = $wp_query->get_queried_object();
        $return_to_first    = apply_filters( 'mella_wc_ppp_return_to_first', false );
        $total              = $wp_query->found_posts;
        $per_page           = $wp_query->get( 'posts_per_page' );
        $_per_page          = mella_get_config('number_products_per_page', 12);

        // Generate per page options
        $products_per_page_options = array();
        while ( $_per_page < $total ) {
            $products_per_page_options[] = $_per_page;
            $_per_page = $_per_page * 2;
        }

        if ( empty( $products_per_page_options ) ) {
            return;
        }

        $products_per_page_options[] = -1;

        $query_string = ! empty( $_GET['QUERY_STRING'] ) ? '?' . add_query_arg( array( 'ppp' => false ), $_GET['QUERY_STRING'] ) : null;

        if ( isset( $cat->term_id ) && isset( $cat->taxonomy ) && $return_to_first ) {
            $action = get_term_link( $cat->term_id, $cat->taxonomy ) . $query_string;
        } elseif ( $return_to_first ) {
            $action = get_permalink( wc_get_page_id( 'shop' ) ) . $query_string;
        }

        if ( ! woocommerce_products_will_display() ) {
            return;
        }
        ?>
        <form method="POST" action="<?php echo esc_url( $action ); ?>" class="form-mella-ppp">
            <?php
            foreach ( $_GET as $key => $value ) {
                if ( 'ppp' === $key || 'submit' === $key ) {
                    continue;
                }
                if ( is_array( $value ) ) {
                    foreach( $value as $i_value ) {
                        ?>
                        <input type="hidden" name="<?php echo esc_attr( $key ); ?>[]" value="<?php echo esc_attr( $i_value ); ?>" />
                        <?php
                    }
                } else {
                    ?><input type="hidden" name="<?php echo esc_attr( $key ); ?>" value="<?php echo esc_attr( $value ); ?>" /><?php
                }
            }
            ?>

            <select name="ppp" onchange="this.form.submit()" class="mella-wc-wppp-select">
                <?php foreach( $products_per_page_options as $key => $value ) { ?>
                    <option value="<?php echo esc_attr( $value ); ?>" <?php selected( $value, $per_page ); ?>><?php
                        $ppp_text = apply_filters( 'mella_wc_ppp_text', esc_html__( 'Show: %s', 'mella' ), $value );
                        esc_html( printf( $ppp_text, $value == -1 ? esc_html__( 'All', 'mella' ) : $value ) );
                    ?></option>
                <?php } ?>
            </select>
        </form>
        <?php
    }
}

function mella_woo_after_shop_loop_before() {
    ?>
    <div class="apus-after-loop-shop clearfix">
    <?php
}
function mella_woo_after_shop_loop_after() {
    ?>
    </div>
    <?php
}
add_action( 'woocommerce_after_shop_loop', 'mella_woo_after_shop_loop_before', 1 );
add_action( 'woocommerce_after_shop_loop', 'mella_woo_after_shop_loop_after', 99999 );
// add_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 30 );
// add_action( 'woocommerce_after_shop_loop', 'mella_wc_products_per_page', 20 );


function mella_woo_display_product_cat($product_id) {
    $terms = get_the_terms( $product_id, 'product_cat' );
    if ( !empty($terms) ) { ?>
        <div class="product-cats">
        <?php foreach ( $terms as $term ) {
            echo '<a href="' . get_term_link( $term->term_id ) . '">' . $term->name . '</a>';
            break;
        } ?>
        </div>
    <?php
    }
}
if ( !function_exists ('mella_onsale_price_show') ) {
    function mella_onsale_price_show() {
        global $product;
        if( $product->is_on_sale() ) {
            return $product->get_regular_price() - $product->get_sale_price();
        }
        return '';
    }
}

// catalog mode
add_action( 'wp', 'mella_catalog_mode_init' );
add_action( 'wp', 'mella_pages_redirect' );


function mella_catalog_mode_init() {

    if( ! mella_get_config( 'enable_shop_catalog' ) ) return false;

    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );

}

function mella_pages_redirect() {
    if( ! mella_get_config( 'enable_shop_catalog' ) ) return false;

    $cart     = is_page( wc_get_page_id( 'cart' ) );
    $checkout = is_page( wc_get_page_id( 'checkout' ) );

    wp_reset_postdata();

    if ( $cart || $checkout ) {
        wp_redirect( home_url() );
        exit;
    }
}


/*
 * Start for only Mella theme
 */
function mella_is_ajax_request() {
    if ( isset( $_REQUEST['load_type'] ) ) {
        return true;
    }
    return false;
}

function mella_wc_product_dropdown_categories( $args = array() ) {
    global $wp_query;

    $current_product_cat = isset( $wp_query->query_vars['product_cat'] ) ? $wp_query->query_vars['product_cat'] : '';
    $defaults            = array(
        'pad_counts'         => 1,
        'show_count'         => 1,
        'hierarchical'       => 1,
        'hide_empty'         => 1,
        'show_uncategorized' => 1,
        'orderby'            => 'name',
        'selected'           => $current_product_cat,
        'menu_order'         => false,
        'option_select_text' => esc_html__( 'All', 'mella' ),
    );

    $args = wp_parse_args( $args, $defaults );

    if ( 'order' === $args['orderby'] ) {
        $args['menu_order'] = 'asc';
        $args['orderby']    = 'name';
    }

    $terms = get_terms( 'product_cat', apply_filters( 'wc_product_dropdown_categories_get_terms_args', $args ) );

    if ( empty( $terms ) ) {
        return;
    }
    $shop_page_id = wc_get_page_id( 'shop' );
    $shopurl = esc_url ( get_permalink( $shop_page_id ) );

    $count_products = wp_count_posts('product');
    
    $count = 0;
    if ( !empty($count_products) ) {
        $count = (int)$count_products->publish;
    }

    $output  = "<ul>";
    $output .= '<li ' . ( $current_product_cat == '' ? 'class="active"' : '' ) . '>'
            . '<a href="'.esc_url($shopurl).'">'
            . esc_html( $args['option_select_text'] ) . ' ('.$count.')' 
            . '</a>'
            . '</li>';
    $output .= mella_wc_walk_category_dropdown_tree( $terms, 0, $args );
    $output .= "</ul>";

    echo trim($output);
}

function mella_wc_walk_category_dropdown_tree() {
    $args = func_get_args();

    // the user's options are the third parameter
    if ( empty( $args[2]['walker'] ) || ! is_a( $args[2]['walker'], 'Walker' ) ) {
        $walker = new Mella_WC_Product_Cat_Dropdown_Walker;
    } else {
        $walker = $args[2]['walker'];
    }

    return call_user_func_array( array( &$walker, 'walk' ), $args );
}


function mella_category_menu_create_list( $category, $current_cat_id ) {
    $output = '<li class="cat-item-' . $category->term_id;
                    
    if ( $current_cat_id == $category->term_id ) {
        $output .= ' current-cat';
    }
    
    $output .=  '"><a href="' . esc_url( get_term_link( (int) $category->term_id, 'product_cat' ) ) . '">' . esc_attr( $category->name ) . '</a></li>';
    
    return $output;
}

/*
 *  Product category menu
 */
if ( ! function_exists( 'mella_category_menu' ) ) {
    function mella_category_menu() {
        global $wp_query;

        $current_cat_id = ( is_tax( 'product_cat' ) ) ? $wp_query->queried_object->term_id : '';
        $is_category = ( strlen( $current_cat_id ) > 0 ) ? true : false;
        $hide_empty = true;
        $shop_categories_top_level = true;
        // Should top-level categories be displayed?
        if ( !$shop_categories_top_level && $is_category ) {
            mella_sub_category_menu_output( $current_cat_id, $hide_empty );
        } else {
            mella_category_menu_output( $is_category, $current_cat_id, $hide_empty );
        }
    }
}

    

/*
 *  Product category menu: Output
 */
function mella_category_menu_output( $is_category, $current_cat_id, $hide_empty ) {
    global $wp_query;
    
    $page_id = wc_get_page_id( 'shop' );
    $page_url = get_permalink( $page_id );
    $hide_sub = true;
    $all_categories_class = '';
    
    // Is this a category page?                                                             
    if ( $is_category ) {
        $hide_sub = false;
        
        // Get current category's direct children
        $direct_children = get_terms( 'product_cat',
            array(
                'fields'        => 'ids',
                'parent'        => $current_cat_id,
                'hierarchical'  => true,
                'hide_empty'    => $hide_empty
            )
        );
        
        $category_has_children = ( empty( $direct_children ) ) ? false : true;
    } else {
        // No current category, set "All" as current (if not product tag archive or search)
        if ( ! is_product_tag() && ! isset( $_REQUEST['s'] ) ) {
            $all_categories_class = ' class="current-cat"';
        }
    }
    
    $output = '<li' . $all_categories_class . '><a href="' . esc_url ( $page_url ) . '">' . esc_html__( 'All', 'mella' ) . '</a></li>';
    $sub_output = '';
    
    // Categories order
    $orderby = 'slug';
    $order = 'asc';
    
    
    $categories = get_categories( array(
        'type'          => 'post',
        'orderby'       => $orderby, // Note: 'name' sorts by product category "menu/sort order"
        'order'         => $order,
        'hide_empty'    => $hide_empty,
        'hierarchical'  => 1,
        'taxonomy'      => 'product_cat'
    ) );
             
    foreach( $categories as $category ) {
        // Is this a sub-category?
        if ( $category->parent != '0' ) {
            // Should sub-categories be included?
            if ( $hide_sub ) {
                continue;
            } else {
                if ( 
                    $category->term_id == $current_cat_id ||
                    $category->parent == $current_cat_id ||
                    ! $category_has_children && $category->parent == $wp_query->queried_object->parent
                ) {
                    $sub_output .= mella_category_menu_create_list( $category, $current_cat_id );
                }
                continue;
            }
        }
        
        $output .= mella_category_menu_create_list( $category, $current_cat_id );
    }
    
    if ( strlen( $sub_output ) > 0 ) {
        $sub_output = '<ul class="apus-shop-sub-categories">' . $sub_output . '</ul>';
    }
    
    $output = $output . $sub_output;
    
    echo trim($output);
}

/*
 *  Product category menu: Output sub-categories
 */
function mella_sub_category_menu_output( $current_cat_id, $hide_empty ) {
    global $wp_query;
    
    
    $output_sub_categories = '';
    
    // Categories order
    $orderby = 'slug';
    $order = 'asc';
    
    $sub_categories = get_categories( array(
        'type'          => 'post',
        'parent'        => $current_cat_id,
        'orderby'       => $orderby,
        'order'         => $order,
        'hide_empty'    => $hide_empty,
        'hierarchical'  => 1,
        'taxonomy'      => 'product_cat'
    ) );
    
    $has_sub_categories = ( empty( $sub_categories ) ) ? false : true;
    
    // Is there any sub-categories available
    if ( $has_sub_categories ) {
        $current_cat_name = apply_filters( 'mella_shop_parent_category_title', $wp_query->queried_object->name );
        
        foreach ( $sub_categories as $sub_category ) {
            $output_sub_categories .= mella_category_menu_create_list( $sub_category, $current_cat_id );
        }
    } else {
        $current_cat_name = $wp_query->queried_object->name;
    }
    
    $current_cat_url = get_term_link( (int) $current_cat_id, 'product_cat' );
    $output_current_cat = '<li class="current-cat"><a href="' . esc_url( $current_cat_url ) . '">' . esc_html( $current_cat_name ) . '</a></li>';
    
    echo trim($output_current_cat . $output_sub_categories);
}

function mella_count_filtered() {
    $return = 0;
    if ( isset($_GET['min_price']) && isset($_GET['max_price']) ) {
        $return++;
    }
    // filter by attributes
    $attribute_taxonomies = wc_get_attribute_taxonomies();

    if ( ! empty( $attribute_taxonomies ) ) {
        foreach ( $attribute_taxonomies as $tax ) {
            if ( taxonomy_exists( wc_attribute_taxonomy_name( $tax->attribute_name ) ) ) {
                if ( isset($_GET['filter_'.$tax->attribute_name]) ) {
                    $return++;
                }
            }
        }
    }
    return $return;
}

// remove shop and archive descripton
remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
remove_action( 'woocommerce_archive_description', 'woocommerce_product_archive_description', 10 );

// add to mella
add_action( 'mella_woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
add_action( 'mella_woocommerce_archive_description', 'woocommerce_product_archive_description', 10 );


function mella_woo_orderby() {
    global $wp_query;
    
    $output = '';
    
    if ( 1 != $wp_query->found_posts || woocommerce_products_will_display() ) {
        $output .= '<div class="wrapper-limit">';
        $output .= '<ul id="apus-orderby-sorting" class="apus-orderby-sorting">';
        
        $orderby = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
        $orderby == ( $orderby ===  'title' ) ? 'menu_order' : $orderby; // Fixed: 'title' is default before WooCommerce settings are saved
        
        $catalog_orderby_options = apply_filters( 'woocommerce_catalog_orderby', array(
            'menu_order'    => esc_html__( 'Default', 'mella' ),
            'popularity'    => esc_html__( 'Popularity', 'mella' ),
            'rating'        => esc_html__( 'Average rating', 'mella' ),
            'date'          => esc_html__( 'Newness', 'mella' ),
            'price'         => esc_html__( 'Price: Low to High', 'mella' ),
            'price-desc'    => esc_html__( 'Price: High to Low', 'mella' )
        ) );

        if ( get_option( 'woocommerce_enable_review_rating' ) === 'no' ) {
            unset( $catalog_orderby_options['rating'] );
        }
        
        
        /* Build entire current page URL (including query strings) */
        global $wp;
        $link = home_url( $wp->request ); // Base page URL
                
        // Unset query strings used for Ajax shop filters
        unset( $_GET['shop_load'] );
        unset( $_GET['_'] );
        
        $qs_count = count( $_GET );
        
        // Any query strings to add?
        if ( $qs_count > 0 ) {
            $i = 0;
            $link .= '?';
            
            // Build query string
            foreach ( $_GET as $key => $value ) {
                $i++;
                $link .= $key . '=' . $value;
                if ( $i != $qs_count ) {
                    $link .= '&';
                }
            }
        }
        
        
        foreach ( $catalog_orderby_options as $id => $name ) {
            if ( $orderby == $id ) {
                $output .= '<li class="active">' . esc_attr( $name ) . '</li>';
            } else {
                // Add 'orderby' URL query string
                $link = add_query_arg( 'orderby', $id, $link );
                $output .= '<li><a href="' . esc_url( $link ) . '">' . esc_attr( $name ) . '</a></li>';
            }
        }
               
        $output .= '</ul>';
        $output .= '</div>';
    }
    
    echo trim($output);
}

function mella_product_video() {
    global $post;
    $video = get_post_meta( $post->ID, 'apus_product_review_video', true );

    if (!empty($video)) {
        ?>
        <div class="video">
          <a href="<?php echo esc_url($video); ?>" class="popup-video">
            <i class="fa fa-play"></i>
          </a>
        </div>
        <?php
    }
}
function mella_placeholder_fields($fields){
    if ( !empty($fields['billing']['billing_first_name']) ) {
        $fields['billing']['billing_first_name']['placeholder'] = esc_html__('First Name*', 'mella');
    }
    if ( !empty($fields['billing']['billing_last_name']) ) {
        $fields['billing']['billing_last_name']['placeholder'] = esc_html__('Last Name*', 'mella');
    }
    if ( !empty($fields['billing']['billing_company']) ) {
        $fields['billing']['billing_company']['placeholder'] = esc_html__('Company Name', 'mella');
    }
    if ( !empty($fields['billing']['billing_phone']) ) {
        $fields['billing']['billing_phone']['placeholder'] = esc_html__('Phone*', 'mella');
    }
    if ( !empty($fields['billing']['billing_city']) ) {
        $fields['billing']['billing_city']['placeholder'] = esc_html__('Town / City*', 'mella');
    }
    if ( !empty($fields['billing']['billing_postcode']) ) {
        $fields['billing']['billing_postcode']['placeholder'] = esc_html__('ZIP*', 'mella');
    }
    if ( !empty($fields['billing']['billing_email']) ) {
        $fields['billing']['billing_email']['placeholder'] = esc_html__('Email', 'mella');
    }

    return $fields;
}
add_filter('woocommerce_checkout_fields', 'mella_placeholder_fields', 10);

function mella_placeholder_address_fields( $address_fields ) {
    if ( !empty($address_fields['first_name']) ) {
        $address_fields['first_name']['placeholder'] = esc_html__('First Name*', 'mella');
    }
    if ( !empty($address_fields['last_name']) ) {
        $address_fields['last_name']['placeholder'] = esc_html__('Last Name*', 'mella');
    }
    if ( !empty($address_fields['address_1']) ) {
        $address_fields['address_1']['placeholder'] = esc_html__('Adresse', 'mella');
    }
    if ( !empty($address_fields['company']) ) {
        $address_fields['company']['placeholder'] = esc_html__('Company Name ', 'mella');
    }
    if ( !empty($address_fields['state']) ) {
        $address_fields['state']['placeholder'] = esc_html__('State', 'mella');
    }
    if ( !empty($address_fields['postcode']) ) {
        $address_fields['postcode']['placeholder'] = esc_html__('Postcode', 'mella');
    }
    if ( !empty($address_fields['city']) ) {
        $address_fields['city']['placeholder'] = esc_html__('City', 'mella');
    }
    if ( !empty($address_fields['phone']) ) {
        $address_fields['phone']['placeholder'] = esc_html__('phone', 'mella');
    }
    if ( !empty($address_fields['email']) ) {
        $address_fields['email']['placeholder'] = esc_html__('Email', 'mella');
    }
    return $address_fields;
}
add_filter('woocommerce_default_address_fields', 'mella_placeholder_address_fields', 10);
<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<div class="widget-banner <?php echo esc_attr($el_class); ?>">
    <div class="widget-banner-item ">
        <?php if ( $url ) { ?>
            <a class="link-img" href="<?php echo esc_url($url); ?>">
        <?php } ?>
            <?php echo trim(mella_get_attachment_thumbnail($image, 'full')); ?>
        <?php if ( $url ) { ?>
            </a>
        <?php } ?>
        <div class="<?php echo esc_attr($style.' '.$size.' '.$align); ?>">
            <div class="infor">
                <?php if ($subtitle!=''): ?>
                    <div class="subtitle">
                        <?php echo trim( $subtitle ); ?>
                    </div>
                <?php endif; ?>
                <?php if (wpb_js_remove_wpautop( $content, true )): ?>
                    <div class="title">
                        <?php echo wpb_js_remove_wpautop( $content, true ); ?>
                    </div>
                <?php endif; ?>
                <?php if ($des!=''): ?>
                    <div class="des">
                        <?php echo trim( $des ); ?>
                    </div>
                <?php endif; ?>
                <?php if ( !empty($url)) { ?>
                    <div class="more">
                      <a href="<?php echo esc_url($url); ?>" class="btn-readmore"><?php echo trim($text_url); ?></a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
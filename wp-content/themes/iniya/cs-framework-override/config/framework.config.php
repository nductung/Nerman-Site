<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => constant('INIYA_THEME_NAME').' '.esc_html__('Options', 'iniya'),
  'menu_type'       => 'theme', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'cs-framework',
  'ajax_save'       => true,
  'show_reset_all'  => false,
  'framework_title' => sprintf(esc_html__('Designthemes Framework %1$s', 'iniya'), '<small>'.esc_html__('by Designthemes', 'iniya').'</small>'),
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();

$options[]      = array(
  'name'        => 'general',
  'title'       => esc_html__('General', 'iniya'),
  'icon'        => 'fa fa-gears',

  'fields'      => array(

	array(
	  'type'    => 'subheading',
	  'content' => esc_html__( 'General Options', 'iniya' ),
	),

	array(
	  'id'  	 => 'show-pagecomments',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Globally Show Page Comments', 'iniya'),
	  'info'	 => esc_html__('YES! to show comments on all the pages. This will globally override your "Allow comments" option under your page "Discussion" settings.', 'iniya'),
	  'default'  => true,
	),

	array(
	  'id'  	 => 'showall-pagination',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Show all pages in Pagination', 'iniya'),
	  'info'	 => esc_html__('YES! to show all the pages instead of dots near the current page.', 'iniya')
	),

	array(
	  'id'  	 => 'enable-stylepicker',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Style Picker', 'iniya'),
	  'info'	 => esc_html__('YES! to show the style picker.', 'iniya')
	),

	array(
	  'id'  	 => 'use-site-loader',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Site Loader', 'iniya'),
	  'info'	 => esc_html__('YES! to use site loader.', 'iniya')
	),

	array(
	  'id'      => 'google-map-key',
	  'type'    => 'text',
	  'title'   => esc_html__('Google Map API Key', 'iniya'),
	  'after' 	=> '<p class="cs-text-info">'.esc_html__('Put a valid google account api key here', 'iniya').'</p>',
	),

	array(
	  'id'      => 'mailchimp-key',
	  'type'    => 'text',
	  'title'   => esc_html__('Mailchimp API Key', 'iniya'),
	  'after' 	=> '<p class="cs-text-info">'.esc_html__('Put a valid mailchimp account api key here', 'iniya').'</p>',
	),

  ),
);

$options[]      = array(
  'name'        => 'layout_options',
  'title'       => esc_html__('Layout Options', 'iniya'),
  'icon'        => 'dashicons dashicons-exerpt-view',
  'sections' => array(

	// -----------------------------------------
	// Header Options
	// -----------------------------------------
	array(
	  'name'      => 'breadcrumb_options',
	  'title'     => esc_html__('Breadcrumb Options', 'iniya'),
	  'icon'      => 'fa fa-sitemap',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Breadcrumb Options", 'iniya' ),
		  ),

		  array(
			'id'  		 => 'show-breadcrumb',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Breadcrumb', 'iniya'),
			'info'		 => esc_html__('YES! to display breadcrumb for all pages.', 'iniya'),
			'default' 	 => true,
		  ),

		  array(
			'id'           => 'breadcrumb-delimiter',
			'type'         => 'select',
			'title'        => esc_html__('Breadcrumb Delimiter', 'iniya'),
			'options'      => array(
			  'fa default' 					=> esc_html__('Default', 'iniya'),
			  'fa fa-angle-double-right'    => esc_html__('Double Angle Right', 'iniya'),
			  'fa fa-sort'  				=> esc_html__('Sort', 'iniya'),
			  'fa fa-arrow-circle-right'    => esc_html__('Arrow Circle Right', 'iniya'),
			  'fa fa-angle-right'     		=> esc_html__('Angle Right', 'iniya'),
			  'fa fa-caret-right'  			=> esc_html__('Caret Right', 'iniya'),
			  'fa fa-arrow-right'  			=> esc_html__('Arrow Right', 'iniya'),
			  'fa fa-chevron-right'  		=> esc_html__('Chevron Right', 'iniya'),
			  'fa fa-hand-o-right'  		=> esc_html__('Hand Right', 'iniya'),
			  'fa fa-plus'  				=> esc_html__('Plus', 'iniya'),
			  'fa fa-remove'  				=> esc_html__('Remove', 'iniya'),
			  'fa fa-glass'  				=> esc_html__('Glass', 'iniya'),
			),
			'class'        => 'chosen',
			'default'      => 'fa default',
			'info'         => esc_html__('Choose delimiter style to display on breadcrumb section.', 'iniya'),
		  ),

		  array(
			'id'           => 'breadcrumb-style',
			'type'         => 'select',
			'title'        => esc_html__('Breadcrumb Style', 'iniya'),
			'options'      => array(
			  'default' 							=> esc_html__('Default', 'iniya'),
			  'aligncenter'    						=> esc_html__('Align Center', 'iniya'),
			  'alignright'  						=> esc_html__('Align Right', 'iniya'),
			  'breadcrumb-left'    					=> esc_html__('Left Side Breadcrumb', 'iniya'),
			  'breadcrumb-right'     				=> esc_html__('Right Side Breadcrumb', 'iniya'),
			  'breadcrumb-top-right-title-center'  	=> esc_html__('Top Right Title Center', 'iniya'),
			  'breadcrumb-top-left-title-center'  	=> esc_html__('Top Left Title Center', 'iniya'),
			),
			'class'        => 'chosen',
			'default'      => 'default',
			'info'         => esc_html__('Choose alignment style to display on breadcrumb section.', 'iniya'),
		  ),

		  array(
			'id'    => 'breadcrumb_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'iniya'),
			'desc'  => esc_html__('Choose background options for breadcrumb title section.', 'iniya')
		  ),

		),
	),

  ),
);

$options[]      = array(
  'name'        => 'allpage_options',
  'title'       => esc_html__('All Page Options', 'iniya'),
  'icon'        => 'fa fa-files-o',
  'sections' => array(

	// -----------------------------------------
	// Post Options
	// -----------------------------------------
	array(
	  'name'      => 'post_options',
	  'title'     => esc_html__('Post Options', 'iniya'),
	  'icon'      => 'fa fa-file',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Single Post Options", 'iniya' ),
		  ),
		
		  array(
			'id'  		 => 'single-post-authorbox',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Author Box', 'iniya'),
			'info'		 => esc_html__('YES! to display author box in single blog posts.', 'iniya')
		  ),

		  array(
			'id'  		 => 'single-post-related',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Related Posts', 'iniya'),
			'info'		 => esc_html__('YES! to display related blog posts in single posts.', 'iniya')
		  ),

		  array(
			'id'  		 => 'single-post-navigation',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Post Navigation', 'iniya'),
			'info'		 => esc_html__('YES! to display post navigation in single posts.', 'iniya')
		  ),

		  array(
			'id'  		 => 'single-post-comments',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Posts Comments', 'iniya'),
			'info'		 => esc_html__('YES! to display single blog post comments.', 'iniya'),
			'default' 	 => true,
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Post Archives Page Layout", 'iniya' ),
		  ),

		  array(
			'id'      	 => 'post-archives-page-layout',
			'type'       => 'image_select',
			'title'      => esc_html__('Page Layout', 'iniya'),
			'options'    => array(
			  'content-full-width'   => INIYA_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => INIYA_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => INIYA_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => INIYA_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'post-archives-page-layout',
			),
		  ),

		  array(
			'id'  		 => 'show-standard-left-sidebar-for-post-archives',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Standard Left Sidebar', 'iniya'),
			'dependency' => array( 'post-archives-page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  		 => 'show-standard-right-sidebar-for-post-archives',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Standard Right Sidebar', 'iniya'),
			'dependency' => array( 'post-archives-page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Post Archives Post Layout", 'iniya' ),
		  ),

		  array(
			'id'      	   => 'post-archives-post-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Post Layout', 'iniya'),
			'options'      => array(
			  'one-column' 		  => INIYA_THEME_URI . '/cs-framework-override/images/one-column.png',
			  'one-half-column'   => INIYA_THEME_URI . '/cs-framework-override/images/one-half-column.png',
			  'one-third-column'  => INIYA_THEME_URI . '/cs-framework-override/images/one-third-column.png',
			  '1-2-2'			  => INIYA_THEME_URI . '/cs-framework-override/images/1-2-2.png',
			  '1-2-2-1-2-2' 	  => INIYA_THEME_URI . '/cs-framework-override/images/1-2-2-1-2-2.png',
			  '1-3-3-3'			  => INIYA_THEME_URI . '/cs-framework-override/images/1-3-3-3.png',
			  '1-3-3-3-1' 		  => INIYA_THEME_URI . '/cs-framework-override/images/1-3-3-3-1.png',
			),
			'default'      => 'one-half-column',
		  ),

		  array(
			'id'           => 'post-style',
			'type'         => 'select',
			'title'        => esc_html__('Post Style', 'iniya'),
			'options'      => array(
			  'blog-default-style' 		=> esc_html__('Default', 'iniya'),
			  'entry-date-left'      	=> esc_html__('Date Left', 'iniya'),
			  'entry-date-author-left'  => esc_html__('Date and Author Left', 'iniya'),
			  'blog-medium-style'       => esc_html__('Medium', 'iniya'),
			  'blog-medium-style dt-blog-medium-highlight'     					 => esc_html__('Medium Hightlight', 'iniya'),
			  'blog-medium-style dt-blog-medium-highlight dt-sc-skin-highlight'  => esc_html__('Medium Skin Highlight', 'iniya'),
			),
			'class'        => 'chosen',
			'default'      => 'blog-default-style',
			'info'         => esc_html__('Choose post style to display post archives pages.', 'iniya'),
		  ),

		  array(
			'id'  		 => 'post-archives-enable-excerpt',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Allow Excerpt', 'iniya'),
			'info'		 => esc_html__('YES! to allow excerpt', 'iniya'),
			'default'    => true,
		  ),

		  array(
			'id'  		 => 'post-archives-excerpt',
			'type'  	 => 'number',
			'title' 	 => esc_html__('Excerpt Length', 'iniya'),
			'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Put Excerpt Length', 'iniya').'</span>',
			'default' 	 => 40,
		  ),

		  array(
			'id'  		 => 'post-archives-enable-readmore',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Read More', 'iniya'),
			'info'		 => esc_html__('YES! to enable read more button', 'iniya'),
			'default'	 => true,
		  ),

		  array(
			'id'  		 => 'post-archives-readmore',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Read More Shortcode', 'iniya'),
			'info'		 => esc_html__('Paste any button shortcode here', 'iniya'),
			'default'	 => '[dt_sc_button title="Read More" class="no-btn-bg"]',
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Single Post & Post Archive options", 'iniya' ),
		  ),

		  array(
			'id'      => 'post-format-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Post Format Meta', 'iniya' ),
			'info'	  => esc_html__('YES! to show post format meta information', 'iniya'),
			'default' => false
		  ),

		  array(
			'id'      => 'post-author-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Author Meta', 'iniya' ),
			'info'	  => esc_html__('YES! to show post author meta information', 'iniya'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-date-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Date Meta', 'iniya' ),
			'info'	  => esc_html__('YES! to show post date meta information', 'iniya'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-comment-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Comment Meta', 'iniya' ),
			'info'	  => esc_html__('YES! to show post comment meta information', 'iniya'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-category-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Category Meta', 'iniya' ),
			'info'	  => esc_html__('YES! to show post category information', 'iniya'),
			'default' => false
		  ),

		  array(
			'id'      => 'post-tag-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Tag Meta', 'iniya' ),
			'info'	  => esc_html__('YES! to show post tag information', 'iniya'),
			'default' => false
		  ),
		  array(
			'id'      => 'post-likes',
			'type'    => 'switcher',
			'title'   => esc_html__('Post Likes', 'iniya' ),
			'info'	  => esc_html__('YES! to show post likes information', 'iniya'),
			'default' => true
		  ),
		  array(
			'id'      => 'post-views',
			'type'    => 'switcher',
			'title'   => esc_html__('Post Views', 'iniya' ),
			'info'	  => esc_html__('YES! to show post views information', 'iniya'),
			'default' => true
		  ),

		),
	),

	// -----------------------------------------
	// 404 Options
	// -----------------------------------------
	array(
	  'name'      => '404_options',
	  'title'     => esc_html__('404 Options', 'iniya'),
	  'icon'      => 'fa fa-warning',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "404 Message", 'iniya' ),
		  ),
		  
		  array(
			'id'      => 'enable-404message',
			'type'    => 'switcher',
			'title'   => esc_html__('Enable Message', 'iniya' ),
			'info'	  => esc_html__('YES! to enable not-found page message.', 'iniya'),
			'default' => true
		  ),

		  array(
			'id'           => 'notfound-style',
			'type'         => 'select',
			'title'        => esc_html__('Template Style', 'iniya'),
			'options'      => array(
			  'type1' 	   => esc_html__('Modern', 'iniya'),
			  'type2'      => esc_html__('Classic', 'iniya'),
			  'type4'  	   => esc_html__('Diamond', 'iniya'),
			  'type5'      => esc_html__('Shadow', 'iniya'),
			  'type6'      => esc_html__('Diamond Alt', 'iniya'),
			  'type7'  	   => esc_html__('Stack', 'iniya'),
			  'type8'  	   => esc_html__('Minimal', 'iniya'),
			),
			'class'        => 'chosen',
			'default'      => 'type1',
			'info'         => esc_html__('Choose the style of not-found template page.', 'iniya')
		  ),

		  array(
			'id'      => 'notfound-darkbg',
			'type'    => 'switcher',
			'title'   => esc_html__('404 Dark BG', 'iniya' ),
			'info'	  => esc_html__('YES! to use dark bg notfound page for this site.', 'iniya')
		  ),

		  array(
			'id'           => 'notfound-pageid',
			'type'         => 'select',
			'title'        => esc_html__('Custom Page', 'iniya'),
			'options'      => 'pages',
			'class'        => 'chosen',
			'default_option' => esc_html__('Choose the page', 'iniya'),
			'info'       	 => esc_html__('Choose the page for not-found content.', 'iniya')
		  ),
		  
		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Background Options", 'iniya' ),
		  ),

		  array(
			'id'    => 'notfound_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'iniya')
		  ),

		  array(
			'id'  		 => 'notfound-bg-style',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Custom Styles', 'iniya'),
			'info'		 => esc_html__('Paste custom CSS styles for not found page.', 'iniya')
		  ),

		),
	),

	// -----------------------------------------
	// Underconstruction Options
	// -----------------------------------------
	array(
	  'name'      => 'comingsoon_options',
	  'title'     => esc_html__('Under Construction Options', 'iniya'),
	  'icon'      => 'fa fa-thumbs-down',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Under Construction", 'iniya' ),
		  ),
	
		  array(
			'id'      => 'enable-comingsoon',
			'type'    => 'switcher',
			'title'   => esc_html__('Enable Coming Soon', 'iniya' ),
			'info'	  => esc_html__('YES! to check under construction page of your website.', 'iniya')
		  ),
	
		  array(
			'id'           => 'comingsoon-style',
			'type'         => 'select',
			'title'        => esc_html__('Template Style', 'iniya'),
			'options'      => array(
			  'type1' 	   => esc_html__('Diamond', 'iniya'),
			  'type2'      => esc_html__('Teaser', 'iniya'),
			  'type3'  	   => esc_html__('Minimal', 'iniya'),
			  'type4'      => esc_html__('Counter Only', 'iniya'),
			  'type5'      => esc_html__('Belt', 'iniya'),
			  'type6'  	   => esc_html__('Classic', 'iniya'),
			  'type7'  	   => esc_html__('Boxed', 'iniya')
			),
			'class'        => 'chosen',
			'default'      => 'type1',
			'info'         => esc_html__('Choose the style of coming soon template.', 'iniya'),
		  ),

		  array(
			'id'      => 'uc-darkbg',
			'type'    => 'switcher',
			'title'   => esc_html__('Coming Soon Dark BG', 'iniya' ),
			'info'	  => esc_html__('YES! to use dark bg coming soon page for this site.', 'iniya')
		  ),

		  array(
			'id'           => 'comingsoon-pageid',
			'type'         => 'select',
			'title'        => esc_html__('Custom Page', 'iniya'),
			'options'      => 'pages',
			'class'        => 'chosen',
			'default_option' => esc_html__('Choose the page', 'iniya'),
			'info'       	 => esc_html__('Choose the page for comingsoon content.', 'iniya')
		  ),

		  array(
			'id'      => 'show-launchdate',
			'type'    => 'switcher',
			'title'   => esc_html__('Show Launch Date', 'iniya' ),
			'info'	  => esc_html__('YES! to show launch date text.', 'iniya'),
		  ),

		  array(
			'id'      => 'comingsoon-launchdate',
			'type'    => 'text',
			'title'   => esc_html__('Launch Date', 'iniya'),
			'attributes' => array( 
			  'placeholder' => '10/30/2016 12:00:00'
			),
			'after' 	=> '<p class="cs-text-info">'.esc_html__('Put Format: 12/30/2016 12:00:00 month/day/year hour:minute:second', 'iniya').'</p>',
		  ),

		  array(
			'id'           => 'comingsoon-timezone',
			'type'         => 'select',
			'title'        => esc_html__('UTC Timezone', 'iniya'),
			'options'      => array(
			  '-12' => '-12', '-11' => '-11', '-10' => '-10', '-9' => '-9', '-8' => '-8', '-7' => '-7', '-6' => '-6', '-5' => '-5', 
			  '-4' => '-4', '-3' => '-3', '-2' => '-2', '-1' => '-1', '0' => '0', '+1' => '+1', '+2' => '+2', '+3' => '+3', '+4' => '+4',
			  '+5' => '+5', '+6' => '+6', '+7' => '+7', '+8' => '+8', '+9' => '+9', '+10' => '+10', '+11' => '+11', '+12' => '+12'
			),
			'class'        => 'chosen',
			'default'      => '0',
			'info'         => esc_html__('Choose utc timezone, by default UTC:00:00', 'iniya'),
		  ),

		  array(
			'id'    => 'comingsoon_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'iniya')
		  ),

		  array(
			'id'  		 => 'comingsoon-bg-style',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Custom Styles', 'iniya'),
			'info'		 => esc_html__('Paste custom CSS styles for under construction page.', 'iniya'),
		  ),

		),
	),

  ),
);

// -----------------------------------------
// Widget area Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'widgetarea_options',
  'title'       => esc_html__('Widget Area', 'iniya'),
  'icon'        => 'fa fa-trello',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Custom Widget Area for Sidebar", 'iniya' ),
	  ),

	  array(
		'id'           => 'wtitle-style',
		'type'         => 'select',
		'title'        => esc_html__('Sidebar widget Title Style', 'iniya'),
		'options'      => array(
		  'type1' 	   => esc_html__('Double Border', 'iniya'),
		  'type2'      => esc_html__('Tooltip', 'iniya'),
		  'type3'  	   => esc_html__('Title Top Border', 'iniya'),
		  'type4'      => esc_html__('Left Border & Pattren', 'iniya'),
		  'type5'      => esc_html__('Bottom Border', 'iniya'),
		  'type6'  	   => esc_html__('Tooltip Border', 'iniya'),
		  'type7'  	   => esc_html__('Boxed Modern', 'iniya'),
		  'type8'  	   => esc_html__('Elegant Border', 'iniya'),
		  'type9' 	   => esc_html__('Needle', 'iniya'),
		  'type10' 	   => esc_html__('Ribbon', 'iniya'),
		  'type11' 	   => esc_html__('Content Background', 'iniya'),
		  'type12' 	   => esc_html__('Classic BG', 'iniya'),
		  'type13' 	   => esc_html__('Tiny Boders', 'iniya'),
		  'type14' 	   => esc_html__('BG & Border', 'iniya'),
		  'type15' 	   => esc_html__('Classic BG Alt', 'iniya'),
		  'type16' 	   => esc_html__('Left Border & BG', 'iniya'),
		  'type17' 	   => esc_html__('Basic', 'iniya'),
		  'type18' 	   => esc_html__('BG & Pattern', 'iniya'),
		),
		'class'          => 'chosen',
		'default_option' => esc_html__('Choose any type', 'iniya'),
		'info'           => esc_html__('Choose the style of sidebar widget title.', 'iniya')
	  ),

	  array(
		'id'              => 'widgetarea-custom',
		'type'            => 'group',
		'title'           => esc_html__('Custom Widget Area', 'iniya'),
		'button_title'    => esc_html__('Add New', 'iniya'),
		'accordion_title' => esc_html__('Add New Widget Area', 'iniya'),
		'fields'          => array(

		  array(
			'id'          => 'widgetarea-custom-name',
			'type'        => 'text',
			'title'       => esc_html__('Name', 'iniya'),
		  ),

		)
	  ),

	),
);

// -----------------------------------------
// Woocommerce Options
// -----------------------------------------
if( function_exists( 'is_woocommerce' ) && ! class_exists ( 'DTWooPlugin' ) ){

	$options[]      = array(
	  'name'        => 'woocommerce_options',
	  'title'       => esc_html__('Woocommerce', 'iniya'),
	  'icon'        => 'fa fa-shopping-cart',

	  'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Woocommerce Shop Page Options", 'iniya' ),
		  ),

		  array(
			'id'  		 => 'shop-product-per-page',
			'type'  	 => 'number',
			'title' 	 => esc_html__('Products Per Page', 'iniya'),
			'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Number of products to show in catalog / shop page', 'iniya').'</span>',
			'default' 	 => 12,
		  ),

		  array(
			'id'           => 'product-style',
			'type'         => 'select',
			'title'        => esc_html__('Product Style', 'iniya'),
			'options'      => array(
			  'woo-type1' 	   => esc_html__('Thick Border', 'iniya'),
			  'woo-type4'      => esc_html__('Diamond Icons', 'iniya'),
			  'woo-type8' 	   => esc_html__('Modern', 'iniya'),
			  'woo-type10' 	   => esc_html__('Easing', 'iniya'),
			  'woo-type11' 	   => esc_html__('Boxed', 'iniya'),
			  'woo-type12' 	   => esc_html__('Easing Alt', 'iniya'),
			  'woo-type13' 	   => esc_html__('Parallel', 'iniya'),
			  'woo-type14' 	   => esc_html__('Pointer', 'iniya'),
			  'woo-type16' 	   => esc_html__('Stack', 'iniya'),
			  'woo-type17' 	   => esc_html__('Bouncy', 'iniya'),
			  'woo-type20' 	   => esc_html__('Masked Circle', 'iniya'),
			  'woo-type21' 	   => esc_html__('Classic', 'iniya')
			),
			'class'        => 'chosen',
			'default' 	   => 'woo-type1',
			'info'         => esc_html__('Choose products style to display shop & archive pages.', 'iniya')
		  ),

		  array(
			'id'      	 => 'shop-page-product-layout',
			'type'       => 'image_select',
			'title'      => esc_html__('Product Layout', 'iniya'),
			'options'    => array(
				  1   => INIYA_THEME_URI . '/cs-framework-override/images/one-column.png',
				  2   => INIYA_THEME_URI . '/cs-framework-override/images/one-half-column.png',
				  3   => INIYA_THEME_URI . '/cs-framework-override/images/one-third-column.png',
				  4   => INIYA_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
			),
			'default'      => 4,
			'attributes'   => array(
			  'data-depend-id' => 'shop-page-product-layout',
			),
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Detail Page Options", 'iniya' ),
		  ),

		  array(
			'id'      	   => 'product-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'iniya'),
			'options'      => array(
			  'content-full-width'   => INIYA_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => INIYA_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => INIYA_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => INIYA_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'iniya'),
			'dependency'   	 => array( 'product-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'iniya'),
			'dependency' 	 => array( 'product-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  		 	 => 'enable-related',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Related Products', 'iniya'),
			'info'	  		 => esc_html__("YES! to display related products on single product's page.", 'iniya')
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Category Page Options", 'iniya' ),
		  ),

		  array(
			'id'      	   => 'product-category-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'iniya'),
			'options'      => array(
			  'content-full-width'   => INIYA_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => INIYA_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => INIYA_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => INIYA_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-category-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-category-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'iniya'),
			'dependency'   	 => array( 'product-category-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-category-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'iniya'),
			'dependency' 	 => array( 'product-category-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),
		  
		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Tag Page Options", 'iniya' ),
		  ),

		  array(
			'id'      	   => 'product-tag-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'iniya'),
			'options'      => array(
			  'content-full-width'   => INIYA_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => INIYA_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => INIYA_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => INIYA_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-tag-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-tag-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'iniya'),
			'dependency'   	 => array( 'product-tag-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-tag-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'iniya'),
			'dependency' 	 => array( 'product-tag-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

	  ),
	);
}

// -----------------------------------------
// Sociable Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'sociable_options',
  'title'       => esc_html__('Sociable', 'iniya'),
  'icon'        => 'fa fa-share-alt-square',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Sociable", 'iniya' ),
	  ),

	  array(
		'id'              => 'sociable_fields',
		'type'            => 'group',
		'title'           => esc_html__('Sociable', 'iniya'),
		'info'            => esc_html__('Click button to add type of social & url.', 'iniya'),
		'button_title'    => esc_html__('Add New Social', 'iniya'),
		'accordion_title' => esc_html__('Adding New Social Field', 'iniya'),
		'fields'          => array(
		  array(
			'id'          => 'sociable_fields_type',
			'type'        => 'select',
			'title'       => esc_html__('Select Social', 'iniya'),
			'options'      => array(
			  'delicious' 	 => esc_html__('Delicious', 'iniya'),
			  'deviantart' 	 => esc_html__('Deviantart', 'iniya'),
			  'digg' 	  	 => esc_html__('Digg', 'iniya'),
			  'dribbble' 	 => esc_html__('Dribbble', 'iniya'),
			  'envelope' 	 => esc_html__('Envelope', 'iniya'),
			  'facebook' 	 => esc_html__('Facebook', 'iniya'),
			  'flickr' 		 => esc_html__('Flickr', 'iniya'),
			  'google-plus'  => esc_html__('Google Plus', 'iniya'),
			  'gtalk'  		 => esc_html__('GTalk', 'iniya'),
			  'instagram'	 => esc_html__('Instagram', 'iniya'),
			  'lastfm'	 	 => esc_html__('Lastfm', 'iniya'),
			  'linkedin'	 => esc_html__('Linkedin', 'iniya'),
			  'myspace'		 => esc_html__('Myspace', 'iniya'),
			  'picasa'		 => esc_html__('Picasa', 'iniya'),
			  'pinterest'	 => esc_html__('Pinterest', 'iniya'),
			  'reddit'		 => esc_html__('Reddit', 'iniya'),
			  'rss'		 	 => esc_html__('RSS', 'iniya'),
			  'skype'		 => esc_html__('Skype', 'iniya'),
			  'stumbleupon'	 => esc_html__('Stumbleupon', 'iniya'),
			  'technorati'	 => esc_html__('Technorati', 'iniya'),
			  'tumblr'		 => esc_html__('Tumblr', 'iniya'),
			  'twitter'		 => esc_html__('Twitter', 'iniya'),
			  'viadeo'		 => esc_html__('Viadeo', 'iniya'),
			  'vimeo'		 => esc_html__('Vimeo', 'iniya'),
			  'yahoo'		 => esc_html__('Yahoo', 'iniya'),
			  'youtube'		 => esc_html__('Youtube', 'iniya'),
			),
			'class'        => 'chosen',
			'default'      => 'delicious',
		  ),

		  array(
			'id'          => 'sociable_fields_url',
			'type'        => 'text',
			'title'       => esc_html__('Enter URL', 'iniya')
		  ),
		)
	  ),

   ),
);

// -----------------------------------------
// Hook Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'hook_options',
  'title'       => esc_html__('Hooks', 'iniya'),
  'icon'        => 'fa fa-paperclip',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Top Hook", 'iniya' ),
	  ),

	  array(
		'id'  	=> 'enable-top-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Top Hook', 'iniya'),
		'info'	=> esc_html__("YES! to enable top hook.", 'iniya')
	  ),

	  array(
		'id'  		 => 'top-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Top Hook', 'iniya'),
		'info'		 => esc_html__('Paste your top hook, Executes after the opening &lt;body&gt; tag.', 'iniya')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Content Before Hook", 'iniya' ),
	  ),

	  array(
		'id'  	=> 'enable-content-before-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Content Before Hook', 'iniya'),
		'info'	=> esc_html__("YES! to enable content before hook.", 'iniya')
	  ),

	  array(
		'id'  		 => 'content-before-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Content Before Hook', 'iniya'),
		'info'		 => esc_html__('Paste your content before hook, Executes before the opening &lt;#primary&gt; tag.', 'iniya')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Content After Hook", 'iniya' ),
	  ),

	  array(
		'id'  	=> 'enable-content-after-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Content After Hook', 'iniya'),
		'info'	=> esc_html__("YES! to enable content after hook.", 'iniya')
	  ),

	  array(
		'id'  		 => 'content-after-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Content After Hook', 'iniya'),
		'info'		 => esc_html__('Paste your content after hook, Executes after the closing &lt;/#main&gt; tag.', 'iniya')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Bottom Hook", 'iniya' ),
	  ),

	  array(
		'id'  	=> 'enable-bottom-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Bottom Hook', 'iniya'),
		'info'	=> esc_html__("YES! to enable bottom hook.", 'iniya')
	  ),

	  array(
		'id'  		 => 'bottom-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Bottom Hook', 'iniya'),
		'info'		 => esc_html__('Paste your bottom hook, Executes after the closing &lt;/body&gt; tag.', 'iniya')
	  ),

   array(
		'id'  	=> 'enable-analytics-code',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Tracking Code', 'iniya'),
		'info'	=> esc_html__("YES! to enable site tracking code.", 'iniya')
	  ),

	  array(
		'id'  		 => 'analytics-code',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Google Analytics Tracking Code', 'iniya'),
		'info'		 => esc_html__('Enter your Google tracking id (UA-XXXXX-X) here. If you want to offer your visitors the option to stop being tracked you can place the shortcode [dt_sc_privacy_google_tracking] somewhere on your site', 'iniya')
	  ),

   ),
);

// ------------------------------
// backup                       
// ------------------------------
$options[]   = array(
  'name'     => 'backup_section',
  'title'    => esc_html__('Backup', 'iniya'),
  'icon'     => 'fa fa-shield',
  'fields'   => array(

    array(
      'type'    => 'notice',
      'class'   => 'warning',
      'content' => esc_html__('You can save your current options. Download a Backup and Import.', 'iniya')
    ),

    array(
      'type'    => 'backup',
    ),

  )
);

// ------------------------------
// license
// ------------------------------
$options[]   = array(
  'name'     => 'theme_version',
  'title'    => constant('INIYA_THEME_NAME').esc_html__(' Log', 'iniya'),
  'icon'     => 'fa fa-info-circle',
  'fields'   => array(

    array(
      'type'    => 'heading',
      'content' => constant('INIYA_THEME_NAME').esc_html__(' Theme Change Log', 'iniya')
    ),
    array(
      'type'    => 'content',
	  'content' => '<pre>

2020.02.01 - version 1.9

* Compatible with wordpress 5.3.2
* Updated: All premium plugins
* Updated: All wordpress theme standards
* Updated: Privacy and Cookies concept
* Updated: Gutenberg editor support for custom post types

* Fixed: Google Analytics issue
* Fixed: Mailchimp email client issue
* Fixed: Privacy Button Issue
* Fixed: Gutenberg check for old wordpress version

* Improved: Tags taxonomy added for portfolio
* Improved: Single product breadcrumb section
* Improved: Revisions options added for all custom posts

2019.11.20 - version 1.8

* Updated WordPress Standards
* Gutenberg Update
* Other Minor Fixes
	  

2019.10.25 - version 1.7
* Compatible with wordpress 5.2.4
* Updated: All WordPress Standards for WP Requirements Compliant
* Updated: Gutenberg editor support fixes	  

2019.08.22 - version 1.6
 * Compatible with wordpress 5.2.2
 * Updated: All premium plugins
 * Updated: Revisions added to all custom post types
 * Updated: Gutenberg editor support for custom post types
 * Updated: Link for phone number module
 * Updated: Online documentation link, check readme file

 * Fixed: Google Analytics issue
 * Fixed: Mailchimp email client issue
 * Fixed: Gutenberg check for old wordpress version
 * Fixed: Edit with Visual Composer for portfolio
 * Fixed: Site title color
 * Fixed: Privacy popup bg color 

 * Improved: Single product breadcrumb section
 * Improved: Tags taxonomy added for portfolio
 * Improved: Woocommerce cart module added with custom class option

 * New: Whatsapp Shortcode

2019.05.22 - version 1.5
 * Gutenberg Latest update compatible
 * Portfolio Video option
 * Coming Soon page fix
 * GDPR product single page fix
 * Codestar framework update
 * wpml xml file updated
 * disable options for likes and views in single post page
 * Updated latest version of all third party plugins
 * Image Caption module link
 * RTL Design Fix
 * Some design tweaks
       
2019.01.19 - version 1.4
 * Gutenberg compatible
 * Latest WordPress version 5.0.3 compatible
 * Updated latest version of all third party plugins
 * Some design tweaks
       
2018.11.10 - version 1.3
 * Gutenberg plugin compatible
 * Latest wordpress version 4.9.8 compatible
 * Updated latest version of all third party plugins
 * Updated documentation

2018.08.04 - version 1.2
 * GDPR Compliant update in comment form, mailchimp form etc.
 * Compatible with wordpress 4.9.8
 * Google Map Api Key Fix
 * Fix - Bulk plugins install issue
 * Fix - Iphone sidebar issue
 * Fix - Add target attribute for social media
 * Fix - Option for change the site title color
 * Fix - Unyson Page Bilder Conflict
 * Fix - Nav Menu Role Plugin compatible.
 * Fix - Added the smile fonts folder
 * Fix - Woocommerce custom sidebar issue
 * Updated language files
 * Updated all third party plugins
 * Updated designthemes core features plugin
 * Fix - Buddypress issue
 * Fix - youtube and vimeo video issue in https
 * Fix - Twitter feeds links issue

2017.12.01 - version 1.1
 * Optimized Dummy Content Updated

2017.10.07 - version 1.0
 * First release! </pre>',
    ),

  )
);

// ------------------------------
// Seperator
// ------------------------------
$options[] = array(
  'name'   => 'seperator_1',
  'title'  => esc_html__('Plugin Options', 'iniya'),
  'icon'   => 'fa fa-plug'
);


CSFramework::instance( $settings, $options );
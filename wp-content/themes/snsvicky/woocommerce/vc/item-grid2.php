<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product, $woocommerce_loop;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div <?php post_class(); ?>>
	<div class="block-product-inner style2 grid-view product-inner">
		<div class="item-inner">
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );
	?>
			<div class="item-img clearfix">
				<div class="item-img-info">
					<a class="product-image" href="<?php the_permalink(); ?>">
	<?php
	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked snsvicky_product_thumbnail - 11
	 */
	
	do_action( 'woocommerce_before_shop_loop_item_title' );
	?>
					</a>
				</div>
			</div>
			<div class="item-info">
				<div class="item-content">
					<h3 class="item-title">
					 	<a href="<?php the_permalink(); ?>">
					 		<?php the_title(); ?>
					 	</a>
					</h3>
	<?php
	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	// do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	add_action('woocommerce_after_shop_loop_item_title_grid2', 'woocommerce_template_loop_price', 5);
	do_action( 'woocommerce_after_shop_loop_item_title_grid2' );
	?>
				</div>
				<div class="item-box-hover"><div class="item-box-hover-wrap">
	<?php
	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
				</div></div>
			</div>
		</div>
	</div>
</div>

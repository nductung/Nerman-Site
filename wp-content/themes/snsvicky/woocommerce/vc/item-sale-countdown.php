<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product, $woocommerce_loop;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div <?php post_class(); ?>>
	<div class="block-product-inner">
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );
	?>
			<div class="item-img">
					<a class="product-image" href="<?php the_permalink(); ?>">
	<?php
	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	// do_action( 'woocommerce_before_shop_loop_item_title' );
						echo woocommerce_get_product_thumbnail('shop_single');
	?>
					</a>
			</div>
			<div class="item-info">
				<div class="item-content">
					<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<div class="cat-links">', '</div>' ); ?>
					<h3 class="item-title">
					 	<a href="<?php the_permalink(); ?>">
					 		<?php the_title(); ?>
					 	</a>
					</h3>
	<?php
	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	// do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * snsvicky_after_shop_loop_item_title_scd hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	add_action( 'snsvicky_after_shop_loop_item_title_scd', 'woocommerce_template_loop_rating', 5 ); 
	add_action( 'snsvicky_after_shop_loop_item_title_scd', 'woocommerce_template_loop_price', 6 ); 
	add_action( 'snsvicky_after_shop_loop_item_title_scd', 'woocommerce_template_single_excerpt', 10 );
	add_action( 'snsvicky_after_shop_loop_item_title_scd', 'snsvicy_scd_sold_bar', 15 );
	add_action( 'snsvicky_after_shop_loop_item_title_scd', 'snsvicy_scd_time_countdown', 20 );
	do_action( 'snsvicky_after_shop_loop_item_title_scd' );

	?>
	
				</div>
			</div>
	</div>
</div>

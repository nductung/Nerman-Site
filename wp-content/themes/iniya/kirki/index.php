<?php

require_once get_template_directory() . '/kirki/kirki-utils.php';
require_once get_template_directory() . '/kirki/include-kirki.php';
require_once get_template_directory() . '/kirki/kirki.php';

$config = iniya_kirki_config();

add_action('customize_register', 'iniya_customize_register');
function iniya_customize_register( $wp_customize ) {

	$wp_customize->remove_section( 'colors' );
	$wp_customize->remove_section( 'header_image' );
	$wp_customize->remove_section( 'background_image' );
	$wp_customize->remove_section( 'static_front_page' );

	$wp_customize->remove_section('themes');
	$wp_customize->get_section('title_tagline')->priority = 10;
}

add_action( 'customize_controls_print_styles', 'iniya_enqueue_customizer_stylesheet' );
function iniya_enqueue_customizer_stylesheet() {
	wp_register_style( 'iniya-customizer-css', INIYA_THEME_URI.'/kirki/assets/css/customizer.css', NULL, NULL, 'all' );
	wp_enqueue_style( 'iniya-customizer-css' );	
}

add_action( 'customize_controls_print_footer_scripts', 'iniya_enqueue_customizer_script' );
function iniya_enqueue_customizer_script() {
	wp_register_script( 'iniya-customizer-js', INIYA_THEME_URI.'/kirki/assets/js/customizer.js', array('jquery', 'customize-controls' ), false, true );
	wp_enqueue_script( 'iniya-customizer-js' );
}

# Theme Customizer Begins
INIYA_Kirki::add_config( $config , array(
	'capability'    => 'edit_theme_options',
	'option_type'   => 'theme_mod',
) );

	# Site Identity	
		# use-custom-logo
		INIYA_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'use-custom-logo',
			'label'    => esc_html__( 'Logo ?', 'iniya' ),
			'section'  => 'title_tagline',
			'priority' => 1,
			'default'  => iniya_defaults('use-custom-logo'),
			'description' => esc_html__('Switch to Site title or Logo','iniya'),
			'choices'  => array(
				'on'  => esc_attr__( 'Logo', 'iniya' ),
				'off' => esc_attr__( 'Site Title', 'iniya' )
			)			
		) );

		# custom-logo
		INIYA_Kirki::add_field( $config, array(
			'type' => 'image',
			'settings' => 'custom-logo',
			'label'    => esc_html__( 'Logo', 'iniya' ),
			'section'  => 'title_tagline',
			'priority' => 2,
			'default' => iniya_defaults( 'custom-logo' ),
			'active_callback' => array(
				array( 'setting' => 'use-custom-logo', 'operator' => '==', 'value' => '1' )
			)
		));

		# custom-dark-logo
		INIYA_Kirki::add_field( $config, array(
			'type' => 'image',
			'settings' => 'custom-dark-logo',
			'label'    => esc_html__( 'Dark Logo', 'iniya' ),
			'section'  => 'title_tagline',
			'priority' => 3,
			'default' => iniya_defaults( 'custom-dark-logo' ),
			'active_callback' => array(
				array( 'setting' => 'use-custom-logo', 'operator' => '==', 'value' => '1' )
			)
		));	
		
		# site-title-color
		INIYA_Kirki::add_field( $config, array(
			'type' => 'color',
			'settings' => 'custom-title',
			'label'    => esc_html__( 'Site Title Color', 'iniya' ),
			'section'  => 'title_tagline',
			'priority' => 4,
			'active_callback' => array(
				array( 'setting' => 'use-custom-logo', 'operator' => '!=', 'value' => '1' )
			),
			'output' => array(
				array( 'element' => '#header .logo-title > h1 a, #header .logo-title h2 a' , 'property' => 'color' )
			),
			'choices' => array( 'alpha' => true ),
		));	

	# Site Layout
	require_once get_template_directory() . '/kirki/options/site-layout.php';

	# Site Skin
	require_once get_template_directory() . '/kirki/options/site-skin.php';

	# Site Breadcrumb
	INIYA_Kirki::add_panel( 'dt_site_breadcrumb_panel', array(
		'title' => esc_html__( 'Site Breadcrumb', 'iniya' ),
		'description' => esc_html__('Site Breadcrumb','iniya'),
		'priority' => 25
	) );
	require_once get_template_directory() . '/kirki/options/site-breadcrumb.php';
	
	# Site Header
	INIYA_Kirki::add_panel( 'dt_site_header_panel', array(
		'title' => esc_html__( 'Site Header', 'iniya' ),
		'description' => esc_html__('Site Header','iniya'),
		'priority' => 30
	) );
	require_once get_template_directory() . '/kirki/options/site-header.php';

	# Site Menu
	INIYA_Kirki::add_panel( 'dt_site_menu_panel', array(
		'title' => esc_html__( 'Site Menu', 'iniya' ),
		'description' => esc_html__('Site Menu','iniya'),
		'priority' => 35
	) );
	require_once get_template_directory() . '/kirki/options/site-menu/navigation.php';		

	# Site Footer I
		INIYA_Kirki::add_panel( 'dt_site_footer_i_panel', array(
			'title' => esc_html__( 'Site Footer I', 'iniya' ),
			'priority' => 40
		) );
		require_once get_template_directory() . '/kirki/options/site-footer-i.php';

	# Site Footer II
	INIYA_Kirki::add_panel( 'dt_site_footer_ii_panel', array(
		'title' => esc_html__( 'Site Footer II', 'iniya' ),
		'priority' => 45
	) );
	#require_once get_template_directory() . '/kirki/options/site-footer-ii.php';

	# Site Footer Copyright
	INIYA_Kirki::add_panel( 'dt_footer_copyright_panel', array(
		'title' => esc_html__( 'Site Copyright', 'iniya' ),
		'priority' => 50
	) );
	require_once get_template_directory() . '/kirki/options/site-footer-copyright.php';

	# Additional JS
	require_once get_template_directory() . '/kirki/options/custom-js.php';

	# Typography
	INIYA_Kirki::add_panel( 'dt_site_typography_panel', array(
		'title' => esc_html__( 'Typography', 'iniya' ),
		'description' => esc_html__('Typography Settings','iniya'),
		'priority' => 220
	) );	
	require_once get_template_directory() . '/kirki/options/site-typography.php';	
# Theme Customizer Ends
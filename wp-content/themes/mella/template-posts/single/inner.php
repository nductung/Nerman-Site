<?php
$post_format = get_post_format();
global $post;
$gallery = mella_post_gallery( get_the_content(), array( 'size' => 'full' ) );
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
    <div class="top-info <?php echo ( has_post_thumbnail() || !empty($gallery) ) ? 'has-thumb':''; ?>">
        <?php if ( $post_format == 'gallery' ) { ?>
            <?php echo trim($gallery); ?>
        <?php } elseif( $post_format == 'link' ) {
                $format = mella_post_format_link_helper( get_the_content(), get_the_title() );
                $title = $format['title'];
                $link = mella_get_link_attributes( $title );
                $thumb = mella_post_thumbnail('', $link);
                echo trim($thumb);
            } else { ?>
                <?php
                    $thumb = mella_post_thumbnail();
                    echo trim($thumb);
                ?>
        <?php } ?>
        <?php if( mella_get_config('show_blog_social_share', false) ) {
            get_template_part( 'template-parts/sharebox' );
        } ?>
    </div>
    <div class="post-grid no-margin">
        <div class="bottom-info">
            <div class="categories"><?php echo esc_html__('in ','mella') ?><?php mella_post_categories($post); ?></div> 
            <div class="date-post">
                <span class="suffix"><?php echo esc_html__('on','mella') ?></span>
                <a href="<?php the_permalink(); ?>"><?php the_time( get_option('date_format', 'd M, Y') ); ?></a>
            </div>
        </div>
    </div>
	<div class="entry-content-detail">
    	<div class="single-info info-bottom">
            <div class="entry-description">
                <?php
                    if ( $post_format == 'gallery' ) {
                        $gallery_filter = mella_gallery_from_content( get_the_content() );
                        echo trim($gallery_filter['filtered_content']);
                    } else {
                        the_content();
                    }
                ?>
            </div><!-- /entry-content -->
    		<?php
    		wp_link_pages( array(
    			'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'mella' ) . '</span>',
    			'after'       => '</div>',
    			'link_before' => '<span>',
    			'link_after'  => '</span>',
    			'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'mella' ) . ' </span>%',
    			'separator'   => '',
    		) );
    		?>
            <?php mella_post_tags(); ?>
    	</div>
    </div>
</article>
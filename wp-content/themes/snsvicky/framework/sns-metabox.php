<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 *
 * You also should read the changelog to know what has been changed before updating.
 *
 * For more information, please visit:
 * @link http://metabox.io/docs/registering-meta-boxes/
 */
add_filter( 'rwmb_meta_boxes', 'snsvicky_register_meta_boxes' );
/**
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @param array $meta_boxes List of meta boxes
 *
 * @return array
 */
function snsvicky_register_meta_boxes( $meta_boxes ){
	/**
	 * prefix of meta keys (optional)
	 * Use underscore (_) at the beginning to make keys hidden
	 * Alt.: You also can make prefix empty to disable it
	 */
	// Better has an underscore as last sign
	wp_enqueue_script('sns-imgselect', SNSVICKY_THEME_URI . '/framework/meta-box/sns-metabox.js', array('jquery'), '', true);
	wp_enqueue_style('fonts-awesome', SNSVICKY_THEME_URI . '/assets/fonts/awesome/css/font-awesome.min.css'); 
	$prefix = 'snsvicky_';
	global $wpdb, $snsvicky_opt;
	$revsliders =array();
	$revsliders[0] = esc_html__("Don't use", "snsvicky");
	if ( ! function_exists( 'is_plugin_active' ) ) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
	if (is_plugin_active('revslider/revslider.php')) {
		$query = $wpdb->prepare("
			SELECT * 
			FROM {$wpdb->prefix}revslider_sliders 
			ORDER BY %s"
			, "ASC");
	    $get_sliders = $wpdb->get_results($query);
	    if($get_sliders) {
		    foreach($get_sliders as $slider) {
			   $revsliders[$slider->alias] = $slider->title;
		   }
	    }
	}
	//var_dump($get_sliders);
	//
	$default_layout = 'l-m';
	if ( isset($snsvicky_opt['blog_layout']) ) $default_layout = $snsvicky_opt['blog_layout'];
	//
	$siderbars = array();
	foreach ($GLOBALS['wp_registered_sidebars'] as $sidebars) {
		$siderbars[ $sidebars['id']] = $sidebars['name'];
	}
	// Layout config
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'sns_productcfg',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => esc_html__( 'Product Config', 'snsvicky' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'product' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',
		// Auto save: true, false (default). Optional.
		// 'autosave'   => true,
		// List of meta fields

		'fields'     => array(
			array(
				'name'    => esc_html__( 'Gallery Thumbnail Type', 'snsvicky' ),
				'id'       => "{$prefix}woo_gallery_type",
				'type'     => 'select',
				'std'  => '',
				'options'  => array(
					''  => esc_html__( 'Default', 'snsvicky' ),
					'h'  => esc_html__( 'Horizontal', 'snsvicky' ),
					'v'  => esc_html__( 'Vertical', 'snsvicky' ),
					'n'  => esc_html__( 'None - and sticky content', 'snsvicky' ),
				)
			),
		 	array(
				'name'    => esc_html__( 'Product Sidebar', 'snsvicky' ),
				'id'       => "{$prefix}single_product_sidebar",
				'type'     => 'select',
				'std'  => '',
				'options'  => array(
					''  => esc_html__( 'Default', 'snsvicky' ),
					0  => esc_html__( 'No', 'snsvicky' ),
					1  => esc_html__( 'Yes', 'snsvicky' ),
				),
				'desc'		=> esc_html__('Product page with Sidebar. Select "Default" to use in Theme Options.', 'snsvicky'),
			),
			array(
				'name'    	=> esc_html__( 'Primary block three column', 'snsvicky' ),
				'id'       	=> "{$prefix}primary_block_three_col",
				'type'     	=> 'text',
				'std'  		=> 'product-inner-sidebar',
				'desc'		=> esc_html__('Enter slug of Post WCode(Use Post WCode to make content). Just applies if Produc Page have not Sidebar and Gallery Thumbnail Type is Horizontal', 'snsvicky'),
			),
		  	array(
				'name'    => esc_html__( 'Zoom Type for Cloud Zoom', 'snsvicky' ),
				'id'       => "{$prefix}woo_zoomtype",
				'type'     => 'select',
				'std'  => '',
				'options'  => array(
					''  => esc_html__( 'Default', 'snsvicky' ),
					'lens'  => esc_html__( 'Lens', 'snsvicky' ),
					'inner'  => esc_html__( 'Inner', 'snsvicky' ),
				),
				'desc'		=> '',
			),
			array(
				'id'    => "{$prefix}product_video",
				'name'  => esc_html__( 'Product Video', 'snsvicky' ),
				'type'  => 'oembed',
				// Allow to clone? Default is false
				'clone' => false,
				'desc'		  => esc_html__( 'Enter your video url(youtube, video)', 'snsvicky' ),
				// Input size
				'size'  => 50,
			),
			array(
				'name'    => esc_html__( 'Use Variation Thumbnail for Variable product', 'snsvicky' ),
				'id'       => "{$prefix}use_variation_thumb",
				'type'     => 'select',
				'std'  => 1,
				'options'  => array(
					1  => esc_html__( 'Yes', 'snsvicky' ),
					0  => esc_html__( 'No', 'snsvicky' ),
				),
				'desc'		=> esc_html__('Just applies for Variable Product', 'snsvicky'),
			),
		)
	);
	// Layout config
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'sns_layout',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => esc_html__( 'Layout Config', 'snsvicky' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'page' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',
		// Auto save: true, false (default). Optional.
		// 'autosave'   => true,
		// List of meta fields

		'fields'     => array(
			// Layout Type
			array(
				'name'        => esc_html__( 'Layout Type', 'snsvicky' ),
				'id'          => "{$prefix}layouttype",
				'type'        => 'layouttype',
				// Array of 'value' => 'Label' pairs for select box
				'options'     => array(
					'm' => esc_html__( 'Without Sidebar', 'snsvicky' ),
					'l-m' => esc_html__( 'Use Left Sidebar', 'snsvicky' ),
					'm-r' => esc_html__( 'Use Right Sidebar', 'snsvicky' ),
				),
				// Select multiple values, optional. Default is false.
				'multiple'    => false,
				'std'         => $default_layout,
				'placeholder' => esc_html__( '--- Select a layout type ---', 'snsvicky' ),
			),
			// Left Sidebar
			array(
				'name'  => esc_html__( 'Left Sidebar', 'snsvicky' ),
				'id'    => "{$prefix}leftsidebar",
				//'desc'  => esc_html__( 'Text description', 'snsvicky' ),
				'type'  => 'select',
				'options'	=> $siderbars,
				'multiple'	=> false,
				'std'		=> 'left-sidebar',
				'placeholder' => esc_html__( '--- Select a sidebar ---', 'snsvicky' ),
			),
			// Right Sidebar
			array(
				'name'  => esc_html__( 'Right Sidebar', 'snsvicky' ),
				'id'    => "{$prefix}rightsidebar",
				//'desc'  => esc_html__( 'Text description', 'snsvicky' ),
				'type'  => 'select',
				'options'	=> $siderbars,
				'multiple'	=> false,
				'std'		=> 'right-sidebar',
				'placeholder' => esc_html__( '--- Select a sidebar ---', 'snsvicky' ),
			),
			
		)
	);
	
	$menus = get_terms('nav_menu', array( 'hide_empty' => false ));
	$menu_options[''] = __('Default Menu...', 'snsvicky');
	foreach ( $menus as $menu ){
		$menu_options[$menu->term_id] = $menu->name;
	}
	
	// Page config
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'sns_pageconfig',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => esc_html__( 'Page Config', 'snsvicky' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array( 'page' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',
		// Auto save: true, false (default). Optional.
		// 'autosave'   => true,
		// List of meta fields

		'fields'     => array(
			array(
				'name'    => esc_html__( 'Want use custom logo?', 'snsvicky' ),
				'id'      => "{$prefix}header_logo",
				'type'    => 'image_advanced',
				'desc'		=> esc_html__('It priority more than Logon in theme option', 'snsvicky'),
			),

			array(
				'name'    => esc_html__( 'Header Style', 'snsvicky' ),
				'id'       => "{$prefix}header_style",
				'type'     => 'select',
				'std'  => '',
				'options'  => array(
					''   	  => esc_html__( 'Default', 'snsvicky' ),
					'style1'  => esc_html__( 'Style1', 'snsvicky' ),
					'style2'  => esc_html__( 'Style2', 'snsvicky' ),
					'style3'  => esc_html__( 'Style3', 'snsvicky' ),
					'style4'  => esc_html__( 'Style4', 'snsvicky' ),
					'style5'  => esc_html__( 'Style5', 'snsvicky' ),
				),
				'desc'		=> esc_html__('Select Header style. ', 'snsvicky'),
			),
			array(
				'name'    => esc_html__( 'Enable Search Category for Live Ajax Search', 'snsvicky' ),
				'id'       => "{$prefix}enable_search_cat",
				'type'     => 'select',
				'std'  => '',
				'options'  => array(
					''   	  => esc_html__( 'Default', 'snsvicky' ),
					true  => esc_html__( 'Yes', 'snsvicky' ),
					false  => esc_html__( 'No', 'snsvicky' ),
				),
			),
			array(
				'name'    => esc_html__( 'Use Slideshow', 'snsvicky' ),
				'id'      => "{$prefix}useslideshow",
				'type'    => 'radio',
				'options' => array(
					'1' => esc_html__( 'Yes', 'snsvicky' ),
					'2' => esc_html__( 'No', 'snsvicky' ),
				),
				'std'         => '2',
			),
			array(
				'name'    => esc_html__( 'Select Slideshow', 'snsvicky' ),
				'id'      => "{$prefix}revolutionslider",
				'type'    => 'select',
				'options' =>  $revsliders ,
				'std'         => '',
			),
			array(
				'name'    => esc_html__( 'Show Title', 'snsvicky' ),
				'id'      => "{$prefix}showtitle",
				'type'    => 'radio',
				'options' => array(
					'1' => esc_html__( 'Yes', 'snsvicky' ),
					'2' => esc_html__( 'No', 'snsvicky' ),
				),
				'std'         => '1',
			),
			array(
				'name'    => esc_html__( 'Show Breadcrumbs', 'snsvicky' ),
				'id'      => "{$prefix}showbreadcrump",
				'type'    => 'radio',
				'options' => array(
					'1' => esc_html__( 'Yes', 'snsvicky' ),
					'2' => esc_html__( 'No', 'snsvicky' ),
				),
				'std'         => '',
				'desc' => esc_html__( 'Dont apply for Front page.', 'snsvicky' ),
			),
			array(
				'name'    => esc_html__( 'Config Theme Color for this page?', 'snsvicky' ),
				'id'      => "{$prefix}page_themecolor",
				'type'    => 'radio',
				'options' => array(
					'1' => esc_html__( 'Yes', 'snsvicky' ),
					'2' => esc_html__( 'No', 'snsvicky' ),
				),
				'std'         => '2',
			),
			array(
				'name' => esc_html__( 'Sellect Theme Color', 'snsvicky' ),
				'id'   => "{$prefix}theme_color",
				'type' => 'color',
				'desc' => esc_html__( 'It will priority than Theme Color in Theme Option panel', 'snsvicky' ),
			),
			array(
				'name'    => esc_html__( 'Footer Style', 'snsvicky' ),
				'id'       => "{$prefix}footer_layout",
				'type'     => 'select',
				'std'  => '',
				'options'  => array(
					''  => esc_html__( 'Default', 'snsvicky' ),
					'style_1'  => esc_html__( 'Style 1', 'snsvicky' ),
					'style_2'  => esc_html__( 'Style 2', 'snsvicky' ),
					'style_3'  => esc_html__( 'Style 3', 'snsvicky' ),
					'style_4'  => esc_html__( 'Style 4', 'snsvicky' ),
					'style_5'  => esc_html__( 'Style 5', 'snsvicky' ),
				),
				'desc'		=> esc_html__('Select Footer layout. "Default" to use in Theme Options.', 'snsvicky'),
			),
			array(
				'id'      => "{$prefix}page_class",
				'name'    => esc_html__( 'Want to add class for page?', 'snsvicky' ),
				'type'    => 'text',
				'desc'		=> esc_html__('This class to use for some customize css for this page, Example class: .page-header-transparent', 'snsvicky'),
			),
		)
	);
	// Post format - Gallery
	$meta_boxes[] = array(
	    	'id' => 'sns-post-gallery',
		    'title' =>  esc_html__('Gallery Settings','snsvicky'),
	    	'description' => '',
    		'pages'      => array( 'post' ), // Post type
	    	'context'    => 'normal',
		    'priority'   => 'high',
	    	'fields' => array(
			     array(
			        'name'		=> 'Gallery Images',
			        'desc'	    => 'Upload Images for post Gallery ( Limit is 15 Images ).',
			        'type'      => 'image_advanced',
			        'id'	    => "{$prefix}post_gallery",
	         		'max_file_uploads' => 15 
	        	)
			)
	);
	// Post format - Video
    $meta_boxes[] = array(
		'id' => 'sns-post-video',
		'title' => esc_html__('Featured Video','snsvicky'),
		'description' => '',
		'pages'      => array( 'post' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'fields' => array( 
		    array(
				'id'    => "{$prefix}post_video",
				'name'  => esc_html__( 'Video', 'snsvicky' ),
				'type'  => 'oembed',
				// Allow to clone? Default is false
				'clone' => false,
				// Input size
				'size'  => 50,
			)
		)
	);
	// Post format - Audio
    $meta_boxes[] = array(
		'id' => 'sns-post-audio',
		'title' => esc_html__('Featured Audio','snsvicky'),
		'description' => '',
		'pages'      => array( 'post' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'fields' => array( 
		    array(
				'id'    => "{$prefix}post_audio",
				'name'  => esc_html__( 'Audio', 'snsvicky' ),
				'type'  => 'oembed',
				// Allow to clone? Default is false
				'clone' => false,
				// Input size
				'size'  => 50,
			)
		)
	);
	// Post format - quote
    $meta_boxes[] = array(
		'id' => 'sns-post-quote',
		'title' => esc_html__('Featured Quote','snsvicky'),
		'description' => '',
		'pages'      => array( 'post' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'fields' => array( 
		    array(
				'id'    => "{$prefix}post_quotecontent",
				'name'  => esc_html__( 'Quote Content', 'snsvicky' ),
				'type'  => 'textarea',
				// Allow to clone? Default is false
				'clone' => false,
			),
			array(
				'id'      => "{$prefix}post_quoteauthor",
				'name'    => esc_html__( 'Quote author', 'snsvicky' ),
				'type'    => 'text',
				'clone' => false,
			),
		)
	);
	// Post format - Link
    $meta_boxes[] = array(
		'id' => 'sns-post-link',
		'title' => esc_html__('Link Settings','snsvicky'),
		'description' => '',
		'pages'      => array( 'post' ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'fields' => array( 
		    array(
				'id'    => "{$prefix}post_linkurl",
				'name'  => esc_html__( 'Link URL', 'snsvicky' ),
				'type'  => 'text',
				// Allow to clone? Default is false
				'clone' => false,
			),
			array(
				'id'      => "{$prefix}post_linktitle",
				'name'    => esc_html__( 'Link Title', 'snsvicky' ),
				'type'    => 'text',
				'clone' => false,
			),
		)
	);
	// Brand config
	$meta_boxes[] = array(
		'id'         => 'sns_brandconfig',
		'title'      => esc_html__( 'Brand Config', 'snsvicky' ),
		'post_types' => array( 'brand' ),
		'context'    => 'normal',
		'priority'   => 'high',
		'fields'     => array(
			array(
				'name'    => esc_html__( 'Link for brand', 'snsvicky' ),
				'id'      => "{$prefix}brandlink",
				'type'    => 'text'
			),
		)
	);

	return $meta_boxes;
}


if ( class_exists( 'RWMB_Field' ) ) {
	class RWMB_Layouttype_Field extends RWMB_Select_Field {
		static function admin_enqueue_scripts(){
			wp_enqueue_style( 'sns-imgselect', SNSVICKY_THEME_URI . '/framework/meta-box/img-select.css' );
		}
	}
}

<?php $thumbsize = !isset($thumbsize) ? mella_get_config( 'blog_item_thumbsize', 'full' ) : $thumbsize;?>
<article <?php post_class('post post-grid post-list-item'); ?>>
    <?php
        $thumb = mella_display_post_thumb($thumbsize);
        echo trim($thumb);
    ?>
    <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
        <span class="post-sticky"><?php echo esc_html__('Featured','mella'); ?></span>
    <?php endif; ?>
    <div class="content">
        <?php if (get_the_title()) { ?>
            <h4 class="title">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h4>
        <?php } ?>
        <div class="bottom-info">
            <div class="categories"><?php echo esc_html__('in ','mella') ?><?php mella_post_categories($post); ?></div> 
            <div class="date-post">
                <span class="suffix"><?php echo esc_html__('on','mella') ?></span>
                <a href="<?php the_permalink(); ?>"><?php the_time( get_option('date_format', 'd M, Y') ); ?></a>
            </div>
        </div>
    </div>
    <?php if(has_excerpt()){?>
        <div class="description"><?php echo mella_substring( get_the_excerpt(), 50, '...' ); ?></div>
    <?php } else{ ?>
        <div class="description"><?php echo mella_substring( get_the_content(), 50, '...' ); ?></div>
    <?php } ?>
    <div class="more">
        <a class="btn-readmore" href="<?php the_permalink(); ?>"><?php echo esc_html__('continue','mella') ?></a>
    </div>
</article>
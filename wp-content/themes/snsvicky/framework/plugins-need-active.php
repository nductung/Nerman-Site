<?php
add_action( 'tgmpa_register', 'sns_plugin_activation' );
function sns_plugin_activation() {
    $plugins = array(
            // install Redux Framework, it on wordpress.org/plugins
            array(
                'name'      => esc_html__('Redux Framework', 'snsvicky'),
                'slug'      => 'redux-framework',
                'required'  => true,
            ),
            array(
                'name'               => esc_html__('Meta Box', 'snsvicky'),
                'slug'               => 'meta-box',
                'required'           => true,
            ),
            array(
                'name'                  => esc_html__('Slider Revolution', 'snsvicky'),
                'slug'                  => 'revslider',
                'source'                => 'revslider.zip',
                'required'              => true,
            ),
            array(
                'name'                  => esc_html__('WPBakery Visual Composer', 'snsvicky'),
                'slug'                  => 'js_composer',
                'source'                => 'js_composer.zip',
                'required'              => true,
            ),
            array(
                'name'                  => esc_html__('SNS VICKY Extra', 'snsvicky'),
                'slug'                  => 'snsvicky-extra',
                'source'                => 'snsvicky-extra.zip',
                'required'              => true,
            ),
            array(
                'name'                  => esc_html__('SNS VICKY Shortcodes', 'snsvicky'),
                'slug'                  => 'snsvicky-shortcodes',
                'source'                => 'snsvicky-shortcodes.zip',
                'required'              => true,
            ),
            array(
                'name'               => esc_html__('WooCommerce - excelling eCommerce', 'snsvicky'),
                'slug'               => 'woocommerce',
                'required'           => true,
            ),
            array(
                'name'               => esc_html__('YITH WooCommerce Wishlist', 'snsvicky'),
                'slug'               => 'yith-woocommerce-wishlist',
                'required'           => true,
            ),
            array(
                'name'               => esc_html__('YITH WooCommerce Quick View', 'snsvicky'),
                'slug'               => 'yith-woocommerce-quick-view',
                'required'           => true,
            ),
	    	array(
	    		'name'               => esc_html__('YITH WooCommerce Ajax Product Filter', 'snsvicky'),
	    		'slug'               => 'yith-woocommerce-ajax-navigation',
	    		'required'           => true,
	    	),
            array(
                'name'               => esc_html__('Newsletter', 'snsvicky'),
                'slug'               => 'newsletter',
                'required'           => true,
            ),
            array(
                'name'               => esc_html__('Contact Form 7', 'snsvicky'),
                'slug'               => 'contact-form-7',
                'required'           => true,
            ),
            array(
                'name'               => esc_html__('Instagram Feed', 'snsvicky'),
                'slug'               => 'instagram-feed',
                'required'           => true,
            ),
        );
  
    $config = array(
        'default_path' => esc_url('http://demo.snstheme.com/wp/resource/q2-2020/'),
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Is show notices or not?
        'dismissable'  => false,                   // If false then user cannot colose notices above.
        'is_automatic' => false,                    // If false thene plugin cannot auto active after install.
    );
    tgmpa( $plugins, $config );
}
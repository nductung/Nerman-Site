<?php
/**
 *  The template for displaying the shop header
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>
<div class="apus-shop-header">
    <div class="apus-shop-menu">
        <div class="row">
            <div class="col-xs-12">
                <div class="wrapper-filter clearfix">
                    <div class="show-number">
                        <?php do_action( 'mella_woo_number_products' ); ?>
                    </div>
                    <?php if ( mella_get_config('product_archive_top_categories')) { ?>
                        <div id="apus-categories-dropdown" class="apus-categories-dropdown apus-dropdown-custom">
                            <div class="dropdown-toggle category-dropdown-label" data-toggle="dropdown" aria-expanded="true" role="button">
                                <?php esc_html_e('Category: ', 'mella'); ?> <span></span>
                                <b class="caret"></b>
                            </div>
                            <div class="dropdown-menu">
                                <?php
                                    $args = array(
                                        'show_counts' => false,
                                        'hierarchical' => true
                                    );
                                    mella_wc_product_dropdown_categories($args);
                                ?>
                            </div>
                        </div>
                    <?php } ?>

                    <div id="apus-orderby-dropdown" class="apus-orderby-dropdown apus-dropdown-custom">
                        <div class="dropdown-toggle orderby-dropdown-label" data-toggle="dropdown" aria-expanded="true" role="button">
                            <?php esc_html_e('Order By: ', 'mella'); ?> <span><?php esc_html_e('Default', 'mella'); ?></span>
                            <b class="caret"></b>
                        </div>
                        <div class="dropdown-menu">
                            <?php
                                mella_woo_orderby();
                            ?>
                        </div>
                    </div>
                    <?php if ( mella_get_config('product_archive_top_filter')) { ?>
                        <?php if ( is_active_sidebar( 'shop-top-sidebar' ) ) { ?>
                            <span class="show-filter pull-right">
                                <?php esc_html_e('Filters','mella') ?><i class="fa fa-angle-down" aria-hidden="true"></i>
                            </span>
                        <?php } ?>
                    <?php } ?>

                    <div id="apus-display-mode" class="apus-display-mode">
                        <span><?php esc_html_e('View As: ', 'mella'); ?></span>
                        <?php
                            echo trim(mella_woocommerce_display_modes());
                        ?>
                    </div>
                </div>
                <?php if ( mella_get_config('product_archive_top_filter')) { ?>
                    <?php if ( is_active_sidebar( 'shop-top-sidebar' ) ) { ?>
                        <div class="shop-top-sidebar-wrapper">
                            <div class="shop-top-sidebar-wrapper-inner">
                                <?php dynamic_sidebar( 'shop-top-sidebar' ); ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
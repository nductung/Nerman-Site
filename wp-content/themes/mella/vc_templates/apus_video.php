<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>
<div class="widget-video">
    <div class="video-wrapper-inner">
        <div class="info">
            <?php $image_icon = wp_get_attachment_image_src($image_icon,'full'); ?>
            <?php if ( !empty($image_icon) && isset($image_icon[0]) ){ ?>
                <a class="popup-video" href="<?php echo esc_url_raw($video_link); ?>">
                    <img src="<?php echo esc_url_raw($image_icon[0]); ?>" alt="<?php esc_attr_e('Image', 'mella'); ?>">
                </a>
            <?php } else{ ?>
                <a class="popup-video" href="<?php echo esc_url_raw($video_link); ?>">
                    <span class="icon"><i class="fa fa-play" aria-hidden="true"></i></span>
                </a>
            <?php } ?>
        	<?php if ($title!=''): ?>
                <h3 class="widget-title">
                    <span><?php echo trim($title); ?></span>
                </h3>
            <?php endif; ?>
            <?php if ($des!=''): ?>
                <div class="des">
                    <?php echo trim($des); ?>
                </div>
            <?php endif; ?>
        </div>
    
    	<div class="video">
            <?php $img = wp_get_attachment_image_src($image,'full'); ?>
    		<?php if ( !empty($img) && isset($img[0]) ): ?>
                <img src="<?php echo esc_url_raw($img[0]); ?>" alt="<?php esc_attr_e('Image', 'mella'); ?>">
            <?php endif; ?>
    	</div>
	</div>
</div>
<?php
   $job = get_post_meta( get_the_ID(), 'apus_testimonial_job', true );
   $link = get_post_meta( get_the_ID(), 'apus_testimonial_link', true );
?>
<div class="testimonial-body">
  <?php if(has_post_thumbnail()){ ?>
    <div class="image">
      <?php the_post_thumbnail('widget'); ?>
    </div>
  <?php } ?>
  <div class="description">
      <?php the_excerpt(); ?> 
  </div>
  <?php if (!empty($link)) { ?>
    <h3 class="name-client"><a class="text-theme" href="<?php echo esc_url_raw($link); ?>"><?php the_title(); ?></a></h3>
  <?php } else { ?>
    <h3 class="name-client text-theme"><?php the_title(); ?></h3>
  <?php } ?>
  <div class="job text-theme"><?php echo sprintf(__('%s', 'mella'), $job); ?></div>
</div>
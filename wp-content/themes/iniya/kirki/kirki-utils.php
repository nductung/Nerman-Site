<?php
function iniya_kirki_config() {
	return 'iniya_kirki_config';
}

function iniya_defaults( $key = '' ) {
	$defaults = array();

	# site identify
	$defaults['use-custom-logo'] = '1';
	$defaults['custom-logo'] = INIYA_THEME_URI.'/images/logo.png';
	$defaults['custom-dark-logo'] = INIYA_THEME_URI.'/images/light-logo.png';
	$defaults['site_icon'] = INIYA_THEME_URI.'/images/favicon.ico';

	# site layout
	$defaults['site-layout'] = 'wide';

	# site skin
	$defaults['primary-color'] = '#c9a654';
	$defaults['secondary-color'] = '#f1c55f';
	$defaults['tertiary-color'] = '#b0831b';
	$defaults['body-content-color'] = '#000000';
	$defaults['body-a-color'] = '#c9a654';
	$defaults['body-a-hover-color'] = '#000000';

	# site breadcrumb
	$defaults['customize-breadcrumb-title-typo'] = '1';
	$defaults['breadcrumb-title-typo'] = array( 'font-family' => 'Josefin Sans',
		'variant' => '700',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '24px',
		'line-height' => '',
		'letter-spacing' => '0.5px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none' );
	$defaults['customize-breadcrumb-typo'] = '0';
	$defaults['breadcrumb-typo'] = array( 'font-family' => 'Josefin Sans',
		'variant' => 'regular',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '15px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#333333',
		'text-align' => 'unset',
		'text-transform' => 'none' );

	# site header
	$defaults['header-type'] = 'fullwidth-header header-align-left fullwidth-menu-header';
	$defaults['enable-sticy-nav'] = '1';
	$defaults['header-position'] = 'above slider';
	$defaults['header-transparency'] = 'default';
	$defaults['enable-header-darkbg'] = '0';
	$defaults['menu-search-icon'] = '0';
	$defaults['search-box-type'] = 'type2';
	$defaults['menu-cart-icon'] = '0';
	$defaults['enable-top-bar-content'] = '0';
	$defaults['extra-top-bar-content'] = '0';
	$defaults['enable-header-right-content'] = '1';
	$defaults['header-right-content'] = '';

	# site menu
	$defaults['menu-active-style'] =  'menu-with-splitter';
	$defaults['menu-hover-style'] =  '';

	# site footer
	$defaults['show-footer'] = '1';
	$defaults['footer-columns'] = '10';
	$defaults['enable-footer-darkbg'] = '1';
	$defaults['customize-footer-bg'] = '1';
	$defaults['footer-bg-color'] = '#111111';
	$defaults['customize-footer-title-typo'] = '1';
	$defaults['footer-title-typo'] = array( 'font-family' => 'Josefin Sans',
		'variant' => 'regular',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '20px',
		'line-height' => '36px',
		'letter-spacing' => '0',
		'color' => '#2B2B2B',
		'text-align' => 'left',
		'text-transform' => 'none' );
	$defaults['customize-footer-content-typo'] = '1';
	$defaults['footer-content-typo'] = array( 'font-family' => 'Josefin Sans',
		'variant' => 'regular',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '14px',
		'line-height' => '28px',
		'letter-spacing' => '0',
		'color' => '#333333',
		'text-align' => 'left',
		'text-transform' => 'none' );

	# site copyright
	$defaults['show-copyright-text'] = '1';
	$defaults['copyright-text'] = 'Copyright &copy; 2019. All rights reserved by, <a title="DesignThemes" href="http://themeforest.net/user/designthemes">DesignThemes</a>';
	$defaults['enable-copyright-darkbg'] = '0';
	$defaults['copyright-next'] = 'disable';	
	$defaults['customize-footer-copyright-bg'] = '1';
	$defaults['footer-copyright-bg-color'] = '#111111';
	$defaults['customize-footer-copyright-text-typo'] = '0';
	$defaults['customize-footer-menu-typo'] = '0';

	# site social
	$defaults['facebook'] = '#';
	$defaults['twitter'] = '#';
	$defaults['google-plus'] = '#';
	$defaults['instagram'] = '#';

	# site typography
	$defaults['customize-body-h1-typo'] = '1';
	$defaults['h1'] = array(
		'font-family' => 'Josefin Sans',
		'variant' => '400',
		'font-size' => '38px',
		'line-height' => '',
		'letter-spacing' => '1px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h2-typo'] = '1';
	$defaults['h2'] = array(
		'font-family' => 'Josefin Sans',
		'variant' => '400',
		'font-size' => '30px',
		'line-height' => '',
		'letter-spacing' => '0.5px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h3-typo'] = '1';
	$defaults['h3'] = array(
		'font-family' => 'Josefin Sans',
		'variant' => '400',
		'font-size' => '28px',
		'line-height' => '',
		'letter-spacing' => '0.5px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h4-typo'] = '1';
	$defaults['h4'] = array(
		'font-family' => 'Josefin Sans',
		'variant' => '400',
		'font-size' => '24px',
		'line-height' => '',
		'letter-spacing' => '0.5px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h5-typo'] = '1';
	$defaults['h5'] = array(
		'font-family' => 'Josefin Sans',
		'variant' => '400',
		'font-size' => '22px',
		'line-height' => '',
		'letter-spacing' => '0.5px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h6-typo'] = '1';
	$defaults['h6'] = array(
		'font-family' => 'Josefin Sans',
		'variant' => '400',
		'font-size' => '20px',
		'line-height' => '',
		'letter-spacing' => '0.5px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-menu-typo'] = '1';
	$defaults['menu-typo'] = array(
		'font-family' => 'Josefin Sans',
		'variant' => '400',
		'font-size' => '18px',
		'line-height' => '',
		'letter-spacing' => '0.5px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-content-typo'] = '1';
	$defaults['body-content-typo'] = array(
		'font-family' => 'Josefin Sans',
		'variant' => 'normal',
		'font-size' => '16px',
		'line-height' => '1.7',
		'letter-spacing' => '',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);

	$defaults['footer-content-a-color'] = '';
	$defaults['footer-content-a-hover-color'] = '';
	

	if( !empty( $key ) && array_key_exists( $key, $defaults) ) {
		return $defaults[$key];
	}

	return '';
}

function iniya_image_positions() {

	$positions = array( "top left" => esc_attr__('Top Left','iniya'),
		"top center"    => esc_attr__('Top Center','iniya'),
		"top right"     => esc_attr__('Top Right','iniya'),
		"center left"   => esc_attr__('Center Left','iniya'),
		"center center" => esc_attr__('Center Center','iniya'),
		"center right"  => esc_attr__('Center Right','iniya'),
		"bottom left"   => esc_attr__('Bottom Left','iniya'),
		"bottom center" => esc_attr__('Bottom Center','iniya'),
		"bottom right"  => esc_attr__('Bottom Right','iniya'),
	);

	return $positions;
}

function iniya_image_repeats() {

	$image_repeats = array( "repeat" => esc_attr__('Repeat','iniya'),
		"repeat-x"  => esc_attr__('Repeat in X-axis','iniya'),
		"repeat-y"  => esc_attr__('Repeat in Y-axis','iniya'),
		"no-repeat" => esc_attr__('No Repeat','iniya')
	);

	return $image_repeats;
}

function iniya_border_styles() {

	$image_repeats = array(
		"none"	 => esc_attr__('None','iniya'),
		"dotted" => esc_attr__('Dotted','iniya'),
		"dashed" => esc_attr__('Dashed','iniya'),
		"solid"	 => esc_attr__('Solid','iniya'),
		"double" => esc_attr__('Double','iniya'),
		"groove" => esc_attr__('Groove','iniya'),
		"ridge"	 => esc_attr__('Ridge','iniya'),
	);

	return $image_repeats;
}

function iniya_animations() {

	$animations = array(
		'' 					 => esc_html__('Default','iniya'),	
		"bigEntrance"        =>  esc_attr__("bigEntrance",'iniya'),
        "bounce"             =>  esc_attr__("bounce",'iniya'),
        "bounceIn"           =>  esc_attr__("bounceIn",'iniya'),
        "bounceInDown"       =>  esc_attr__("bounceInDown",'iniya'),
        "bounceInLeft"       =>  esc_attr__("bounceInLeft",'iniya'),
        "bounceInRight"      =>  esc_attr__("bounceInRight",'iniya'),
        "bounceInUp"         =>  esc_attr__("bounceInUp",'iniya'),
        "bounceOut"          =>  esc_attr__("bounceOut",'iniya'),
        "bounceOutDown"      =>  esc_attr__("bounceOutDown",'iniya'),
        "bounceOutLeft"      =>  esc_attr__("bounceOutLeft",'iniya'),
        "bounceOutRight"     =>  esc_attr__("bounceOutRight",'iniya'),
        "bounceOutUp"        =>  esc_attr__("bounceOutUp",'iniya'),
        "expandOpen"         =>  esc_attr__("expandOpen",'iniya'),
        "expandUp"           =>  esc_attr__("expandUp",'iniya'),
        "fadeIn"             =>  esc_attr__("fadeIn",'iniya'),
        "fadeInDown"         =>  esc_attr__("fadeInDown",'iniya'),
        "fadeInDownBig"      =>  esc_attr__("fadeInDownBig",'iniya'),
        "fadeInLeft"         =>  esc_attr__("fadeInLeft",'iniya'),
        "fadeInLeftBig"      =>  esc_attr__("fadeInLeftBig",'iniya'),
        "fadeInRight"        =>  esc_attr__("fadeInRight",'iniya'),
        "fadeInRightBig"     =>  esc_attr__("fadeInRightBig",'iniya'),
        "fadeInUp"           =>  esc_attr__("fadeInUp",'iniya'),
        "fadeInUpBig"        =>  esc_attr__("fadeInUpBig",'iniya'),
        "fadeOut"            =>  esc_attr__("fadeOut",'iniya'),
        "fadeOutDownBig"     =>  esc_attr__("fadeOutDownBig",'iniya'),
        "fadeOutLeft"        =>  esc_attr__("fadeOutLeft",'iniya'),
        "fadeOutLeftBig"     =>  esc_attr__("fadeOutLeftBig",'iniya'),
        "fadeOutRight"       =>  esc_attr__("fadeOutRight",'iniya'),
        "fadeOutUp"          =>  esc_attr__("fadeOutUp",'iniya'),
        "fadeOutUpBig"       =>  esc_attr__("fadeOutUpBig",'iniya'),
        "flash"              =>  esc_attr__("flash",'iniya'),
        "flip"               =>  esc_attr__("flip",'iniya'),
        "flipInX"            =>  esc_attr__("flipInX",'iniya'),
        "flipInY"            =>  esc_attr__("flipInY",'iniya'),
        "flipOutX"           =>  esc_attr__("flipOutX",'iniya'),
        "flipOutY"           =>  esc_attr__("flipOutY",'iniya'),
        "floating"           =>  esc_attr__("floating",'iniya'),
        "hatch"              =>  esc_attr__("hatch",'iniya'),
        "hinge"              =>  esc_attr__("hinge",'iniya'),
        "lightSpeedIn"       =>  esc_attr__("lightSpeedIn",'iniya'),
        "lightSpeedOut"      =>  esc_attr__("lightSpeedOut",'iniya'),
        "pullDown"           =>  esc_attr__("pullDown",'iniya'),
        "pullUp"             =>  esc_attr__("pullUp",'iniya'),
        "pulse"              =>  esc_attr__("pulse",'iniya'),
        "rollIn"             =>  esc_attr__("rollIn",'iniya'),
        "rollOut"            =>  esc_attr__("rollOut",'iniya'),
        "rotateIn"           =>  esc_attr__("rotateIn",'iniya'),
        "rotateInDownLeft"   =>  esc_attr__("rotateInDownLeft",'iniya'),
        "rotateInDownRight"  =>  esc_attr__("rotateInDownRight",'iniya'),
        "rotateInUpLeft"     =>  esc_attr__("rotateInUpLeft",'iniya'),
        "rotateInUpRight"    =>  esc_attr__("rotateInUpRight",'iniya'),
        "rotateOut"          =>  esc_attr__("rotateOut",'iniya'),
        "rotateOutDownRight" =>  esc_attr__("rotateOutDownRight",'iniya'),
        "rotateOutUpLeft"    =>  esc_attr__("rotateOutUpLeft",'iniya'),
        "rotateOutUpRight"   =>  esc_attr__("rotateOutUpRight",'iniya'),
        "shake"              =>  esc_attr__("shake",'iniya'),
        "slideDown"          =>  esc_attr__("slideDown",'iniya'),
        "slideExpandUp"      =>  esc_attr__("slideExpandUp",'iniya'),
        "slideLeft"          =>  esc_attr__("slideLeft",'iniya'),
        "slideRight"         =>  esc_attr__("slideRight",'iniya'),
        "slideUp"            =>  esc_attr__("slideUp",'iniya'),
        "stretchLeft"        =>  esc_attr__("stretchLeft",'iniya'),
        "stretchRight"       =>  esc_attr__("stretchRight",'iniya'),
        "swing"              =>  esc_attr__("swing",'iniya'),
        "tada"               =>  esc_attr__("tada",'iniya'),
        "tossing"            =>  esc_attr__("tossing",'iniya'),
        "wobble"             =>  esc_attr__("wobble",'iniya'),
        "fadeOutDown"        =>  esc_attr__("fadeOutDown",'iniya'),
        "fadeOutRightBig"    =>  esc_attr__("fadeOutRightBig",'iniya'),
        "rotateOutDownLeft"  =>  esc_attr__("rotateOutDownLeft",'iniya')
    );

	return $animations;
}
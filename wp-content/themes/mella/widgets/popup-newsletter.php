<div class="popupnewsletter hidden">
  <!-- Modal -->
  <button title="<?php echo esc_attr__('Close (Esc)', 'mella'); ?>" type="button" class="mfp-close apus-mfp-close">×</button>
  <div class="modal-content">
      <div class="row flex-middle">
        <div class="col-sm-6 hidden-xs <?php echo (isset($image) && $image)?'':' hidden'; ?>">
          <img src="<?php echo esc_attr( $image ); ?>" alt="<?php esc_attr_e('Image', 'mella'); ?>">
        </div>
        <div class="col-sm-6 col-xs-12 text-center">
          <div class="popupnewsletter-widget">
            <?php if( isset($logo['url']) && !empty($logo['url']) ): ?>
                <div class="avatar">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" >
                        <img src="<?php echo esc_url( $logo['url'] ); ?>" alt="<?php echo esc_attr(get_bloginfo( 'name' )); ?>">
                    </a>
                </div>
            <?php else: ?>
                <div class="avatar">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" >
                        <img src="<?php echo esc_url_raw( get_template_directory_uri().'/images/logo.png'); ?>" alt="<?php echo esc_attr(get_bloginfo( 'name' )); ?>">
                    </a>
                </div>
            <?php endif; ?>

            <?php if(!empty($title)){ ?>
                <h3>
                  <?php echo esc_html( $title ); ?>
                </h3>
            <?php } ?>
            
            <?php if(!empty($description)){ ?>
                <div class="description">
                    <?php echo trim( $description ); ?>
                </div>
            <?php } ?>      
            <?php mc4wp_show_form(''); ?>

            <a href="javascript:void(0)" class="close-dont-show"><?php esc_html_e('Don\'t show this popup again', 'mella'); ?></a>
          </div>
        </div>
      </div>
  </div>
</div>
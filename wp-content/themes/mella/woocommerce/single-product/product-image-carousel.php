<?php

global $post, $product;

$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$attachment_ids = $product->get_gallery_image_ids();

$nb_columns = 3;

$count_thumbs = (!empty($attachment_ids) && has_post_thumbnail()) ? count($attachment_ids) + 1 : 1;

?>
<div class="apus-woocommerce-product-gallery-wrapper">
    <?php mella_product_video(); ?>

	<div class="slick-carousel apus-woocommerce-product-gallery apus-product-gallery-slick" data-carousel="slick" data-items="1" data-smallmedium="1" data-extrasmall="1" data-pagination="false" data-nav="false" data-slickparent="true">
		<?php
		
		if ( has_post_thumbnail() ) {
			$html  = wc_get_gallery_image_html( $post_thumbnail_id, true );
		} else {
			$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
			$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_attr__( 'Awaiting product image', 'mella' ) );
			$html .= '</div>';
		}

		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );

		do_action( 'woocommerce_product_thumbnails' );
		?>
	</div>
</div>
<?php if ( $attachment_ids && has_post_thumbnail() ) { ?>
	<div class="wrapper-thumbs <?php echo esc_attr($count_thumbs <= $nb_columns ? 'no-padding' : ''); ?>">
		<div class="slick-carousel apus-woocommerce-product-gallery-thumbs apus-product-gallery-slick" data-carousel="slick" data-items="<?php echo esc_attr($nb_columns); ?>" data-smallmedium="<?php echo esc_attr($nb_columns); ?>" data-extrasmall="<?php echo esc_attr($nb_columns); ?>" data-smallest="<?php echo esc_attr($nb_columns); ?>" data-pagination="false" data-nav="true" data-asnavfor=".apus-woocommerce-product-gallery" data-slidestoscroll="1" data-focusonselect="true">
			<?php

			if ( has_post_thumbnail() ) {
				$html  = '<div class="woocommerce-product-gallery__image"><div class="thumbs-inner">';
				$html .= get_the_post_thumbnail( $post->ID, 'woocommerce_gallery_thumbnail' );
				$html .= '</div></div>';
			} else {
				$html  = '<div class="woocommerce-product-gallery__image--placeholder"><div class="thumbs-inner">';
				$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src() ), esc_attr__( 'Awaiting product image', 'mella' ) );
				$html .= '</div></div>';
			}

			echo apply_filters( 'mella_woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );

			
			foreach ( $attachment_ids as $attachment_id ) {
				$html  = '<div class="woocommerce-product-gallery__image"><div class="thumbs-inner">';
				$html .= wp_get_attachment_image( $attachment_id, 'woocommerce_gallery_thumbnail', false );
		 		$html .= '</div></div>';

				echo apply_filters( 'mella_woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
			}

			?>
		</div>
	</div>
<?php } ?>
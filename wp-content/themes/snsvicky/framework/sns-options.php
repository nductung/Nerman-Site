<?php
    if ( ! class_exists( 'Redux' ) ) {
        return;
    }
    // This is your option name where all the Redux data is stored.
    $opt_name = "snsvicky_themeoptions";
    $theme = wp_get_theme(); // For use with some settings. Not necessary.
    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'            => esc_html__( 'SNS Vicky', 'snsvicky' ),
        'page_title'            => esc_html__( 'SNS Vicky', 'snsvicky' ),
        
        'dev_mode'             => false,
        'show_options_object'   => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        // OPTIONAL -> Give you extra features
        'page_priority'        => 50,
        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    Redux::setArgs( $opt_name, $args );

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'snsvicky' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'snsvicky' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'snsvicky' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'snsvicky' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'snsvicky' );
    Redux::setHelpSidebar( $opt_name, $content );

    // Import Demo Content
    $desc = ''; $i_message = '';
    if ( ! function_exists( 'is_plugin_active' ) ) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    if( !is_plugin_active('wordpress-importer/wordpress-importer.php') ){
        $i_message .= '<li><i class="fa fa-angle-double-right"></i> Need install and active plugin <a href="'.esc_url("https://wordpress.org/plugins/wordpress-importer/").'" target="_blank">WordPress Importer</a></li>';
    }
    if( !defined('WP_LOAD_IMPORTERS') ){
        $i_message .= "<li><i class='fa fa-angle-double-right'></i> Need add <code>define('WP_LOAD_IMPORTERS', true);</code> to file wp-config.php</li>";
    }
    if ( $i_message !='' ){
        $subtitle = '
            <p><label><i class="fa fa-exclamation-circle"></i>  Please follow the check list bellow to enable import function:</label></p>
            <ul class="i_message">'.$i_message.'</ul>
        ';
    }else{
        $subtitle = '<input type=\'button\' class=\'button\' name=\'btn_sampledata\' id=\'btn_sampledata\' value=\'Import\' />';
        $subtitle .= '
            <div class=\'sns-importprocess\'>
                <div  class=\'sns-importprocess-width\'></div>
            </div>
            <span id=\'sns-importmsg\'><span class=\'status\'></span></span>
            <div id="sns-import-tablecontent">
                <label>List contents will import:</label>
                <ul>
                  <li class="theme-cfg"><i class="fa fa-hand-pointer-o"></i>Theme config</li>
                  <li class="revslider-cfg"><i class="fa fa-hand-pointer-o"></i>Revolution Slider config</li>
                  <li class="all-content"><i class="fa fa-hand-pointer-o"></i>All contents</li>
                  <li class="widget-cfg"><i class="fa fa-hand-pointer-o"></i>Widget config</li>
                </ul>
            </div>
        ';
    }
    Redux::setSection( $opt_name, array(
        'icon' => 'el-icon-briefcase',
        'title' => esc_html__('Demo content', 'snsvicky'),
        'fields' => array(
            array(
                'title' => '',
                'subtitle' => $subtitle,
                'desc'  => $desc,
                'id' => 'theme_data',
                'icon' => true,
                'type' => 'image_select',
                'default' => 'sns_vicky',
                'options' => array(
                    'sns_logo' => get_template_directory_uri().'/assets/img/logo.png',
                ),
            )
        )
    ) );
    // General
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__( 'General', 'snsvicky' ),
        'icon'      => 'el-icon-cog',
        'id'               => 'general',
        'customizer_width' => '400px'
    ) );
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Color, Layout', 'snsvicky' ),
        'id'               => 'general-layout',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(
            array(
                'id'       => 'theme_color',
                'type'     => 'color',
                'output'   => array( '.site-title' ),
                'title'    => esc_html__( 'Theme Color', 'snsvicky' ),
                'default'  => '#ffb197',
                'transparent'   => false
            ),
            array(
                'id'       => 'use_boxedlayout',
                'type'     => 'switch',
                'title'    => esc_html__( 'Use Boxed Layout', 'snsvicky' ),
                'default'  => false,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'body_bg',
                'type'     => 'background',
                'output'   => array( 'body' ),
                'title'    => esc_html__( 'Body Background', 'snsvicky' ),
                'background-image' => false,
                //'default'  => '#ffffff',
                'preview'   => false,
            ),
            array(
                'id'       => 'body_bg_type',
                'type'     => 'select',
                'title'    => esc_html__( 'Body Background Image', 'snsvicky' ),
                'options'  => array(
                    'none'      => esc_html__( 'No image', 'snsvicky' ),
                    'img'       => esc_html__( 'Image', 'snsvicky' ),
                ),
                'default'  => 'img',
                'select2'  => array( 'allowClear' => false ),
                'required' => array( 'use_boxedlayout', '=', '1' )
            ),
            array(
                'id'        => 'body_bg_type_img',
                'type'      => 'media',
                'title'     => esc_html__( 'Body Background type img', 'snsvicky' ),
                'default'  => '',
                'title'    => 'Body Background type img',
                'required' => array( 'body_bg_type', '=', array( 'img' ) ),
            ),
            
        )
    ));
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Fonts', 'snsvicky' ),
        'id'               => 'general-font',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(
            array(
                'id'          => 'body_font',
                'type'        => 'typography',
                'title'       => esc_html__( 'Body font', 'snsvicky' ),
                'line-height'   => false,
                'text-align'   => false,
                'color'         => true,
                'all_styles'  => true,
                'units'       => 'px',
                // 'subsets'       => true,
                'default'     => array(
                    'font-size'   => '13px',
                    'font-family' => 'Montserrat',
                    'font-weight' => '400',
                    'color'       => '#666666'
                ),
            ),
        )
    ));
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Breadcrumbs', 'snsvicky' ),
        'id'               => 'general-breadcrumb',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(
            array(
                'id'       => 'showbreadcrump',
                'type'     => 'switch',
                'title'    => 'Show Breadcrumbs',
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
        )
    ));
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__( 'Header', 'snsvicky' ),
        'icon'      => 'el el-brush',
        'fields'    => array(
            array(
                'id'       => 'header_style',
                'type' => 'select',
                'title'    => esc_html__( 'Header Style', 'snsvicky' ),
                'default'  => 'style1',
                'options' => array(
                    'style1'        => esc_html__( 'Style1', 'snsvicky'),
                    'style2'        => esc_html__( 'Style2', 'snsvicky'),
                    'style3'        => esc_html__( 'Style3', 'snsvicky'),
                    'style4'        => esc_html__( 'Style4', 'snsvicky'),
                    'style5'        => esc_html__( 'Style5', 'snsvicky'),
                ),
                'desc'      => esc_html__( 'Select Header Style', 'snsvicky' ),
            ),
            array(
                'id'       => 'enable_search_cat',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Search Category for Live Ajax Search', 'snsvicky' ),
                'default'  => false,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'search_title_only',
                'type'     => 'switch',
                'title'    => esc_html__( 'Search by Title only', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'promo_status',
                'type'     => 'switch',
                'title'    => esc_html__( 'Always active promotion bar', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'        => 'header_logo',
                'type'      => 'media',
                'default'   => '',
                'title'     => esc_html__( 'Logo', 'snsvicky' ),
                'subtitle' => esc_html__( 'If this is not selected, This theme will be display logo with "theme/snsvicky/img/logo.png"', 'snsvicky' ),
                'desc'      => esc_html__( 'Image that you want to use as logo', 'snsvicky' ),
            ),
            array(
                'id'       => 'use_stickmenu',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Sticky Menu', 'snsvicky' ),
                'subtitle' => esc_html__( 'Keep menu on top when scroll down/up', 'snsvicky' ),
                'default'  => false,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
        )
    ));
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__( 'Footer', 'snsvicky' ),
        'icon'      => 'el el-link',
        'fields'    => array(
            array(
                'title' => esc_html__( 'Style', 'snsvicky'),
                'id' => 'footer_layout',
                'type'  => 'select',
                'multiselect' => false,
                'options' => array(
                    'style_1'      => esc_html__( 'Style 1', 'snsvicky'),
                    'style_2'      => esc_html__( 'Style 2', 'snsvicky'),
                    'style_3'      => esc_html__( 'Style 3', 'snsvicky'),
                    'style_4'      => esc_html__( 'Style 4', 'snsvicky'),
                    'style_5'      => esc_html__( 'Style 5', 'snsvicky'),
                ),
                'default'  => 'style_1'
            ), 
        )
    ));
    // Blog
    $siderbars = array(
        'widget-area' => esc_html__( 'Main Sidebar', 'snsvicky' ),
        //'home-sidebar' => esc_html__( 'Home Sidebar', 'snsvicky' ),
        'product-sidebar' => esc_html__( 'Product Sidebar', 'snsvicky' ),
        'blog-sidebar' => esc_html__( 'Blog Sidebar', 'snsvicky' ),
        'woo-sidebar' => esc_html__( 'Woo Sidebar', 'snsvicky' ),
    );
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__( 'Blog', 'snsvicky' ),
        'icon'      => 'el el-file-edit',
        'id'               => 'blog',
        'customizer_width' => '400px'
    ) );
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Blog Pages', 'snsvicky' ),
        'id'               => 'blog-page',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(
            array(
                'id'       => 'layouttype',
                'type'     => 'image_select',
                'title'    => esc_html__('Default Blog Layout', 'snsvicky'), 
                'options'  => array(
                    'm'      => array(
                        'alt'   => esc_html__( 'Without Sidebar', 'snsvicky' ), 
                        'img'   => SNSVICKY_THEME_URI.'/assets/img/admin/m.jpg'
                    ),
                    'l-m'      => array(
                        'alt'   => esc_html__( 'Use Left Sidebar', 'snsvicky' ), 
                        'img'   => SNSVICKY_THEME_URI.'/assets/img/admin/l-m.jpg'
                    ),
                    'm-r'      => array(
                        'alt'  => esc_html__( 'Use Right Sidebar', 'snsvicky' ), 
                        'img'  => SNSVICKY_THEME_URI.'/assets/img/admin/m-r.jpg'
                    ),
                ),
                'default' => 'l-m'
            ),
            // Left Sidebar
            array(
                'title'  => esc_html__( 'Left Sidebar', 'snsvicky' ),
                'id'    => "leftsidebar",
                'type'  => 'select',
                'options'   => $siderbars,
                'multiselect'   => false,
                'required' => array( 'layouttype', '=', array( 'l-m', 'l-m-r' ) )
            ),
            // Right Sidebar
            array(
                'title'  => esc_html__( 'Right Sidebar', 'snsvicky' ),
                'id'    => "rightsidebar",
                'type'  => 'select',
                'options'   => $siderbars,
                'multiselect'   => false,
                'required' => array( 'layouttype', '=', array( 'm-r', 'l-m-r' ) )
            ),
            // array( 
            //     'title' => esc_html__( 'Blog Style', 'snsvicky'),
            //     'id' => 'blog_type',
            //     'default' => '',
            //     'type' => 'select',
            //     'multiselect' => false ,
            //     'options' => array(
            //         ''              => esc_html__( 'Blog Layout 1', 'snsvicky'),
            //         'layout2'       => esc_html__( 'Blog Layout 2', 'snsvicky'),
            //         'masonry'       => esc_html__( 'Masonry', 'snsvicky'),
            //     )
            // ),
            array(
                'id'       => 'show_categories',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Categories for Blog Entries Page', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'show_date',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Date for Blog Entries Page', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            
            array(
                'id'       => 'show_author',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Author for Blog Entries Page', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'show_tags',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Tags for Blog Entries Page', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'show_comment_count',
                'type'     => 'switch',
                'title'    => esc_html__( 'Show Comment Count', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'excerpt_length',
                'type'     => 'text',
                'title'    => esc_html__( 'Blog Excerpt Length', 'snsvicky' ),
                'default'  => '55',
            ),
            array(
                'id'       => 'tag_showmore',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable View More Tags', 'snsvicky' ),
                'subtitle' => esc_html__( 'Apply for widget Tag Cloud to show the number for tags display first.', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'tag_display_first',
                'type'     => 'text',
                'title'    => esc_html__( 'The number for Tags display first.', 'snsvicky' ),
                'default'  => '8',
                'required' => array( 'tag_showmore', '=', true )
            ),
        )
    ));

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Single Post', 'snsvicky' ),
        'id'               => 'blog-singlepost',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(
            array(
                'id'       => 'show_postauthor',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Author Info on Post Detail', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'enalble_related',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Related Posts on Post Detail', 'snsvicky' ),
                'default'  => false,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'        => 'related_posts_by',
                'title'     => esc_html__( 'Related Post By', 'snsvicky'),
                'desc'      => esc_html__('Get related post by Categories or Tags', 'snsvicky'),
                'default'   => 'cat',
                'type'      => 'select',
                'multiselect' => false ,
                'options'   => array(
                    'cat'   => 'Categories',
                    'tag'   =>  'Tags',
                ),
                'required' => array( 'enalble_related', '=', true )
            ),
            array(
                'id'       => 'related_num',
                'type'     => 'text',
                'title'    => esc_html__( 'Related Posts Number', 'snsvicky' ),
                'default'  => '5',
                'required' => array( 'enalble_related', '=', true )
            ),
            array(
                'id'       => 'show_postsharebox',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Share Box on Post Detail', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'show_facebook_sharebox',
                'type'     => 'checkbox',
                'title'    => esc_html__('Show Facebook in Sharing Box', 'snsvicky'),
                'required' => array( 'show_postsharebox', '=', true ),
                'default'  => '1'// 1 = on | 0 = off
            ),
            array(
                'id'       => 'show_twitter_sharebox',
                'type'     => 'checkbox',
                'title'    => esc_html__('Show Twitter in Sharing Box', 'snsvicky'), 
                'required' => array( 'show_postsharebox', '=', true ),
                'default'  => '1'// 1 = on | 0 = off
            ),
            array(
                'id'       => 'show_gplus_sharebox',
                'type'     => 'checkbox',
                'title'    => esc_html__('Show G + in Sharing Box', 'snsvicky'),
                'required' => array( 'show_postsharebox', '=', true ), 
                'default'  => '1'// 1 = on | 0 = off
            ),
            array(
                'id'       => 'show_linkedin_sharebox',
                'type'     => 'checkbox',
                'title'    => esc_html__('Show Linkedin in Sharing Box', 'snsvicky'), 
                'required' => array( 'show_postsharebox', '=', true ),
                'default'  => '1'// 1 = on | 0 = off
            ),
            array(
                'id'       => 'show_pinterest_sharebox',
                'type'     => 'checkbox',
                'title'    => esc_html__('Show Pinterest in Sharing Box', 'snsvicky'), 
                'required' => array( 'show_postsharebox', '=', true ),
                'default'  => '1'// 1 = on | 0 = off
            ),
            array(
                'id'       => 'show_tumblr_sharebox',
                'type'     => 'checkbox',
                'title'    => esc_html__('Show Tumblr in Sharing Box', 'snsvicky'), 
                'required' => array( 'show_postsharebox', '=', true ),
                'default'  => '1'// 1 = on | 0 = off
            ),
            array(
                'id'       => 'show_email_sharebox',
                'type'     => 'checkbox',
                'title'    => esc_html__('Show Send Email in Sharing Box', 'snsvicky'), 
                'required' => array( 'show_postsharebox', '=', true ),
                'default'  => '1'// 1 = on | 0 = off
            ),
            array(
                'id'        => 'pagination',
                'title'     => esc_html__( 'Page Navigation', 'snsvicky'),
                'desc'      => esc_html__('Choose Type of navigation for blog and any listing page.', 'snsvicky'),
                'default'   => 'def',
                'type'      => 'select',
                'multiselect' => false ,
                'options'   => array(
                    'def'   => esc_html__('Default PageNavi', 'snsvicky'),
                    'ajax'  =>  esc_html__('Ajax', 'snsvicky'),
                ),
            ),
        )
    ));
    // WooCommerce
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__( 'WooCommerce', 'snsvicky' ),
        'icon'      => 'el el-shopping-cart',
        'id'               => 'woo',
        'desc'             => __( 'These are really basic fields!', 'snsvicky' ),
        'customizer_width' => '400px'
    ) );

    Redux::setSection( $opt_name, array(
        'title'            => __( 'Shop Pages', 'snsvicky' ),
        'id'               => 'woo-shoppage',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(
            array(
                'id'       => 'woo_uselazyload',
                'type'     => 'switch',
                'title'    => esc_html__( 'Use lazyload for Product Image', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'        => 'woo_list_modeview',
                'type'      => 'select',
                'title'     => esc_html__( 'Default mode view for listing page', 'snsvicky' ),
                'options'  => array(
                    'grid' => esc_html__( 'Grid', 'snsvicky' ),
                    'list' => esc_html__( 'List', 'snsvicky' ),
                ),
                'default'  => 'grid'
            ),
            array(
                'id'       => 'woo_usefilterhoz',
                'type'     => 'switch',
                'title'    => esc_html__( 'Use filter horizontal', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'woo_grid_col',
                'type'     => 'select',
                'title'    => esc_html__( 'Grid columns', 'snsvicky' ),
                'subtitle'  => esc_html__( 'We are using grid bootstap - 12 cols layout', 'snsvicky' ),
                'default'  => '3',
                'options'  => array(
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5',
                    '6' => '6',
                ),
            ),
            array(
                'id'       => 'woo_mobile_portrait_per_row',
                'type'     => 'switch',
                'title'    => esc_html__( 'Want to show 2 products per row in mobile portrait?', 'snsvicky' ),
                'default'  => false,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'woo_number_perpage',
                'type'     => 'text',
                'title'    => esc_html__( 'Number products per listing page', 'snsvicky' ),
                'default'  => '12',
            ),
        )
    ));
    Redux::setSection( $opt_name, array(
        'title'            => __( 'Single Product', 'snsvicky' ),
        'id'               => 'woo-singleproduct',
        'subsection'       => true,
        'customizer_width' => '450px',
        'fields'           => array(
            array(
                'id'       => 'woo_usecloudzoom',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Image Zoom', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'woo_usezoommobile',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Image Zoom on Mobile', 'snsvicky' ),
                'default'  => false,
                'on'       => 'Yes',
                'off'      => 'No',
                'required' => array( 'woo_usecloudzoom', '=', true )
            ),
            array(
                'id'       => 'woo_zoomtype',
                'type'     => 'select',  
                'title'    => esc_html__( 'Zoom Type for Cloud Zoom', 'snsvicky' ),
                'options'  => array(
                    'lens'      => esc_html__( 'Lens', 'snsvicky' ),
                    'inner'     => esc_html__( 'Inner', 'snsvicky' ),                                             
                ),
                'default'  => 'lens',       
                'required' => array( 'woo_usecloudzoom', '=', true )    
            ),
            array(
                'id'       => 'woo_lensshape',
                'type'     => 'select',  
                'title'    => esc_html__( 'Lens Shape', 'snsvicky' ),
                'options'  => array(
                    'round'     => esc_html__( 'Round', 'snsvicky' ),
                    'square'    => esc_html__( 'Square', 'snsvicky' ),                                            
                ),
                'default'  => 'round',       
                'required' => array( 'woo_zoomtype', '=', 'lens' )  
            ),
            array(
                'id'       => 'woo_lenssize',
                'type'     => 'text',  
                'title'    => esc_html__( 'Lens Size', 'snsvicky' ),
                'default'  => '200',       
                'required' => array( 'woo_zoomtype', '=', 'lens' )  
            ),
            array(
                'id'       => 'woo_usepopupimage',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Popup Image', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'woo_usethumb',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Thumbnail', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'woo_gallery_type',
                'type'     => 'select',  
                'title'    => esc_html__( 'Gallery Thumbnail Type', 'snsvicky' ),
                'default'  => 'h',
                'options'  => array(
                    'h'     => esc_html__( 'Horizontal', 'snsvicky' ),
                    'v'      => esc_html__( 'Vertical', 'snsvicky' ),
                    'n'      => esc_html__( 'None - and sticky content', 'snsvicky' ),
                ),
                'required' => array( 'woo_usethumb', '=', '1' )
            ),
            array(
                'id'       => 'woo_thumb_num',
                'type'     => 'text',
                'title'    => esc_html__( 'Number Thumbnail to display', 'snsvicky' ),
                'required' => array( 'woo_usethumb', '=', '1' ),
                'default'  => '4',
            ),
            array(
                'id'       => 'single_product_sidebar',
                'type'     => 'switch',
                'title'    => esc_html__( 'Use Sidebar in Single Product Page', 'snsvicky' ),
                'default'  => false,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'primary_block_three_col',
                'type'     => 'text',
                'title'    => esc_html__( 'Primary block three column', 'snsvicky' ),
                'default'  => 'product-inner-sidebar',
                'desc'     => esc_html__('Enter slug of Post WCode(Use Post WCode to make content). Just applies if Produc Page have not Sidebar and Gallery Thumbnail Type is Horizontal', 'snsvicky' ),
            ),
            array(
                'id'       => 'woo_designvariations',
                'type'     => 'switch',
                'title'    => esc_html__( 'Re-design Variations Form', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'woo_sharebox',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Share box', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'woo_upsells',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Upsells Products', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'woo_upsells_num',
                'type'     => 'text',
                'title'    => esc_html__( 'Number limit of Upsells Products', 'snsvicky' ),
                'required' => array( 'woo_upsells', '=', '1' ),
                'default'  => '6',
            ),
            array(
                'id'       => 'woo_related',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Related Products', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'woo_related_num',
                'type'     => 'text',
                'title'    => esc_html__( 'Number limit of Related Products', 'snsvicky' ),
                'required' => array( 'woo_related', '=', '1' ),
                'default'  => '6',
            ),
        )
    ));
    // Not Found
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__( 'Page Not Found', 'snsvicky' ),
        'icon'      => 'el el-warning-sign',
        'customizer_width' => '450px',
        'fields'           => array(
            array(
                'id'       => 'notfound_title',
                'type'     => 'text',
                'title'    => esc_html__( 'Title', 'snsvicky' ),
                'default'  => esc_html__('OOPS!', 'snsvicky'),
            ),
            array(
                'id'       => 'notfound_content',
                'type'     => 'textarea',
                'title'    => esc_html__( 'Message Content', 'snsvicky' ),
                'default'  => esc_html__('It looks like that page no longer exists', 'snsvicky'),
            ),
        )
    ));
    // Advance
    Redux::setSection( $opt_name, array(
        'title'     => esc_html__( 'Advance', 'snsvicky' ),
        'icon'      => 'el el-wrench',
        'fields'    => array(
            array(
                'id'       => 'advance_tooltip',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Tooltip', 'snsvicky' ),
                'default'  => false,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'advance_cpanel',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Cpanel', 'snsvicky' ),
                'default'  => false,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'       => 'advance_scrolltotop',
                'type'     => 'switch',
                'title'    => esc_html__( 'Enable Button Scroll To Top', 'snsvicky' ),
                'default'  => true,
                'on'       => 'Yes',
                'off'      => 'No',
            ),
            array(
                'id'        => 'advance_scss_compile',
                'type'      => 'select',
                'title'     => esc_html__( 'SCSS Compile', 'snsvicky' ),
                'options'  => array(
                    '1' => esc_html__( 'Only compile when don\'t have the css file', 'snsvicky' ),
                    '2' => esc_html__( 'Always compile', 'snsvicky' ),
                ),
                'default'  => '1'
            ),
            array(
                'id'        => 'advance_scss_format',
                'type'      => 'select',
                'title'     => esc_html__( 'CSS Format', 'snsvicky' ),
                'options'  => array(
                    'scss_formatter' => esc_html__( 'scss_formatter', 'snsvicky' ),
                    'scss_formatter_nested' => esc_html__( 'scss_formatter_nested', 'snsvicky' ),
                    'scss_formatter_compressed' => esc_html__( 'scss_formatter_compressed', 'snsvicky' ),
                ),
                'default'  => 'scss_formatter_compressed'
            ),
        )
    ));


